
   		<!-- INCLUIR HEADER-->
       @include('header_admin')
      <!-- INCLUIR HEADER-->

         <style type="text/css">
         	th{text-align: center; font-size: 15px;}
         	td{text-align: center;font-size: 15px;}
          .table td, .table th {
            padding: 10px;
            vertical-align: top;
            border-top: 1px solid #e3e6f0;
          }
          .linkse {
            background: white;
            border-radius: 4px;
            color: #666666;
            border: solid 1px #c6c6c6;
          }
         </style>

         <script type="text/javascript">
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'app.pageview',
            'pageName': 'pedidos',
            'colegio': 'cumbres',
            'pais': 'VE',
            'tipo': 'admin',
            'seccion': 'admin'
            });

            function linknuevos(argument) {
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'nuevos',
                'eventAction': 'nuevos-click',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais': 'VE',
                });
            }

             function linkcajafiscal(argument) {
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'pasados-caja-fiscal',
                'eventAction': 'pasados-caja-fiscal-click',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais': 'VE',
                });
            }

            function exportar(){

                var desde1 = $('#desde').val();
                var hasta1 = $('#hasta').val();

                var concatenar = "Desde: "+desde1+" "+"Hasta: "+hasta1;
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'exportar',
                'eventAction': 'exportar-pedidos-click',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais': 'VE',
                'fechabusqueda': concatenar
                });
            }

         </script>

  <div class="container-fluid" ng-module="PedidosApp" ng-controller="PedidosController"> 




  <div class="modal" id="openmodaldetalle">
        <div class="modal-dialog">
                  <div class="modal-content">
                  
                      <div class="modal-body" style="font-size: 22px;">
                          <div class="container-fluid">
                              <div class="row">
                                  <div class="col-12">
                                      <h5 >Productos vendidos</h5>
                                      <div class="row" style="margin-top: 15px;">
                                          <div class="col">
                                              <table class="table table-responsive-lg">
                                                  <thead>
                                                      <tr>
                                                      <th>Producto</th>
                                                      <th>Cantidad</th>
                                                      <th>Precio</th>
                                                     
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                      <tr ng-repeat="(key, pvend) in productosvendidos">
                                                          <th ng-if="pvend.tipo == 'Producto'"><% pvend.productos.nombre %></th>
                                                          <th ng-if="pvend.tipo == 'Promo'"><% pvend.promocion.nombre %></th>
                                                          <th><% pvend.cantidad %></th>
                                                          <th><% pvend.monto %> $</th>
                                                         
                                                      </tr>
                                                      
                                                  </tbody>
                                              </table>
                                              <div class="d-flex justify-content-end">
                                                <span class="text-bold">Total: </span> <b>  <span><% productosvendidos[0].monto_total %> $</span></b>
                                              </div>
                                              <button type="button" class="btn btn-danger" ng-click="modalelim(productosvendidos[0].id_pedido)">Eliminar venta</button>
                                              <input type="hidden" id="elim_elegido">
                                          </div>
                                      </div>
                                  </div> 
                              </div>
                          </div>                     
                      </div>
                  </div>
        </div>
    </div>


            

    <div class="card shadow mb-4 tam2">

      <div class="card-header  d-sm-flex justify-content-start mb-0">
            <h5 class="m-0">Pedidos</h5>
      </div>
          <div class="card-body"> 
              <div class="row" >
                <div class="col mt-2 mb-0">
                  <div class="input-group mb-1">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Buscar</span>
                    </div>
                    <input type="text" class="form-control" ng-model="buscador">
                  </div>
                </div>
              </div>
           

              <form action="pedidos/exportar-pedidos"> 
                  <div class="input-group mb-0 mt-2">
                    
                      <input type="hidden" id="seleccionado" value="0">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">DESDE</span>
                      </div>
                      <input type="date" class="form-control" value="{{$hoy2}}" id="desde" name="desde">

                      <div class="input-group-prepend" style="margin-left: 10px;">
                        <span class="input-group-text" id="basic-addon1">HASTA</span>
                      </div>
                      <input type="date" class="form-control" value="{{$hoy2}}" id="hasta" name="hasta">
                      <button type="button"  class="btn btn-primary"  ng-click="buscar2()" style="margin-left: 10px;">Buscar</button>
                      <button type="submit"  class="btn btn-primary"   style="margin-left: 10px;" onclick="exportar()">Exportar</button>
                    
                  </div>    
              </form>   
            
                <ul class="nav nav-pills mt-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                      <a class="btn btn-primary" id="reservas-tab" data-toggle="pill" href="#reservas" role="tab" aria-controls="reservas" aria-selected="true" ng-click="buscar(0)" onclick="linknuevos()">Nuevos</a>
                    </li>
                    <li class="nav-item" style="margin-left: 10px;">
                      <a class="btn btn-outline-secondary" id="produtosr-tab" data-toggle="pill" href="#produtosr" role="tab" aria-controls="produtosr" aria-selected="false" ng-click="buscar(1)" onclick="linkcajafiscal()">Pasados a caja fiscal</a>
                    </li>
                </ul>


              <div class="row  mt-3">
                <div class="col-md-12"> 
                  <table class="table table-responsive-xl">
                      <thead class="thead-dark">
                        <tr>
                          <th>Id</th>
                          <th>Cliente</th>
                          <th>Monto Total</th>
                          <th>Medio de pago</th>
                          <th>Fecha de creación</th>
                          <th>Ver</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="ped in pedidos_arr|filter:buscador">
                          <th><% ped.id %></th>
                          <td>
                            <span ng-if="ped.cliente.nombre == null">No existe</span>
                            <% ped.cliente.nombre %>  <% ped.cliente.apellido_alumno %> 
                          </td>
                          <td>
                            <% ped.monto_format %> $
                          </td>
                          <td>
                            <% ped.tipo_pago %>
                          </td> 
                          <td>
                            <% ped.fecha_reistro_format %> 
                          </td>
                          <td ng-click="openmodaldetalle(ped.productosvendidos)">
                            <span ><i class="fa fa-pencil-square-o fon50" style="cursor: pointer;"></i></span>
                          </td>
                        </tr>
                      </tbody>
                  </table>
                  <div style="text-align: center;">
                  <i class="fa fa-spinner fa-spin fa-3x fa-fw load"></i>
                  <div class="nohay" style="display: none;">No hay datos para mostrar</div>
                  </div>

                  <div>
                    TOTAL: <b> <span id="precio"> </span> $</b>
                    <span style="color: #00294d;text-decoration: underline;cursor: pointer;margin-left: 10px;" ng-click="pasaracaja()" id="pasarcajis">Pasar a caja fiscal</span>
                  </div>
                </div>  
              </div>
          </div>   


     <div class="modal" id="openelim">
              <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body" style="font-size: 18px;text-align: center;">
                            ¿Seguro que quieres eliminar esta venta?                    
                        </div>
                        <div class="text-center">
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                        </div>
                        <div class="modal-footer" id="btnselimn">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>
                          <button type="button" class="btn btn-danger" ng-click="eliminarpedido()" >Eliminar venta</button>
                        </div>
                    </div>
              </div>
          </div>        
        

              
      

    </div>
  </div>


  </div>


  <!-- INCLUIR HEADER-->
      @include('footer_admin')
  <!-- INCLUIR HEADER-->


  @if(Session::has('listo'))
    <script type="text/javascript">  
      $("#modalagree").modal();
    </script>
    @endif



       
	   <script src="{{url('js/angularpedidos2.js')}}"></script>
