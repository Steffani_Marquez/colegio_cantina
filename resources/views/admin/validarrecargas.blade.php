   		<!-- INCLUIR HEADER-->
       @include('header_admin')
      <!-- INCLUIR HEADER-->



      <script type="text/javascript">
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'app.pageview',
            'pageName': 'recargas',
            'colegio': 'cumbres',
            'pais': 'VE',
            'tipo': 'admin',
            'seccion': 'admin'
            });
    </script>



         <style type="text/css">
         	th{text-align: center;}
         	td{text-align: center;}
          .modal-dialog {
              max-width: 800px;
              margin: 1.75rem auto;
          }
          .contpagos{    
              background: #fff;
              color: #6c6c6c;
              border-radius: 10px;
              border: solid 1px #dddddd;
              padding: 11px;
          }
         </style>

  <div class="container-fluid" ng-module="ReservaApp" ng-controller="ReservaController"> 



     <div class="modal" tabindex="-1" id="modalvalidar">
        <div class="modal-dialog">
          <div class="modal-content">
            
            <div class="modal-header">
              <h5 class="modal-title">Validar pago</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                
              <div ng-repeat="ped in detalle_arr|filter:buscador">
                 <div class="container-fluid">
                   <div class="row">
                     <div class="col-6">
                       <p class="mb-0">Monto Dolares: <% ped.montodolar  %> $ </p>
                       <p class="mb-0">Monto Bolivares: <% ped.montobs  %> Bs. </p>
                       <p class="mb-0">Fecha de pago: <% ped.fechis  %>  </p>
                       <p class="mb-0">Tipo de pago: <% ped.tipo_pago  %> </p>
                       <p class="mb-0">Referencia: <% ped.referencia_pago  %> </p>
                       <p class="mb-0">Familia: <% ped.cliente.apellido_alumno  %> </p>
                       <p class="mb-0">Email: <% ped.cliente.correo_alumno  %> </p>
                       <p class="mb-0">Teléfono de contacto: <% ped.telefono  %> </p>
                       <p class="mb-0">Origen: <% ped.origen  %> </p>
                     </div>
                     <div class="col-6 text-center">
                        <div ng-if="ped.validado == 0"><span style="background: #aa1a18;color: white;padding: 10px;"> PENDIENTE </span></div>
                        <div ng-if="ped.validado == 1"><span style="background: #41a73a;color: white;padding: 10px;"> APROBADO </span></div>
                        <div ng-if="ped.validado == 2"><span style="background: #181818;color: white;padding: 10px;">  ANULADO </span></div>
                        
                        <span ng-if="ped.validado == 0">
                          <div class="form-group mt-3">
                            <label style="font-size: 20px;">Cambiar status</label>
                            <select class="form-control" id="statuscam">
                              <option value="0">-</option>
                              <option value="1">APROBADO</option>
                              <option value="2">ANULADO</option>
                            </select>
                          </div>
                          <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                          <button type="button" class="btn btn-outline-success bntcam" ng-click="cambiarstatus(ped.id)" style="width: 100%;">Cambiar</button>
                      </span>

                     </div>
                   </div>
                   <hr>
                   
                    <div ng-repeat="ped1 in detalle_arr">
                       <div class="row">
                          <div class="col-6 mt-3" ng-repeat="ped in ped1.carrito">
                             <div class="contpagos">
                                <p class="mb-0"><span>Nombre:</span>   <% ped.cliente.nombre %> </p>
                                <p class="mb-0"><span>Apellido:</span> <% ped.cliente.apellido_alumno %> </p>
                                <p class="mb-0"><span>Email:</span>    <% ped.cliente.correo_alumno %>   </p>
                                <p class="mb-0"><span>Monto en Dolares:</span>   <% ped.montodolar %> $ 
                                <p class="mb-0"><span>Monto en Bolivares:</span> <% ped.montobs %> Bs. 
                                <i class="fas fa-edit" style="cursor: pointer;" ng-click="abrirmodalmonto(ped.id)"></i></p>
                                <p class="mb-0"><span>Tasa:</span>   <% ped1.tasabs %> bs </p>
                                <p class="mb-0"> <span>Grado:</span> <% ped.cliente.curso_actual %> </p>
                            </div>
                          </div>
                        </div>
                    </div>

                  </div>

              </div>

            </div>

          </div>
        </div>
      </div>


      <div class="modal" tabindex="-1" id="modaleditmonto">

        <input type="hidden" id="montoelegido" >

        <div class="modal-dialog">
          <div class="modal-content">
            
            <div class="modal-header">
              <h5 class="modal-title">Validar pago</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">$</span>
                  </div>
                  <input type="text" class="form-control" id="montoact" style="width: 100px;max-width: 111px;">
                </div>
                
                <i class="fa fa-spinner fa-spin fa-3x fa-fw load2" style="display: none;"></i>
                <button type="button" class="btn btn-primary btnactual" ng-click="actualizarmonto()">Actualizar monto</button>

            </div>

          </div>
        </div>
      </div>
            

    <div class="card shadow mb-1 tam2">

      <div class="card-header  d-sm-flex justify-content-start mb-0">
            <h5 class="m-0 ">Validar recargas </h5>
      </div>
      
      <div class="card-body"> 
             
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text">Buscar</span>
                </div>
                <input type="text" class="form-control" ng-model="buscador">
            </div>

            <div class="row">
                <div class="col-md-2 mt-2">
                    <a href="#" class="btn btn-secondary btn-icon-split" ng-click="buscarstatus(0)" style="color: white;width: 100%;background: #aa1a18;border-color: transparent;border-radius: 5px;">
                      <span class="text">Pendientes</span>
                    </a>
                </div>
                <div class="col-md-2 mt-2">
                    <a href="#" class="btn btn-secondary btn-icon-split" ng-click="buscarstatus(1)" style="color: white;width: 100%;background: #41a73a;border-color: transparent;border-radius: 5px;">
                      <span class="text">Aprobados</span>
                    </a>
                </div>
                <div class="col-md-2 mt-2">
                    <a href="#" class="btn btn-secondary btn-icon-split" ng-click="buscarstatus(2)" style="color: white;width: 100%;background: #181818;border-color: transparent;border-radius: 5px;">
                      <span class="text">Anulados</span>
                    </a>
                </div>
            </div>

            <div class="row  mt-4">
              <div class="col-md-12"> 
                <table class="table table-responsive-xl">
                    <thead class="thead-dark">
                      <tr>
                        <th>Monto $</th>
                        <th>Monto Bs</th>
                        <th>Tipo</th>
                        <th>Familia</th>
                        <th>Fecha</th>
                        <th>Correo</th>
                        <th>Ver</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="ped in recargas_arr|filter:buscador">
                        <td>
                          <% ped.montodolar %> $
                        </td>
                        <td>
                          <% ped.montobs %> Bs
                        </td>
                        <td>
                          <% ped.tipo_pago %>
                        </td>
                        <td>
                          <% ped.cliente.apellido_alumno %> 
                        </td>
                        <td>
                          <% ped.fechis %> 
                        </td>
                        <td>
                          <% ped.cliente.correo_alumno %> 
                        </td>
                        <td ng-click="detalles(ped.id)" style="cursor: pointer;">
                          <i class="fa fa-pencil-square-o fon50" style="cursor: pointer;"></i>
                        </td>
                      </tr>
                    </tbody>
                </table>
                <div style="width: 100%;text-align: center;">
                  <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="font-size: 35px;display: none;"></i>
                  <div class="nohay" style="display: none;">No hay datos para mostrar</div>
                </div>
              </div>  
            </div>
      
      </div>

    </div>
  </div>


  <!-- INCLUIR HEADER-->
      @include('footer_admin')
  <!-- INCLUIR HEADER-->


    @if(Session::has('listo'))
      <script type="text/javascript">  
        $("#modalagree").modal();
      </script>
    @endif
     
	
  <script src="{{url('js/angularvalidarrecargas4.js')}}"></script>
