   		<!-- INCLUIR HEADER-->
           @include('header_admin')
         <!-- INCLUIR HEADER-->


         <script type="text/javascript">
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'app.pageview',
                'pageName': 'productos',
                'colegio': 'cumbres',
                'pais': 'VE',
                'tipo': 'admin',
                'seccion': 'admin'
                });
         </script>

         <style type="text/css">
         	
            th{text-align: center;}
         	td{text-align: center;}

            .linkse {
                background: white;
                border-radius: 4px;
                color: #666666;
                border: solid 1px #c6c6c6;
            }

            .error1{ 
                font-size: 15px;
                width: 100%;
                background: #aa1a18;
                color: white;
                border-radius: 10px;
                text-align: center;
                padding: 4px;
                margin-bottom: 12px;
            }

            .contima{
                padding: 30px;
                background: #fafafa;
            }

         </style>

        <div class="container-fluid" ng-module="CantinaProductosApp" ng-controller="CantinaProductosController"> 
             
             <input type="hidden" id="id_elegida" value="1">

			<div class="modal" id="nuevoproducto">
			    <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        
                        <div class="modal-body" style="font-size: 15px;">
                            <form enctype="multipart/form-data" action="productos/nuevo" id="nuevoproductoform" method="post">
                            
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"  id="file-image" name="image" >
                                                    <label class="custom-file-label" for="file-image" data-browse="Adjuntar imagen"></label>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="row mt-2">
                                            <div class="col text-center">
                                                <div class="contima">
                                                    <img src="img/not-available.png"  id="imgSalida" style="height: 199px;"> 
                                                </div>
                                            </div>
                                        </div>                                  
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Nombre</label>
                                                    <input type="text" class="form-control" name="nombre" id="nombre" value="">
                                                </div>
                                            </div>
                                            
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Precio</label>
                                                    <input type="text" class="form-control" name="precio" id="precio" value="">
                                                </div>
                                            </div>

                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Costo</label>
                                                    <input type="text" class="form-control" name="precio_costo" id="precio_costo" value="">
                                                </div>
                                            </div>

                                        </div>			
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Estatus</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="status" name="status">
                                                            <option value="1">ACTIVO</option>
                                                            <option value="0">INACTIVO</option>
                                                        </select>
                                                    </div>
                                                </div>                                
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Categoría</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="categoria_id" name="categoria_id">
                                                            @foreach($categorias as $categoria)
                                                                <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>	                                
                                            </div>                            
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Tipo</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="tipo" name="tipo">
                                                            <option value="NORMAL">NORMAL</option>
                                                            <option value="COMPONENTE">COMPONENTE</option>
                                                        </select>
                                                    </div>
                                                </div>                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Descripción</label>
                                                    <textarea class="form-control" name="descripcion" id="descripcion" value=""></textarea>
                                                </div>                                
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col text-right">
                                                <div id="error" class="error1 mb-2" style="display: none;">Debes ingresar todos los campos</div>
                                                <div>
                                                    <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="font-size: 35px;display: none;"></i>
                                                    <button type="button" class="btn btn-primary mt-2 btnguardar" ng-click="nuevo()">Guardar</button>
                                                </div>
                                            </div>  
                                        </div>

                                    </div>
                                </div>
                                
                            </form>    
                        </div>
                    </div>
			    </div>
			</div>

			<div class="modal" id="editarproducto">
			    <div class="modal-dialog modal-xl">
			        <div class="modal-content">
                       
                        <div class="modal-body" style="font-size: 15px;">
                            <form enctype="multipart/form-data" action="productos/editar" id="editarproductoform" method="post">                        
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"  id="file-imageedit" name="image_edicion" value="<% producto_edicon.imagen %>">
                                                    <label class="custom-file-label" for="file-image" data-browse="Adjuntar imagen"></label>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="row mt-2">
                                            <div class="col text-center">
                                                <div class="contima">
                                                    <img src="<% producto_edicon.imagen %>"  id="imgSalidaedit" style="height: 199px;"> 
                                                </div> 
                                            </div>
                                        </div> 


                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Nombre</label>
                                                    <input type="text" class="form-control" name="nombre_edicion" id="nombre_edicion" value="<% producto_edicon.nombre %>">
                                                    <input type="hidden"  name="id_edicion" id="id_edicion" value="<% producto_edicon.id %>">
                                                </div>
                                            </div>
                                            
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Precio</label>
                                                    <input type="text" class="form-control" name="precio_edicion" id="precio_edicion" value="<% producto_edicon.costo %>">
                                                </div>
                                            </div>

                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Costo</label>
                                                    <input type="text" class="form-control" name="precio_costo_edicion" id="precio_costo_edicion" value="<% producto_edicon.precio_costo %>">
                                                </div>
                                            </div>

                                        </div>			
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Estatus</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="status_edicion" name="status_edicion" >
                                                            <option value="" >SELECCIONA</option>
                                                            <option value="0" ng-if="producto_edicon.status == 0" selected>INACTIVO</option>
                                                            <option value="0" ng-if="producto_edicon.status == 1">INACTIVO</option>
                                                            <option value="1" ng-if="producto_edicon.status == 0">ACTIVO</option>
                                                            <option value="1"  ng-if="producto_edicon.status == 1" selected>ACTIVO</option>
                                                        </select>
                                                    </div>
                                                </div>                                
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Categoría</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="categoria_id_edicion" name="categoria_id_edicion">
                                                            <option value="<% producto_edicon.id_categoria %>" selected><% producto_edicon.categoria.nombre %></option>
                                                            @foreach($categorias as $categoria)
                                                                <option value="{{$categoria->id}}"  >{{$categoria->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>	                                
                                            </div>                            
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Tipo</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="tipo_edicion" name="tipo_edicion">
                                                            <option value="" >SELECCIONA</option>
                                                            <option value="<% producto_edicon.tipo %>" selected><% producto_edicon.tipo %></option>
                                                            <option value="NORMAL" >NORMAL</option>
                                                            <option value="COMPONENTE" >COMPONENTE</option>
                                                        </select>
                                                    </div>
                                                </div>                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Descripción</label>
                                                    <textarea class="form-control" name="descripcion_edicion" id="descripcion_edicion" value="<% producto_edicon.descripcion %>"><% producto_edicon.descripcion %></textarea>
                                                </div>                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col  d-flex justify-content-end">
                                        <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="font-size: 35px;display: none;"></i>
                                        <button type="button" class="btn btn-primary mt-2 edit1" ng-click="editar()">Editar</button>
                                    </div>  
                                </div>
                            </form>    
                        </div>
			        </div>
			    </div>
			</div>

			<div class="modal" id="modalinventario">
			    <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                   
                        <div class="modal-body" style="font-size: 15px;">
                            <div class="container-fluid">
                                <div class="row">
                                
                                    <div class="col-sm-2">
                                        <div> <img src="<% invprod.imagen %>" style="max-width: 100%;margin: 0px auto;"> </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                            <div style="font-weight: 800;"><% invprod.nombre %></div>
                                            <div style="margin-top: 10px;"><span style="font-weight: 800;">Cantidad:</span> <% invprod.stock %> </div>                                   
                                    </div>

                                    <div class="col-sm-7" >
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Acción</label>
                                                    <select class="form-control" name="accion" id="accion">
                                                        <option  value="0">-</option>
                                                        <option  value="1">Ingreso</option>
                                                        <option  value="2">Salida</option>                                   
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Cantidad</label>
                                                    <input type="number" class="form-control" id="cantidad" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group" >
                                                    <label for="exampleFormControlTextarea1">DETALLE</label>
                                                    <textarea class="form-control" id="detalle" rows="2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-group justify-content-end" >
                                            <div id="error" class="error1" style="display: none;">Debes ingresar todos los campos</div>
                                            <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                                            <button type="button" class="btn btn-secondary btningreso" ng-click="guardarinventario(invprod.id)">Guardar</button>
                                        </div>                       
                                    </div>
                                </div>
                                
                                <hr>
                                <div class="row">
                                    <div class="col-12">
                                        <div style="color: #171717;font-weight: 700;padding: 5px 10px;background: #e9ecef4d;">SEGUIMIENTO</div>
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col">
                                                <table class="table table-responsive-lg">
                                                    <thead>
                                                        <tr>
                                                        <th>Acción</th>
                                                        <th>Cantidad</th>
                                                        <th>Fecha</th>
                                                        <th>Nota</th>
                                                        <th>Ususario</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="(key, segui) in seguimientos_arr">
                                                            <th><% segui.accion %></th>
                                                            <th><% segui.cantidad %></th>
                                                            <th><% segui.fecha %></th>
                                                            <th><% segui.nota %></th>
                                                            <td><% segui.user.name %></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>                     
                        </div>
                    </div>
			    </div>
			</div>


            <input type="hidden" name="detalleelegido" id="detalleelegido">

             <div class="card shadow mb-4 tam2">

			    <div class="card-header  d-sm-flex align-items-center justify-content-between mb-0">
                      <h5 class="m-0 font-weight-bold">Productos </h5>
                      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" ng-click="nuevoproducto()"><i
                        class="fas  fa-plus fa-sm text-white-50"></i> Crear nuevo</a>
                </div>


             
                
                
				<div class="card-body"> 

                        <ul class="nav nav-pills mt-0 mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                              <a class="btn btn-primary" id="reservas-tab" data-toggle="pill" href="#reservas" role="tab" aria-controls="reservas" aria-selected="true" ng-click="buscar(1)">Activos</a>
                            </li>
                            <li class="nav-item" style="margin-left: 10px;">
                              <a class="btn btn-outline-secondary" id="produtosr-tab" data-toggle="pill" href="#produtosr" role="tab" aria-controls="produtosr" aria-selected="false" ng-click="buscar(0)">Inactivos</a>
                            </li>
                        </ul>

                        <form action="productos/prodvendidos">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">DESDE</span>
                                </div>
                                <input type="date" class="form-control" value="{{$hoy2}}" id="desdereser" name="desde">

                                <div class="input-group-prepend" style="margin-left: 10px;">
                                    <span class="input-group-text" id="basic-addon1">HASTA</span>
                                </div>
                                <input type="date" class="form-control" value="{{$hoy2}}" id="hastareser" name="hasta">
                                
                                <button type="btn"  class="btn btn-primary" style="margin-left: 10px;" ng-click="buscar()">Exportar vendidos</button>
                            </div>  
                        </form>  

                        <form action="productos/exportarinventario">
                            <button type="btn"  class="btn btn-primary" ng-click="buscar()">Exportar inventario</button>
                        </form>
                    



            			<div class="mt-2 mb-4">
        					<div class="input-group mb-3">
        					  <div class="input-group-prepend">
        					    <span class="input-group-text">Buscar</span>
        					  </div>
        					  <input type="text" class="form-control" ng-model="buscador">
        					</div>
        				</div>

        				<div>
					
						
						<div class="row ">
						   <div class="col-md-2 mt-2 mb-4" ng-repeat="(key, combi) in productos_arr|filter:buscador|orderBy:id">
							  	<div class="contproduct" style="cursor: pointer;">
                                    <span style="display: block;cursor:pointer;margin-bottom: 10px;" ng-click="editarproducto(combi)"><i class="fa fa-pencil" aria-hidden="true"></i> Editar </span>
							  	   <div class="contfto">
							  			<img src="<% combi.imagen %>"  style="max-width: 100%;">
							  		</div>
							  		<div class="contleter mt-1 text-center" style="font-size: 13px;">
							  			<b> <% combi.nombre %> </b>
							  		</div>
                                    <div class="contleter mt-1 text-center" style="font-size: 13px;">
                                       <b>  Cantidad:</b>  <% combi.stock %> 
                                    </div>  
                                    <div class="contleter mt-1 text-center" style="font-size: 13px;">
                                        <button class="btn btn-secondary" type="button" ng-click="modalinventario(combi)" style="line-height: 18px;">Ingresar</button> 
                                    </div>  
    		
    							  	</div>
    						  </div>
    					  </div>
                          <div style="width: 100%;text-align: center;">
                          <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                          </div>
    					
    				</div>
             </div>


        </div>


        </div>


        <!-- INCLUIR HEADER-->
            @include('footer_admin')
        <!-- INCLUIR HEADER-->


        @if(Session::has('listo'))
			<script type="text/javascript">  
			 $("#modalagree").modal();
			</script>
		@endif



       
	   <script src="{{url('js/angularproductos.js')}}"></script>
