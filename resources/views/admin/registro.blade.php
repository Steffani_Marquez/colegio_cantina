@include('header_admin')
    <!-- Page Heading -->
    <div class="container-fluid" ng-module="CantinaProductosApp" ng-controller="CantinaProductosController"> 

        <div class="card  shadow  mb-4 tam2">
            <div class="card-header py-3  mb-4">
                <h5 class="mb-0 text-white lh-100">Registro de usuario</h5>
            </div>
            <div class="card-body">
                <form action="register/store" method="POST">
                @if(Session('success'))
                <div class="alert alert-success">
                    {{Session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if($errors->has())
                <div class="alert alert-danger alert-disappear">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Error!</strong>
                    <ul>
                        @foreach ($errors->all() as $error) 
                            <li>{{$error}}</li>
                        @endforeach
                    </ul> 
                </div>
                @endif
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input id="name" type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="name">Username</label>
                            <input id="name" type="text" class="form-control" name="username">
                        </div>
                    </div>                
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="text" class="form-control" name="email">
                        </div>
                    </div>  
                    <div class="col">
                        <div class="form-group">
                            <label for="empresa">Teléfono</label>
                            <input id="phone" type="text" class="form-control" name="telefono">
                        </div>
                    </div>
                </div>                  
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input id="password" type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="col">    
                        <div class="form-group">
                            <label for="password2">Repetir Contraseña</label>
                            <input id="password2" type="password" class="form-control" name="password2">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">    
                            <label for="">Roles</label>
                            <select name="roles[]" class="form-control" id="roles" multiple>
                                @foreach($roles as $rol)
                                <option value="{{$rol->id}}"> {{$rol->name}} </option>
                                @endforeach
                            </select>                
                        </div>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="text-center">
                            <button type="submit" class="btn btn-template-outlined"><i class="fa fa-user-md"></i> Registrar</button>
                        </div>
                    </div>
                </div>
                
                </form>
            </div>
        </div>
    </div>
@include('footer_admin')