    <!-- INCLUIR HEADER-->
    @include('header_admin')
     <!-- INCLUIR HEADER-->

      <title>CONTROL DE USUARIOS</title>

     <style type="text/css">
         th{text-align: center;}
     </style>

<main role="main"  ng-module="UsersApp" ng-controller="UsersController">

    <div class="media text-muted pt-3">
        <div class="container-fluid">

       <div class="card shadow mb-4 tam2">

            <div class="card-header py-3">
               <h5 class="mb-0 text-white lh-100">Usuarios</h5>
            </div>
                        
            <div class="card-body"> 
        
                

        @if(session('success'))
        <div class="alert alert-success alert-disappear">
             Registro Exitoso
        </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger alert-disappear">
        {{ session('error') }}
        </div>
        @endif

        @if($errors->has())
        <div class="alert alert-danger alert-disappear">
            <strong>Error!</strong>
            <ul>
                @foreach ($errors->all() as $error) 
                    <li>{{$error}}</li>
                @endforeach
            </ul> 
        </div>
        @endif


        <table class="table mt-1" >
            <thead class="colortablis">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Status</th>
                <th scope="col">Roles</th>
                <th scope="col">Editar</th>
              </tr>
            </thead>
            <tbody>
              <tr  ng-repeat="ped in users_arr|filter:buscador|orderBy:name">
                <th><% ped.id %></th>
                <th> <% ped.name %></th>
                <th> <% ped.username %></th>
                <th><% ped.email %></th>
                <th style="cursor: pointer;">
                    <span ng-if="ped.active==1" style="background:  #12ada6;color: #fff;padding: 3px 10px;font-size: 15px;" ng-click="estatususer(ped.id,$index)">
                            ACTIVO
                    </span>
                    <span ng-if="ped.active==0" style="background: #343a40;color: #fff;padding: 3px 10px;font-size: 15px;" ng-click="estatususer(ped.id,$index)">
                           INACTIVO
                    </span>
                </th>
                <th>
                    <div ng-repeat="rol in ped.roles"><% rol.name %></div>
                </th>
                <th><i class="fas fa-edit fa-2x" aria-hidden="true" ng-click="openeditmodal(ped.id)" style="cursor: pointer;font-size: 20px;"></i></th>

              </tr>
            </tbody>
          </table>


                </div>

        </div>
    </div>


      <div class="modal" tabindex="-1" role="dialog" id="ediatruser">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Editar usuario</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('register/update')}}" id="formeditar" onsubmit="return false">                    
                        <div class="container-fluid">
                            
                                    <div class="row">
                                        <div class="col">
                                            <label for="">Nombre</label>   
                                            <input type="text" name="name" id ="name" class="form-control"  value="<% datosuser.name %>">
                                            <input type="hidden" name="id" class="form-control" value="<% datosuser.id %>">
                                        </div>    
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label for="">Username</label>   
                                            <input type="text" name="username" id="username" class="form-control" value="<% datosuser.username %>">
                                        </div>    
                                    </div>
                                    
                                    <div class="row mt-2">
                                        <div class="col">
                                            <label for="">Email</label>
                                            <input type="email" name="email" id ="email" class="form-control" value="<% datosuser.email %>">
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col">
                                            <label for="">Teléfono</label>
                                            <input type="number" name="telefono" id="telefono" class="form-control" value="<% datosuser.phone %>">
                                        </div>
                                    </div>                                    
                                    <div class="row mt-2">
                                        <div class="col">
                                            <label for="">Password</label>
                                            <input type="password" name="password" id="password" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col">
                                            <label for="">Confirma Password</label>
                                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <label for="">Roles</label>
                                            <select class="form-control roles" name="roles[]" id="roles"  multiple>
                                                @foreach($roles as $role)
                                                <option value="{{$role->id}}" ng-if="datosuser.roles_id.includes({{$role->id}}) === true" selected>{{$role->name}}</option>
                                                <option value="{{$role->id}}" ng-if="datosuser.roles_id.includes({{$role->id}}) === false" >{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
        
                        </div>
                        <div class="container-fluid text-right" style="margin-top: 20px;">
                            <button type="buttom" class="btn btn-primary btns" ng-click="guardar(datosuser.id)">GUARDAR</button>
                            <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                        </div>
                    </form>
                </div>
              </div>
            </div>
            </div>
      </div>

</main>
@include('footer_admin')
<script src="{{url('/js/users.js')}}"></script>
