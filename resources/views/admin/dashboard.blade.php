   		<!-- INCLUIR HEADER-->
           @include('header_admin')
    	<!-- INCLUIR HEADER-->


        <script type="text/javascript">
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'app.pageview',
            'pageName': 'dashboard',
            'colegio': 'cumbres',
            'pais': 'VE',
            'tipo': 'admin',
            'seccion': 'admin'
            });
        </script>


        <div class="container-fluid">
          	<!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow-custom h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-gray text-uppercase mb-1">
                                                Ventas del día</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">$ {{number_format($totaldaypedido,2,',','.')}} </div>
                                            <form action="dashboard/exportarventa1">
                                                <div class="mt-2"><i class="fas fa-download"></i> <button type="submit" style="background: transparent;border: none;">Descargar</button> </div>
                                            </form>
                                        </div>

                                        <div class="col-auto">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow-custom h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold  text-gray text-uppercase mb-1">
                                                Ventas del mes</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">$ {{number_format($totalmespedido,2,',','.')}} </div>
                                            <form action="dashboard/exportarventa2">
                                                <div class="mt-2"><i class="fas fa-download"></i> <button type="submit" style="background: transparent;border: none;">Descargar</button> </div>
                                            </form>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow-custom h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold  text-gray text-uppercase mb-1">
                                            Cant de Pedidos del día
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"> {{$cantdaypedido}} </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-rocket fa-2x text-gray-300" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow-custom h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-gray text-uppercase mb-1">
                                                Cant. Reservas Pendientes Hoy</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$cantdayreserva}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-rocket fa-2x text-gray-300" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow-custom h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-gray text-uppercase mb-1">
                                                Cant. Estudiantes</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$cantestudiantes}}</div>
                                            <form action="dashboard/clientes">
                                                <div class="mt-2"><i class="fas fa-download"></i> <button type="submit" style="background: transparent;border: none;">Descargar clientes</button> </div>
                                            </form>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fa fa-child fa-2x text-gray-300" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
         

        </div>



      <!-- INCLUIR FOOTER-->
      @include('footer_admin')
      <!-- INCLUIR FOOTER-->
             