<!-- INCLUIR HEADER-->
    @include('header_admin')
    <!-- INCLUIR HEADER-->

     <style type="text/css">
     	
     	th{text-align: center;}
		td{text-align: center;}
		p{ margin-bottom: 5px !important;}
		.cuora{
			color: #d2d2d2;
    		margin-left: 5px; cursor: pointer;
    	}
    	.cuora2{
    		color: #aa1a18;
    		margin-left: 5px; cursor: pointer;
    	}

    	.mlg{display: block;}
		.mxs{display: none;}

		.td1{padding: 1px !important;}

		.btnvv {
		    background: white;
		    color: #303030;
		    border: solid 1px #a3a3a3;
		}

		.btnactive {
		    color: #fff;
    background-color: #2e59d9;
    border-color: #2653d4;
    box-shadow: 0 0 0 0.2rem rgb(105 136 228 / 50%);
		}

		



		@media (max-width: 600px) {
			.mlg{display: none;}
			.mxs{display: block;}
		}

     </style>


	<div class="container-fluid"  ng-module="RoncolapedidosApp" ng-controller="PedidosController">

	

	<div class="modal" tabindex="-1" id="modalnuevocliente">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title">Nuevo cliente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body">
				  	<h5 class="modal-title">Datos Cliente</h5>  
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="exampleFormControlInput1">Tipo de cliente</label>						
								<select class="form-control" name="tipo_cliente" id="tipo_cliente">
									@foreach($TipoCliente as $tipo)
										<option value="{{$tipo->id}}">{{$tipo->nombre}}</option> 
									@endforeach
								</select>
							</div>
						</div>
						<div class="col"></div>
					</div>
					  
					<div class="row">
						<div class="col">
							<div class="form-group">
					    		<label for="exampleFormControlInput1">Nombre</label>
					    		<input type="text" class="form-control" id="nombre" value="">
					  		</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="exampleFormControlInput1">Apellido</label>
								<input type="text" class="form-control" id="apellido" value="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group">
					    		<label for="exampleFormControlInput1">Cedula</label>
					    		<input type="text" class="form-control" id="cedula" value="">
					  		</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="exampleFormControlInput1">Edad</label>
								<input type="text" class="form-control" id="edad" value="">
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="col">
							<div class="form-group">
					    		<label for="exampleFormControlInput1">Grado</label>
					    		<input type="text" class="form-control" id="curso" value="">
					  		</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="exampleFormControlInput1">Seccion</label>
								<input type="text" class="form-control" id="edad" value="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group">
					    		<label for="exampleFormControlInput1">Email</label>
					    		<input type="text" class="form-control" id="email" value="">
					  		</div>
						</div>
						<div class="col">
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group">
					    		<label for="exampleFormControlInput1">Password</label>
					    		<input type="password" class="form-control" id="password_1" value="">
					  		</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="exampleFormControlInput1">Repite Password</label>
								<input type="password" class="form-control" id="password_2" value="">
							</div>
						</div>
					</div>	
					<h5 class="modal-title" data-toggle="collapse" href="#collapseRepresentante" role="button" aria-expanded="false" aria-controls="collapseRepresentante">Datos Representantes</h5>				
					<div class="collapse" id="collapseRepresentante">
						<div class="row">
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Nombre Representante 1</label>
									<input type="text" class="form-control" id="r_nombre_1" value="">
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Apellido Representante 1</label>
									<input type="text" class="form-control" id="r_apellido_1" value="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Cedula Representante 1</label>
									<input type="text" class="form-control" id="r_cedula_1" value="">
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Email Representante 1</label>
									<input type="text" class="form-control" id="r_email_1" value="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Password representante 1</label>
									<input type="password" class="form-control" id="r_password_1" value="">
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Repite Password</label>
									<input type="password" class="form-control" id="r_password_2" value="">
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Nombre Representante 2</label>
									<input type="text" class="form-control" id="r_nombre_2" value="">
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Apellido Representante 2</label>
									<input type="text" class="form-control" id="r_apellido_2" value="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Cedula Representante 2</label>
									<input type="text" class="form-control" id="r_cedula_2" value="">
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Email Representante 2</label>
									<input type="text" class="form-control" id="r_email_2" value="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Password representante 2</label>
									<input type="password" class="form-control" id="r2_password_1" value="">
								</div>
							</div>
							<div class="col">
								<div class="form-group">
									<label for="exampleFormControlInput1">Repite Password</label>
									<input type="password" class="form-control" id="r_2password_2" value="">
								</div>
							</div>
						</div>	
					</div>
					<h5 class="modal-title">Datos de cuenta</h5>
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="exampleFormControlInput1">Saldo</label>
								<input type="text" class="form-control" id="saldo" value="">
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="exampleFormControlInput1">Limite compra diario</label>
								<input type="text" class="form-control" id="limite_diario" value="">
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group">
					    		<label for="exampleFormControlTextarea1">Agregar observación</label>
					    		<textarea class="form-control" id="observacion" rows="3"></textarea>
					  		</div>
						</div>
					</div>  
			          
				</div>			      
				<div class="modal-footer"> 
					<i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
					<button type="button" id="btnguardar" class="btn btn-secondary" ng-click="insertar()">Guardar</button>
				</div>
			</div>
		</div>
	</div>
			<div class="modal" tabindex="-1" id="modalimportarclientes">
				<div class="modal-dialog">
					<div class="modal-content">
						<form method="POST" action="verclientes/importar"  enctype="multipart/form-data">      
							<div class="modal-header">
								<h5 class="modal-title">Cargar Archivo</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col">
										<div class="form-group">
											<h4 for="exampleFormControlFile1" >Listado</h4>
											<input type="file" class="form-control-file" id="exampleFormControlFile1" name="fichero" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required="required">
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer"> 
							<i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
							<button type="submit" id="btnguardar" class="btn btn-info" >Cargar</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			
			
			<div class="modal" tabindex="-1" id="modaldetalle">
			  <div class="modal-dialog" style="max-width: 90%;">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Detalle</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <div class="container-fluid">
			          <div class="row">
			            <div class="col-6">
					        <p class="mb-1"> Nombre:   <%  detalle_arr.nombre_alumno %></p>
					        <p class="mb-1"> Apellido: <%  detalle_arr.apellido_alumno %></p>
					        <p class="mb-1"> Correo:   <%  detalle_arr.correo_alumno %></p>
					        <p class="mb-1"> Cedula:   <%  detalle_arr.cedula %></p>
					        <p class="mb-1"> Grado:    <%  detalle_arr.curso_actual %></p>
				        </div>
				        <div class="col-6">
                            <p class="mb-1"> Saldo Dolares:   <%  detalle_arr.abonodolar %> $</p>
                            <p class="mb-1"> Saldo Bolivares: <%  detalle_arr.abonobolivar %> Bs</p>
                            <p class="mb-1"> Credito máximo:  <%  detalle_arr.monto_deuda_max %> $</p>
				            <p class="mb-1"> Código familiar: <%  detalle_arr.codigo_familiar %> </p>
				            <p class="mb-1"> Password: <%  detalle_arr.passwordalumno %> </p>
				            <p class="mb-1"> Qr: <span style="background: #f3f5f9;padding: 0px 10px;"> <%  detalle_arr.codigo %> </span> </p>
				        </div>
				       </div>
				       <h5 class="mt-4 mb-0" style="font-size: 18px;">Resumen de transacciones </h5>
				       <hr style="padding: 0px;margin: 0px;">

				            <form action="verclientes/exportar-pedidos"> 

				            	<input type="hidden" id="id_elegido" name="id_elegido">

			                  	<div class="input-group mb-0 mt-2">
				                      <input type="hidden" id="transaccionesselect" value="0">
				                      <div class="input-group-prepend">
				                        <span class="input-group-text" id="basic-addon1">DESDE</span>
				                      </div>
				                      <input type="date" class="form-control" value="{{$ayer2}}" id="desdetra" name="desdetra">
				                      <div class="input-group-prepend" style="margin-left: 10px;">
				                        <span class="input-group-text" id="basic-addon1">HASTA</span>
				                      </div>
				                      <input type="date" class="form-control" value="{{$hoy2}}" id="hastatra" name="hastatra">
			                          <button type="submit"  class="btn btn-primary btnvv" style="margin-left: 10px;">Descargar Excel</button>
			                  	</div> 

			                </form>   
			            
			            <div class="row mb-3 mt-2">
			            	<button type="button"  class="btn btn-primary btnvv btnactive"  ng-click="clicktran(1)" style="margin-left: 10px;">Pedidos</button>
			            	<button type="button"  class="btn btn-primary btnvv"  ng-click="clicktran(2)" style="margin-left: 10px;">Recargas</button>
			               
			            </div>

			            

			            <div id="contpedidos">

			            	<div style="font-size: 20px;margin-bottom: 11px;font-weight: 700;">
			            	TOTAL: <% pedidos_arr[0].totalmontis %> $
			            </div>
					       	<table class="table table-striped">
								  <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">Monto</th>
								      <th scope="col">Fecha</th>
								      <th scope="col">Tipo</th>
								    </tr>
								  </thead>
								  <tbody>
								    <tr ng-repeat="(key, combi) in pedidos_arr">
								      <td class="td1"><% key + 1 %></td>
								      <td class="td1"><% combi.monto %> $</td>
								      <td class="td1"><% combi.fechareg %></td>
								      <td class="td1"><% combi.tipo_pago %></td>
								    </tr>
								  </tbody>
							</table>
						</div>

						<div id="contrecargas" style="display: none;">

						<div style="font-size: 20px;margin-bottom: 11px;font-weight: 700;">
			            	TOTAL: <% recargas_arr[0].totalmontis %> $
			            </div>
					       	<table class="table table-striped">
								  <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">Monto</th>
								      <th scope="col">Fecha</th>
								      <th scope="col">Pago</th>
								      <th scope="col">Status</th>
								    </tr>
								  </thead>
								  <tbody>
								    <tr ng-repeat="(key, combi) in recargas_arr">
								       <td class="td1"><% key + 1 %></td>
								      <td class="td1">
								      	<% combi.monto %> 
								      	<span ng-if="combi.recarga.tipo_pago == 'pago movil' "> Bs.</span>
								      	<span ng-if="combi.recarga.tipo_pago != 'pago movil' ">$</span>
								      </td>
								      <td class="td1"><% combi.fechareg %></td>
								      <td class="td1"><% combi.recarga.tipo_pago %></td>
								      <td class="td1">
								      	<span ng-if="combi.valido == '0' " style="color: #cfbc38;">POR VALIDAR</span>
								      	<span ng-if="combi.valido == '1' " style="color: #35b982;">VALIDADO</span>
								      	<span ng-if="combi.valido == '2' " style="color: #aa1a18;">NEGADO</span>

								      </td>
								    </tr>
								  </tbody>
							</table>
						</div>

			        </div>
			      </div>
			    </div>
			  </div>
			</div>

			<div class="modal" tabindex="-1" id="modaleditar">
			  <div class="modal-dialog" style="max-width: 700px;">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Detalle</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <div class="container-fluid">
						<div class="row mb-2">
							<div class="col-6">
								<p class="mb-0"> Nombre: <input type="text" class="form-control" id="nombre_alumno" value="<% cliente_editar.nombre_alumno %> or ''"></p>
							</div>
							<div class="col-6">
								<p class="mb-0"> Apellido: <input type="text" class="form-control" id="apellido_alumno" value="<% cliente_editar.apellido_alumno %>"></p>
							</div>    
						</div>
						<div class="row mb-2">
							<div class="col-6">	
								<p class="mb-0"> Correo: <input type="text" class="form-control" id="correo_alumno" value="<% cliente_editar.correo_alumno %>"></p>
							</div>        
							<div class="col-6">
								<p class="mb-0"> Cedula: <input type="text" class="form-control" id="cedula_editar" value="<% cliente_editar.cedula %>"></p>
							</div>
						</div>
						<div class="row mb-2">
							<div class="col-6">    
								<p class="mb-0"> Saldo Bs.: <input type="number" class="form-control" id="grado" value="<% cliente_editar.abono_bs %>"></p>
							</div>
							<div class="col-6">
								<p class="mb-0"> Saldo $: <input type="number" class="form-control" id="abono_actual" value="<% cliente_editar.abono_actual %>"> </p>
							</div>
						</div>

						<div class="row mb-2">
							<div class="col-6">    
								<p class="mb-0">Limite gasto diario: <input type="number" class="form-control" id="limitediario" value="<% cliente_editar.limitediario %>"></p>
							</div>
							<div class="col-6">
								<p class="mb-0">Limite de credito:   <input type="number" class="form-control" id="limitecredito" value="<% cliente_editar.monto_deuda_max %>"> </p>
							</div>
						</div>

						<div class="row mb-2">
							<div class="col-6">    
								<p class="mb-0">Qr: <input type="text" class="form-control" id="codigoqredit" value="<% cliente_editar.codigo %>"></p>
							</div>
						</div>

						


			        </div>
			      </div>
				  <div class="modal-footer">
				  	<div class="row" style="width: 100%;">
				  		<div class="col-6" style="text-align: left;">
				  			<button type="button" class="btn btn-danger" data-dismiss="modal" ng-click="inactivar(cliente_editar.id)">INACTIVAR</button>
				  		</div>
				  		<div class="col-6" style="text-align: right;">
                            <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="editarCliente(cliente_editar.id)">Editar</button>					  
				  			<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>	
				  		</div>
				  		
				  	</div>
				  					  
				  </div>
			    </div>
			  </div>
			</div>

			<div class="modal" tabindex="-1" id="modalmax">
			  <div class="modal-dialog" style="max-width: 700px;">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title">Cambiar credito</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <div class="container-fluid">
			            <div class="form-group">
						    <label for="exampleFormControlInput1">Monto</label>

						    <div class="input-group mb-3">
							  <div class="input-group-prepend">
							    <span class="input-group-text" id="basic-addon1">$</span>
							  </div>
							  <input type="text" class="form-control" id="limite" style="max-width: 70px;">
							</div>
						</div>
						 <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
						 <button type="button" class="btn btn-primary" id="botolin" ng-click="cambiolin()">Guardar</button>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>

           <div class="d-flex align-items-center p-3 my-3 justify-content-between text-white-50 bg-purple rounded box-shadow clentesbox">
              <div class="lh-100">
                <h5 class="mb-0 text-white lh-100">Clientes</h5>
              </div>
			  <div>
			  	<a  href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" ng-click="importarclientes()"><i class="fas  fa-plus fa-sm text-white-50"></i> Importar Clientes</a>
				
			  </div>
			  
            </div>
            
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="validationTooltipUsernamePrepend">Buscar</span>
                </div>
                <input type="text" class="form-control" ng-model="buscador">
            </div>
 
            <div class="my-3 p-3 bg-white rounded box-shadow">
            	<div class="col-12 mb-3" style="font-size: 20px;">
			  		Nro. clientes: <b> <% clientes_arr.length  %> </b>
			    </div>
               
               <div class="table-responsive">
               <table class="table">
				  <thead>
				    <tr>
				      <th class="mlg">#</th>
				      <th class="mxs">#</th>
				      <th>Nombre</th>
				      <th>Apellido</th>
				      <th>Limite diario</th>
					  <th>Tipo Cliente</th>
				      <th>Saldo</th>
				      <th>Cod. Familiar</th>
				      <th>Detalle</th>
				      <th>Editar</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr ng-repeat="(key, combi) in clientes_arr|filter:buscador">
				      <td class="mlg"> <%  key + 1 %>   </td>
				      <td class="mxs" ng-click="editar(combi)" style="cursor: pointer;"> <i class="fas fa-edit"  ></i>   </td>
				      <td> <%  combi.nombre_alumno  %>   </td>
				      <th> <%  combi.apellido_alumno  %> </th>
				      <td> <%  combi.limitediario  %> $   </td>
				      <td> <%  combi.tipo_cliente.nombre  %>   </td>
				      <td>
                        <span ng-if="combi.abono_actual < 0" style="color: #aa1a18;font-weight: 700;"> <% combi.nuformatdolar  %> $</span>
                        <span ng-if="combi.abono_actual >= 0"> <%  combi.nuformatdolar  %> $</span> /
                        <span ng-if="combi.abono_bs < 0" style="color: #aa1a18;font-weight: 700;"> <% combi.nuformatbs  %> Bs.</span>
                        <span ng-if="combi.abono_bs >= 0"> <%  combi.nuformatbs  %> Bs.</span>
                        

				      </td>
				      <td> <%  combi.codigo_familiar  %>   </td>
				      <td ng-click="detalle(combi.id)" style="cursor: pointer;"><i class="fas fa-eye"></i></td>
				      <td ng-click="editar(combi)" style="cursor: pointer;"><i class="fas fa-edit"></i></td>
				    </tr>
				  </tbody>
				</table>
				</div>
            </div>
         </div>



  	<!-- INCLUIR FOOTER-->
    @include('footer_admin')
    <!-- INCLUIR FOOTER-->

    <script src="{{url('/js/angularclientes7.js')}}"></script>