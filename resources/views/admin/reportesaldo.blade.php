<p>Saldo en Dolares: {{$saldodolar}} $</p>

<p>Saldo en Bolivares: {{$saldobs}} Bs</p>


<p>Saldo deudor en Dolares: <span style="color: #aa1a18"> {{$deudadolar}} $ </span></p>

------------------------------------------------------------------------------------
<p style="margin-top: 20px;margin-bottom: 20px;">RECARGAS</p>

<p>Recargas Zelle:  {{$recargazelle}} $ </p>

<p>Recargas Punto de venta:  {{$recargapunto}} Bs </p>

<p>Recargas Paypal:  {{$recargapaypal}} $ </p>

<p>Recargas Efectivo:  {{$recargaefectivo}} $ </p>

<p>Recargas Pago Movil:  {{$recargamovil}} Bs</p>

<p style="margin-top: 20px;margin-bottom: 20px;">VENTAS</p>

<p>Ventas usando QR:  {{$pedido}} $</p>

<p>Ventas sin usar QR:  {{$pedido2}} $</p>