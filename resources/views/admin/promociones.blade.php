   		<!-- INCLUIR HEADER-->
           @include('header_admin')
         <!-- INCLUIR HEADER-->

         <style type="text/css">
         	th{text-align: center;}
         	td{text-align: center;}
         </style>


         <script type="text/javascript">
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'app.pageview',
                'pageName': 'promociones',
                'colegio': 'cumbres',
                'pais': 'VE',
                'tipo': 'admin',
                'seccion': 'admin'
            });
         </script>

        <div class="container-fluid" ng-module="CantinaPromosionesApp" ng-controller="CantinaPromosionesController"> 



			<div class="modal" id="nuevopromociones">
			    <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        
                        <div class="modal-body" style="font-size: 16px;">
                            <form enctype="multipart/form-data" action="promociones/nuevo" id="nuevopromocionform" method="post">
                            
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"  id="file-image" name="image" >
                                                    <label class="custom-file-label" for="file-image" data-browse="Buscar"></label>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="row mt-2">
                                            <div class="col">
                                                    <img src="img/not-available.png"  id="imgSalida" class=" img-thumbnail" style="with:100% !important"> 
                                            </div>
                                        </div>                                  
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Nombre</label>
                                                    <input type="text" class="form-control" name="nombre" id="nombre" value="">
                                                </div>
                                            </div>
                                            
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Precio</label>
                                                    <input type="text" class="form-control" name="precio" id="precio" value="">
                                                </div>
                                            </div>
                                        </div>			
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Estatus</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="status" name="status">
                                                            <option value="">SELECCIONA</option>
                                                            <option value="0">INACTIVO</option>
                                                            <option value="1">ACTIVO</option>
                                                        </select>
                                                    </div>
                                                </div>                                
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Categoría</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="categoria_id" name="categoria_id">
                                                            <option value="">SELECCIONA</option>
                                                            @foreach($categorias as $categoria)
                                                                <option value="{{$categoria->id}}"  >{{$categoria->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>	                                
                                            </div>                            
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Descripción</label>
                                                    <textarea class="form-control" name="descripcion" id="descripcion" value=""></textarea>
                                                </div>                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <h5 class="mt-2">Promociones y productos compuestos</h5>
                                <div class="row mt-2 mb-2">
                                    <div class="col">
                                        <button class="btn btn-info" type="button" id="nuevoclonproducto">Más productos</button>
                                    </div>
                                </div>
                                
                                <div class="row formclone mt-3 mb-2" >
                                    <div class="col">
                                        <label>Seleccione un producto</label>
                                        <select class="form-control" name="productos[]" >
                                            
                                            <option value="">Seleccione</option>
                                            
                                            @foreach($productos as $producto)
                                                <option value="{{$producto->id}}">{{$producto->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label>Cantidad</label>
                                        <input class="form-control" type="text" name="cantidad[]" value>
                                    </div>
                                    <div class="col eliminar" hidden>
                                        <button type="button" class="btn eliminarc" > Eliminar</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col  d-flex justify-content-end">
                                        <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="font-size: 35px;display: none;"></i>
                                        <button type="button" class="btn btn-primary mt-2" ng-click="nuevo()">Guardar</button>
                                    </div>  
                                </div>
                            </form>    
                        </div>
                    </div>
			    </div>
			</div>

			<div class="modal" id="editarpromociones">
			    <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        
                        <div class="modal-body" style="font-size: 22px;">
                            <form enctype="multipart/form-data" action="promociones/editar" id="editarpromocionform" method="post">
                            
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"  id="file-imageedit" name="image_edicion" >
                                                    <label class="custom-file-label" for="file-image" data-browse="Buscar"></label>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="row mt-2">
                                            <div class="col">
                                                    <img src="<% promo_edicon.imagen %>"  id="imgSalidaedit" class=" img-thumbnail" style="with:100% !important"> 
                                            </div>
                                        </div>                                  
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Nombre</label>
                                                    <input type="text" class="form-control" name="nombre_edicion" id="nombre_edicion" value="<% promo_edicon.nombre %>">
                                                    <input type="hidden" class="form-control" name="id_edicion" id="id_edicion" value="<% promo_edicon.id %>">
                                                </div>
                                            </div>
                                            
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Precio</label>
                                                    <input type="text" class="form-control" name="precio_edicion" id="precio_edicion" value="<% promo_edicon.costo %>">
                                                </div>
                                            </div>
                                        </div>			
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Estatus</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="status_edicion" name="status_edicion">
                                                        <option value="" >SELECCIONA</option>
                                                            <option value="0"  ng-if="promo_edicon.status == 0" selected>INACTIVO</option>
                                                            <option value="0"  ng-if="promo_edicon.status == 1">INACTIVO</option>
                                                            <option value="1"  ng-if="promo_edicon.status == 0">ACTIVO</option>
                                                            <option value="1"  ng-if="promo_edicon.status == 1" selected>ACTIVO</option>
                                                        </select>
                                                    </div>
                                                </div>                                
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Categoría</label>
                                                    <div class="form-group">
                                                        <select class="form-control" id="categoria_id_edicion" name="categoria_id_edicion">
                                                            <option value="" >SELECCIONA</option>
                                                            <option value="<% promo_edicon.id_categoria %>" selected><% promo_edicon.categoria.nombre %></option>
                                                            @foreach($categorias as $categoria)
                                                                <option value="{{$categoria->id}}"  >{{$categoria->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>	                                
                                            </div>                            
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Descripción</label>
                                                    <textarea class="form-control" name="descripcion_edicion" id="descripcion_edicion" value="<% promo_edicon.descripcion %>"><% promo_edicon.descripcion %></textarea>
                                                </div>                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <h5>Productos</h5>
                                <div class="row mt-2 mb-2">
                                    <div class="col">
                                        <button class="btn btn-info" type="button" id="nuevoclonproductoedit">Mas productos</button>
                                    </div>
                                </div>
                                <div class="row formcloneedit mt-2 mb-2">
                                    <div class="col">
                                        <select class="form-control productosselect" name="productos[]" >
                                            <option value="">Seleccione</option>
                                            @foreach($productos as $producto)
                                                <option value="{{$producto->id}}">{{$producto->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col">
                                        <input class="form-control cantidadinput" type="text" name="cantidad[]" value="">
                                    </div>
                                    <div class="col eliminaredit" hidden>
                                        <button type="button" class="btn eliminaredit" > Eliminar</button>
                                    </div>
                                </div>
                                <h5>Existentes</h5>
                                <hr>

                                <div class="row formcloneedit mt-2 mb-2" ng-repeat="produc in productospro">
                                    <div class="col">
                                        <select class="form-control" name="productos[]" >
                                            <option value="<% produc.producto.id %>"><% produc.producto.nombre %></option>

                                            @foreach($productos as $producto)
                                                <option value="{{$producto->id}}">{{$producto->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col">
                                        <input class="form-control" type="text" name="cantidad[]" value="<% produc.cant_producto %>">
                                    </div>
                                    <div class="col" >
                                        <button type="button" class="btn eliminaredit" > Eliminar</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col  d-flex justify-content-end">
                                        <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="font-size: 35px;display: none;"></i>
                                        <button type="button" class="btn btn-primary mt-2" ng-click="editar()">Guardar</button>
                                    </div>  
                                </div>
                            </form>    
                        </div>
                    </div>
			    </div>
			</div>



            <input type="hidden" name="detalleelegido" id="detalleelegido">

             <div class="card shadow mb-4 tam2">

			    <div class="card-header py-3 d-sm-flex align-items-center justify-content-between mb-4">
                      <h5 class="m-0 font-weight-bold">Promociones</h5>
                      <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" ng-click="nuevopromociones()"><i
                        class="fas  fa-plus fa-sm text-white-50"></i> Crear nuevo</a>
                </div>
				<div class="card-body"> 

    			<div class="mt-2 mb-4">
					<div class="input-group mb-3">
					  <div class="input-group-prepend">
					    <span class="input-group-text">Buscar</span>
					  </div>
					  <input type="text" class="form-control" ng-model="buscador">
					</div>
				</div>

				<div>
					
						
						<div class="row ">
						   <div class="col-md-2 mt-2 mb-4" ng-repeat="(key, combi) in promociones_arr|filter:buscador|orderBy:id">
							  	<div class="contproduct" style="cursor: pointer;">
                                    <span style="cursor:pointer;" ng-click="editarpromociones(combi)"><i class="fa fa-pencil" aria-hidden="true"></i> Editar </span>
							  	   <div class="contfto">
							  			<img src="<% combi.imagen %>"  style="max-width: 100%;">
							  		</div>
							  		<div class="contleter mt-2 text-center" style="font-size: 13px;">
							  			<% combi.nombre %> 
							  		</div> 
		
							  	</div>
						  </div>
					  </div>
					
				</div>
         </div>


        </div>


        </div>

        <!-- INCLUIR HEADER-->
            @include('footer_admin')
        <!-- INCLUIR HEADER-->

        @if(Session::has('listo'))
			<script type="text/javascript">  
			 $("#modalagree").modal();
			</script>
		@endif
       
	   <script src="{{url('js/angularpromociones2.js')}}"></script>
