   		<!-- INCLUIR HEADER-->
       @include('header_admin')
         <!-- INCLUIR HEADER-->

         <style type="text/css">
         	th{text-align: center;}
         	td{text-align: center;}

          .linkse{    background: white;
            border-radius: 4px;
            color: #666666;
            border: solid 1px #c6c6c6;}

          .table td, .table th {
          padding: 10px;font-weight: 500;
          vertical-align: top;
          border-top: 1px solid #e3e6f0;
          }

         </style>


  <script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
    'event': 'app.pageview',
    'pageName': 'reservas',
    'colegio': 'cumbres',
    'pais': 'VE',
    'tipo': 'admin',
    'seccion': 'admin'
    });

    function reservastotallink() {
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
        'event': 'GenericGAEvent',
        'eventCategory': 'reservas',
        'eventAction': 'reservas-click',
        'tipo': 'admin',
        'colegio': 'cumbres',
        'pais': 'VE',
        });
    }

    function reservasproductlink() {
       
       window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
        'event': 'GenericGAEvent',
        'eventCategory': 'productos-reservados',
        'eventAction': 'productos-reservados-click',
        'tipo': 'admin',
        'colegio': 'cumbres',
        'pais': 'VE',
        });
        
    }


  </script>

  <div class="container-fluid" ng-module="ReservaApp" ng-controller="ReservaController"> 



  <div class="modal" id="openmodalanular">
    <div class="modal-dialog ">
      <div class="modal-content">

          <div class="modal-body" style="font-size: 22px;">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col text-center mb-2" >
                          <span>¿Deseas anular esta reserva?</span>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col d-flex justify-content-center">
                          <input type="hidden" id="reserva_id" value="">
                          <button type="button" class="btn btn-info" ng-click="anular()">SI</button>
                          <button type="button" class="btn btn-info" data-dismiss="modal" style="margin-left: 10px;">No</button>

                      </div>
                  </div>                  
              </div>                     
          </div>
      </div>
    </div>
  </div>

  <div class="modal" id="openmodalanularprod">
    <div class="modal-dialog ">
      <div class="modal-content">
     
          <div class="modal-body" style="font-size: 22px;">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col text-center" style="line-height: 27px;margin-bottom: 15px;">
                          <span>¿Deseas eliminar este producto de las reservas?</span>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col d-flex justify-content-center">
                          <input type="hidden" id="productores_id" value="">
                          <button type="button" class="btn btn-info" ng-click="anularrespro()">SI</button>
                          <button type="button" class="btn btn-info" data-dismiss="modal" style="margin-left: 10px;">No</button>

                      </div>
                  </div>                  
              </div>                     
          </div>
      </div>
    </div>
  </div>  
  
  <div class="modal" id="openmodaldetalle">
        <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="headermodal" style="padding: 14px 0px 0px 18px;"> 
                          <h4 style="margin: 0px"> Detalle de productos </h4>
                      </div>
                      <div class="modal-body">
                          <div class="container-fluid">
                              <div class="row">
                                  <div class="col-12">
                                      <div class="row" style="margin-top: 15px;">
                                          <div class="col">
                                              <table class="table table-responsive-lg">
                                                  <thead>
                                                      <tr>
                                                      <th>Producto</th>
                                                      <th>Cantidad</th>
                                                      <th>Precio</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                      <tr ng-repeat="(key, carro) in carrito">
                                                          <th ng-if="carro.tipo == 'Producto'"><% carro.producto.nombre %></th>
                                                          <th ng-if="carro.tipo == 'Promo'"><% carro.promocion.nombre %></th>
                                                          <th><% carro.cantidad %></th>
                                                          <th><% carro.precio_unitario %> $</th>
                                                      </tr>
                                                      
                                                  </tbody>
                                              </table>
                                              <div class="d-flex justify-content-end">
                                                <span class="text-bold">Total: </span> <span> <b> <% carrito[0].monto_total %> $ </b> </span>
                                              </div>
                                          </div>
                                      </div>
                                  </div> 
                              </div>
                          </div>                     
                      </div>
                  </div>
        </div>
    </div>


            

    <div class="card shadow mb-4 tam2">

       <div class="card-header  d-sm-flex justify-content-start mb-0">
            <h5 class="m-0">Reservas</h5>
      </div>

      <div class="card-header  d-sm-flex justify-content-start mb-0">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
              <li class="nav-item">
                <a class="btn btn-primary" id="reservas-tab" data-toggle="pill" href="#reservas" role="tab" aria-controls="reservas" aria-selected="true" onclick="reservastotallink()">Reservas</a>
              </li>
              <li class="nav-item" style="margin-left: 10px;">
                <a class="btn btn-outline-secondary" id="produtosr-tab" data-toggle="pill" href="#produtosr" role="tab" aria-controls="produtosr" aria-selected="false" onclick="reservasproductlink()" >Productos Reservados</a>
              </li>
            </ul>
      </div>
      
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="reservas" role="tabpanel" aria-labelledby="reservas-tab">
          <div class="card-body"> 
              <div class="row" >
                <div class="col mt-0 mb-0">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Buscar</span>
                    </div>
                    <input type="text" class="form-control" ng-model="buscador">
                  </div>
                </div>
              </div>
              <div class="row" hidden>

                  <div class="col-md-10 mt-2 mt-2 mb-2">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">DESDE</span>
                        </div>
                        <input type="date" class="form-control" value="{{$hoy2}}" id="desdereser" name="desde">

                        <div class="input-group-prepend" >
                          <span class="input-group-text" id="basic-addon1">HASTA</span>
                        </div>
                        <input type="date" class="form-control" value="{{$hoy2}}" id="hastareser" name="hasta">
                    </div>    
                  </div>
                  
                  <div class="col-md-2 mt-2 mt-2 mb-2">
                    <div class="input-group mb-3">
                        <div class="input-group">
                          <button type="btn"  class="btn btn-primary" style="margin-right: 20px;" ng-click="buscar()">Buscar</button>
                        </div>
                    </div>
                  </div>
            
              </div>            
              <div class="row">

                  <div class="col-md-2 mt-2">
                    <a href="#" class="btn btn-secondary btn-icon-split" ng-click="buscar('Pendiente')" style="color: white;width: 100%;background: #aa1a18;border-color: transparent;border-radius: 5px;">
                        <span class="text">Pendientes</span>
                    </a>
                  </div>
                  <div class="col-md-2 mt-2">
                    <a href="#" class="btn btn-secondary btn-icon-split" ng-click="buscar('Anulado')" style="color: white;width: 100%;background: #181818;border-color: transparent;border-radius: 5px;">
                        <span class="text">Anulados</span>
                    </a>
                  </div>
            
              </div>

              <div class="row  mt-4">
                <div class="col-md-12"> 
                  <table class="table table-responsive-xl">
                      <thead class="thead-dark">
                        <tr>
                          <th>Id</th>
                          <th>Cliente</th>
                          <th>Monto Total</th>
                          <th>Fecha de creación</th>
                          <th>Horario de despacho</th>
                          <th>Anular</th>
                          <th>Ver</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="res in reservas_arr|filter:buscador">
                          <th><% res.id %></th>
                          <td>
                            <% res.cliente.nombre %>
                          </td>
                          <td>
                            <% res.monto_total %> $
                          </td>
                          <td>
                            <% res.fecha_registro_format %> 
                          </td>
                          <td>
                            <% res.horario %> 
                          </td>
                          <td ng-if="res.status.id == 1">
                           <i class="fas fa-minus-circle" ng-click="openmodalanular(res.id)" style="color: #aa1a18;cursor: pointer;"></i>
                          </td>
                          <td ng-if="res.status.id != 1">
                            - 
                          </td>
                          <td>
                            <span ng-click="openmodaldetalle( res.carrito )"><i class="fa fa-pencil-square-o fon50" style="cursor: pointer;"></i></span>
                          </td>
                        </tr>
                      </tbody>
                  </table>
                </div>  
              </div>
          </div>          
        </div>
        <div class="tab-pane fade" id="produtosr" role="tabpanel" aria-labelledby="produtosr-tab">
          <div class="card-body"> 
              <div class="row" >
                <div class="col mt-2 mb-0">
                  <div class="input-group mb-0">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Buscar</span>
                    </div>
                    <input type="text" class="form-control" ng-model="buscador">
                  </div>
                </div>
              </div>
            
              <div class="row" hidden>

                  <div class="col-md-10 mt-0 mb-0">
                    <div class="input-group mb-0">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">DESDE</span>
                        </div>
                        <input type="date" class="form-control" value="{{$hoy2}}" id="desde" name="desde">

                        <div class="input-group-prepend" >
                          <span class="input-group-text" id="basic-addon1">HASTA</span>
                        </div>
                        <input type="date" class="form-control" value="{{$hoy2}}" id="hasta" name="hasta">
                    </div>    
                  </div>
                  <div class="col-md-2 mt-2 mt-2 mb-2">
                    <div class="input-group mb-3">
                        <div class="input-group">
                          <button type="btn"  class="btn btn-primary" style="margin-right: 20px;" ng-click="buscarproducto()">Buscar</button>
                        </div>
                    </div>
                  </div>
            
              </div>

              <div class="row  mt-4">
                <div class="col-md-12"> 
                  <table class="table table-responsive-xl">
                      <thead class="thead-dark">
                        <tr>
                          <th>Id</th>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Horario de despacho</th>
                          <th>Eliminar</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="prod in productosreservas_arr|filter:buscador">
                          <th><% prod.id %></th>
                          <td>
                            <% prod.nombre_producto %>
                          </td>
                          <td>
                            <% prod.cantidad %> 
                          </td>
                          <td>
                            <% prod.horario %> 
                          </td>
                          <td>
                            <i class="fas fa-trash-alt" ng-click="openmodalanularprod(prod.id)" style="color: #aa1a18;cursor: pointer;"></i>
                          </td>
                        </tr>
                      </tbody>
                  </table>
                </div>  
              </div>
          </div>          
        </div>
              
      </div>

    </div>
  </div>


  <!-- INCLUIR HEADER-->
      @include('footer_admin')
  <!-- INCLUIR HEADER-->


  @if(Session::has('listo'))
    <script type="text/javascript">  
      $("#modalagree").modal();
    </script>
    @endif



       
	   <script src="{{url('js/angularreservas.js')}}"></script>
