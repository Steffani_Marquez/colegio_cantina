
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->

    <link rel="shortcut icon" href="{{url('img/logo.png')}}">
    <title>LA CANTINA DIGITAL</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/recargarsaldo.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">




</head>

<style type="text/css">
  th{text-align: center;}
  td{text-align: center;}
</style>
   
   <body class="fondis2">

      <div class="container-fluid">
			<div class="contenshe mt-3 container" id="headercliente" >
				<div class="codigobar seccionh" id="codigobar">
                    <div class="text-center mb-4">
                        <img src="img/loguis.svg" style="width: 80%;">
                    </div>         
                    <form action="login/login" method="POST">
                        <div class="mb-3 temp">Por favor, introduzca su email y contraseña</div>
                                            
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-circle"></i></span>
                                        </div>
                                        <input type="text" class="form-control inpute" placeholder="Ingresa tu nombre de usuario" id="username" name="username">
                                    </div>

                                    <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                                            </div>
                                            <input type="password" class="form-control inpute" placeholder="Contraseña" name="password" id="password">
                                    </div>
                                    @if(Session('error'))
                                        <div id="error" class="error"> {{Session('error')}} </div>
                                    @endif
                                    <div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i></div>
                                    <button type="submit" class="btn btn-primary btningre bntingre"  style="width: 100%;">INGRESAR</button>
                            </div>
                        </div>
                    </form>

            </div>
        </div>

  <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>


    <script src="js/angular.min.js"></script>
  	<script src="js/angular.ng-modules.js"></script>

</body>
</html>