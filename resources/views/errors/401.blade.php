<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

             <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 61px;
                margin-bottom: 40px;
            }
            .no{
                color: #00294d;
              background-color: transparent;
              border: solid thin #00294d;
                border-radius: 25px;
                text-decoration: none;
                padding: 10px 15px;
                font-size: 20px;}

            .no:hover{
                  color: #fff;
                  background-color: #00294d;
                  border-color: #00294d;
            }

            .no:not(:disabled):not(.disabled):active, .no:not(:disabled):not(.disabled).active,
            .show > .no.dropdown-toggle {
              color: #fff;
              background-color: #003e74;
              border-color: #003e74;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="footerlogo text-center" style="margin-bottom: 15px;">
                <img src="img/loguis.svg" style="max-width: 200px;">
            </div>
            <div class="content" style="max-width: 70%;">
                <div class="title" style="font-family: arial;font-size: 25px;color: #858585;">Es posible que hayas iniciado sesión con un usuario que no tiene permisos para entrar a este enlace.
                <br><br> Por favor inicia sesión nuevamente</div>
            </div>
            <div>
               <a href="https://cumbres.cafe/digital/login" class="no" style="font-family: arial">   Ir al login</a>
            </div>

        </div>
    </body>
</html>
