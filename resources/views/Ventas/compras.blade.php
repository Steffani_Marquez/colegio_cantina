<!DOCTYPE html>
<html lang="en">
<head>


	 <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MPXZNJP');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{url('img/logo.png')}}">
    <title>LA CANTINA DIGITAL</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/compras3.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">

</head>


<style type="text/css">
	#mydiv {
width: 400px;
    position: absolute;
    z-index: 9;
    background-color: #f1f1f1;
    text-align: center;
    border: 1px solid #d3d3d3;
}
.table td, .table th {
    padding: 5px;
    vertical-align: top;
    border-top: 1px solid #e3e6f0;
}

#mydivheader {
  padding: 10px;
    cursor: move;
    z-index: 10;
    background-color:#435756;
    color: #fff;
}
th{text-align: center;}
td{text-align: center;}
</style>

  <script type="text/javascript">
        function caculularpaypal(){
            var suma = document.calculator.ans.value=eval(document.calculator.ans.value);
        }

        var abierta = 0;


        function calculadoraabrir(){

           

            if (abierta == 0) {
               
                abierta = 1;

                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'calculadora',
                'eventAction': 'calculadora-click-abrir',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais' :'VE'
                });

            }else{

                abierta = 0;
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'calculadora',
                'eventAction': 'calculadora-click-cerrar',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais' :'VE'
                });
            }
           $("#mydiv").toggle();
        }

        function cacululardolar(){
          var tasadolar =  $("#tasamanual").val();
          var suma = document.calculator.ans.value=eval(document.calculator.ans.value);
          var total = parseFloat(suma) * parseFloat(tasadolar);


          document.calculator.ans.value=eval(total);
        }


        function dolarmodal(){
           $("#modaldolar").modal();
           var tasado = $("#tasadolartoday").val();
           $("#tasatext").text(tasado);
        }

        function dolarvalor(){
           $("#changemodal").modal();
        }


  </script>


   <script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
		'event': 'app.pageview',
		'pageName': 'compras',
		'colegio': 'cumbres',
		'pais': 'VE',
		'tipo': 'admin',
		'seccion': 'admin'
		});
	</script>



 <div id="mydiv" style="display: none;" >
      <div class="close" onclick="calculadoraabrir()"><i class="far fa-times-circle"></i></div>
      <div id="mydivheader">CALCULADORA</div>
        <form name="calculator" style="padding: 5px;">
            <input type="textfield" name="ans" id="calculador" value="">
            <br>
            <input type="button" value="1" onClick="document.calculator.ans.value+='1'">
            <input type="button" value="2" onClick="document.calculator.ans.value+='2'">
            <input type="button" value="3" onClick="document.calculator.ans.value+='3'">
            <input type="button" value="+" onClick="document.calculator.ans.value+='+'">
            <br>
            <input type="button" value="4" onClick="document.calculator.ans.value+='4'">
            <input type="button" value="5" onClick="document.calculator.ans.value+='5'">
            <input type="button" value="6" onClick="document.calculator.ans.value+='6'">
            <input type="button" value="-" onClick="document.calculator.ans.value+='-'">
            <br>
            <input type="button" value="7" onClick="document.calculator.ans.value+='7'">
            <input type="button" value="8" onClick="document.calculator.ans.value+='8'">
            <input type="button" value="9" onClick="document.calculator.ans.value+='9'">
            <input type="button" value="*" onClick="document.calculator.ans.value+='*'">
            <br>
            <input type="button" value="0" onClick="document.calculator.ans.value+='0'">
            <input type="button" value="," onClick="document.calculator.ans.value+='.'">
            <input type="button" value="/" onClick="document.calculator.ans.value+='/'">
            <input type="button" value="=" onClick="document.calculator.ans.value=eval(document.calculator.ans.value)">
            <br>
            <input type="reset" value="Borrar">
        </form>
  </div>

    <script>
			//Make the DIV element draggagle:
			dragElement(document.getElementById("mydiv"));

			function dragElement(elmnt) {
			  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
			  if (document.getElementById(elmnt.id + "header")) {
			    /* if present, the header is where you move the DIV from:*/
			    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
			  } else {
			    /* otherwise, move the DIV from anywhere inside the DIV:*/
			    elmnt.onmousedown = dragMouseDown;
			  }

			  function dragMouseDown(e) {
			    e = e || window.event;
			    e.preventDefault();
			    // get the mouse cursor position at startup:
			    pos3 = e.clientX;
			    pos4 = e.clientY;
			    document.onmouseup = closeDragElement;
			    // call a function whenever the cursor moves:
			    document.onmousemove = elementDrag;
			  }

			  function elementDrag(e) {
			    e = e || window.event;
			    e.preventDefault();
			    // calculate the new cursor position:
			    pos1 = pos3 - e.clientX;
			    pos2 = pos4 - e.clientY;
			    pos3 = e.clientX;
			    pos4 = e.clientY;
			    // set the element's new position:
			    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
			    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
			  }

			  function closeDragElement() {
			    /* stop moving when mouse button is released:*/
			    document.onmouseup = null;
			    document.onmousemove = null;
			  }
			}
</script>



<body id="page-top" class="colorbody">

	<!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPXZNJP"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

    

   	<div class="bodyven"  ng-module="CantinaApp" ng-controller="CantinaController">


   			<div class="modal" id="modaldispon">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title">STOCK INSUFICIENTE</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body" style="font-size: 23px;">
					      No tenemos la cantidad de stock suficiente para tu pedido
				      </div>
				    </div>
				  </div>
			</div>



   	       <div class="modal" id="modalreservar">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title">Reservar compra</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
					        <div id="opcionesreservas">
					           		<div class="row">
					           		   <div class="col-6">
								        	<div class="form-group">
											    <label for="exampleFormControlSelect1">Momento</label>
											    <select class="form-control" id="horario">
											      <option value="Primer recreo">Primer recreo</option>
											      <option value="Segundo recreo">Segundo recreo</option>
											      <option value="En este momento">En este momento</option>
											      <option value="En otro momento del día"> En otro momento del día</option>
											    </select>
										  	</div>
									  	</div>
									  	<div class="col-6">
								        	<div class="form-group">
											    <label for="exampleFormControlSelect1">Día</label>
											    <input type="date" id="dia" value="{{$hoy}}" class="form-control">
										  	</div>
									  	</div>

									  	<div class="col-12">
								        	<div class="form-group">
											    <label for="exampleFormControlSelect1">Realizar reserva para</label>
											    <select class="form-control" id="reservapara">
											      <option ng-repeat="combi in cliente_arr.familias"  value="<% combi.id %>" >
											      	<% combi.nombre  %> <% combi.apellido_alumno %>  <% combi.tipo %>
											      </option>
							          	        </select>
										  	</div>
									  	</div>

								   </div>

								  <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
								  <button type="button" class="btn btn-primary" id="btnlis" ng-click="listo()">Reservar</button>
								  <button type="button" class="btn btn-light" ng-click="volver()">Volver</button>
							</div>

							<div id="mensajereserva" style="display: none;">
							    <div id="resersinstock" style="display: none;">
							    	
							    </div>
								<div id="reservadoexito" style="display: none;">
	                                 
								</div>
								<div id="reservadonoexito" style="display: none;">
	                                 
								</div>
								<button type="button" class="btn btn-light" ng-click="volver()">Volver</button>
							</div>
				      </div>
				    </div>
				  </div>
			</div>

			<div class="headercli">

                   
                    <div class="container-fluid pad2">
                      <div class="row">
                          <div class="col-5 text-left">
                          <span class="iconw">   <i class="fas fa-user-circle" ng-click="elegir('perfilsec')" style="color: #ffb517; cursor: pointer;font-size: 25px;"></i> </span>
                        </div>

                        <div class="col-1">
                          <a href="https://cumbres.cafe/">
                            <span class="headerlogo">
                                <img src='img/logocumbres.png' alt title="Logo Cumbres Cafe C.A.">
                            </span>
                          </a>
                        </div>

                        <div class="col-6 text-right">
                         
                          <span class="titulsa">  
                          <a href="accesoclientes" style="text-decoration: none;">
                          <button type="button" class="btn btn-secondary bntcer">Recargar saldo</button>
                          </a>
                          </span>
                          
                          <span class="titulsa">  
                          <a href="#" style="text-decoration: none;">
                          <button type="button" class="btn btn-secondary bntcer" ng-click="elegir('reservascar')">Ver reservas futuras</button>
                          </a>
                          </span>
                          
                          <span class="titulsa">  
                          <button type="button" class="btn btn-secondary bntcer" ng-click="elegir('productoscarr')">Productos</button>
                          </span>

                          <span class="titulsa">  
                          <button type="button" class="btn-outline-primary btnabono" style="width: 100%;" ng-click="elegir('finalizarcar')">
		   		      	 	<i class="fas fa-cart-plus"></i> <% carrito_arr[0].totaltotal %> <span id="cerototal2">0,00</span> $ </button>
		   		      	 </span>
                        </div>

                        
                      </div>
                    </div>

                    <div class="desplegable" style="display: none;">
                    	<span class="titulsa mobile">  
                          <button type="button" class="btn-secondary btnabono mobile" style="width: 100%" ng-click="elegir('finalizarcar')">
		   		      	 	<i class="fas fa-cart-plus"></i> <% carrito_arr[0].totaltotal %> <span id="cerototal2">0,00</span> $ </button>
		   		      	 </span>
                        <!-- Collapse button -->
                          <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fas fa-bars fa-1x"></i></span></button>
                          
                    </div>


                 </div>

                 <!-- Collapsible content -->
                          <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                            <!-- Links -->
                            <ul class="navbar-nav mr-auto">
                              <li class="nav-item active">
                                <a class="nav-link" href="accesoclientes">Recargar saldo<span class="sr-only">(current)</span></a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#" ng-click="elegir('reservascar')">Ver reservas futuras</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href= "#" ng-click="elegir('productoscarr')">Productos</a>
                              </li>
                            </ul>
                            <!-- Links -->
                          </div>
                          <!-- Collapsible content -->

   	      	<div class="headertienda" id="headeruser" style="display: none;">
			    <div class="container-fluid">

						<div id="cliente_vista" class="mt-3 text-center" style="display: none;">
				    		<p class="mb-0 msg"><span class="bold2 co">Hola  <% cliente_arr.nombre %>. </br>Su saldo actual es:</span></p>
				    		<p class="mb-0 mt-3"><span class="bold3 co">  
				    		<% cliente_arr.abono_actual_format %> $ </span></p>
				    		<p class="mb-0 mt-0"><span class="bold3 co">  
				    		<% cliente_arr.abono_bs_format %> Bs. </span></p>

				    	</div>
			   	</div>
		   	</div>


   			<div class="contenshe mt-3 " id="headercliente" >
				    	<div class="codigobar seccionh" id="codigobar">
				    	        <div class="text-center mb-4">
				    	        <img src="img/loguis.svg" style="width: 272px;">
				    	        </div>
                                <div class="mb-3 temp">Por favor, introduzca su email y contraseña</div>
				    	        
				    	        <div class="input-group mb-3">
								  <div class="input-group-prepend">
								    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-circle"></i></span>
								  </div>
								  <input type="text" class="form-control inpute" placeholder="Usuario" id="email">
								</div>

								<div class="input-group mb-3">
								  <div class="input-group-prepend">
								    <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
								  </div>
								  <input type="password" class="form-control inpute" placeholder="Contraseña" id="password">
								</div>

								<div id="error" class="error" style="display: none;">Usuario no encontrado</div>
								<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i></div>
                                 
								<button type="button" class="btn btn-primary btningre bntingre" ng-click='buscarcliente()'>INGRESAR</button>
				    	</div>
				    	
		    </div>

   			<div class="modal" tabindex="-1" id="modal1">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-body">
			        <div id="textomodal" style="font-size: 25px;text-align: center;"></div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
			      </div>
			    </div>
			  </div>
			</div>

		    <div class="modal" tabindex="-1" id="modal2">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-body">
			        <div style="font-size: 25px;text-align: center;">
			        	La reserva ha sido realizado con exito
			        </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-primary" onclick="refres()">LISTO</button>
			      </div>
			    </div>
			  </div>
			</div>

   	    <input type="hidden" id="codigo_personal" name="codigo_personal" value="{{$codidoinvitado}}">
   	    <input type="hidden" id="codigo_personal_antiguo" name="codigo_personal_antiguo" value="{{$codidoinvitado}}">

   		<div class="container-fluid pad50">
	   		<div class="row">

	   		<div class="col-sm-12 col-lg-12 mt-3 seccionh" id="reservascar" style="display: none;">
	   		      	<div class="headercont1">
		   		   	  			<h4 class="mb-0">Reservas futuras</h4>
		   		   	  			<div class="row">
				    			    <div class="col-md-12 col-lg-4 mt-3" ng-repeat="combi in cliente_arr.reservas">
                                            <div class="cajisreser" id="reser<% combi.id %>" ng-click="elegirreserva(combi.id)">
                                            	<p class="mb-0"><b> Usuario: </b>     <% combi.cliente.nombre  %>  <% combi.cliente.apellido_alumno  %></p>
							    			    <p class="mb-0"><b>Realizada: </b>     <% combi.fecharegi  %> </p>
							    			    <p class="mb-0"><b>Reservada para: </b> <% combi.fecharees  %> </p>
							    			    <p class="mb-0"><b>Horario:  </b>    <% combi.horario  %>   </p>
							    			    <p class="mb-0"><b>Total:</b>      <% combi.totalres %> $ </p>
							    			    <p>
							    			    	<span ng-if="combi.status == 1" style="background: #b9bb2d;color: white;padding: 3px 10px;display: block;margin-top: 5px;border-radius: 4px;"> Pendiente</span>
							    			    	<span ng-if="combi.status == 2" style="background: #2dbb3e;color: white;padding: 3px 10px;display: block;margin-top: 5px;border-radius: 4px;"> Entregada</span>
							    			    	<span ng-if="combi.status == 3" style="background: #aa1a18;color: white;padding: 3px 10px;display: block;margin-top: 5px;border-radius: 4px;"> Anulada por cantina</span>
							    			    </p>

							    			    <table class="table mt-3">
														<thead>
														  <tr>
														    <th scope="col">Cantidad</th>
														    <th scope="col">Producto</th>
														    <th scope="col">Precio</th>
														  </tr>
														</thead>
														<tbody>
														  <tr ng-repeat="car in combi.carrito">
														    <td><% car.cantidad  %></td>
														    <td>
														    	<span ng-if="car.tipo == 'Promo'"><%     car.promocion.nombre  %></span>
													      		<span ng-if="car.tipo == 'Producto'"><%  car.producto.nombre  %></span>
													        </td>
														    <td>
														    	<span ng-if="car.tipo == 'Promo'">  <%   car.promocion.costo  %> $</span>
													      		<span ng-if="car.tipo == 'Producto'"><%  car.producto.costo   %> $</span>
													      	</td>
														  </tr>
														</tbody>
												</table>
												

							    			</div>
						    			</div>
				    			</div>

		   		   	</div>

	   		   </div>

	   		   <div class="col-sm-12 col-lg-12 mt-3 seccionh" id="perfilsec" style="display: none;">
	   		      	<div class="headercont1">
		   		   	  			<h4 class="mb-3">Datos del perfil</h4>

		   		   	  			<p class="mb-0"><b> Nombre: </b> <% cliente_arr.nombre %>  </p>
		   		   	  			<p class="mb-0"><b> Saldo: </b> <% cliente_arr.abono_actual %> $  </p>
		   		   	  			<p class="mb-0"><b> Email: </b> <% cliente_arr.correo_alumno %>   </p>
		   		   	  			<p class="mb-0"><b> Pedidos realizados: </b> <% cliente_arr.numpedidos %>   </p>

				    			<input  type="text" id="codigoqr" style="width:80%;display:none;"/><br/>
    							<div id="qrcode" style="width:150px; height:150px; margin-top:15px;"></div>
		   		   	</div>

	   		   </div>



	   		   <div class="col-sm-12 col-lg-12 mt-3 seccionh" id="productoscarr" style="display: none;">
	   		      	<div class="headercont1">
		   		   		<div class="input-group mb-1">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
						  </div>
						  <input type="text" class="form-control" placeholder="Buscar" ng-model="buscador" ng-keypress="buscarproducto('Todos')">
						</div>
				    </div>
				    <div class="headercont1">
				       <div class="btnilera">
				            <div class="btnbotn btntTodos activecar2" onclick="cambiar2('Todos')" ng-click="buscarproducto('Todos')">Todos</div>
				         	@foreach ($categorias as $cat) 
		                    <div class="btnbotn btnt{{$cat->nombre}} mt-2" onclick="cambiar2('{{$cat->nombre}}')" ng-click="buscarproducto({{$cat->id}})">{{$cat->nombre}}</div>
	                        @endforeach
                       </div>
				    </div>

				    <div class="headercont1productos mt-3">
				       <div class="row">

				       		<div class="col-6 col-lg-3 mb-3" ng-repeat="combi2 in promos_arr|filter:buscador">
						       <div class="btnproductos">
			                      <img src="<%  combi2.imagen  %>">
			                        <div class="nombtet"><% combi2.nombre  %> </div>
			                        <div class="contcosto"> $<% combi2.costo %></div>

			                        <div class="contenbtns" ng-if="combi2.agotado == 0"> 
			                           <div class="text-center" style="display: inline-block;"> 
						                        <span ng-click="restarinipromo(combi2.id)" class="rem">-</span>
						                        <span class="inputtext ng-binding" id="inputinipromo<% combi2.id %>">1</span>
						                        <span ng-click="sumarinipromo(combi2.id)" class="rem">+</span>
				                        </div>
			                           <div class="btn-outline-secondary btncarris" ng-click='agregaralcarr_carpromo(combi2.id)'><i class="fas fa-cart-plus"></i> Agregar</div>
	                           	    </div>
	                           	    <div class="contenbtns" ng-if="combi2.agotado == 1"> 
                                       <span class="agotado">AGOTADO</span> 
	                           	    </div>
		                       </div>
	                        </div>

				            <div class="col-6 col-lg-3 mb-3" ng-repeat="combi3 in productos_arr|filter:buscador">
						       <div class="btnproductos">
			                      <img src="<%  combi3.imagen  %>">
			                        <div class="nombtet"><% combi3.nombre  %> </div>
			                        <div class="contcosto"> $<% combi3.costo %></div>

			                        <div class="contenbtns" ng-if="combi3.agotado == 0"> 
			                           <div class="text-center" style="display: inline-block;"> 
						                        <span ng-click="restariniproducto(combi3.id)" class="rem">-</span>
						                        <span class="inputtext ng-binding" id="inputiniproducto<% combi3.id %>">1</span>
						                        <span ng-click="sumariniproducto(combi3.id)" class="rem">+</span>
				                           	</div> 
			                           <div class="btn-outline-secondary btncarris" ng-click='agregaralcarr_carproducto(combi3.id)'><i class="fas fa-cart-plus"></i> Agregar</div>
	                           	    </div>
	                           	    <div class="contenbtns" ng-if="combi3.agotado == 1"> 
                                       <span class="agotado">AGOTADO</span>  
	                           	    </div>

			                        
		                       </div>
	                        </div>

                       </div>
				    </div>

	   		   </div>
	   		   <div class="col-sm-12 col-lg-12 seccionh" id="finalizarcar" style="display: none;">


				    <div class="contenshe mt-3" id="headercarrito" >
				    	 	<div class="carritobar mt-5" id="emtycarr">
				    	    	<div><img src="img/emtycard.svg" class="carritoenty"></div>
					    		<div class="mt-2">
					    			Añade el primer producto al carrito
								</div>
				    		</div>

				    		<div class="carritobar mt-5" id="carlleno">
				    	    	<div class="cajitadecarro" ng-repeat="car in carrito_arr">
				    	    		<div class="row">
				    	    			<div class="col-3" style="padding: 0px; margin: 0px;">
				    	    			  <span><img class="tamcar"  src="<% car.imagenpro %>">  </span> 
				    	    			  <div><% car.nombrepro %></div>	
				    	    			</div>
				    	    			<div class="col-5" style="font-weight: 600;font-size: 15px;color: #858796">
				    	    			 	<div class="text-center" style="display: inline-block;"> 
						                        <span ng-click="restarppv(car.idpro,car.tipo)" class="rem">-</span>
						                        <span class="inputtext" id="inputvalor<% car.tipo %><% car.idpro %>"><% car.cantidad %></span>
						                        <span ng-click="sumarppv(car.idpro,car.tipo)" class="rem">+</span>
				                           	</div>
				    	    			</div>

				    	    			<div class="col-2" style="font-weight: 600;font-size: 14px;color: #858796">
				    	    			  <span>$ <% car.costopro %></span>
				    	    			</div>

				    	    			<div class="col-2" style="font-weight: 600;font-size: 14px;color: #000">
				    	    			  <span>$ <% car.totalindi %></span>
				    	    			</div>

				    	    			<div ng-if="car.agotado == 1">
				    	    			   <div ng-if="car.tipo == 'Promo'">
					    	    			   	<span class="stocko">Esta Promo no tiene stock suficiente.  
						    	    			   	<span ng-repeat="po in car.productpromo"> 
						    	    			   	    <span ng-if="po.mostrarsi == 1">
						    	    			   			<span style="font-weight: 700">  <span><% po.producto.nombre %>: <% po.canti %> </span> / </span>
						    	    			   		</span>
						    	    			   	</span>
					    	    			   	</span>	
				    	    			   </div>
				    	    			   <div ng-if="car.tipo == 'Producto'">
				    	    			   	<span class="stocko">Este producto no tiene stock suficiente. <span style="font-weight: 700"> stock disponible: <% car.cantiagot %></span> </span>	
				    	    			   </div>
				    	    			</div>

				    	    		</div>
				    	    		<hr class="rayisbajo">
				    	    	</div>
				    		</div>
				    	<div class="subtotalcont mt-5">
				    		<div class="row">
				    			<div class="col-9 text-left">
				    				SUB. TOTAL
				    			</div>
				    			<div class="col-3 text-right">
				    				$<% carrito_arr[0].totaltotal %>
				    				<span id="cerosub">0,00</span>
				    			</div>
				    		</div>
				    		<hr class="hrpago">
				    	</div>

				    	<div class="subtotalcont mt-3">
				    		<div class="row">
				    			<div class="col-9 text-left">
				    			 	<span class="azulin">TOTAL</span>	
				    			</div>
				    			<div class="col-3 text-right">
				    				<span class="azulin"> 
				    				$<% carrito_arr[0].totaltotal %> 
				    				<span id="cerototal">0,00</span>
				    				</span>
				    			</div>
				    		</div>
				    	</div>


				    	<div class="botonbtncont text-right">
				    		<div class="btn-outline-secondary btnbotncar btnnetxcarris" ng-click="vaciar()" style="cursor: pointer;">
				    			<i class="fas fa-cart-arrow-down"></i> Vaciar carrito
				    		</div>

				    		<div class="btn-primary btnbotncar btnnetxcarriscompra" ng-click="reservarcompramodal()" style="cursor: pointer;">
				    			<i class="fas fa-check-circle"></i> Finalizar reserva
				    		</div>
				    	</div>

				    </div>

				    

	   		   </div>

	   		   <div class="carpro" style="display: none;">
	   		   	<i class="fas fa-cart-plus"></i> Agregado al carrito
	   		   </div>

	   		</div>
   		</div>
   	</div>

   	

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>
    <script src="js/angular.min.js"></script>
	<script src="js/angular.ng-modules.js"></script>
	<script src="js/angularcompras3.js"></script>
	<script type="text/javascript" src="js/qr.js"></script>



 


    <script type="text/javascript">  

    var tam = $( window ).height() - 180;
    $(".headercont1productos").css("height",tam);

    	
   function refres(){
		location.reload();
	}


    function cambio(id) {

    	$(".contenshe").hide();
     	$("#"+id).show();
     	$(".btnbotn2").removeClass("activecar");
     	$(".btn"+id).addClass("activecar");

    }

    function cambiar2(id){

    	window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
		'event': 'GenericGAEvent',
		'eventCategory': 'navegacioncompra',
		'eventAction': id+"-"+"click",
		'tipo': 'admin',
		'colegio': 'cumbres',
		'pais': 'VE'
		});

    	$(".btnbotn").removeClass("activecar2");
    	$(".btnt"+id).addClass("activecar2");

    }


    	
    	
    </script>


</body>
</html>