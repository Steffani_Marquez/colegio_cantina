
<!DOCTYPE html>
<html lang="en">
<head>

   <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MPXZNJP');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
  	<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
  	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->

    <link rel="shortcut icon" href="{{url('img/logo.png')}}">
    <title>LA CANTINA DIGITAL</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/recargarsaldo.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">




</head>

<style type="text/css">
  th{text-align: center;}
  td{text-align: center;}
  .contregis{background: #fff;margin-top: 20px; border-radius: 5px;
    border: solid 2px #dedede;
    padding: 15px 25px;}
  .contcon{max-width: 700px; margin-bottom: 30px; }
  .rojis{color: #aa1a18;}
  .contheader{}
  .error{text-align: center;}
  .listo{font-size: 20px;}
</style>
   
   <body class="fondis1">

      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPXZNJP"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

      <div ng-module="CantinaApp" ng-controller="CantinaController">

           <div class="container contcon">
              <div class="contregis">
                <div class="contheader">
                    <div class="footerlogo text-center">
                      <img src="img/loguis.svg" style="max-width: 200px;">
                    </div>
               </div>
               <hr>
               <h2>Registro Plataforma Cumbres Cafe </h2>

               <h4 class="mt-5"> Datos del representante</h4>
               <p>Deja los datos de uno de los representantes del alumno</p>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre <span class="rojis">*</span></label>
                    <input type="text" class="form-control" id="nombrer1">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Apellido <span class="rojis">*</span></label>
                    <input type="text" class="form-control" id="apellidor1">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Email <span class="rojis">*</span></label>
                    <input type="text" class="form-control" id="emailr1">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Cédula de identidad <span class="rojis">*</span></label>
                    <input type="number" class="form-control" id="cedular1">
                  </div>

                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Número de hijos que estudian en el colegio</label>
                    <select class="form-control" id="numhijos">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>

                  <h4 class="mt-5"> Datos de tus hijos</h4>
                  <p>Deja los datos del primer hijo</p>

                  <div class="conthijos" id="hijo1">

                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="nombreh1">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Apellido <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="apellidoh1">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Correo del colegio <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="emailh1">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Cédula de identidad (si aplica)</label>
                      <input type="text" class="form-control" id="cedulah1">
                    </div>


                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Grado que cursa <span class="rojis">*</span></label>
                      <select class="form-control" id="gradoh1">
                        <option value="1er grado">1er grado</option>
                        <option value="2do grado">2do grado</option>
                        <option value="3er grado">3er grado</option>
                        <option value="4to grado">4to grado</option>
                        <option value="5to grado">5to grado</option>
                        <option value="6to grado">6to grado</option>
                        <option value="1er año">1er año</option>
                        <option value="2do año">2do año</option>
                        <option value="3er año">3er año</option>
                        <option value="4to año">4to año</option>
                        <option value="5to año">5to año</option>
                      </select>
                    </div>

                  </div>


                   <div class="conthijos mt-4" id="hijo2" style="display: none;">

                   <p>Deja los datos del segundo hijo</p>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="nombreh2">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Apellido <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="apellidoh2">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Correo del colegio <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="emailh2">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Cédula de identidad (si aplica)</label>
                      <input type="text" class="form-control" id="apellidoh2">
                    </div>


                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Grado que cursa <span class="rojis">*</span></label>
                      <select class="form-control" id="gradoh2">
                        <option value="1er grado">1er grado</option>
                        <option value="2do grado">2do grado</option>
                        <option value="3er grado">3er grado</option>
                        <option value="4to grado">4to grado</option>
                        <option value="5to grado">5to grado</option>
                        <option value="6to grado">6to grado</option>
                        <option value="1er año">1er año</option>
                        <option value="2do año">2do año</option>
                        <option value="3er año">3er año</option>
                        <option value="4to año">4to año</option>
                        <option value="5to año">5to año</option>
                      </select>
                    </div>

                  </div>



                  <div class="conthijos mt-4" id="hijo3" style="display: none;">

                  <p>Deja los datos del tecer hijo</p>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="nombreh3">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Apellido <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="apellidoh3">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Correo del colegio <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="emailh3">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Cédula de identidad (si aplica)</label>
                      <input type="text" class="form-control" id="apellidoh3">
                    </div>


                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Grado que cursa <span class="rojis">*</span></label>
                      <select class="form-control" id="gradoh3">
                        <option value="1er grado">1er grado</option>
                        <option value="2do grado">2do grado</option>
                        <option value="3er grado">3er grado</option>
                        <option value="4to grado">4to grado</option>
                        <option value="5to grado">5to grado</option>
                        <option value="6to grado">6to grado</option>
                        <option value="1er año">1er año</option>
                        <option value="2do año">2do año</option>
                        <option value="3er año">3er año</option>
                        <option value="4to año">4to año</option>
                        <option value="5to año">5to año</option>
                      </select>
                    </div>

                  </div>



                    <div class="conthijos mt-4" id="hijo4" style="display: none;">

                    <p>Deja los datos del cuarto hijo</p>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="nombreh4">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Apellido <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="apellidoh4">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Correo del colegio <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="emailh4">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Cédula de identidad (si aplica)</label>
                      <input type="text" class="form-control" id="apellidoh4">
                    </div>


                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Grado que cursa <span class="rojis">*</span></label>
                      <select class="form-control" id="gradoh4">
                        <option value="1er grado">1er grado</option>
                        <option value="2do grado">2do grado</option>
                        <option value="3er grado">3er grado</option>
                        <option value="4to grado">4to grado</option>
                        <option value="5to grado">5to grado</option>
                        <option value="6to grado">6to grado</option>
                        <option value="1er año">1er año</option>
                        <option value="2do año">2do año</option>
                        <option value="3er año">3er año</option>
                        <option value="4to año">4to año</option>
                        <option value="5to año">5to año</option>
                      </select>
                    </div>

                  </div>

                    <div class="conthijos mt-4" id="hijo5" style="display: none;">


                    <p>Deja los datos del quinto hijo</p>



                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="nombreh5">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Apellido <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="apellidoh5">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Correo del colegio <span class="rojis">*</span></label>
                      <input type="text" class="form-control" id="emailh5">
                    </div>

                     <div class="form-group">
                      <label for="exampleInputEmail1">Cédula de identidad (si aplica)</label>
                      <input type="text" class="form-control" id="apellidoh5">
                    </div>


                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Grado que cursa <span class="rojis">*</span></label>
                      <select class="form-control" id="gradoh5">
                        <option value="1er grado">1er grado</option>
                        <option value="2do grado">2do grado</option>
                        <option value="3er grado">3er grado</option>
                        <option value="4to grado">4to grado</option>
                        <option value="5to grado">5to grado</option>
                        <option value="6to grado">6to grado</option>
                        <option value="1er año">1er año</option>
                        <option value="2do año">2do año</option>
                        <option value="3er año">3er año</option>
                        <option value="4to año">4to año</option>
                        <option value="5to año">5to año</option>
                      </select>
                    </div>

                  </div>


                    <div class="form-group mt-5">
                      <label for="exampleFormControlSelect1">¿Desea dejar los datos del otro representante?</label>
                      <select class="form-control" id="opcionrepres2">
                        <option value="SI">Si</option>
                        <option value="NO">NO</option>
                      </select>
                    </div>

                    <div class="contrepren2 mt-4" id="represent2">

                      <h4 class="mt-4"> Datos del segundo representante</h4>
                      <p>Deja los datos del segundo representante</p>


                      <div class="form-group">
                          <label for="exampleInputEmail1">Nombre <span class="rojis">*</span></label>
                          <input type="text" class="form-control" id="nombrer2">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Apellido <span class="rojis">*</span></label>
                          <input type="text" class="form-control" id="apellidor2">
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">Email <span class="rojis">*</span></label>
                          <input type="text" class="form-control" id="emailr2">
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">Cédula de identidad <span class="rojis">*</span></label>
                          <input type="text" class="form-control" id="cedular2">
                        </div>
                      
                    </div>
                  
                  <div class="text-right mt-4 mb-3">
                    <div class="error text-center" style="display: none;" >Debes llenar todos los campos que estan marcados como obligatorios</div>
                    
                    <div class="error text-center" id="eroremail" style="display: none;" >Uno de los Email introducidos es incorrecto, por favor verificar y agregarlo correctamente</div>
                    
                    <div class="error text-center" id="eroremail2" style="display: none;" >Uno de los Email de uno de sus hijos es incorrecto, recuerde que debe ser el correo institucional de el colegio. "ejemplo@cumbrescaracas.edu.ve" </div>

                    <i class="fa fa-spinner fa-spin fa-3x fa-fw load"  style="display: none;"></i>
                    <button type="button" class="btn btn-primary" ng-click="insertar()" id="btnenvio">Enviar datos</button>
                     
                     <div class="listo mt-4" style="text-align: left; display: none;">
                       
                        ¡Ya estás registrado! Para ingresar por primera vez y configurar tu perfil debes ingresar en 
                         <a href="https://cumbres.cafe/digital/accesoclientes">https://cumbres.cafe/digital/accesoclientes</a>
                         y utilizar tu cédula de identidad como contraseña inicial. <br>
                         Una vez ingreses, podrás establecer tu nueva contraseña confidencial.<br><br>

                        Luego que tengas tu nueva contraseña, podrás recargar saldo a tu cuenta y disfrutar de la experiencia digital que te ofrece Cumbres Café.
                     </div>

                     <div class="listo2 mt-4" style="text-align: left;font-size: 18px;color: #aa1a18;line-height: 22px;display: none;">
                        No se pudo realizar el registro correctamente, hemos detectado que uno de los email ya esta siendo utilizado anteriormente.
                     </div>

                  </div>

               </div>
           </div>

     </div>

  <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>
    <script src="js/angular.min.js"></script>
  	<script src="js/angular.ng-modules.js"></script>
  	<script src="js/angularregistrocliente12.js"></script>


    <script type="text/javascript">

         $( "#opcionrepres2" ).change(function() {
                
                if ($("#opcionrepres2").val() == "SI") {
                  $("#represent2").show();
                }else{
                   $("#represent2").hide();
                }


          });
      
         $( "#numhijos" ).change(function() {

             var num = $("#numhijos").val();

             if (num == 1) {
              $("#hijo1").show();

              $("#hijo2").hide();
              $("#hijo3").hide();
              $("#hijo4").hide();
              $("#hijo5").hide();
             }

              if (num == 2) {
              $("#hijo1").show();

              $("#hijo2").show();
              $("#hijo3").hide();
              $("#hijo4").hide();
              $("#hijo5").hide();
             }

             if (num == 3) {
              $("#hijo1").show();

              $("#hijo2").show();
              $("#hijo3").show();
              $("#hijo4").hide();
              $("#hijo5").hide();
             }

             if (num == 4) {
              $("#hijo1").show();

              $("#hijo2").show();
              $("#hijo3").show();
              $("#hijo4").show();
              $("#hijo5").hide();
             }


             if (num == 5) {
              $("#hijo1").show();

              $("#hijo2").show();
              $("#hijo3").show();
              $("#hijo4").show();
              $("#hijo5").show();
             }



           
        });

    </script>


</body>
</html>