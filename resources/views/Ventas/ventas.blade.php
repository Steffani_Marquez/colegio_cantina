<!DOCTYPE html>
<html lang="en">
<head>

	<script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MPXZNJP');
    </script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{url('img/logo.png')}}">
    <title>LA CANTINA DIGITAL</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/tienda3.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">

</head>


<style type="text/css">
	#mydiv {
	width: 400px;
    position: absolute;
    z-index: 9;
    background-color: #f1f1f1;
    text-align: center;
    border: 1px solid #d3d3d3;
}

.recarre{
    margin-top: 10px;
    padding: 8px 15px;
    background: #28ad26;
    color: white;
  }

#mydivheader {
  padding: 10px;
    cursor: move;
    z-index: 10;
    background-color:#435756;
    color: #fff;
}

    .naranja{
	background: #ff6000;
	color: white;
	border-radius: 7px;}

	.naranja:hover{
	background: #ff6000;
	color: white;
	border-radius: 7px;}

	.azul{
	background: #0082ca;
	color: white;
	border-radius: 7px;}

	.azul:hover{
	background: #0082ca;
	color: white;
	border-radius: 7px;}

	.verde{
	background: #3daf2c;
	color: white;
	border-radius: 7px;}

		.verde:hover{
	background: #3daf2c;
	color: white;
	border-radius: 7px;}

	.fucia{
	background: #e60895;
	color: white;
	border-radius: 7px;}


	.fucia:hover{
	background: #e60895;
	color: white;
	border-radius: 7px;}

	.rojo{
		background: #b7041f;
		color: white;
		border-radius: 7px;
	}

	.rojo:hover{
		background: #b7041f;
		color: white;
		border-radius: 7px;
	}

	.restricciones{color: white;
    padding: 5px 15px;
    background: #994c4c;}

    .loadmoadl{height: 100%;
    position: absolute;
    background: #f7f7f79c;
    width: 100%;
    text-align: center;
    z-index: 99;}

    .ventro{
    transform: translate(-50%, -50%);
    position: fixed;
    left: 50%;
    top: 50%;
    font-size: 26px;
    color: #2e2e2e;
	}

</style>

  <script type="text/javascript">
        function caculularpaypal(){
            var suma = document.calculator.ans.value=eval(document.calculator.ans.value);
        }

        var abierta = 0;


        function calculadoraabrir(){

            if (abierta == 0) {
               
                abierta = 1;

                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'calculadora',
                'eventAction': 'calculadora-click-abrir',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais' :'VE'
                });

            }else{

                abierta = 0;
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'calculadora',
                'eventAction': 'calculadora-click-cerrar',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais' :'VE'
                });
            }
           $("#mydiv").toggle();
        }

        function cacululardolar(){
          var tasadolar =  $("#tasamanual").val();
          var suma = document.calculator.ans.value=eval(document.calculator.ans.value);
          var total = parseFloat(suma) * parseFloat(tasadolar);


          document.calculator.ans.value=eval(total);
        }


        function dolarmodal(){
           $("#modaldolar").modal();
           var tasado = $("#tasadolartoday").val()
           $("#tasatext").text(tasado);
        }

        function dolarvalor(){
           $("#changemodal").modal();
        }


  </script>


  <div class="loadmoadl">
  	<div class="ventro">
  	<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
  	</div>
  </div>



 <div id="mydiv" style="display: none;" >
      <div class="close" onclick="calculadoraabrir()"><i class="far fa-times-circle"></i></div>
      <div id="mydivheader">CALCULADORA</div>
        <form name="calculator" style="padding: 5px;">
            <input type="textfield" name="ans" id="calculador" value="">
            <br>
            <input type="button" value="1" onClick="document.calculator.ans.value+='1'">
            <input type="button" value="2" onClick="document.calculator.ans.value+='2'">
            <input type="button" value="3" onClick="document.calculator.ans.value+='3'">
            <input type="button" value="+" onClick="document.calculator.ans.value+='+'">
            <br>
            <input type="button" value="4" onClick="document.calculator.ans.value+='4'">
            <input type="button" value="5" onClick="document.calculator.ans.value+='5'">
            <input type="button" value="6" onClick="document.calculator.ans.value+='6'">
            <input type="button" value="-" onClick="document.calculator.ans.value+='-'">
            <br>
            <input type="button" value="7" onClick="document.calculator.ans.value+='7'">
            <input type="button" value="8" onClick="document.calculator.ans.value+='8'">
            <input type="button" value="9" onClick="document.calculator.ans.value+='9'">
            <input type="button" value="*" onClick="document.calculator.ans.value+='*'">
            <br>
            <input type="button" value="0" onClick="document.calculator.ans.value+='0'">
            <input type="button" value="," onClick="document.calculator.ans.value+='.'">
            <input type="button" value="/" onClick="document.calculator.ans.value+='/'">
            <input type="button" value="=" onClick="document.calculator.ans.value=eval(document.calculator.ans.value)">
            <br>
            <input type="reset" value="Borrar">
        </form>
  </div>

    <script>
//Make the DIV element draggagle:
dragElement(document.getElementById("mydiv"));

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    /* if present, the header is where you move the DIV from:*/
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
</script>



<body id="page-top">


	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
		'event': 'app.pageview',
		'pageName': 'tienda',
		'colegio': 'cumbres',
		'pais': 'VE',
		'tipo': 'admin',
		'seccion': 'admin'
		});
	</script>

	
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPXZNJP"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

    <div class="headertienda">
	    <div class="container-fluid">
    	  		<nav class="navbar">
    	  		  <div class="bold"><img src="img/loguisblanco.svg" style="width: 55px;"> LA CANTINA DIGITAL</div>
    	  		  <a class="navbar-brand"><i class="fas fa-calculator cbla" onclick="calculadoraabrir()" style="cursor: pointer;"></i></a>
      			  <a class="navbar-brand" a href="/dashboard"><i class="fas fa-home cbla" style="color: ;cursor: pointer;"></i></a>
      			  <div class="form-inline vcol" >
      			    <i class="fas fa-compress" onclick="go_full_screen()" style="cursor: pointer;"></i>
      			  </div>
              <span class="botonitos" onclick="dolarmodal()" style="margin-left: 15px;"><i class="fas fa-dollar-sign" style="color: white;"></i></span>
    			</nav>
	   	</div>
   	</div>

   	<div class="bodyven"  ng-module="CantinaApp" ng-controller="CantinaController">



              <div class="modal" tabindex="-1" id="modaldolar">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">DOLAR DEL DÍA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>TASA OFICIAL: <span id="tasatext"></span> Bs. </p>
                        <p>TASA MANUAL:  <span><%  auxiliar_arr.dolar %> </span> Bs. </p>
                      </div>
                    </div>
                  </div>
                </div>


        
        <input type="hidden" id="tasadolartoday">
         <input type="hidden" id="codigo_familiar">


   		 <div class="container-fluid mt-3 mb-5" id="recargarsaldo" style="display: none;">


   		 	<div class="btnback">
                <button type="button" class="btn btn-outline-dark" ng-click="volverrecarga()">Volver</button>
            </div>

            <div id="elecciondemetodo" class="mt-3">
                 
                    <p style="font-size: 20px;">Elige el modo de la recarga</p>
                     
                    <div class="mt-3">
                         <button type="button" class="btn btn-outline-primary bntcer doo1" ng-click="cambiomodo('dolares')">Dolares</button>
                         <button type="button" class="btn btn-outline-primary bntcer boo1" ng-click="cambiomodo('bolivares')">Bolivares</button>
                    </div>
               
            </div>


               <div id="recarga1" style="display: none;">

                 <h3 class="mb-3 mt-2">Seleccione la cantidad a recargar</h3>
                   <p>Por favor, coloca el monto de la cantidad que va a recargar</p>
              
                    <div class="conuser mt-2">
                        <div class="row">
                           <div class="col-sm-12  col-lg-4 userselc text-center" ng-repeat="(key, combi) in cliente2_arr">
                                <div style="width: 100%;"><i class="fas fa-user-circle" style="color: #1c4587;font-size: 25px;"></i></div>
                                <%  combi.nombre %> -
                                <span ng-if="combi.tipo_cliente_id == 1">HIJO</span>
                                <span ng-if="combi.tipo_cliente_id == 2">REPRESENTANTE</span>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text palbs" >DOLARES A RECARGAR</span>
                                  </div>
                                  <input type="number" value="0" class="number11 form-control text-left" id="input<% combi.id %>" ng-keyup="inser(combi.id)" style="text-align: left !important;" >
                                </div>

                           </div>
                        </div>
                    </div>


                    <div class="totaldol"> <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon" id="totalmontodolares">0</span> </p> </div>
                    <div class="totalbs"> <p class="mb-1 mont"><span class="bold2"> Total Bs:</span> <span class="totalmonbs" id="totalmontobolivares">0</span> </p> </div>

            
                
                      <h3 class="mb-3 mt-2">Por favor, introduce un número de celular de contacto</h3>
                        <input type="number" placeholder="Teléfono celular de contacto" class="inputxe" id="teleinfo" ng-keyup="unmtele()">
                       
                        <div class="error1 mt-3 mb-3" id="telerror" style="display: none;">
                            <span class="wrroa">Debes ingresar el número celular para poder contactarte por alguna duda</span>  
                        </div>

                        <div class="mt-3 mb-3">
                            <span >Para poder visualizar los métodos de pagos, debes introducir el número celular de contacto</span>  
                        </div>

               </div>

               <div id="armasonpagos">

              <div  id="pagobolivares" style="display: none;">

                <h3 class="mb-3 mt-3">Elige tu método de pago</h3>
                    
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsepagomovil" aria-expanded="false" aria-controls="collapsepagomovil">
                        PAGO MOVIL
                      </button>
                    </h5>
                  </div>
                  <div id="collapsepagomovil" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                          <p>
                            Realiza el pago del monto total de tu pedido a través de Pago Móvil con los siguientes datos: 
                               <br>
                              -<b>Cel: 0412 2369486</b><br> 
                              -<b>RIF: J-50089373-6</b> <br>
                              -<b>Banco Banplus</b> <br> 
                              - A nombre de -<b>DAILY SERVING COMPANY C.A.</b>  <br>

                              Al realizar el pago indícanos el número celular del titular de la cuenta para poder validar el pago en nuestro banco.
                          </p>
                          <div class="totaldol"> <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span> </p>    </div>
                          <div class="totalbs"> <p class="mb-1 mont"><span class="bold2">Total Bs:</span> <span class="totalmonbs">0</span> </p> </div>
                          
                          <div>
                            <input type="number" placeholder="Número celular del titular"  class="inputxe" id="refepagomovil">
                          </div>

                        <div class="mt-3">

                            <div class="error1 mb-3" id="movilerror" style="display: none;">
                                <span class="wrroa">Debes ingresar el número celular del titular de la cuenta del pago realizado</span>  
                            </div>

                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btn-primary btnfinalpago" ng-click="recargar('pago movil')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsepuntoventa" aria-expanded="false" aria-controls="collapsepuntoventa">
                        PUNTO DE VENTA
                      </button>
                    </h5>
                  </div>
                  <div id="collapsepuntoventa" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                          <p>
                            Este pago será verificado luego de que la persona pase por el punto de venta el monto 
                          </p>
                          <div class="totaldol"> <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span> </p>    </div>
                          <div class="totalbs"> <p class="mb-1 mont"><span class="bold2">Total Bs:</span> <span class="totalmonbs">0</span> </p> </div>
                          
                        <div class="mt-3">

                            <div class="error1 mb-3" id="movilerror" style="display: none;">
                                <span class="wrroa">Debes ingresar el número celular del titular de la cuenta del pago realizado</span>  
                            </div>

                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btn-primary btnfinalpago" ng-click="recargar('punto de venta')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>

              </div>

              <div  id="pagodolares"  style="display: none;">

               <div class="bodydd mt-4">

               <div class="bodydd mt-4">
                 <h3 class="mb-3">Elige tu método de pago</h3>
                 <div class="conuser mt-2">
                    <div class="mt-3" >
              
               <div id="accordion">
                
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        ZELLE
                      </button>
                    </h5>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">

                      <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span>  </p>

                          <p class="mb-0">- Envía tu Zelle a la dirección: <b>mw2@mways.net</b> que pertenece a <b>(MWbpmconsulting LLC)</b></p>
                          <p class="mb-0">- Indispensable que coloques <b>$Cumbres</b> (sin espacios) en la descripción de la transacción.</p>
                          <p class="mb-0">- Indícanos a continuación el nombre completo del titular de la cuenta</p>

                        <div class="mt-2">
                          <input type="text" placeholder="Titular de la cuenta" class="inputxe" id="refezelle">
                        </div>
                        <div class="mt-3">
                            <div class="error1 mb-3" id="zelleerror" style="display: none;">
                                <span class="wrroa">Debes ingresar el titular de la cuenta del pago realizado </span>  
                            </div>
                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btn-primary btnfinalpago" ng-click="recargar('zelle')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>

               


                <div class="card">
                    <div class="card-header" id="headingcash">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsecash" aria-expanded="false" aria-controls="collapsepagomovil">
                          CASH
                        </button>
                      </h5>
                    </div>
                    <div id="collapsecash" class="collapse" aria-labelledby="headingcash" data-parent="#accordion">
                        <div class="card-body">
                            <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span> </p>
                            
                            <div>
                              <input type="text" placeholder="Indicanos si tienes algun comentario"  class="inputxe" id="refeefect">
                              
                            </div>

                         

                          <div class="mt-3">
                             <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                            <div class="btn-primary btnfinalpago" ng-click="recargar('efectivo')">RECARGAR SALDO</div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingPaypal">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed btn-paypal" data-toggle="collapse" data-target="#collapsePaypal" aria-expanded="false" aria-controls="collapsePaypal">
                        TARJETA INTERNACIONAL O PAYPAL
                      </button>
                    </h5>
                  </div>
                  <div id="collapsePaypal" class="collapse" aria-labelledby="headingPaypal" data-parent="#accordion">
                      <div class="card-body">
                          <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span>  </p>
                    
                    <input type="hidden" id="totalpaypal" value="">
                    <div id="paypal-button-container" style="width: max-content"></div>
                          <div><input type="hidden" placeholder="Referencia del pago" id="refepaypal"></div>
                        <div class="mt-3">
                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btn-primary btnfinalpago btnfinalpagopaypal" style="display: none;" ng-click="recargar('paypal')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>


              </div>
            </div>
                 </div>
               </div>
             </div>

           
              </div>

            </div>


     </div>



   			<div class="modal" tabindex="-1" id="modal1">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-body">
			        <div id="textomodal" style="font-size: 25px;text-align: center;"></div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
			      </div>
			    </div>
			  </div>
			</div>

		    <div class="modal" tabindex="-1" id="modal2">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-body">
			        <div style="font-size: 25px;">
			        	El pedido fue realizado con exito 
			        </div>
			        <div class="mt-3" ng-if="clientecomprado_arr != 0">
			          <h5>	Cliente: <span> <% clientecomprado_arr.nombre %> <% clientecomprado_arr.apellido_alumno %>  </span></h5>
			          <p class="mb-0"><span class="bold2">Saldo disponible en dolares: </span> <% clientecomprado_arr.abonodolar %> $ </p>
			     	  <p class="mb-0"><span class="bold2">Saldo disponible en bolivares: </span> <% clientecomprado_arr.abonobolivar %> bs (tasa actual: <span class="tasis"></span>bs = <% cliente_arr.totalcontasa %> $ )</p>
			     	  <p class="mb-0"><span class="bold2">Total: </span> <b> <% clientecomprado_arr.totaldola %> $ </b> </p>
			           
			        </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-primary" ng-click="refres()">LISTO</button>
			      </div>
			    </div>
			  </div>
			</div>

   	    <input type="hidden" id="codigo_personal" name="codigo_personal" value="{{$codidoinvitado}}">

   	    <input type="hidden" id="reservaelegida" name="reservaelegida" value="0">

   	    <input type="hidden" id="codigo_personal_antiguo" name="codigo_personal_antiguo" value="{{$codidoinvitado}}">

   		<div class="container-fluid" id="conttodo">
	   		<div class="row">
	   		   <div class="col-7 mt-3">
	   		      	<div class="headercont1">
		   		   		<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
						  </div>
						  <input type="text" class="form-control" placeholder="Buscar" ng-model="buscador" ng-keypress="buscarproducto('Todos')">
						</div>
				    </div>
				    <div class="headercont1">
				       <div class="btnilera">
				            <div class="btnbotn btntTodos activecar2 mt-2" onclick="cambiar2('Todos')" ng-click="buscarproducto('Todos')">Todos</div>
				         	@foreach ($categorias as $cat) 
		                    	<div class="btnbotn btnt{{$cat->nombre}} mt-2" onclick="cambiar2('{{$cat->nombre}}')" ng-click="buscarproducto({{$cat->id}})">{{$cat->nombre}}</div>
	                        @endforeach
                       </div>
				    </div>

				    <div class="headercont1productos mt-3">
				       <div class="row">


				       		<div class="col-3 mb-3" ng-repeat="(key, combi) in promos_arr|filter:buscador">
						       <div class="btnproductos">
			                      <img src="<%  combi.imagen  %>">
			                        <div class="nombtet"><% combi.nombre  %>  </div>
			                        <div class="contcosto"> $<% combi.costo %></div>

			                        <div class="contenbtns" ng-if="combi.agotado == 0"> 
			                          
	                           	        
	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 20"> 
			                           		<div class="btncarris naranja" ng-click='agregaralcarr_carpromo(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 21"> 
			                           		<div class="btncarris azul" ng-click='agregaralcarr_carpromo(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 22"> 
			                           		<div class="btncarris verde" ng-click='agregaralcarr_carpromo(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 23"> 
			                           		<div class="btncarris fucia" ng-click='agregaralcarr_carpromo(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 24"> 
			                           		<div class="btncarris" ng-click='agregaralcarr_carpromo(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 25"> 
			                           		<div class="btncarris " ng-click='agregaralcarr_carpromo(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 26"> 
			                           		<div class="btncarris rojo" ng-click='agregaralcarr_carpromo(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	    </div>
	                           	    <div class="contenbtns" ng-if="combi.agotado == 1"> 
                                       <span class="agotado">AGOTADO</span> 
	                           	    </div>
		                       </div>
	                        </div>


				           <div class="col-3 mb-3" ng-repeat="(key, combi) in productos_arr|filter:buscador">
						       <div class="btnproductos">
			                      <img src="<%  combi.imagen  %>">
			                        <div class="constock">Stock: <% combi.cantisto %> </div>
			                        <div class="nombtet"><% combi.nombre  %> </div>
			                        <div class="contcosto"> $<% combi.costo %></div>

			                        <div class="contenbtns" ng-if="combi.agotado == 0"> 

			                        	<div class="contenbtns" ng-if="combi.id_categoria == 20"> 
			                           		<div class="btncarris naranja" ng-click='agregaralcarr_carproducto(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 21"> 
			                           		<div class="btncarris azul" ng-click='agregaralcarr_carproducto(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 22"> 
			                           		<div class="btncarris verde" ng-click='agregaralcarr_carproducto(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 23"> 
			                           		<div class="btncarris fucia" ng-click='agregaralcarr_carproducto(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 24"> 
			                           		<div class="btncarris" ng-click='agregaralcarr_carproducto(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 25"> 
			                           		<div class="btncarris " ng-click='agregaralcarr_carproducto(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	        <div class="contenbtns" ng-if="combi.id_categoria == 26"> 
			                           		<div class="btncarris rojo" ng-click='agregaralcarr_carproducto(combi.id)'><i class="fas fa-cart-plus xshiden"></i> Agregar</div>
	                           	        </div>

	                           	    </div>
	                           	    <div class="contenbtns" ng-if="combi.agotado == 1"> 
                                       <span class="agotado">AGOTADO</span>  
	                           	    </div>
		                       </div>
	                        </div>


                       </div>
				    </div>

	   		   </div>
	   		   <div class="col-5">

	   		        <div class="headercont1contw">
				       <div class="btnilera">
				           <div class="btnbotn2 btnheadercliente activecar" onclick="cambio('headercliente')"><i class="fas fa-user-circle"></i> Cliente</div>
	                       <div class="btnbotn2  btnheadercarrito" onclick="cambio('headercarrito')"><i class="fas fa-store"></i> Carrito</div>
	                       
	                       <div class="btnbotn2 btnheaderpago" onclick="cambio('headerpago')"><i class="far fa-credit-card"></i> Pago</div>
                       </div>
				    </div>

            <div class="recarre" style="display: none;">Recarga realizada satisfactoriamente</div>

				    <div class="contcliente mt-3" id="topcliente" style="display: none;">
					     <p class="mb-0"><span class="bold2">Cliente:</span> <% cliente_arr.nombre %>  <% cliente_arr.apellido_alumno %></p>
     					 <p class="mb-0"><span class="bold2">Saldo disponible en dolares: </span> <% cliente_arr.abonodolar %> $ </p>
     					 <p class="mb-0"><span class="bold2">Saldo disponible en bolivares: </span> <% cliente_arr.abonobolivar %> bs (tasa actual: <span class="tasis"></span>bs = <% cliente_arr.totalcontasa %> $ )</p>
     					 
     					 <p class="mb-0"><span class="bold2">Reservas pendientes: </span> <% cliente_arr.numpendientesreservas %> </p>
     					 <p class="mb-1"><span class="bold2">Limite diario:</span>  <% cliente_arr.limitediario %> $</p>
     					 <button type="button" class="btn btn-outline-primary mt-2"  ng-click="recargarsaldo()" >Recargar saldo</button>
     					 <hr>
     					 <div class="btncli" ng-click="back()">Nuevo cliente</div>
				    </div>

				    <input type="hidden" id="tasisactual">


				    <div class="contenshe mt-3" id="headercarrito" style="display: none;">
				    	 	
				    	 	<div class="carritobar mt-5" id="emtycarr">
				    	    	<div><img src="img/emtycard.svg" class="carritoenty"></div>
					    		<div class="mt-2">
					    			Añade el primer producto al carrito
								</div>
				    		</div>

				    		<div class="carritobar mt-5" id="carlleno">
				    	    	<div class="cajitadecarro" ng-repeat="(key, car) in carrito_arr">
				    	    		<div class="row">
				    	    			<div class="col-1 xshiden" style="padding: 0px; margin: 0px;">
				    	    			  <span><img style="max-width: 100%;"  src="<% car.imagenpro %>">  </span> 	
				    	    			</div>
				    	    			<div class="col-3" style="padding-top: 2%;font-weight: 600;font-size: 18px;color: #000;line-height: 16px;padding-left: 0px;padding-right: 0px;margin: 0px;">
				    	    			  <span><% car.nombrepro %></span>
				    	    			</div>

				    	    			<div class="col-4 xsmayortam" style="padding-top: 1%;font-weight: 600;font-size: 20px;color: #09adaa">
				    	    			 	<div class="text-center" style="display: inline-block;"> 
						                        <span ng-click="restarppv(car.idpro)" class="rem">-</span>
						                        <span class="inputtext" id="inputvalor<% car.idpro %>" ><% car.cantidad %></span>
						                        <span ng-click="sumarppv(car.idpro)" class="rem">+</span>
				                           	</div>
				    	    			</div>

				    	    			<div class="col-2 xshiden" style="padding-top: 2%;font-weight: 600;font-size: 14px;color: #b3b3b3">
				    	    			  <span>$ <% car.costopro %></span>
				    	    			</div>

				    	    			<div class="col-2 xsmayortam2" style="padding-top: 1%;font-weight: 600;font-size: 19px;color: #000">
				    	    			  <span>$ <% car.totalindi %></span>
				    	    			</div>
				    	    			<div ng-if="car.agotado == 1">
				    	    			   <div ng-if="car.tipo == 'Promo'">
					    	    			   	<span class="stocko">Esta Promo no tiene stock suficiente.  
						    	    			   	<span ng-repeat="(key, po) in car.productpromo"> 
						    	    			   	    <span ng-if="po.mostrarsi == 1">
						    	    			   			<span style="font-weight: 700">  <span><% po.producto.nombre %>: <% po.canti %> </span> / </span>
						    	    			   		</span>
						    	    			   	</span>
					    	    			   	</span>	
				    	    			   </div>
				    	    			   <div ng-if="car.tipo == 'Producto'">
				    	    			   	<span class="stocko">Este producto no tiene stock suficiente. <span style="font-weight: 700"> stock disponible: <% car.cantiagot %></span> </span>	
				    	    			   </div>
				    	    			</div>
				    	    		</div>
				    	    		<hr class="rayisbajo">
				    	    	</div>
				    		</div>
				    	<div class="subtotalcont mt-5">
				    		<div class="row">
				    			<div class="col-9 text-left">
				    				SUB. TOTAL
				    			</div>
				    			<div class="col-3 text-right">
				    				$<% carrito_arr[0].totaltotal %>
				    				<span id="cerosub">0,00</span>
				    			</div>
				    		</div>
				    		<hr class="hrpago">
				    	</div>

				    	<div class="subtotalcont mt-3">
				    		<div class="row">
				    			<div class="col-9 text-left">
				    			 	<span class="azulin">TOTAL</span>	
				    			</div>
				    			<div class="col-3 text-right">
				    				<span class="azulin"> 
				    				$<% carrito_arr[0].totaltotal %> 
				    				<span id="cerototal">0,00</span>
				    				</span>
				    			</div>
				    		</div>
				    	</div>

				    	<div class="botonbtncont text-right">
				    		<div class="btn-secondary btnbotncar btnnetxcarris" ng-click="vaciar()" style="cursor: pointer;">Vaciar</div>
	                        <div class="btn-primary btnbotncar btnnetxpago" style="cursor: pointer;" onclick="cambio('headerpago')">PAGAR</div>
				    	</div>

				    </div>

				    <div class="contenshe mt-3" id="headercliente" >
				    	<div class="codigobar mt-3" id="codigobar">
				    	        <div class="mb-2 text-center">
				    	        	<img src="img/loguis.svg" style="width: 175px;">
				    	        </div>
				    	    	<div style="font-size: 20px;">
				    	    	Por favor, leer el código QR del cliente o ingresarlo manualmente
				    	    	</div>
					    		<div>
					    			<div class="form-group mt-2">
								     <div class="input-group mb-3">
										  <input type="text" class="form-control borderform" id="clientecodigo" autocomplete="off">
										  <div class="input-group-prepend" style="cursor: pointer;">
										    <span class="input-group-text" id="basic-addon1" onclick="limpiar()" style="background: #00294d;color: white;">
										    <i class="fas fa-broom"></i></span>
										  </div>
										</div>
								  	</div>
								</div>
								<div class="erri" style="display: none;">Usuario no encontrado</div>
								<div class="text-center">
									<i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="font-size: 35px;display: none;"></i>
								</div>
								<button type="button" class="btn btn-primary botonis" ng-click='buscarcliente()' style="width: 100%;">Ingresar nuevo cliente</button>
				    	</div>
				    	<div id="cliente_vista" style="display: none;">

				    			<div class="row">
				    				<div class="col-4">
				    				    <div class="contavanta">
				    						<i class="fas fa-user-alt" style="font-size: 65px;"></i>
				    					</div>
				    				</div>
				    				<div class="col-8">
				    				 	<p class="mb-1"><span class="bold2">Nombre:</span>  <% cliente_arr.nombre %> <% cliente_arr.apellido_alumno %></p>
				    			    	<p class="mb-1"><span class="bold2">Curso:</span> <% cliente_arr.grado %>   </p>
				    					<p class="mb-1"><span class="bold2">Nro. compras:</span>  <% cliente_arr.numpedidos %> - Total: <% cliente_arr.total %> $</p>
                                       
                                        

				    				</div>
				    				   
				    			</div>
				    			        <label class="mt-3">Restricciones alimenticias</label>
				    					<div class=" restricciones" ng-if="cliente_arr.restriccion_alimenticia != null ">
                                        	<% cliente_arr.restriccion_alimenticia %>
                                        </div>
                                        <div ng-if="cliente_arr.restriccion_alimenticia == null ">
                                        	- No posee restricciones alimenticias
                                        </div>
				    			
				    			<div class="row">
				    		
						    			<div class="col-6 mb-2" ng-repeat="(key, combi) in cliente_arr.reservaspen">
                                            <div class="cajisreser" id="reser<% combi.id %>" ng-click="elegirreserva(combi.id)">
							    			  
							    			    <p class="mb-0">Reservada para: <% combi.fecharees  %> </p>
							    			    <p class="mb-0">Horario: <% combi.horario  %> </p>

								    			<div ng-repeat="(key, car) in combi.carrito">
								    			      <span ng-if="car.tipo == 'Producto'">
								    			        <% car.cantidad  %>	- <% car.producto.nombre  %>
								    			       </span>
								    			       <span ng-if="car.tipo == 'Promo'">
								    			       	<% car.cantidad  %> - <% car.promocion.nombre  %>
								    			       </span>
								    			</div>
							    			</div>
						    			</div>
					    			
				    			</div>
				    			<hr>
				    			<div id="ultimacompra">
				    				<p class="mb-0 bold2 mb-2">Ultima compra realizada</p>
				    				<p class="mb-0">Monto total: <% cliente_arr.pedido.monto %> $ </p>
				    				<p class="mb-2">Fecha de compra: <% cliente_arr.pedido.fecha %> </p>
						    			
						    			<table class="table table-striped">
												  <thead>
												    <tr>
												      <th class="text-center">Producto</th>
												      <th class="text-center">Cantidad</th>
												      <th class="text-center">Monto</th>
												    </tr>
												  </thead>
												  <tbody>
												   <tr ng-repeat="(key, cli) in cliente_arr.productovendido">
												      <th class="text-center">
	                                                      <span ng-if="cli.tipo == 'Promo'"><%  cli.promocion.nombre  %></span>
													      <span ng-if="cli.tipo == 'Producto'"><%  cli.productos.nombre  %></span>
												      </th>
												      <td class="text-center"><%  cli.cantidad  %></td>
												      <td class="text-center">
												      	  <span ng-if="cli.tipo == 'Promo'"><%  cli.promocion.costo  %> $</span>
													      <span ng-if="cli.tipo == 'Producto'"><%  cli.productos.costo  %> $</span>
												      </td>
												    </tr>
												  </tbody>
										</table>

				    			</div>
    			    	</div>
				    </div>

				    <div class="contenshe mt-3" id="headerpago" style="display: none;">
				    	
				    	 <div id="accordion">
							  
                <div class="card" id="abonopago" style="display: none;">
							    <div class="card-header" id="headingOne">
							      <h5 class="mb-0">
							        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							         ABONADO
							        </button>
							      </h5>
							    </div>

							    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							      <div class="card-body">
							        <p class="mb-1"><span class="bold2">Nombre:</span> <% cliente_arr.nombre %> <% cliente_arr.apellido_alumno %></p>
							        <p class="mb-0"><span class="bold2">Saldo disponible en dolares: </span> <% cliente_arr.abono_actual %> $ </p>
			     					 <p class="mb-0"><span class="bold2">Saldo disponible en bolivares: </span> <% cliente_arr.abono_bs %> bs (tasa actual: <span class="tasis"></span>bs = <% cliente_arr.totalcontasa %> $ )</p>
			     					 
							        <hr>
							         <p class="mb-1"><span class="bold2">Sub. total.:</span> <% carrito_arr[0].totaltotal %> $ </p>
							         <p class="mb-1"><span class="bold2">Total:</span> <% carrito_arr[0].totaltotal %> $</p>
							         <div class="mt-3">
							            <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
							         	<div class="btn-primary btnfinalpago paguis" ng-click="finalizarpago('abonado')">FINALIZAR PAGO</div>
							         </div>
							      </div>
							    </div>
							  </div>
							 
                <div class="card sincodigo">
							    <div class="card-header" id="headingTwo">
							      <h5 class="mb-0">
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          CASH EFECTIVO (CAJA FISCAL)
							        </button>
							      </h5>
							    </div>
							    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
								    
								    <div class="card-body">

								        <div class="row">
								            <div class="col-6">
									        	<label for="exampleInputEmail1">Cash cliente</label>
												<div class="input-group">
														<div class="input-group-prepend">
													    <span class="input-group-text" id="basic-addon1">$</span>
													  </div>
												    <input type="number" class="form-control" id="cash_cliente" name="cash_cliente">
												</div>
											</div>
                                            
                                            <div class="col-6">
                                             	<label for="exampleInputEmail1">Vuelto cliente</label>
												<div class="input-group">
														<div class="input-group-prepend">
													    <span class="input-group-text" id="basic-addon1">$</span>
													  </div>
												    <input type="number" class="form-control" id="vuelto_cliente" name="vuelto_cliente">
												</div>
											</div>

	
								        </div>
                                         <hr>
								        <p class="mb-1"><span class="bold2">Sub. total.:</span> <% carrito_arr[0].totaltotal %> $ </p>
							            <p class="mb-1"><span class="bold2">Total:</span> <% carrito_arr[0].totaltotal %> $</p>
									    <div class="mt-3">
									       <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
									      	<div class="btn-primary btnfinalpago paguis" ng-click="finalizarpago('efectivo')">ENVIAR A CAJA FISCAL</div>
									    </div>
								        
								    </div>

							    </div>
							  </div>
							  
							  <div class="card sincodigo">
							    <div class="card-header" id="headingThree">
							      <h5 class="mb-0">
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							          ZELLE (CAJA FISCAL)
							        </button>
							      </h5>
							    </div>
							    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							      	<div class="card-body">

								        <p class="mb-1"><span class="bold2">Sub. total.:</span> <% carrito_arr[0].totaltotal %> $ </p>
							            <p class="mb-1"><span class="bold2">Total:</span> <% carrito_arr[0].totaltotal %> $</p>
								        <div class="mt-3">
								           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
								         	<div class="btn-primary btnfinalpago paguis" ng-click="finalizarpago('zelle')">ENVIAR A CAJA FISCAL</div>
								        </div>
							      	</div>
							    </div>
							  </div>

							   <div class="card sincodigo">
							    <div class="card-header" id="headingThree">
							      <h5 class="mb-0">
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsepunto" aria-expanded="false" aria-controls="collapsepunto">
							          PUNTO DE VENTA (CAJA FISCAL)
							        </button>
							      </h5>
							    </div>
							    <div id="collapsepunto" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							      	<div class="card-body">

								        <p class="mb-1"><span class="bold2">Sub. total.:</span> <% carrito_arr[0].totaltotal %> $ </p>
							            <p class="mb-1"><span class="bold2">Total:</span> <% carrito_arr[0].totaltotal %> $</p>
								        <div class="mt-3">
								           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
								         	<div class="btn-primary btnfinalpago paguis" ng-click="finalizarpago('punto de venta')">ENVIAR A CAJA FISCAL</div>
								        </div>
							      	</div>
							    </div>
							  </div>


							  <div class="card sincodigo">
							    <div class="card-header" id="headingThree">
							      <h5 class="mb-0">
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsepaypal" aria-expanded="false" aria-controls="collapsepaypal">
							          PAYPAL (CAJA FISCAL)
							        </button>
							      </h5>
							    </div>
							    <div id="collapsepaypal" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							      	<div class="card-body">

								        <p class="mb-1"><span class="bold2">Sub. total.:</span> <% carrito_arr[0].totaltotal %> $ </p>
							            <p class="mb-1"><span class="bold2">Total:</span> <% carrito_arr[0].totaltotal %> $</p>
								        <div class="mt-3">
								           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
								         	<div class="btn-primary btnfinalpago paguis" ng-click="finalizarpago('paypal')">ENVIAR A CAJA FISCAL</div>
								        </div>
							      	</div>
							    </div>
							  </div>



							  <div class="card sincodigo">
							    <div class="card-header" id="headingmovil">
							      <h5 class="mb-0">
							        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsemovil" aria-expanded="false" aria-controls="collapsemovil">
							          PAGO MOVIL (CAJA FISCAL)
							        </button>
							      </h5>
							    </div>
							    <div id="collapsemovil" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							      	<div class="card-body">

								        <p class="mb-1"><span class="bold2">Sub. total.:</span> <% carrito_arr[0].totaltotal %> $ </p>
							            <p class="mb-1"><span class="bold2">Total:</span> <% carrito_arr[0].totaltotal %> $</p>
								        <div class="mt-3">
								           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
								         	<div class="btn-primary btnfinalpago paguis" ng-click="finalizarpago('pago movil')">ENVIAR A CAJA FISCAL</div>
								        </div>
							      	</div>
							    </div>
							  </div>



							</div>
				    </div>

	   		   </div>
	   		</div>
   		</div>
   	</div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>
    <script src="js/angular.min.js"></script>
  	<script src="js/angular.ng-modules.js"></script>
  	<script src="js/angularventas19.js"></script>


    <script type="text/javascript"> 

       function dolarmodal(){

           $("#modaldolar").modal();
           var tasado = $("#tasisactual").val()
           $("#tasatext").text(tasado);
        }

      tasa = 0;
      $.ajax({
            url: 'https://s3.amazonaws.com/dolartoday/data.json',
            type: 'GET',
            crossDomain: true,
            dataType: 'json',
      }).done(function (respuesta) {
            tasa = respuesta.USD.promedio_real;
            console.log(tasa);
            $(".tasis").text(tasa);
            $("#tasisactual").val(tasa);
      })
        .fail(function (jqXHR, textStatus) {
            tasa = textStatus;
      }); 


    	function limpiar(){
           $("#clientecodigo").val("");
    	}

      var tam = $( window ).height() - 180;
      $(".headercont1productos").css("height",tam);

    	
   


    function cambio(id) {

    	$(".contenshe").hide();
     	$("#"+id).show();
     	$(".btnbotn2").removeClass("activecar");
     	$(".btn"+id).addClass("activecar");

    }

    function cambiar2(id){

    	window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
		'event': 'GenericGAEvent',
		'eventCategory': 'navegaciontienda',
		'eventAction': id+"-"+"click",
		'tipo': 'admin',
		'colegio': 'cumbres',
		'pais': 'VE'
		});

    	$(".btnbotn").removeClass("activecar2");
    	$(".btnt"+id).addClass("activecar2");

    }


    var pantallafull = 0;


    function go_full_screen(){

    	if (pantallafull == 0) {
		  var elem = document.documentElement;
		  if (elem.requestFullscreen) {
		    elem.requestFullscreen();
		  } else if (elem.msRequestFullscreen) {
		    elem.msRequestFullscreen();
		  } else if (elem.mozRequestFullScreen) {
		    elem.mozRequestFullScreen();
		  } else if (elem.webkitRequestFullscreen) {
		    elem.webkitRequestFullscreen();
		  }

		  pantallafull = 1;
		 var tam = $( window ).height() - 50;
    	 $(".headercont1productos").css("height",tam);

		}else{

			if(document.mozCancelFullScreen) {
			    document.mozCancelFullScreen();
			  }
			  //Google Chrome
			  else if(document.webkitCancelFullScreen) {
			    document.webkitCancelFullScreen();
			  }
			  //Otro
			  else if(document.cancelFullScreen) { 
			    document.cancelFullScreen(); 
			  }

			  pantallafull = 0;

			  var tam = $( window ).height() - 330;
    	 	 $(".headercont1productos").css("height",tam);
		}
	 
	}
	
	$(window).on("load",function(){$('.loadmoadl').hide();});

    </script>


</body>
</html>