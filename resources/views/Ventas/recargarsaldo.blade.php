
<!DOCTYPE html>
<html lang="en">
<head>

    <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MPXZNJP');
    </script>

    

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->

    <link rel="shortcut icon" href="{{url('img/logo.png')}}">
    <title>LA CANTINA DIGITAL</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/recargarsaldo.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">




</head>

<style type="text/css">

  .tip2 .btn{width: 100% !important;}

  th{text-align: center;}
  td{text-align: center;}
  .modal {
    background: white;
  }
  .contre{background: #f5f5f5;
    padding: 4px 10px;}
  .mt-3.temp.text-center{user-select: none;
    font-weight: 700;
    color: #00294d;
    cursor: pointer;
    font-size: 18px;}
  .mt-3.temp.text-center:hover{color: #00294d ;
    cursor: pointer;text-decoration: underline;
    font-size: 18px;}
  .links{user-select: none;
    font-weight: 700;
    color: #00294d;
    cursor: pointer;
    font-size: 18px;}
    .links:hover{color: #00294d ;
    cursor: pointer;text-decoration: underline;
    font-size: 18px;}
    .contform{
      border: solid 2px #ededed;
    padding: 27px 40px;
    margin-bottom: 30px;
    border-radius: 10px;
  }
  .actualisi{background: #4cc77e;
    color: white;
    font-size: 18px;
    padding: 5px 15px;
    border-radius: 6px;
    margin-bottom: 10px;}
    .marcado{background: #003e74;
    color: white !important;}
</style>






   
   <body class="fondis1">

      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPXZNJP"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

      <div ng-module="CantinaApp" ng-controller="CantinaController">


           <input type="hidden" id="tasadolartoday">


           <div class="modal" tabindex="-1" id="montocero">
                <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-body">
                          El monto a recargar no puede ser menor o igual a cero (0 $)
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>
                      </div>
                    </div>
                </div>
          </div>



            <div class="modal" tabindex="-1" id="modallisto">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                     <div style="text-align: center;color: #46c07f;font-size: 26px;">
                       <p class="mensajemodallisto">Su solicitud ha sido recibida, estaremos revisando la transacción para confirmar el pago</p>
                     </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="volver1()">OK</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal" tabindex="-1" id="modalerror">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-body">
                     <div style="text-align: center;color: #aa1a18;font-size: 26px;">
                       <p class="mensajemodalerror"></p>
                     </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="volver1()">OK</button>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="modal" tabindex="-1" id="modalqr">
              <div class="modal-dialog">
                <div class="modal-content" style="background: transparent;text-align: center;border: none;">
                  <input  type="text" id="codigoqr" style="width:80%;display:none;"/><br/>
                  <div id="qrcode" style="width:150px; height:150px; margin-top:15px;margin: 0px auto"></div>
                </div>
              </div>
            </div>


            <div class="modal" tabindex="-1" id="modaldetalle">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Detalle de pedido</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="ultimacompra">
                            
                            <p class="mb-0"><b> Cliente:</b> <% detallepedido.cliente.nombre %> </p>
                            <p class="mb-0"><b>Monto total:</b> <% detallepedido.monto %> $ </p>
                            <p class="mb-3"><b>Fecha de compra:</b> <% detallepedido.fechis %> </p>
                              
                              <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th class="text-center">Producto</th>
                                      <th class="text-center">Cantidad</th>
                                      <th class="text-center">Monto</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                   <tr ng-repeat="(key, cli) in detallepedido.productos">
                                      <th class="text-center">
                                        <span ng-if="cli.tipo == 'Promo'">  <%   cli.promocion.nombre  %></span>
                                        <span ng-if="cli.tipo == 'Producto'"><%  cli.productos.nombre  %></span>
                                      </th>
                                      <td class="text-center"><%  cli.cantidad  %></td>
                                      <td class="text-center">
                                          <span ng-if="cli.tipo == 'Promo'"><%  cli.promocion.costo  %> $</span>
                                        <span ng-if="cli.tipo == 'Producto'"><%  cli.productos.costo  %> $</span>
                                      </td>
                                    </tr>
                                  </tbody>
                            </table>

                          </div>
                      </div>
                    </div>
                  </div>
                </div>




      <input type="hidden" name="codigo_familiar" id="codigo_familiar">

         <div class="contenshe mt-3 container" id="headercliente">

              <div class="codigobar seccionh" id="primeravez" style="display: none;">
                   
                      <div class="mb-3 temp">Por ser la primera vez que ingresa a nuestro sistema, por seguridad debe elegir una nueva contraseña, la cual deberá recordar, ya que será la que usará a partir de ahora</div>
                              
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control inpute" placeholder="Nueva contraseña" id="clave1" name="clave1">
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control inpute" placeholder="Repita la nueva contraseña" name="clave2" id="clave2">
                    </div>

                      <div id="error2" class="error" style="display: none;">Las contraseñas deben ser iguales</div>
                      <div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i></div>
                      <button type="button" class="btn btn-outline-primary btningre bntingre2" ng-click='nuevaclave()' style="width: 100%;">Guardar nueva contraseña</button>
              </div>

                
                <div class="codigobar seccionh" id="codigobar">
                      
                      <div class="text-center mb-4">
                        <img src="img/loguis.svg" style="width: 80%;">
                      </div>
                      
                      <div class="mb-3 temp">Por favor, introduzca su email o c.i y contraseña</div>
                              
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-circle"></i></span>
                        </div>
                        <input type="text" class="form-control inpute" placeholder="Email o c.i" id="email" name="email">
                      </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control inpute" placeholder="Contraseña" name="password" id="password">
                    </div>



                      <div id="error" class="error" style="display: none;">Usuario no encontrado</div>
                      <div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i></div>
                          <button type="button" class="btn btn-primary btningre bntingre" ng-click='buscarcliente()' style="width: 100%;">INGRESAR</button>
                      
                      <a href="/digital/onlivarpass" target="_blank">
                        <div class="mt-3 temp text-center">¿Olvidaste tu contraseña?</div>
                      </a>

                </div>

            </div>


            <div id="entradacliente" style="display: none;">

                 <div class="headercli">

                    <div class="container-fluid pad2">
                      <div class="row">
                          <div class="col-5 text-left">
                          <span class="iconw">   <i class="fas fa-user-circle" style="color: #ffb517;font-size: 25px;"></i> </span>
                          <span class="titulsa"> Familia <%  montos_arr.apellido_alumno %> </span>
                        </div>

                        <div class="col-1">
                          <a href="https://cumbres.cafe/">
                            <span class="headerlogo">
                                <img src='img/logocumbres.png' alt title="Logo Cumbres Cafe C.A.">
                            </span>
                          </a>
                        </div>

                        <div class="col-6 text-right">
                         
                          <span class="titulsa">  
                          <a href="compras" style="text-decoration: none;">
                          <button type="button" class="btn btn-secondary bntcer" ng-click="reservag()">Realizar reserva</button>
                          </a>
                          </span>
                          
                          <span class="titulsa">  
                          <a href="compras" style="text-decoration: none;">
                          <button type="button" class="btn btn-secondary bntcer" ng-click="vermenug()" >Ver menu</button>
                          </a>
                          </span>
                          
                          <span class="titulsa">  
                          <button type="button" class="btn btn-outline-primary bntcer" ng-click="cerrar()">Cerrar sesión</button>
                          </span>

                        </div>

                        
                      </div>
                    </div>

                    <div class="desplegable" style="display: none;">
                        <!-- Collapse button -->
                          <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation"><span class="dark-blue-text"><i class="fas fa-bars fa-1x"></i></span></button>
                          
                    </div>


                 </div>

                 <!-- Collapsible content -->
                          <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                            <!-- Links -->
                            <ul class="navbar-nav mr-auto">
                              <li class="nav-item active">
                                <a class="nav-link" href="compras">Realizar reserva<span class="sr-only">(current)</span></a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="compras">Ver menu</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href= "#" ng-click="cerrar()">Cerrar sesión</a>
                              </li>
                            </ul>
                            <!-- Links -->
                          </div>
                          <!-- Collapsible content -->

                  <div class="container mt-5" id="paneles1">

                    <div class="row">

                      <div class="col-12 col-lg-4 mb-3">

           
                            <div class="col-12 col-lg-12 mb-3">
                                <div class="cardvcajs">
                                <div class="ttulcaja">Saldo</div> 
                                <div class="bodycaja mt-3">
                                
                                 <div><b>  $ <% montos_arr.montototaldolar %> </b></div>
                                 <div><b>  Bs. <% montos_arr.montototalbs %>  </b></div>
                                </div>
                                
                                  <div class="tip2">
                                    <button type="button" class="btn btn-primary recarbtn" ng-click="abrirrecargas()" >RECARGAR SALDO</button>
                                  </div>

                                  <div class="tip2">
                                    <button type="button" class="btn btn-primary recarbtn" ng-click="transferir(1)">TRANSFERIR SALDO</button>
                                  </div>

                                   <div class="tip2">
                                    <button type="button" class="btn btn-primary recarbtn" ng-click="limitediario(1)">LIMITE DIARIO</button>
                                  </div>

                                  <div class="tip2">
                                    <button type="button" class="btn btn-secondary" ng-click="restricciones(1)" style="font-size: 14px;border-radius: 20px;">RESTRICCIONES ALIMENTICIAS</button>
                                  </div>

                                  <div class="tip2">
                                   <button type="button" class="btn btn-outline-primary otonffoteram" ng-click="detalle()" >Ver detalle ></button>
                                  </div>


                              </div>
                            </div>
                       

                      </div>



                      <div class="col-12 col-lg-4 mb-3">

                            <div class="col-12 col-lg-12 mb-3">
                                <div class="cardvcajs">
                                <div class="ttulcaja">Por validar </div> 
                                <div class="bodycaja mt-2">
                                    <div>  $ <% montos_arr.montoporvalidardolar  %>  </div>
                                    <div>  Bs. <% montos_arr.montoporvalidarbs  %>    </div>
                                </div>
                                <div class="topmt">
                                  <button type="button" class="btn btn-outline-primary otonffoteram" ng-click="detalle2()" >Ver detalle ></button>
                                </div>
                                <hr>

                                <div class="ttulcaja">Rechazados</div> 
                                <div class="bodycaja mt-2">
                                  <div> $ <% montos_arr.montorechazadodolar  %></div>
                                  <div> Bs. <% montos_arr.montorechazadobs  %></div>  
                                </div>
                                   <div class="topmt">
                                     <button type="button" class="btn btn-outline-primary otonffoteram" ng-click="detalle3()" >Ver detalle ></button>
                                   </div>
                              </div>
                            </div>

                      </div>

                      

                      <div class="col-12 col-lg-4 mb-3">

                      <div class="col-12 col-lg-12 mb-3">
                       
                            <div class="cardvcajs">
                             <div class="row">
                                  <div class="col-11">
                                      <div class="ttulcaja">Compras de hoy</div> 
                                      <div class="bodycaja mt-0">
                                        $ <% montos_arr.comprasdehoy  %> 
                                      </div>
                                  </div>
                                  <div class="col-1" style="padding: 27px 0px;">
                                     <span ng-click="detalle4('hoy')"> <i class="fas fa-chevron-right" style="font-size: 34px;cursor: pointer;"></i></span>
                                  </div>
                              </div>
                          </div>

                      </div>

                      <div class="col-12 col-lg-12 mb-3">
                          
                          <div class="cardvcajs">
                             <div class="row">
                                  <div class="col-11">
                                      <div class="ttulcaja">Compras del mes</div> 
                                      <div class="bodycaja mt-0">
                                        $ <% montos_arr.comprasdemes  %>
                                      </div>
                                  </div>
                                  <div class="col-1" style="padding: 27px 0px;">
                                     <span ng-click="detalle4('mes')"> <i class="fas fa-chevron-right" style="font-size: 34px;cursor: pointer;"></i></span>
                                  </div>
                              </div>
                          </div>

                      </div>
                      <div class="col-12 col-lg-12 mb-3">
                      
                         <div class="cardvcajs">
                             <div class="row">
                                  <div class="col-11">
                                      <div class="ttulcaja">Compras del año</div> 
                                      <div class="bodycaja mt-0">
                                        $ <% montos_arr.pedidoano  %>
                                      </div>
                                  </div>
                                  <div class="col-1" style="padding: 27px 0px;">
                                     <span ng-click="detalle4('anual')"> <i class="fas fa-chevron-right" style="font-size: 34px;cursor: pointer;"></i></span>
                                  </div>
                              </div>
                          </div>


                      </div>

                      </div>



                     </div>
                  </div>  

            </div>

             <div class="container mt-3 mb-5"  id="recargarsaldo" style="display: none;">
               
               <div class="btnback">
                <button type="button" class="btn btn-outline-dark" ng-click="volver1('recarga')">Volver</button>
               </div>

               <div id="elecciondemetodo" class="mt-3">
                 
                    <p style="font-size: 20px;">Elige el modo de tu recarga</p>
                     
                    <div class="mt-3">
                      <span class = seleccion>
                        <img src="img/dolares.png">
                        <button type="button" id="abajo_dolar" class="btn btn-primary bntcer doo1" ng-click="cambio('dolares')">Dólares</button>
                      </span>

                      <span class = seleccion>
                        <img src="img/bolivares.png">
                        <button type="button" id="abajo_bs" class="btn btn-primary bntcer boo1" ng-click="cambio('bolivares')">Bolívares</button>
                      </span>
                    </div>
               
               </div>


               <div id="recarga1" style="display: none;">

                 <h3 class="mb-3 mt-5">Seleccione la cantidad a recargar</h3>
                   <p>Por favor, coloca el monto de la cantidad que va a recargar</p>
                   <p>Si desea ver o cambiar el límite de gasto diario de cada usuario, oprimir <span ng-click="limitediario(2)" style="cursor: pointer;color: #00294d;font-weight: 700;text-decoration: underline;">aquí</span> </p>
                    <div class="conuser mt-2">
                        <div class="row">
                           <div class="col-sm-12  col-lg-4 userselc text-center" ng-repeat="(key, combi) in clientes_arr">
                                <div style="width: 100%;"><i class="fas fa-user-circle" style="color: #00294d;font-size: 25px;"></i></div>
                                <%  combi.nombre %> -
                                <span ng-if="combi.tipo_cliente_id == 1">HIJO</span>
                                <span ng-if="combi.tipo_cliente_id == 2">REPRESENTANTE</span>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text palbs">DOLARES A RECARGAR</span>
                                  </div>
                                  <input type="number" value="0" class="number11 form-control text-left" id="input<% combi.id %>" ng-keyup="inser(combi.id)" style="text-align: left !important;" >
                                </div>

                           </div>
                        </div>
                    </div>


                    <div class="totaldol"> <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon" id="totalmontodolares">0</span> </p> </div>
                    <div class="totalbs"> <p class="mb-1 mont"><span class="bold2">Total Bs:</span> <span class="totalmonbs" id="totalmontobolivares">0</span> </p> </div>

            
                
                      <h3 class="mb-3 mt-2">Por favor, introduce un número de celular de contacto</h3>
                        <input type="number" placeholder="Teléfono celular de contacto" class="inputxe" id="teleinfo" ng-keyup="unmtele()">
                       
                        <div class="error1 mt-3 mb-3" id="telerror" style="display: none;">
                            <span class="wrroa">Debes ingresar el número celular para poder contactarte por alguna duda</span>  
                        </div>

                        <div class="mt-3 mb-3">
                            <span >Para poder visualizar los métodos de pagos, debes introducir el número celular de contacto</span>  
                        </div>

               </div>

               <div id="armasonpagos" style="display: none;">

              <div style="display: none;" id="pagobolivares">

                <h3 class="mb-3 mt-3">Elige tu método de pago</h3>
                    
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsepagomovil" aria-expanded="false" aria-controls="collapsepagomovil">
                        PAGO MOVIL
                      </button>
                    </h5>
                  </div>
                  <div id="collapsepagomovil" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                          <p>
                            Realiza el pago del monto total de tu pedido a través de Pago Móvil con los siguientes datos: 
                               <br>
                              - <b>Cel: 0412 2369486</b><br> 
                              - <b>RIF: J-50089373-6</b> <br>
                              - <b>Banco Banplus</b> <br> 
                              - A nombre de -<b>DAILY SERVING COMPANY C.A.</b>  <br>

                              Al realizar el pago indícanos el número celular del titular de la cuenta para poder validar el pago en nuestro banco.
                          </p>
                          <div class="totaldol"> <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span> </p>    </div>
                          <div class="totalbs"> <p class="mb-1 mont"><span class="bold2">Total Bs:</span> <span class="totalmonbs">0</span> </p> </div>
                          
                          <div>
                            <input type="number" placeholder="Número celular del titular"  class="inputxe" id="refepagomovil">
                          </div>

                        <div class="mt-3">

                            <div class="error1 mb-3" id="movilerror" style="display: none;">
                                <span class="wrroa">Debes ingresar el número celular del titular de la cuenta del pago realizado</span>  
                            </div>

                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btnfinalpago" ng-click="recargar('pago movil')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>


                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsepuntoventa" aria-expanded="false" aria-controls="collapsepuntoventa">
                        PUNTO DE VENTA
                      </button>
                    </h5>
                  </div>
                  <div id="collapsepuntoventa" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                          <p>
                            Este pago será verificado luego de que la persona pase por el punto de venta el monto 
                          </p>
                          <div class="totaldol"> <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span> </p>    </div>
                          <div class="totalbs"> <p class="mb-1 mont"><span class="bold2">Total Bs:</span> <span class="totalmonbs">0</span> </p> </div>
                          
                        <div class="mt-3">

                            <div class="error1 mb-3" id="movilerror" style="display: none;">
                                <span class="wrroa">Debes ingresar el número celular del titular de la cuenta del pago realizado</span>  
                            </div>

                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btnfinalpago" ng-click="recargar('punto de venta')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>

              </div>

              <div style="display: none;" id="pagodolares">

               <div class="bodydd mt-4">
                 
                   

               <div class="bodydd mt-4">
                 <h3 class="mb-3">Elige tu método de pago</h3>
                 <div class="conuser mt-2">
                    <div class="contenshe mt-3" id="headerpago" >
              

               <div id="accordion">
                
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        ZELLE
                      </button>
                    </h5>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">

                      <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span>  </p>
                          <div><b>Opcion 1: </b></div>
                          <p class="mb-0">  - Envía tu Zelle a la dirección: <b> {{$auxiliar->zelle}} </b> que pertenece a <b> {{$auxiliar->zelle2}} </b></p>
                          
                          <div><b>Opcion 2:</b></div>
                          <p class="mb-0">  - Envía tu Zelle a la dirección: <b> mw12@mways.net </b> que pertenece a <b>LIPICAN MMXXI LLC</b></p>
                          

                          <div><b>Opcion 3:</b></div>
                          <p class="mb-0">  - Envía tu Zelle a la dirección: <b>mw4@mways.net </b> que pertenece a <b>MNGMTADMNSERVICES LLC</b></p>
                          
                          <div><b>Opcion 4:</b></div>
                          <p class="mb-0">  - Envía tu Zelle a la dirección: <b>mw6@mways.net  </b> que pertenece a <b>MNGMTADMNSERVICES LLC</b></p>
                          

                          <p class="mb-0 mt-3">  - Indispensable que coloques <b>$Cumbres</b> (sin espacios) en la descripción de la transacción.</p>
                          

                          <p class="mb-0">  - Indícanos a continuación el nombre completo del titular de la cuenta</p>

                        <div class="mt-3">
                          <input type="text" placeholder="Titular de la cuenta" class="inputxe" id="refezelle">
                        </div>
                        <div class="mt-3">
                            <div class="error1 mb-3" id="zelleerror" style="display: none;">
                                <span class="wrroa">Debes ingresar el titular de la cuenta del pago realizado </span>  
                            </div>
                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btnfinalpago" ng-click="recargar('zelle')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>

               


                <div class="card">
                    <div class="card-header" id="headingcash">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsecash" aria-expanded="false" aria-controls="collapsepagomovil">
                          CASH
                        </button>
                      </h5>
                    </div>
                    <div id="collapsecash" class="collapse" aria-labelledby="headingcash" data-parent="#accordion">
                        <div class="card-body">
                            <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span> </p>
                            
                            <div>
                              <input type="text" placeholder="Indicanos si tienes algun comentario"  class="inputxe" id="refeefect">
                              
                            </div>

                         

                          <div class="mt-3">
                             <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                            <div class="btnfinalpago" ng-click="recargar('efectivo')">RECARGAR SALDO</div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingPaypal">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed btn-paypal" data-toggle="collapse" data-target="#collapsePaypal" aria-expanded="false" aria-controls="collapsePaypal">
                        TARJETA INTERNACIONAL O PAYPAL
                      </button>
                    </h5>
                  </div>
                  <div id="collapsePaypal" class="collapse" aria-labelledby="headingPaypal" data-parent="#accordion">
                      <div class="card-body">
                          <p class="mb-1 mont"><span class="bold2">Total $:</span> <span class="totalmon">0</span>  </p>
                    
                    <input type="hidden" id="totalpaypal" value="">
                    <div id="paypal-button-container" style="width: max-content"></div>
                          <div><input type="hidden" placeholder="Referencia del pago" id="refepaypal"></div>
                        <div class="mt-3">
                           <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="color: #d1d1d1;display: none;" ></i>
                          <div class="btnfinalpago btnfinalpagopaypal" style="display: none;" ng-click="recargar('paypal')">RECARGAR SALDO</div>
                        </div>
                      </div>
                  </div>
                </div>


              </div>
            </div>
                 </div>
               </div>
             </div>

           
              </div>

            </div>



             






     </div>

       <div class="container mt-3"  id="detalles" style="display: none;">
                 <div class="btnback"><button type="button" class="btn btn-outline-dark" ng-click="volver2(4)">Volver</button></div>
                   <div class="row">
                     <div class="col-12 col-lg-4 mt-3" ng-repeat="(key, combi) in detalle_arr">
                          <div class="cotine">
                               <p class="mb-0">Nombre: <% combi.nombre_alumno  %> </p>
                               <div class="contre">
                                  <span ng-if="combi.tipo_cliente_id == 1">HIJO</span>
                                  <span ng-if="combi.tipo_cliente_id == 2">REPRESENTANTE</span>
                               </div>
                               <p class="mb-0">Saldo disponible: <% combi.abono_actual  %> $ y </p>
                               <p class="mb-0"><% combi.abono_bs  %> Bs.</p>
                               <p class="mb-0">Limite de gasto diario: <% combi.limitediario  %> $</p>
                               <hr>
                               <p class="mb-0"><span  class="links" ng-click="mostrarqr(combi.codigo)"> Ver código QR > </span></p>
                               <p class="mb-0"><span  class="links" ng-click="abriredit(combi.id)"> Editar datos del perfil  > </span></p>
                          </div>
                     </div>
                     <div class="nohayregistros" style="display: none;">No hay registros para mostrar.</div>
                   </div>
             </div>


            <div class="container mt-3"  id="transferir" style="display: none;margin-bottom: 30px;">
                  <div class="btnback">
                    <button type="button" class="btn btn-outline-dark" ng-click="volver2('transferencia')">Volver</button>
                  </div>

                  <div class="row">

                      <div class="col-12 mt-2">
                        <p class="mb-0" style="font-size: 25px;">Saldo total en dolares:   <% montos_arr.montototaldolar %> $</p>
                        <p class="mb-0" style="font-size: 25px;">Saldo total en bolivares: <% montos_arr.montototalbs %> bs.</p>
                      </div>
                     <div class="col-12 col-lg-4 mt-3" ng-repeat="(key, combi) in detalle_arr">
                          
                          <div class="cotine">
                            <p class="mb-0">Nombre: <% combi.nombre_alumno  %> <% combi.apellido_alumno  %></p>
                            <div class="contre">
                              <span ng-if="combi.tipo_cliente_id == 1">HIJO</span>
                              <span ng-if="combi.tipo_cliente_id == 2">REPRESENTANTE</span>
                            </div>

                             <input type="hidden" name="person_id<% combi.id %>" id="person_id<% combi.id %>" value="<% combi.id %>">
                            
                             <p class="mb-0">Saldo disponible: </p>
                             <p class="mb-0">Dolares: <% combi.abono_actual %> $ </p>
                             <p class="mb-2">Bolivares: <% combi.abono_bs %> Bs</p>

                              <div class="form-group mb-2">
                                   <label for="exampleFormControlInput1">Persona a transferir</label>           
                                   <select class="form-control" name="persontrans" id="persontrans<% combi.id %>">
                                      <option ng-repeat="(key, combi2) in detalle_arr" value="<% combi2.id %>"><% combi2.nombre_alumno  %> <% combi2.apellido_alumno  %> </option> 
                                   </select>
                              </div>
                              
                              <label for="exampleFormControlInput1">Monto a transferir</label> 
                              <div class="row mb-3">
                                  <div class="col-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="basic-addon1">$</span>
                                        </div>
                                        <input type="number" class="form-control" id="montodolartransferir<% combi.id %>" value="0"> 
                                    </div>
                                  </div>
                                  <div class="col-6">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="basic-addon1">Bs</span>
                                        </div>
                                        <input type="number" class="form-control" id="montobolostransferir<% combi.id %>" value="0"> 
                                    </div>
                                  </div>
                              </div>

                               <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;font-size: 40px"></i>
                               <button type="button" class="btn btn-primary mt-2 form-control btnlimite" ng-click="transferirsaldo(combi.id)">Transferir saldo</button>
                          
                          </div>
                     </div>
                  </div>
             </div>


             <div class="container mt-3"  id="restriccionesmodal" style="display: none;margin-bottom: 30px;">
                  <div class="btnback">
                    <button type="button" class="btn btn-outline-dark" ng-click="volver2('restriccion')">Volver</button>
                  </div>

                  <div class="row">

                          <div class="col-12 col-lg-4 mt-3" ng-repeat="(key, combi) in detalle_arr">
                          
                          <div class="cotine">
                            <p class="mb-0">Nombre: <% combi.nombre_alumno  %> <% combi.apellido_alumno  %></p>
                            <div class="contre mb-2">
                              <span ng-if="combi.tipo_cliente_id == 1">HIJO</span>
                              <span ng-if="combi.tipo_cliente_id == 2">REPRESENTANTE</span>
                            </div>

                                                       
                              <label for="exampleFormControlInput1">Restricción alimenticia</label> 
                              <div class=" mb-3">
                                 <textarea class="form-control" id="resticinput<% combi.id %>" rows="2"><% combi.restriccion_alimenticia  %></textarea>
                              </div>

                               <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;font-size: 40px"></i>
                               <button type="button" class="btn btn-primary mt-2 form-control btnlimite" ng-click="storerestriccion(combi.id)">Actualizar datos</button>
                          
                          </div>
                     </div>
                  </div>
             </div>



            <div class="container mt-3"  id="limitediario" style="display: none;">
                 <div class="btnback"><button type="button" class="btn btn-outline-dark" ng-click="volver2('limitediario')">Volver</button></div>
                  
                  <div class="row">
                     <div class="col-12 col-lg-4 mt-3" ng-repeat="(key, combi) in detalle_arr">
                          <div class="cotine">
                              <p class="mb-0">Nombre: <% combi.nombre_alumno  %> </p>
                              <div class="contre">
                                <span ng-if="combi.tipo_cliente_id == 1">HIJO</span>
                                <span ng-if="combi.tipo_cliente_id == 2">REPRESENTANTE</span>
                              </div>
                               <p class="mb-0">Saldo disponible: <% combi.abono_actual  %> $</p>
                               <p class="mb-0">Limite de gasto diario: 
                              <div class="input-group mt-1">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1">$</span>
                                 </div>
                                <input type="number" class="form-control" id="limite<% combi.id %>" value="<% combi.limitediario  %>"> 
                              </div>
                               <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;font-size: 20px;"></i>
                               <button type="button" class="btn btn-primary mt-2 form-control btnlimite" ng-click="actualizarlimite(combi.id)">Actualizar limite diario</button>
                          </div>
                     </div>
                  </div>

            </div>



             <div class="container mt-3"  id="edituser" style="display: none;">
                   <div class="btnback"><button type="button" class="btn btn-outline-dark" ng-click="volver3()">Volver</button></div>
                   <div class="container-fluid mt-3 contform">
                       <div class="row">

                            <form style="width: 100%;">

                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nombre</label>
                                    <input type="text" class="form-control" id="nombreedit" value="<%  detalleedit_arr.nombre  %>" >
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputPassword1">Apellido</label>
                                    <input type="text" class="form-control" id="apellidoedit" value="<%  detalleedit_arr.apellido_alumno  %>" >
                                  </div>

                                  <div class="form-group">
                                    <label for="exampleInputPassword1">Email</label>
                                    <input type="text" class="form-control" id="emailedit" value="<%  detalleedit_arr.correo_alumno  %>">
                                  </div>

                                  <div class="form-group">
                                    <label for="exampleInputPassword1">Cedula</label>
                                    <input type="text" class="form-control" id="cedulaedit"  value="<%  detalleedit_arr.cedula  %>" >
                                  </div>

                                  <div class="form-group">
                                    <label for="exampleInputPassword1">Teléfono celular</label>
                                    <input type="text" class="form-control" id="telefonoedit" value="<%  detalleedit_arr.telefono  %>"  >
                                  </div>
                                
                                  <span ng-if="detalleedit_arr.tipo_cliente_id == 1">
                                    <div class="form-group">
                                      <label for="exampleFormControlSelect1">Grado que cursa (solo si aplica)</label>
                                      <select class="form-control" id="gradoh1">
                                        <option value="<%  detalleedit_arr.curso_actual  %>"><%  detalleedit_arr.curso_actual  %></option>
                                        <option value="1er grado">1er grado</option>
                                        <option value="2do grado">2do grado</option>
                                        <option value="3er grado">3er grado</option>
                                        <option value="4to grado">4to grado</option>
                                        <option value="5to grado">5to grado</option>
                                        <option value="6to grado">6to grado</option>
                                        <option value="1er año">1er año</option>
                                        <option value="2do año">2do año</option>
                                        <option value="3er año">3er año</option>
                                        <option value="4to año">4to año</option>
                                        <option value="5to año">5to año</option>
                                      </select>
                                    </div>
                                  </span>
                                  <div class="actualisi" style="display: none;"> Los datos se han actualizado correctamente</div>
                                  <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                                  <button type="submit" class="btn btn-secondary bntactual"  ng-click="actualizardatos(detalleedit_arr.id)">Actualizar datos</button>
                                
                            </form>
                         
                        
                       </div>
                   </div>
             </div>


             <div class="container mt-3"  id="detalles2" style="display: none;">
               <div class="btnback"><button type="button" class="btn btn-outline-dark" ng-click="volver2()">Volver</button></div>
                 <div class="row">
                   <div class="col-12 col-lg-5 mt-3" ng-repeat="(key, combi) in detalle2_arr">
                        <div class="cotine">
                           <p class="mb-0"> Monto: <% combi.total  %> <% combi.signo %></p>
                           <p class="mb-0"> Fecha de pago: <% combi.fechis  %> </p>
                           <p class="mb-0"> Modo pago: <% combi.tipo_pago  %> </p>
                            <p class="mb-0">Referencia: <% combi.referencia_pago  %> </p>
                        </div>
                   </div>
                   <div class="nohayregistros" style="display: none;">No hay registros para mostrar.</div>
                 </div>
             </div>


             <div class="container mt-3"  id="detalles3" style="display: none;">
               <div class="btnback"><button type="button" class="btn btn-outline-dark" ng-click="volver2()">Volver</button></div>
                 <div class="row">
                   <div class="col-12 col-lg-5 mt-3" ng-repeat="(key, combi) in detalle2_arr">
                        <div class="cotine">
                           <p class="mb-0">Monto: <% combi.total  %> <% combi.signo  %></p>
                           <p class="mb-0">Fecha de pago: <% combi.fechis  %> </p>
                           <p class="mb-0">Modo pago: <% combi.tipo_pago  %> </p>
                           <p class="mb-0">Referencia: <% combi.referencia_pago  %> </p>
                        </div>
                   </div>
                   <div class="nohayregistros" style="display: none;">No hay registros para mostrar.</div>
                 </div>
             </div>



             <div class="container mt-3"  id="detalles4" style="display: none;">
               
                 <div class="container-fluid">
                      <div class="btnback mb-3"><button type="button" class="btn btn-outline-dark" ng-click="volver2(5)">Volver</button></div>

                      <div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1">DESDE</span>
                                </div>
                                <input type="date" class="form-control" value="{{$primerodelmes}}" id="desde" name="desde">
                                <div class="input-group-prepend" style="margin-left: 1%">
                                  <span class="input-group-text" id="basic-addon1">HASTA</span>
                                </div>
                                <input type="date" class="form-control" value="{{$ultimodelmes}}" id="hasta" name="hasta">

                                <button type="button" class="btn btn-outline-dark" ng-click="buscarpedidos()" style="margin-left: 1%;">Buscar</button>
                            </div>
                      </div>
                     <div>
                       <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Monto</th>
                                <th scope="col">Detalle</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr  ng-repeat="(key, combi) in detalle4_arr">
                                <td><% combi.cliente.nombre %></td>
                                <td><% combi.fechis %></td>
                                <td><% combi.monto %> $</td>
                                <td><i class="fas fa-edit" style="cursor: pointer;" ng-click="detis(combi.id)"></i></td>
                              </tr>
                            </tbody>
                          </table>
                          <div class="nohayregistros" style="display: none;">No hay registros para mostrar.</div>
                     </div>
                 </div>
             </div>

  <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>


    <script src="js/angular.min.js"></script>
    <script src="js/angular.ng-modules.js"></script>
    <script type="text/javascript" src="js/qr.js"></script>
    <script type="text/javascript" src="js/number.js"></script>
    <script src="js/angularrecargarsaldo17.js"></script>
    <script src="https://www.paypal.com/sdk/js?client-id=AXq9NLZ09Di8OJ2yxxBu5wIx-5vXXbtAPjaJgzGdFod0e0QzOxJoCAAfuXBZl6QEKZfidT72sdUnPsqZ"></script>
    
    <script type="text/javascript">
      
        tasa = 0;
        $.ajax({
            url: 'https://s3.amazonaws.com/dolartoday/data.json',
            type: 'GET',
            crossDomain: true,
            dataType: 'json',
        }).done(function (respuesta) {
            tasa = respuesta.USD.promedio_real;
            console.log(tasa);
            $("#tasadolartoday").val(tasa);
        })
        .fail(function (jqXHR, textStatus) {
            tasa = textStatus;
        });

    </script>

    


    <script>

      $('.btn-paypal').on('click',function(){
        
        if($('#totalpaypal').val() == 0){
            $('.btn-paypal').addClass('collapsed');  
            $("#modalerror").modal("toggle");  
            $('.mensajemodalerror').text('Debes ingresar el monto que deseas recargar');
        }

        if ($("#teleinfo").val() == "") { 
          $('.btn-paypal').addClass('collapsed');  
          $("#modalerror").modal("toggle");  
          $('.mensajemodalerror').text('Debes ingresar un telefono de contacto');
        }

      });

    paypal.Buttons({

      createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
        amount: {
          value: $('#totalpaypal').val()
        }
        }]
      });
      },
      onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
        return actions.order.capture().then(function(details) {
          // This function shows a transaction success message to your buyer.
          console.log(details);
          $('#refepaypal').val(details.purchase_units[0].payments.captures[0].id);
            $('.mensajemodallisto').text('Su pago fue procesado con éxito, ya puede utilizar su saldo disponible.');
            $("#modallisto").modal("toggle");
            //alert();
          $('.btnfinalpagopaypal').click();
        });
      },
      onError: function (err) {
        // For example, redirect to a specific error page
        //window.location.href = "/accesoclientes";
        $('.mensajemodallisto').text('Transacción Fallida error: ' + err);

      }
    }).render('#paypal-button-container');
    //This function displays Smart Payment Buttons on your web page.


    // scroll down
    var altura = $(document).height();
     
    $("#abajo_dolar").click(function(){
          $("html, body").animate({scrollTop:altura+"px"});
    });

    $("#abajo_bs").click(function(){
          $("html, body").animate({scrollTop:altura+"px"});
    });

  </script>

 



</body>
</html>