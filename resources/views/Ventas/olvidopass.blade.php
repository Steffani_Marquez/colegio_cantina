
<!DOCTYPE html>
<html lang="en">
<head>


   <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MPXZNJP');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
  	<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
  	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->

    <link rel="shortcut icon" href="{{url('img/logo.png')}}">
    <title>LA CANTINA DIGITAL</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/recargarsaldo.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">




</head>

<style type="text/css">
  th{text-align: center;}
  td{text-align: center;}
  .modal {
    background: white;
}

.contre{background: #ece9ff;
    padding: 4px 10px;}

  .links{color: #5581bd;
    cursor: pointer;
    font-size: 18px;}
    .links:hover{color: #5581bd;
    cursor: pointer;text-decoration: underline;
    font-size: 18px;}
    .contform{
      border: solid 2px #ededed;
    padding: 27px 40px;
    margin-bottom: 30px;
    border-radius: 10px;
  }
  .actualisi{background: #4cc77e;
    color: white;
    font-size: 18px;
    padding: 5px 15px;
    border-radius: 6px;
    margin-bottom: 10px;}
</style>
   
   <body class="fondis1">

      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPXZNJP"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->

      <div ng-module="CantinaApp" ng-controller="CantinaController">

          <input type="hidden" name="codigo_familiar" id="codigo_familiar">
    		

        <div class="contenshe mt-3 container" id="headercliente">

				
                <div class="codigobar seccionh" id="codigobar">
        				    	
                    <div class="mb-3 temp">Introduzca su email o C.I para recuperar su contraseña</div>

        						<div class="input-group mb-3">
        								<div class="input-group-prepend">
        								  <span class="input-group-text" id="basic-addon1"><i class="fas fa-user-circle"></i></span>
        								</div>
        								<input type="text" class="form-control inpute"  name="email" id="email">
        						</div>
                      <div class="text-center">
                      <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                      </div>
                      <button type="button" class="btn btn-outline-primary btningre bntingre" ng-click='enviardatoscliente()' style="width: 100%;">ENVIAR DATOS</button>
                     
                      <div class="listoem" style="text-align: center;font-size: 18px;line-height: 23px;margin-top: 21px;display: none;">
                         Hemos enviado un correo con su usuario actual y contraseña a esta dirección
                         <b id="dir">  </b>
                      </div>

                      <div class="listono" style="text-align: center;font-size: 18px;line-height: 23px;margin-top: 21px;display: none; color: #aa1a18">
                         Los datos que ha suministrado son incorrectos, por favor vuelva a introducirlos
                      </div>

                </div>

      </div>

      	   

     </div>

  <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/sb-admin-2.min.js"></script>


    <script src="js/angular.min.js"></script>
  	<script src="js/angular.ng-modules.js"></script>
    <script type="text/javascript" src="js/qr.js"></script>
    <script type="text/javascript" src="js/number.js"></script>
  	<script src="js/recuperar.js"></script>
  	<script src="https://www.paypal.com/sdk/js?client-id=AXq9NLZ09Di8OJ2yxxBu5wIx-5vXXbtAPjaJgzGdFod0e0QzOxJoCAAfuXBZl6QEKZfidT72sdUnPsqZ"></script>


</body>
</html>