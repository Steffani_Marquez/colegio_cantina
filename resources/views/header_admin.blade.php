<!DOCTYPE html>
<html lang="en">

<head>

   <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MPXZNJP');
    </script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>La Cantina</title>

    <!-- Custom fonts for this template-->
    <link href="{{url('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    
    <link rel="stylesheet" href="{{url('css/font-awesome-4.7.0/css/font-awesome.min.css')}}" >

    <!-- Custom styles for this template-->
    <link href="{{url('css/sb-admin-2.css')}}"  rel="stylesheet">  

   

    <link rel="shortcut icon" href="{{url('img/favicon.jpg')}}">
    
</head>

<style type="text/css">

.circle12 {
    width: 25px;
    height: 25px;
    color: white;
    background: #FEB514;
}

    #wrapper #content-wrapper {
    width: 100%;
    overflow-x: hidden;
}

.sidebar .nav-item .collapse, .sidebar .nav-item .collapsing {
    margin: 0 10px;
}



.sidebar .nav-item .nav-link {
    display: block;
    width: 100%;
    text-align: left;
    padding: 10px 20px;
    width: 13rem;
}

.botonitos {
    cursor: pointer;
    color: #ffb517;
    font-size: 30px;
}

.sidebar .nav-item .nav-link i {
    font-size: 13px;
    color: white !important;
    margin-right: .25rem;
}

.sidebar .nav-item .nav-link span {
    font-size: 16px;
    display: inline;
}

.sidebar .nav-item .collapse .collapse-inner, .sidebar .nav-item .collapsing .collapse-inner {
    padding: 0px 0;
    font-size: .85rem;
    margin: 0 0 0px 0;
}

.sidebar-dark #sidebarToggle {
    color: #00294d !important;
}

.logimg{
    width: 35%;
    padding: 0px;
}

.tam1{
    padding: 4px !important;
    height: 100px !important;
}

.sidebar .nav-item .collapse .collapse-inner .collapse-item, .sidebar .nav-item .collapsing .collapse-inner .collapse-item {
    padding: .5rem 1rem;
    margin: 0px;
    display: block;
    color: #3a3b45;
    text-decoration: none;
    border-radius: .35rem;
    white-space: nowrap;
}


#mydiv {
width: 400px;
    position: absolute;
    z-index: 9;
    background-color: #f1f1f1;
    text-align: center;
    border: 1px solid #d3d3d3;
}

.bg-gradient-primary {
background-color: ##00294d;
    background-size: cover;
    background: #00294d;
}

#mydivheader {
  padding: 10px;
    cursor: move;
    z-index: 10;
    background-color: #ae8d79;
    color: #fff;
}

.sidebar-dark #sidebarToggle {
    background: #ffb517 !important;
}
</style>

<body >

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MPXZNJP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php
$roles = collect(Auth::user()->roles->lists('name'))->toArray();
$user_id = Auth::user()->id;
?>

<input type="hidden" id="tasadolartoday">
<input type="hidden" id="tasamanual">


  <div id="mydiv" style="display: none;" >
      <div class="close" onclick="calculadoraabrir()"><i class="far fa-times-circle"></i></div>
      <div id="mydivheader">CALCULADORA</div>
        <form name="calculator">
            <input type="textfield" name="ans" id="calculador" value="">
            <br>
            <input type="button" value="1" onClick="document.calculator.ans.value+='1'">
            <input type="button" value="2" onClick="document.calculator.ans.value+='2'">
            <input type="button" value="3" onClick="document.calculator.ans.value+='3'">
            <input type="button" value="+" onClick="document.calculator.ans.value+='+'">
            <br>
            <input type="button" value="4" onClick="document.calculator.ans.value+='4'">
            <input type="button" value="5" onClick="document.calculator.ans.value+='5'">
            <input type="button" value="6" onClick="document.calculator.ans.value+='6'">
            <input type="button" value="-" onClick="document.calculator.ans.value+='-'">
            <br>
            <input type="button" value="7" onClick="document.calculator.ans.value+='7'">
            <input type="button" value="8" onClick="document.calculator.ans.value+='8'">
            <input type="button" value="9" onClick="document.calculator.ans.value+='9'">
            <input type="button" value="*" onClick="document.calculator.ans.value+='*'">
            <br>
            <input type="button" value="0" onClick="document.calculator.ans.value+='0'">
            <input type="button" value="," onClick="document.calculator.ans.value+='.'">
            <input type="button" value="/" onClick="document.calculator.ans.value+='/'">
            <input type="button" value="=" onClick="document.calculator.ans.value=eval(document.calculator.ans.value)">
            <br>
            <input type="reset" value="Borrar">
            <input type="button" value="Dolar" onclick="cacululardolar()">
        </form>
  </div>

  <script type="text/javascript">
        function caculularpaypal(){
            var suma = document.calculator.ans.value=eval(document.calculator.ans.value);
        }

        function linkreserva(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'gestion',
            'eventAction': 'reservas-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });

        }

        function linkpedidos(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'gestion',
            'eventAction': 'pedidos-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

        function linkrecargas(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'gestion',
            'eventAction': 'recargas-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

        function linkproductos(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'inventario',
            'eventAction': 'productos-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

        function linkpromociones(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'inventario',
            'eventAction': 'promociones-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

        function linkcompuestos(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'inventario',
            'eventAction': 'compuestos-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

         function linkrealizarventa(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'tienda',
            'eventAction': 'venta-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

         function linkrealizarreserva(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'tienda',
            'eventAction': 'reserva-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

        function linkclientes(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'clientes',
            'eventAction': 'clientes-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

        function linknuevousuario(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'seguridad',
            'eventAction': 'nuevo-usuario-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }

        function linkcontrolusuario(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'seguridad',
            'eventAction': 'control-usuario-click',
            'tipo': 'navegacion',
            'colegio': 'cumbres',
            'pais' :'VE'
            });
            
        }
        
        
        var abierta = 0;


        function calculadoraabrir(){

           

            if (abierta == 0) {
               
                abierta = 1;

                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'calculadora',
                'eventAction': 'calculadora-click-abrir',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais' :'VE'
                });

            }else{

                abierta = 0;
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'calculadora',
                'eventAction': 'calculadora-click-cerrar',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais' :'VE'
                });
            }

            

           $("#mydiv").toggle();
        }

        function cacululardolar(){

          var tasadolar =  $("#tasamanual").val();
          var suma = document.calculator.ans.value=eval(document.calculator.ans.value);
          var total = parseFloat(suma) * parseFloat(tasadolar);
          document.calculator.ans.value=eval(total);

        }


        function cerrarsesion(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'cerrar-sesion',
            'eventAction': 'cerrar-sesion-click',
            'tipo': 'admin',
            'colegio': 'cumbres',
            'pais' :'VE'
            });

        }


        function zellemodal(){
          $("#zellemodal").modal();
        }


        function dolarmodal(){

            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
            'event': 'GenericGAEvent',
            'eventCategory': 'dolar-dia',
            'eventAction': 'dolar-dia-click',
            'tipo': 'admin',
            'colegio': 'cumbres',
            'pais' :'VE'
            });


           $("#modaldolar").modal();
           var tasado = $("#tasadolartoday").val()
           $("#tasatext").text(tasado);
        }

        function dolarvalor(){
           $("#changemodal").modal();
        }




  </script>



    <!-- Page Wrapper -->
    <div id="wrapper" >

            <div ng-module="AuxiliarApp" ng-controller="AuxiliarController">
                
                <div class="modal" tabindex="-1" id="modaldolar">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">DOLAR DEL DÍA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>TASA OFICIAL: <span id="tasatext"></span> Bs. </p>
                        <p>TASA MANUAL:  <span><%  auxiliar_arr.dolar %> </span> Bs. <span style="cursor: pointer;" onclick="dolarvalor()"><i class="fas fa-edit"></i></span> </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal" tabindex="-1" id="changemodal">
                  <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">CAMBIAR VALOR DE DOLAR</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Introduzca un nuevo valor</label>
                                <input type="number" class="form-control" id="nuevodolar" >
                            </div>
                        </div>
                        <div class="modal-footer">
                            <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                            <button type="button" class="btn btn-primary" id="btnguardardolar" ng-click="guardardolir()">Guardar cambio</button>
                        </div>
                    </div>
                  </div>
                </div>


                <div class="modal" tabindex="-1" id="zellemodal">
                  <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">CAMBIAR ZELLE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Envía tu Zelle a la dirección:</label>
                                <input type="text" class="form-control" id="zelle1" value="<%  auxiliar_arr.zelle %>">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlInput1">Que pertenece a</label>
                                <input type="text" class="form-control" id="zelle2" value="<%  auxiliar_arr.zelle2 %>">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <i class="fa fa-spinner fa-spin fa-3x fa-fw load" style="display: none;"></i>
                            <button type="button" class="btn btn-primary" id="btnguardarzelle" ng-click="guardarcambios()">Guardar cambio</button>
                        </div>
                    </div>
                  </div>
                </div>

        </div>

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center tam1" href="{{url('/dashboard')}}">
                <img src="{{url('img/loguisblanco.svg')}}" style="width: 50%;">
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0 mb-2">
            @if(in_array('administrador', $roles) || in_array('superadmin', $roles))
            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="{{url('/dashboard')}}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsesix"
                    aria-expanded="true" aria-controls="collapsesix">
                    <i class="fas fa-cash-register"></i>
                    <span>Gestión</span>
                </a>
                <div id="collapsesix" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white collapse-inner rounded">
                        <a class="collapse-item" href="{{url('reservas')}}" onclick="linkreserva()">Reservas</a>
                        <a class="collapse-item" href="{{url('pedidos')}}"  onclick="linkpedidos()" >Pedidos</a>
                        <a class="collapse-item" href="{{url('recargas')}}" onclick="linkrecargas()">Recargas</a>
                    </div>
                </div>
            </li>
          

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-cash-register"></i>
                    <span>Inventario</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white collapse-inner rounded">
                        <a class="collapse-item" href="{{url('productos')}}"   onclick="linkproductos()">Productos</a>
                        <a class="collapse-item" href="{{url('promociones')}}" onclick="linkpromociones()">Promociones</a>
                        <a class="collapse-item" href="{{url('productos_compuestos')}}" onclick="linkcompuestos()" >Productos compuestos</a>
                    </div>

                </div>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapstienda"
                    aria-expanded="true" aria-controls="collapstienda">
                  <i class="fas fa-store"></i>
                    <span>Tienda</span>
                </a>
                <div id="collapstienda" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white  collapse-inner rounded">
                        @if(in_array('cajero', $roles) || in_array('administrador', $roles) || in_array('superadmin', $roles))
                        <a class="collapse-item" href="{{url('ventas')}}" onclick="linkrealizarventa()" >Realizar venta</a>
                        @endif
                        @if(in_array('administrador', $roles) || in_array('superadmin', $roles))
                        <a class="collapse-item" href="{{url('compras')}}"  onclick="linkrealizarreserva()" >Realizar reserva</a>
                        @endif
                    </div>

                </div>
            </li>



            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseclientes"
                    aria-expanded="true" aria-controls="collapseclientes">
                    <i class="fas fa-user-tag"></i>
                    <span>Clientes</span>
                </a>
                <div id="collapseclientes" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white collapse-inner rounded">
                        <a class="collapse-item" href="{{url('verclientes')}}"  onclick="linkclientes()">Ver clientes</a>
                    </div>

                </div>
            </li>

            @if(in_array('superadmin', $roles))
            
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsseguridad"
                    aria-expanded="true" aria-controls="collapsseguridad">
                  <i class="fas fa-lock-open"></i>
                    <span>Seguridad</span>
                </a>
                <div id="collapsseguridad" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white  collapse-inner rounded">
                        <a class="collapse-item" href="{{url('register')}}" onclick="linknuevousuario()">Nuevo Usuario</a>
                        <a class="collapse-item" href="{{url('register/list-users')}}" onclick="linkcontrolusuario()" >Control Usuarios</a>
                    </div>

                </div>
            </li>
            @endif

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

          

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                   
                   <span class="botonitos" onclick="dolarmodal()" style="margin-left: 15px;"><i class="fas fa-dollar-sign"></i></span> 
                   <span class="botonitos" onclick="zellemodal()" style="margin-left: 15px;"><i class="fas fa-money-check"></i></span> 

      
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small tamle">{{Auth::user()->name}}</span>
                                <button class="rounded-circle border-0 circle12">{{substr(ucwords(Auth::user()->name," "),0,1)}}</button>
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                             
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" onclick="cerrarsesion()">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>



<script>
//Make the DIV element draggagle:
dragElement(document.getElementById("mydiv"));

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    /* if present, the header is where you move the DIV from:*/
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
</script>

