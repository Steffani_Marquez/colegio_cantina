           


           </div>
            <!-- End of Main Content -->

            <!-- Footer -->
          
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cerrar sesión</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Selecciona "Cerrar sesión" si estas listo para cerrar tu sesión.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-primary" href="{{url('logout')}}">Cerrar sesión</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Perdio Sesion Modal-->
    <div class="modal fade" id="lostsessionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sesión</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Perdiste la Sesión que tenias activa</div>
                <div class="modal-footer">
                    <a class="btn btn-primary" href="{{url('/')}}">Inicia Sesión</a>
                </div>
            </div>
        </div>
    </div>    

    <!-- Bootstrap core JavaScript-->
    <script src="{{url('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{url('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{url('js/sb-admin-2.min.js')}}"></script>

    <script src="{{url('js/angular.min.js')}}"></script>
    <script src="{{url('js/angular.ng-modules.js')}}"></script>
    <script src="{{url('js/preview-photos.js')}}"></script>
     <script src="{{url('js/buscarauxiliar.js')}}"></script>


    <script type="text/javascript">
       
        tasa = 0;
        $.ajax({
            url: 'https://s3.amazonaws.com/dolartoday/data.json',
            type: 'GET',
            crossDomain: true,
            dataType: 'json',
        }).done(function (respuesta) {
            tasa = respuesta.USD.promedio_real;
            console.log(tasa);
            $("#tasadolartoday").val(tasa);
        })
        .fail(function (jqXHR, textStatus) {
            tasa = textStatus;
        });

    </script>

</body>

</html>