<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeguimientoInventario extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'seguimiento_inventario';
    protected $fillable = ['id_producto','id_deposito','accion','cantidad','user_id','nota','fecha'];

    public function producto() {
		return $this->hasOne('App\Productos','id','id_producto');
	}
    
    public function user() {
		return $this->hasOne('App\User','id','user_id');
	}
}
