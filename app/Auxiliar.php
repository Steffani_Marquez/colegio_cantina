<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auxiliar extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'auxiliar';
    protected $fillable = ['dolar','zelle','zelle2'];
}
