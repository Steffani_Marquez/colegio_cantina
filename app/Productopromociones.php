<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productopromociones extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'producto_promociones';
    protected $fillable = ['id_producto','id_promocion','cant_producto'];

    public function producto() {
		return $this->hasOne('App\Productos','id','id_producto');
	}

    public function promociones() {
		return $this->hasOne('App\Promociones','id','id_promocion');
	}
}
