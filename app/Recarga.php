<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recarga extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'recargas';
    protected $fillable = ['monto','fecha_registro','tipo_pago','validado','codigo_familiar','referencia_pago','tasabs','telefono','origen'];
    
    public function carrito() {
      return $this->hasMany('App\Carritoporvalidar','id_recarga','id');
    }
      
    
}
