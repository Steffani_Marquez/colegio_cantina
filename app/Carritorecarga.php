<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carritorecarga extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'carritorecarga';
    protected $fillable = ['id_cliente','fecha_registro','monto','codigo_familiar','tasa'];

	
	public function cliente() {
		return $this->hasOne('App\Cliente','codigo','codigo_cliente');
	}

}
