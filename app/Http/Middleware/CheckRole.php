<?php 

namespace App\Http\Middleware;

// First copy this file into your middleware directoy

use Closure;

class CheckRole{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
    public function handle($request, Closure $next)
    {
        if ($request->user() === null) {
            return redirect('/login');
            //return response()->json(['error' => 'Insufficient permissions.'], 401);
        }
        //return $request->user();
        if ($request->user()->active === 0) {
            return redirect('/login')->with('error', 'Acceso Cancelado');
            //return response()->json(['error' => 'Insufficient permissions.'], 401);
        }
        $actions = $request->route()->getAction();
		//return $actions; 
        $roles = isset($actions['roles']) ? $actions['roles'] : null;
        if ($request->user()->hasAnyRole($roles) || !$roles) {
            return $next($request);
        }
        abort(401);
        //return response()->json(['error' => 'Insufficient permissions.'], 401);
    }



}