<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::controller('reporte_saldos','ReportesaldoController');


Route::group(['middleware' => 'roles','roles' => ['superadmin']], function () 
{
    Route::controller('register','RegistroController');
    Route::controller('usuario','RegistroController');
});

Route::group(['middleware' => 'roles','roles' => ['superadmin','administrador']], function () 
{
    Route::controller('dashboard','DashboardController');
    Route::controller('verclientes','VerclientesController');
    Route::controller('productos','ProductosController');
    Route::controller('promociones','PromocionesController');
    Route::controller('pedidos','PedidosController');
    Route::controller('reservas','ReservasController');
    Route::controller('productos_compuestos','ProductoscompuestosController');

});

Route::controller('onlivarpass','OlvidarpassController');
Route::controller('ventas','VentasController');
Route::controller('registroclienteonly','Registrocliente2Controller');
Route::controller('registrocliente','RegistroclienteController');
Route::get('logout', 'Auth\AuthController@getLogout');    
Route::controller('login','LoginController');
Route::controller('recargas','ValidarrecargasController');
Route::controller('accesoclientes','AccesoclientesController');
Route::controller('compras','ComprasController');
Route::controller('prueba','PruebaController');
Route::controller('/','HomeController');