<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Productos;
use App\Categoria;
use App\Productodeposito;
use App\Productovendido;
use App\SeguimientoInventario;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {   
        $hoy2 = Carbon::now('America/Caracas')->format('Y-m-d');
        $categorias = Categoria::all();

        return view('admin.productos',compact('categorias','hoy2'));
    }



    public function getExportarinventario(Request $request){


        $desde =  Carbon::parse($request->desde2)->format('Y-m-d 00:00:00');
        $hasta =  Carbon::parse($request->hasta2)->format('Y-m-d 23:59:59');

        $deposito = Productodeposito::
                        with('producto')
                        ->orderBy("cantidad",'asc')
                        ->get();



        $transacciones_array = [];    

        foreach($deposito as $ped){

            if ($ped->producto != null) {
                $new_array['producto'] = $ped->producto->nombre;
                $new_array['cantidad'] = $ped->cantidad;
                $new_array['precio'] =  $ped->producto->costo;
                $new_array['costo'] = $ped->producto->precio_costo;
                $transacciones_array[] = $new_array;      
            }               
        }
 

        $nombre_archivo = 'REPORTE DE VENTA'.date('Ymdhis');
        Excel::create($nombre_archivo, function($excel) use ($transacciones_array) {

                $excel->sheet('REPORTE DE VENTA', function($sheet) use($transacciones_array) {
                    $sheet->setAutoSize(true);
                    $sheet->cell('A1','PRODUCTO');
                    $sheet->cell('B1','CANTIDAD');
                    $sheet->cell('C1','PRECIO');
                    $sheet->cell('D1','COSTO');
                    
                    $sheet->row(1, function($row) {
                        $row->setFontSize(13);
                        $row->setFontWeight('bold');
                    });
                    $sheet->cells('A1:G1', function ($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->fromArray($transacciones_array, null, 'A2', true,false);
                });                  
            })->download('xlsx'); 
    }



    public function getProdvendidos(Request $request){


        $desde =  Carbon::parse($request->desde)->format('Y-m-d 00:00:00');
        $hasta =  Carbon::parse($request->hasta)->format('Y-m-d 23:59:59');

        $pedidos = Productovendido::
                        with('productos')
                        ->with('promocion')
                        ->orderBy("id",'desc')
                        ->where('fecha_registro',">=",$desde)
                        ->where('fecha_registro',"<=",$hasta)
                        ->get();

                

        $transacciones_array = [];    

        foreach($pedidos as $ped){
            
            $totaltotal = 0;
            $ped->monto_format = number_format($ped->monto,2,',','.');
            $ped->fecha_reistro_format = Carbon::parse($ped->fecha_registro)->format('d/m/Y h:i A');

            if ($ped->promocion == NULL) {
                $nombre = $ped->productos->nombre;
            }else{
                $nombre = $ped->promocion->nombre;
            }

            $fechis =  Carbon::parse($ped->fecha_registro)->format('d/m/Y 23:59:59');
          
            $new_array['producto'] =  $nombre;
            $new_array['cantidad'] = $ped->cantidad;
            $new_array['precio'] =  $ped->monto;
            $new_array['tipo'] = $ped->tipo;
            $new_array['fecha_vendido'] = $fechis;
            $transacciones_array[] = $new_array;                     
        }


            

        $nombre_archivo = 'REPORTE DE VENTA'.date('Ymdhis');
        Excel::create($nombre_archivo, function($excel) use ($transacciones_array) {

                $excel->sheet('REPORTE DE VENTA', function($sheet) use($transacciones_array) {
                    $sheet->setAutoSize(true);
                    $sheet->cell('A1','PRODUCTO');
                    $sheet->cell('B1','CANTIDAD');
                    $sheet->cell('C1','PRECIO');
                    $sheet->cell('D1','TIPO');
                    $sheet->cell('E1','FECHA VENDIDO');
                    
                    $sheet->row(1, function($row) {
                        $row->setFontSize(13);
                        $row->setFontWeight('bold');
                    });
                    $sheet->cells('A1:G1', function ($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->fromArray($transacciones_array, null, 'A2', true,false);
                });                  
            })->download('xlsx'); 
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBuscar(Request $request){
        
        $productos = Productos::
            with('categoria')
            ->where('status',$request->status)
            ->get();

        foreach($productos as $product){
            $stock = Productodeposito::
                where('id_producto',$product->id)
                ->where('id_deposito',1)
                ->sum('cantidad');  

            $product->stock = $stock;
        }

        return $productos;

    }

    public function getSeguimientoinventario(Request $request){
        
        $seguimiento = SeguimientoInventario::
                            where('id_producto',$request->id)
                            ->with("user")
                            ->get();

        return $seguimiento;
    }
    
    public function getGuardarinventario(Request $request){
        
        $Productodeposito = Productodeposito::where('id_producto',$request->id)->where('id_deposito',1)->first();
        
        if(empty($Productodeposito)){
            $cantidad = $request->cantidad;
            Productodeposito::create([
                'id_producto' => $request->id
                ,'id_deposito' => 1
                ,'cantidad' =>  $cantidad
            ]);
            if($request->accion == 1){
                $accion = 'Ingreso';
             
            }
            if($request->accion == 2){
                $accion = 'Salida';
             
            }
        }else{
            if($request->accion == 1){
                $accion = 'Ingreso';
                $cantidad = $Productodeposito->cantidad + $request->cantidad;
            }
            if($request->accion == 2){
                $accion = 'Salida';
                $cantidad = $Productodeposito->cantidad - $request->cantidad;
            }
            $Productodeposito->cantidad = $cantidad;
            $Productodeposito->save();
        }


        $seguimiento = SeguimientoInventario::create([
            'id_producto' => $request->id
            ,'id_deposito' => 1
            ,'accion' => $accion
            ,'cantidad' => $request->cantidad
            ,'user_id' => 1//Auth::user()->id
            ,'nota' => $request->nota 
            ,'fecha' => Carbon::now('America/Caracas')
        ]);
        
        
        $productos = Productos::with('categoria')->get();
        foreach($productos as $product){
            $stock = Productodeposito::where('id_producto',$product->id)->where('id_deposito',1)->sum('cantidad');            
            $product->stock = $stock;
            if($product->id == $request->id){
                $producto = $product;
            }
        }

        $seguimientos = SeguimientoInventario::where('id_producto',$request->id)->get();
        $datos['productos'] = $productos;
        $datos['producto'] = $producto;
        $datos['seguimientos'] = $seguimientos;
        
        return $datos;

    }
    

    public function postNuevo(Request $request){
         
        $file = $request->file('image'); 
        $file2 = \Input::file('image'); 

        if ($request->file('image') == "") {
           $imagen = "img/not-available.png";
        }else{
        
        $name2 = date('dmYhis').'image'.'.'.$file->getClientOriginalExtension();
        $carga = $file2->move(public_path('img/products'),$name2 );
        $imagen = 'img/products/'.$name2;

        }

        $producto = Productos::create([
            'id_categoria' => $request->categoria_id
            ,'nombre' => $request->nombre
            ,'status' => $request->status
            ,'descripcion' => $request->descripcion
            ,'costo' => $request->precio
            ,'tipo' => $request->tipo
            ,'imagen' => $imagen
            ,'precio_costo' => $request->precio_costo
        ]);
        
        return $producto;
    }

    public function postEditar(Request $request){
        
        
        $producto = Productos::find($request->id_edicion);
        $file = $request->file('image_edicion'); 
        $file2 = \Input::file('image_edicion'); 
         
        if(!empty($file)){
        
            $name2 = date('dmYhis').'image'.'.'.$file->getClientOriginalExtension();
            $carga = $file2->move(public_path('img/products'),$name2 );
            $producto->imagen = 'img/products/'.$name2;
    
        }
            
        if($producto->id_categoria != $request->categoria_id_edicion){
            $producto->id_categoria = $request->categoria_id_edicion;
        }

        if($producto->nombre != $request->nombre_edicion){
            $producto->nombre = $request->nombre_edicion;
        }
            
        if( $producto->status != $request->status_edicion){
            $producto->status = $request->status_edicion;
        }

        if($producto->descripcion != $request->descripcion_edicion){
            $producto->descripcion = $request->descripcion_edicion;
        }

        if($producto->costo != $request->precio_edicion){
            $producto->costo = $request->precio_edicion;
        }
        if($producto->tipo != $request->tipo_edicion){
            $producto->tipo = $request->tipo_edicion;
        }

        if($producto->precio_costo != $request->precio_costo_edicion){
            $producto->precio_costo = $request->precio_costo_edicion;
        }


        $producto->save();
        
        return $producto;
    }

}
