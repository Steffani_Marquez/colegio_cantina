<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pedido;
use App\Productovendido;
use App\Productopromociones;
use App\Productodeposito;
use App\Cliente;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;


class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        
        $hoy2 = Carbon::now('America/Caracas')->format('Y-m-d');
        $manana2 = Carbon::now('America/Caracas')->addDays(1)->format('Y-m-d');

        return view('admin.pedidos',compact('hoy2','manana2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function getEliminarpedido(Request $request){

            $pedidos = Pedido::
                        where('id',$request->id)
                        ->with('cliente')
                        ->with('productos')
                        ->first();

            foreach ($pedidos->productos as $key => $value) { 

                if ($value->tipo == "Producto") {
                
                    $productodepo = Productodeposito::
                                        where('id_producto',$value->id_producto)
                                        ->first();
                    
                    $productodepo->update(['cantidad' => (float)$productodepo->cantidad + (float)$value->cantidad]);

                }

                if ($value->tipo == "Promo") {
                
                        $productopromo = Productopromociones::
                                                 where('id_promocion',$value->id_producto)
                                                 ->get();
      
                            foreach ($productopromo as $key => $prop) {

                                $productodepo = Productodeposito::
                                                    where('id_producto',$prop->id_producto)
                                                    ->first();
                                
                                $productodepo->update(['cantidad' => (float)$productodepo->cantidad + ((float)$value->cantidad * (float)$prop->cant_producto)]);

                            }
                    
                    

                }

                $value->delete();

            }  

            if ($pedidos->tipo_pago == "abonado") {
               
                $pedidos->cliente->update(['abono_actual' => (float)$pedidos->cliente->abono_actual + (float)$pedidos->menosdolar]);
                
                 $pedidos->cliente->update(['abono_bs' => (float)$pedidos->cliente->abono_bs + (float)$pedidos->menosbs]);
            } 

            $pedidos->delete();

            return 0;


    }



    public function getPasaracaja(Request $request)
    {
             $desde =  Carbon::parse($request->desde)->format('Y-m-d 00:00:00');
             $hasta =  Carbon::parse($request->hasta)->format('Y-m-d 23:59:59');

            $pedidos = Pedido::
                with('cliente')
                ->orderBy("id",'desc')
                ->where('fecha_registro',">=",$desde)
                ->where('fecha_registro',"<=",$hasta)
                ->get();

            foreach ($pedidos as $key => $ped) {
                
                $ped->update(['caja_fiscal' => 1]);

            }

            $pedidos = Pedido::
                with('cliente')
                ->orderBy("id",'desc')
                ->where('fecha_registro',">=",$desde)
                ->where('fecha_registro',"<=",$hasta)
                ->where('caja_fiscal',0)
                ->get();

            return $pedidos;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBuscar(Request $request){


        $desde =  Carbon::parse($request->desde)->format('Y-m-d 00:00:00');
        $hasta =  Carbon::parse($request->hasta)->format('Y-m-d 23:59:59');
        
        $pedidos = Pedido::
                    with('cliente')
                    ->orderBy("id",'desc')
                    ->where('fecha_registro',">=",$desde)
                    ->where('fecha_registro',"<=",$hasta)
                    ->where('caja_fiscal',$request->caja)
                    ->get();

        $totaltotal = 0;
       
        foreach($pedidos as $pedido){
          
            $pedido->monto_format = number_format($pedido->monto,2,',','.');
            $pedido->fecha_reistro_format = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i A');
            
            $Productovendido = Productovendido::
                                with('productos','promocion')
                                ->where('id_pedido',$pedido->id)
                                ->get();
          
            $Productovendido[0]->monto_total = $pedido->monto_format;
           
            foreach($Productovendido as $pv){

                $pv->monto_format = number_format($pv->monto,2,',','.');
                $pv->subtotal = number_format($pv->cantidad * $pv->monto,2,',','.');
            }

            $pedido->productosvendidos = $Productovendido;
            $totaltotal = $totaltotal + $pedido->monto;
        }


        if (count($pedidos) > 0) {
            $pedidos[0]->total_total = $totaltotal;
        }

        return $pedidos;

    }

    public function getExportarPedidos(Request $request){


        $desde =  Carbon::parse($request->desde)->format('Y-m-d 00:00:00');
        $hasta =  Carbon::parse($request->hasta)->format('Y-m-d 23:59:59');
        
        $pedidos = Pedido::
            with('cliente')
            ->orderBy("id",'desc')
            ->where('fecha_registro',">=",$desde)
            ->where('fecha_registro',"<=",$hasta)
            ->get();

    

        $transacciones_array = [];       
        foreach($pedidos as $pedido){
            $totaltotal = 0;
         
            
            $pedido->monto_format = number_format($pedido->monto,2,',','.');
            
            $pedido->fecha_reistro_format = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i A');
            
            $Productovendido = Productovendido::
            with('productos','promocion')
            ->where('id_pedido',$pedido->id)
            ->get();
          
            $Productovendido[0]->monto_total = $pedido->monto_format;

            $cliente = Cliente::where('codigo',$pedido->id_codigo)->first();
            if(!empty($cliente)){
                $nombre = $cliente->nombre_alumno.' '.$cliente->apellido_alumno; 
                
            }else{
                $nombre = 'No existe';
            }
            $new_array['id'] = $pedido->id;
            $new_array['cliente'] = $nombre;
            $new_array['monto_total'] = $pedido->monto_format;
            $new_array['medio_pago'] = $pedido->tipo_pago;
            $new_array['caja_fiscal'] = $pedido->caja_fiscal == 0 ? 'NO' : 'SI';
            $new_array['fecha_registro'] = $pedido->fecha_registro;
            $transacciones_array[] = $new_array;                     
        }

        

        $nombre_archivo = 'REPORTE DE VENTA'.date('Ymdhis');
        Excel::create($nombre_archivo, function($excel) use ($transacciones_array) {

                $excel->sheet('REPORTE DE VENTA', function($sheet) use($transacciones_array) {
                    $sheet->setAutoSize(true);
                    $sheet->cell('A1','ID');
                    $sheet->cell('B1','CLIENTE');
                    $sheet->cell('C1','MONTO');
                    $sheet->cell('D1','MEDIO DE PAGO');
                    $sheet->cell('E1','PASADO A CAJA FISCAL');
                    $sheet->cell('F1','FECHA');
                    
                    $sheet->row(1, function($row) {
                        $row->setFontSize(13);
                        $row->setFontWeight('bold');
                    });
                    $sheet->cells('A1:G1', function ($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->fromArray($transacciones_array, null, 'A2', true,false);
                });                  
            })->download('xlsx'); 


    }    
}
