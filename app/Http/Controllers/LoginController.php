<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Producto;
use App\Cliente;
use App\Mensajero;
use App\Statustrx;
use Carbon\Carbon;
use App\Trx;
use App\Usuario;
use App\Zona;
use App\PromocionesVendidas;
use App\PagoCombinado;
use App\ProductoVendido;
use App\Seguimientopedido;
use App\User;
use App\Role;
use App\Promociones;
use Auth;
use Hash;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('admin.login');
    }
    
    public function postLogin(Request $request)
    {

        
        
        $username = $request->username;
        $user = User::whereUsername($username)->first();
        if(empty($user)){
            return redirect('login')->with('error','Usuario no encontrado');
        }
        
        $password = $request->password;
        if ( $user && Hash::check($password ,$user->password)){
        //    return Hash::check($password ,$user->password) ? 'si': 'no';
            
            Auth::loginUsingId($user->id,true);
            
            if (Auth::check()) {
                $user = Auth::user();
                $auth = $user->roles()->first();

                switch ($auth->name) {
                        case 'superadmin':
                            return  redirect('dashboard');  
                        break;
                        case 'administrador':
                            return  redirect('dashboard');  
                        break;
                        case 'cajero':
                            return  redirect('ventas');  
                        break;
                    } 
            }
        }else{
            return redirect('login')->with('error','Password es incorrecto');
        }    
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
