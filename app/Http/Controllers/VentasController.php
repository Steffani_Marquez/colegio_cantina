<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categoria;
use App\Productos;
use App\Cliente;
use App\Carrito;
use App\Promociones;
use App\Pedido;
use App\Reserva;
use App\Recarga;
use App\Carritorecarga;
use App\Carritoporvalidar;
use App\Productodeposito;
use App\Productopromociones;
use App\Productovendido;
use Carbon\Carbon;
use Mail;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        
        $categorias = Categoria::
                    get();

        $codidoinvitado = strtoupper(str_random(16));

        return view('Ventas.ventas')
                    ->with('categorias',$categorias)
                    ->with('codidoinvitado',$codidoinvitado);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBuscar()
    {
       
        $productos = Productos::where('tipo','NORMAL')->where('status',1)->get();

        foreach ($productos as $key => $pro) {
           
            $productodepo = Productodeposito::
                            where('id_producto',$pro->id)
                            ->first();
            
            if (empty($productodepo)) {
                $pro->agotado = 1;
                $pro->cantisto =  0;
            }else{
            
                if ($productodepo->cantidad < 1) {
                   $pro->agotado =  1;
                   $pro->cantisto =  $productodepo->cantidad;
                }else{
                   $pro->agotado =  0;
                   $pro->cantisto =  $productodepo->cantidad;
                }
            }

        }

        return $productos;
    }


     public function getRecargar(Request $request)
    {         

                $carrritocarga = Carritorecarga::
                            where('codigo_familiar',$request->codigo_familiar)
                            ->get();

              

                $montototal = 0;

                foreach ($carrritocarga as $key => $car) {
                   $montototal = (float)$montototal + (float)$car->monto;
                }

                if ($montototal <= 0) {
                    return 0;
                }

                if ($request->tipo_pago == "zelle") {
                   $ref = $request->refezelle;
                }

                if ($request->tipo_pago == "pago movil") {
                   $ref = $request->refepagomovil;
                }

                if ($request->tipo_pago == "paypal") {
                    $ref = $request->refepaypal;
                }

                if ($request->tipo_pago == "efectivo") {
                    $ref = $request->refeefectivo;
                }

                if ($request->tipo_pago == "punto de venta") {
                    $ref = 0;
                }


                $recarga1 =  Recarga::create([
                        'id_cliente' => $request->id_cliente,
                        'fecha_registro' => Carbon::now('America/Caracas'),
                        'monto' => $montototal,
                        'codigo_familiar' => $request->codigo_familiar,
                        'tipo_pago' => $request->tipo_pago,
                        'validado' => 1,
                        'referencia_pago' => $ref,
                        'tasabs' => $request->tasa,
                        'telefono' => $request->teleinfo,
                        'origen' => "Modulo de ventas"
                ]);


                foreach ($carrritocarga as $key => $car) {
                   
                        $carritopor =  Carritoporvalidar::create([
                            'id_cliente' => $car->id_cliente,
                            'fecha_registro' => Carbon::now('America/Caracas'),
                            'monto' => $car->monto,
                            'id_recarga' => $recarga1->id,
                            'codigo_familiar' => $request->codigo_familiar,
                            'valido' => 1,
                        ]);

                }

                $recargas = Recarga::
                                where('id',$recarga1->id)
                                ->with("carrito")
                                ->first();


                foreach ($recargas->carrito as $key => $caror) {

                            $clien = Cliente::
                                        where('id',$caror->cliente->id)
                                        ->first();

                            if (($recargas->tipo_pago == "pago movil") or ($recargas->tipo_pago == "punto de venta")) {

                                if ((float)$clien->abono_actual < 0 ) {
                                    
                                    $montodolis = (float)$caror->monto / (float)$recargas->tasabs;
                                    $nuevosaldodolar = (float)$clien->abono_actual + (float)$montodolis;

                                    if ($nuevosaldodolar < 0 ) {
                                        $clien->update(['abono_actual' => $nuevosaldodolar]);
                                    }else{
                                        $clien->update(['abono_actual' => 0]);
                                        $nuevomontobs = (float)$nuevosaldodolar * (float)$recargas->tasabs;
                                        $clien->update(['abono_bs' => $nuevomontobs + (float)$clien->abono_bs]);
                                    }
                                }else{
                                    $clien->update(['abono_bs' => (float)$caror->monto + (float)$clien->abono_bs]);
                                }

                            }else{
                                    $clien->update(['abono_actual' => (float)$caror->monto + (float)$clien->abono_actual]);
                            }

                            
                }

                
                // $cliente = Cliente::
                //             where('codigo_familiar',$request->codigo_familiar)
                //             ->lists('correo_alumno');
                
                
                // self::getEmailNotifica($cliente);

                return 1;
            

    }



    public function getBuscarpromos()
    {
        $promociones = Promociones::
                        where('status',1)
                        ->get();


        foreach ($promociones as $key => $pro) {
           
            $producpromo = Productopromociones::
                            where('id_promocion',$pro->id)
                            ->get();

            $pro->agotado = 0;

            foreach ($producpromo as $key => $value) {
               
               $productodepo = Productodeposito::
                            where('id_producto',$value->id_producto)
                            ->first();

                if (empty($productodepo)) {
                                $pro->agotado = 1;
                }else{
               
                    if ($productodepo->cantidad < $value->cant_producto) {
                        $pro->agotado = 1;
                    }
                 }

            }

        }



        return $promociones;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getBuscarcategorias(Request $request)
    {

        if ($request->categoria == "Todos") {
            $productos = Productos::where('tipo','NORMAL')->where('status',1)->get();
        }else{
            
            $productos = Productos::where('id_categoria',$request->categoria)->where('status',1)->where('tipo','NORMAL')->get();

        }



        foreach ($productos as $key => $pro) {
               
            $productodepo = Productodeposito::
                            where('id_producto',$pro->id)
                            ->first();


            if (empty($productodepo)) {
                $pro->agotado =  1;
                $pro->cantisto =  0;
            }else{
                                        
                    if ($productodepo->cantidad < 1) {
                        $pro->agotado =  1;
                        $pro->cantisto =  $productodepo->cantidad;
                    }else{
                        $pro->agotado =  0;
                        $pro->cantisto =  $productodepo->cantidad;
                    }
            }

        }

        return $productos;

    }


     public function getBuscarcategoriaspromos(Request $request)
    {

        if ($request->categoria == "Todos") {
            $productos = Promociones::where('status',1)->get();
        }else{
            $productos = Promociones::
                     where('id_categoria',$request->categoria)
                     ->where('status',1)
                     ->get();

        }


                    foreach ($productos as $key => $pro) {
                       
                        $producpromo = Productopromociones::
                                        where('id_promocion',$pro->id)
                                        ->get();

                        $pro->agotado = 0;

                        foreach ($producpromo as $key => $value) {
                           
                           $productodepo = Productodeposito::
                                        where('id_producto',$value->id_producto)
                                        ->first();

                            if (empty($productodepo)) {
                                $pro->agotado = 1;
                            }else{
                           
                                if ($productodepo->cantidad < $value->cant_producto) {
                                    $pro->agotado = 1;
                                }
                            }

                        }

                    }

        return $productos;

    }


    public function getBuscarcliente2(Request $request)
    {

            $cliente = Cliente::
                     where('codigo',$request->codigocliente)
                     ->first();

            if (empty($cliente)) {
              
               $cliente = Cliente::
                     where('correo_alumno',$request->codigocliente)
                     ->first();

            }

            
            if (empty($cliente)) {
                return 0;
            }
           
            //Parche mientras se encuentra el lugar donde se esta actualizando a vacio el monto del abono
            if ($cliente->abono_actual == "") {
                 $cliente->update(['abono_actual' => 0]);
            }

            $pedidos = Pedido::
                     where('id_codigo',$cliente->codigo)
                     ->get();

            $pedido = "";


            $total = 0;

            foreach ($pedidos as $key => $value) {
                $total = (float)$total + (float)$value->monto;
            }

            if (count($pedidos) > 0) {

                $pedido = Pedido::
                            where('id_codigo',$cliente->codigo)
                            ->orderBy("id",'desc')
                            ->first();

                $pedido->fecha = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i:s A');
               
                $productovendido = Productovendido::
                                    where('id_pedido',$pedido->id)
                                    ->with("productos")
                                    ->with("promocion")
                                    ->get();
                
                $cliente->productovendido = $productovendido;
            }


            $reservas = Reserva::
                         where('id_codigo',$cliente->codigo)
                         ->with("carrito.producto")
                         ->with("carrito.promocion")
                         ->orderBy("fecha_reserva",'asc')
                         ->where('status',1)
                         ->get();

            foreach ($reservas as $key => $res) {
                $res->fecharees = Carbon::parse($res->fecha_reserva)->format('d/m/Y');
            }

            $total = number_format($total,2,',','.');
            $cliente->numpendientesreservas = count($reservas);
            $cliente->numpedidos = count($pedidos);
            $cliente->total = $total;
            $cliente->pedido = $pedido;
            $cliente->reservaspen = $reservas;

            $cliente->abonodolar = number_format($cliente->abono_actual,2,',','.');
            $cliente->abonobolivar = number_format($cliente->abono_bs,2,',','.');
                
            $cliente->totalcontasa = number_format(((float)$cliente->abono_bs / (float)$request->tasa_actual),2,',','.');
            $cliente->totaldola = number_format(((float)$cliente->abono_bs / (float)$request->tasa_actual) + (float)$cliente->abono_actual,2,',','.');

            return $cliente;
      

    }

    


     public function getElegirreserva(Request $request)
    {
          

        $cliente = Cliente::
                     where('codigo',$request->codigo_personal)
                     ->first();

        if (empty($cliente)) {
            $cliente = Cliente::
                     where('correo_alumno',$request->codigo_personal)
                     ->first();
        }


        $carrito_actual = Carrito::
                     where('codigo_cliente',$cliente->codigo)
                     ->where('reserva_id',$request->id)
                     ->with("producto")
                     ->with("promo")
                     ->get();


        $carrito = $carrito_actual;
        
        
        $totaltotal = 0;

        if (count($carrito) > 0) {
            
            foreach ($carrito as $key => $val) {
                
                $val->update(['codigo_cliente' => $cliente->codigo]); 

                if ($val->tipo =="Promo") {
                   
                        $costopro = (float)$val->promo->costo;
                        $val->costopro = (float)$val->promo->costo;
                        $val->imagenpro = $val->promo->imagen;
                        $val->nombrepro = $val->promo->nombre;
                        $val->tipo = "Promo";
                        $val->idpro = $val->promo->id;

                        $producpromo = Productopromociones::
                                            where('id_promocion',$val->promo->id)
                                            ->with("producto")
                                            ->get();

                        $val->agotado = 0;

                        foreach ($producpromo as $key => $value) {
                                   
                            $productodepo = Productodeposito::
                                        where('id_producto',$value->id_producto)
                                        ->first();
                                   
                            if ($productodepo->cantidad < ($value->cant_producto * $val->cantidad)) {
                                $val->agotado = 1;
                                $value->canti = $productodepo->cantidad;
                                $value->mostrarsi = 1;
                            }else{
                                 $value->mostrarsi = 0;
                            }

                        }

                        $val->productpromo = $producpromo;
             
                }else{
                      
                        $costopro = (float)$val->producto->costo;
                        $val->costopro  = (float)$val->producto->costo;
                        $val->imagenpro = $val->producto->imagen;
                        $val->nombrepro = $val->producto->nombre;
                        $val->tipo = "Producto";
                        $val->idpro = $val->producto->id;

                        $productodepo = Productodeposito::
                                    where('id_producto',$val->producto->id)
                                    ->first();
                                    
                        if ($productodepo->cantidad < $val->cantidad) {
                            $val->agotado =  1;
                            $val->cantiagot =  $productodepo->cantidad;
                        }else{
                            $val->agotado =  0;
                            $val->cantiagot =  $productodepo->cantidad;
                        }

                } 

                $val->totalindi =  (float)$costopro * (float)$val->cantidad;
                $totaltotal = (float)$totaltotal + (float)$costopro * (float)$val->cantidad;

                $totaltotal = number_format($totaltotal, 2, '.', '');
            }

              $carrito[0]->totaltotal = $totaltotal;
              $cliente->tienecar = 1;
            
        }else{

          $cliente->tienecar = 0;   
        }

        $cliente->carrito = $carrito; 

        return $cliente;


    }


    public function getBuscarcliente3(Request $request)
    {   

        $cliente = Cliente::
                    where('codigo',$request->codigocliente)
                    ->first();

            if (empty($cliente)) {
                  
                  $cliente = Cliente::
                    where('correo_alumno',$request->codigocliente)
                    ->first();
            }
       
                 
            $carrritocarga = Carritorecarga::
                                    where('codigo_familiar',$cliente->codigo_familiar)
                                    ->get();

            foreach ($carrritocarga as $key => $val) {
               $val->delete();
            }

            $familias = Cliente::
                        where('codigo_familiar',$cliente->codigo_familiar)
                         ->get();

            return $familias;
            
    
    }




     public function getBuscarcliente(Request $request)
    {

            
            $cliente = Cliente::
                     where('correo_alumno',$request->email)
                     ->where('passwordalumno',$request->password)
                     ->first();

            
            if (empty($cliente)) {
               return 0;
            }

            $pedidos = Pedido::
                     where('id_codigo',$request->codigocliente)
                     ->get();

            $pedido = "";
            $total = 0;

            foreach ($pedidos as $key => $value) {
                $total = (float)$total + (float)$value->monto;
            }

            if (count($pedidos) > 0) {

                $pedido = Pedido::
                     where('id_codigo',$request->codigocliente)
                     ->orderBy("id",'desc')
                     ->first();

                $pedido->fecha = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i:s A');
                $productovendido = Productovendido::
                     where('id_pedido',$pedido->id)
                     ->with("productos")
                     ->with("promocion")
                     ->get();
                
                $cliente->productovendido = $productovendido;
            }


             $reservas = Reserva::
                     where('id_codigo',$request->codigocliente)
                     ->where('status',1)
                     ->get();

            $cliente->numpedidos = count($pedidos);
            $cliente->total = $total;
            $cliente->pedido = $pedido;
            $cliente->numpendientesreservas = count($reservas);


            return $cliente;
      

    }
    

    


    public function getBuscarclientecarrito(Request $request)
    {
          
            $cliente = Cliente::
                     where('codigo',$request->codigo_personal)
                     ->first();


            $reservas = Reserva::
                     where('id_codigo',$request->codigo_personal)
                     ->where('status',1)
                     ->get();

            $cliente->reservas = $reservas;
            $cliente->numpendientesreservas = count($reservas);


            return $cliente;
       

    }


    public function getFinalizarpago(Request $request)
    {
               
                $hoy = Carbon::now('America/Caracas');



                if ($request->reserva_elegida != 0) {
                   
                    $carrito = Carrito::
                         where('codigo_cliente',$request->codigo_personal)
                         ->with('producto')
                         ->with("promo")
                         ->where("reserva_id",$request->reserva_elegida)
                         ->get();
                
                }else{
                    
                    $carrito = Carrito::
                         where('codigo_cliente',$request->codigo_personal)
                         ->where("reserva_id",0)
                         ->with('producto')
                         ->with("promo")
                         ->get();
                }

                if (count($carrito) == 0) {
                    return "Debes agregar productos al carrito";
                }

                $cliente = Cliente::
                                 where('codigo',$request->codigo_personal)
                                 ->first();

                $total = 0;

                foreach ($carrito as $key => $val){

                    if ($val->tipo == "Promo") {

                        $costopro = (float)$val->promo->costo;
                        $val->costopro = (float)$val->promo->costo;
                        $val->imagenpro = $val->promo->imagen;
                        $val->nombrepro = $val->promo->nombre;
                        $val->tipo = "Promo";
                        $val->idpro = $val->promo->id;

                        $producpromo = Productopromociones::
                                            where('id_promocion',$val->promo->id)
                                            ->with("producto")
                                            ->get();

                        foreach ($producpromo as $key => $value) {
                                   
                            $productodepo = Productodeposito::
                                        where('id_producto',$value->id_producto)
                                        ->first();
                                   
                            if ($productodepo->cantidad < ($value->cant_producto * $val->cantidad)) {
                                 return "No posees stock suficiente en uno de tus productos para realizar esta compra";
                            }

                        }

                    $val->productpromo = $producpromo;


                    }else{
                       
                        $costopro = (float)$val->producto->costo;
                        $val->costopro = (float)$val->producto->costo;
                        $val->imagenpro = $val->producto->imagen;
                        $val->nombrepro = $val->producto->nombre;
                        $val->tipo = "Producto";
                        $val->idpro = $val->producto->id;


                        $productodepo = Productodeposito::
                                    where('id_producto',$val->producto->id)
                                    ->first();
                                    
                        if ($productodepo->cantidad < $val->cantidad) {
                            return "No posees stock suficiente en uno de tus productos para realizar esta compra";
                        }


                    } 


                    $total = (float)$total + ((float)$costopro * (float)$val->cantidad); 

                }

                $menosdolar = 0;
                $menosbs = 0;



                if ($request->tipo == "abonado") {

                                        
                    if (empty($cliente)) {
                        return "No hay ningun cliente registrado, por lo tanto no puedes pagar con abonado";
                    }

                    $totalbs = ((float)$cliente->abono_bs / (float)$request->tasa);

                    $hoytarde =  Carbon::now('America/Caracas')->format('Y-m-d 23:59:59');
                    $hoytemprano =  Carbon::now('America/Caracas')->format('Y-m-d 00:00:00');


                    $montohoy = Pedido::
                                  where('fecha_registro',"<",$hoytarde)
                                  ->where('fecha_registro',">",$hoytemprano)
                                  ->where('tipo_pago',"abonado")
                                  ->where('id_codigo',$cliente->codigo)
                                  ->sum('monto'); 
                     

                    $montoparahoy = (float)$montohoy + (float)$total;

                    $montopermitidohoy = (float)$cliente->limitediario - (float)$montohoy;

                    if ((float)$montohoy > (float)$cliente->limitediario ) {
                        $montopermitidohoy = 0;
                    }

                    if ((float)$montohoy > (float)$cliente->limitediario ) {
                        return "Lo sentimos, con este pedido excedes el monto de tu limite diario permitido. tu limite restante para hoy es: ".$montopermitidohoy."$";
                    }


                    $montobolos = (float)$cliente->abono_bs / (float)$request->tasa;

                    if ((float)$cliente->monto_deuda_max  < ( (((float)$cliente->abono_actual + $montobolos)  * -1) + (float)$total ) ) {
                        return "Lo sentimos, con este pedido superas el limite de credito permitido";
                    }
                       
      
                    $cajafiscal = 0;
                    $resta = (float)$totalbs - (float)$total;

                    $menosdolar = 0;
                    $menosbs = 0;

                    if ($resta < 0) {

                        $menosbs    = (float)$cliente->abono_bs;
                        
                        $cliente->update(['abono_bs' => 0]);
                        $resta  = (float)$total - (float)$totalbs;
                        $resta2 = (float)$cliente->abono_actual - (float)$resta;
                        $cliente->update(['abono_actual' => $resta2]);

                        $menosdolar = (float)$resta;
                        

                    }else{

                        $menosdolar = (float)$menosdolar;
                        $menosbs    = (float)$total *  (float)$request->tasa;

                        $sumbs = (float)$resta *  (float)$request->tasa;
                        $cliente->update(['abono_bs' => $sumbs]);
                    }

                   
                    $clientes = Cliente::where('codigo_familiar',$cliente->codigo_familiar)->where('tipo_cliente_id',2)->lists('correo_alumno');
                    $clientes[] = $cliente->correo_alumno;
                   
                    if( ($cliente->abono_actual <= 0) and ($cliente->abono_bs <= 0) ){
                        //return "entre";
                        self::getEmailNotifica($clientes,$cliente);
                    }


                }else{
                  
                    $cajafiscal = 1; 
                    $menosdolar = 0;
                    $menosbs = 0;

                }


                if ($request->tipo == "efectivo") {

                    if ($request->cash_cliente == "") {
                        $request->cash_cliente = 0;
                    }

                    if ($request->vuelto_cliente == "") {
                        $request->vuelto_cliente = 0;
                    }
                   
                    if ((float)$request->cash_cliente < (float)$total){
                        return "El monto cash, no puede ser menor al monto total del pedido";
                    }

                }


                $pedido =  Pedido::create([
                    'id_codigo' => $request->codigo_personal,
                    'monto' => $total,
                    'fecha_registro' => $hoy,
                    'tipo_pago' => $request->tipo,
                    'cash_cliente' => $request->cash_cliente,
                    'caja_fiscal' => $cajafiscal,
                    'menosbs' => $menosbs,
                    'menosdolar' => $menosdolar

                ]);


                if ($request->reserva_elegida != 0) {

                    $reserele = Reserva::
                                    where('id',$request->reserva_elegida)
                                    ->first();

                    $reserele->update(['status' => 2]);

                }

                foreach ($carrito as $key => $value) {

                    
                    if ($value->tipo == "Producto") {
                        
                            $creado =  Productovendido::create([
                                'id_pedido' => $pedido->id,
                                'id_producto' => $value->producto->id,
                                'monto' => $value->producto->costo,
                                'fecha_registro' => $hoy,
                                'cantidad' => $value->cantidad,
                                'tipo' => "Producto"
                            ]);

                            $productodepo = Productodeposito::
                                                where('id_producto',$value->producto->id)
                                                ->first();
                                
                            $productodepo->update(['cantidad' => (float)$productodepo->cantidad - (float)$value->cantidad]);
                                

                    }else{

                            $creado =  Productovendido::create([
                                'id_pedido' => $pedido->id,
                                'id_producto' => $value->promo->id,
                                'monto' => $value->promo->costo,
                                'fecha_registro' => $hoy,
                                'cantidad' => $value->cantidad,
                                'tipo' => "Promo"
                            ]);


                            $productopromo = Productopromociones::
                                                 where('id_promocion',$value->promo->id)
                                                 ->get();
      
                            foreach ($productopromo as $key => $prop) {

                                $productodepo = Productodeposito::
                                                    where('id_producto',$prop->id_producto)
                                                    ->first();
                                
                                $productodepo->update(['cantidad' => (float)$productodepo->cantidad - ((float)$value->cantidad * (float)$prop->cant_producto)]);

                            }

                    }
                     
                        
                }

                foreach ($carrito as $key => $value){
                    $value->delete();
                }

                $cliente = Cliente::
                    where('codigo',$request->codigo_personal)
                    ->first();


                $objetoventas = [];


                $pedidowow = Pedido::
                                where('id',$pedido->id)
                                ->with('cliente')
                                ->with("productos.productos.categoria")
                                ->with("productos.promocion.categoria")
                                ->first();

                //return $pedidowow;

                $i = 0;

                foreach ($pedidowow->productos as  $prod_vend) {
                    $i++;
                    $new_array['sku'] = $prod_vend->id_producto;
                   
                    if($prod_vend->tipo == 'Producto'){

                         $new_array['name'] = $prod_vend->productos->nombre;
                         $new_array['category'] = $prod_vend->productos->categoria->nombre;
                         $new_array['price'] = $prod_vend->productos->costo;
                         $new_array['quantity'] = $prod_vend->cantidad;

                    }else{

                        $new_array['name'] = $prod_vend->promocion->nombre;
                        $new_array['category'] = $prod_vend->promocion->categoria->nombre;
                        $new_array['price'] = $prod_vend->promocion->costo;
                        $new_array['quantity'] = $prod_vend->cantidad;

                    }

                    $datos_fact[] = $new_array;
            }    

                $codidoinvitado = strtoupper(str_random(15));

                $pedidowow->datos_fact = $datos_fact;

                $pedidowow->codigoinv = $codidoinvitado;


                    
                if (!empty($cliente)) {
                  
                    $clientes = Cliente::
                                    where('codigo_familiar',$cliente->codigo_familiar)
                                    ->where('tipo_cliente_id',2)
                                    ->lists('correo_alumno');
                    
                    $clientes[] = $cliente->correo_alumno;
                    
                    self::getEmailFactura($clientes,$pedido->id);
                    
                }
                

                             

                return $pedidowow;
      
    }


    public function getVaciarcarrito(Request $request)
    {

            $carrito = Carrito::
                        where('codigo_cliente',$request->codigo_personal)
                        ->get();

            foreach ($carrito as $key => $car) {
               $car->delete();
            }
            
            return $carrito;
      
    }

  



     public function getActualizarcarrito(Request $request)
    {
         
            $hoy = Carbon::now('America/Caracas');


            if ($request->reserva_elegida != 0) {
                 $carrito = Carrito::
                                where('codigo_cliente',$request->codigo_personal)
                                ->where('id_producto',$request->id)
                                ->where('reserva_id',$request->reserva_elegida)
                                ->first();
            }else{
                $carrito = Carrito::
                                where('codigo_cliente',$request->codigo_personal)
                                ->where('id_producto',$request->id)
                                ->where('reserva_id',0)
                                ->first();
            }

            if ($request->tipo == "Promo") {
                
                $productosbusq = Promociones::
                                where('id',$request->id)
                                ->with("categoria")
                                ->first();
            }else{
                
                $productosbusq = Productos::
                                where('id',$request->id)
                                ->with("categoria")
                                ->first();
            }

            if (empty($carrito)) {

                if ($request->reserva_elegida != 0) {
                
                    $creado =  Carrito::create([
                        'id_producto'    => $request->id,
                        'codigo_cliente' => $request->codigo_personal,
                        'fecha_registro' => $hoy,
                        'cantidad' =>   $request->cantidad,
                        'tipo' =>       $request->tipo,
                        'reserva_id' => $request->reserva_elegida
                    ]);

                }else{

                    $creado =  Carrito::create([
                        'id_producto'    => $request->id,
                        'codigo_cliente' => $request->codigo_personal,
                        'fecha_registro' => $hoy,
                        'cantidad' => $request->cantidad,
                        'tipo' =>     $request->tipo,
                        'reserva_id' => 0
                    ]);

                }


            }else{

                if ($request->inicial == 0) {
                    $carrito->update(['cantidad' => $request->cantidad]);
                } 

                if ($carrito->cantidad == 0) {
                    $carrito->delete();
                }

            }


            if ($request->reserva_elegida != 0) {

                $carrito = Carrito::
                        where('codigo_cliente',$request->codigo_personal)
                        ->where('reserva_id',$request->reserva_elegida)
                        ->with("producto")
                        ->with("promo")
                        ->get();
            }else{

                $carrito = Carrito::
                        where('codigo_cliente',$request->codigo_personal)
                        ->where('reserva_id',0)
                        ->with("producto")
                        ->with("promo")
                        ->get();

            }


            $totaltotal = 0;



            foreach ($carrito as $key => $car) {

                 if ($car->tipo == "Promo") {
                        $costopro = (float)$car->promo->costo;
                        $car->costopro = (float)$car->promo->costo;
                        $car->imagenpro = $car->promo->imagen;
                        $car->nombrepro = $car->promo->nombre;
                        $car->tipo = "Promo";
                        $car->idpro = $car->promo->id;

                        $producpromo = Productopromociones::
                                            where('id_promocion',$car->promo->id)
                                            ->with("producto")
                                            ->get();

                        $car->agotado = 0;

                        foreach ($producpromo as $key => $value) {
                                   
                            $productodepo = Productodeposito::
                                        where('id_producto',$value->id_producto)
                                        ->first();
                                   
                            if ($productodepo->cantidad < ($value->cant_producto * $car->cantidad)) {
                                $car->agotado = 1;
                                $value->canti = $productodepo->cantidad;
                                $value->mostrarsi = 1;
                            }else{
                                 $value->mostrarsi = 0;
                            }

                        }

                        $car->productpromo = $producpromo;

                }else{
                        $costopro =      (float)$car->producto->costo;
                        $car->costopro = (float)$car->producto->costo;
                        $car->imagenpro = $car->producto->imagen;
                        $car->nombrepro = $car->producto->nombre;
                        $car->tipo = "Producto";
                        $car->idpro = $car->producto->id;

                         $productodepo = Productodeposito::
                                    where('id_producto',$car->producto->id)
                                    ->first();
                                    
                        if ($productodepo->cantidad < $car->cantidad) {
                            $car->agotado =  1;
                            $car->cantiagot =  $productodepo->cantidad;
                        }else{
                            $car->agotado =  0;
                            $car->cantiagot =  $productodepo->cantidad;
                        }
                } 


               $car->totalindi =  (float)$costopro * (float)$car->cantidad;
               $totaltotal = (float)$totaltotal + (float)$costopro * (float)$car->cantidad;

               $totaltotal = number_format($totaltotal, 2, '.', '');
            }

            if (count($carrito) > 0) {
               $carrito[0]->totaltotal = $totaltotal;
               $carrito[0]->productosbusq = $productosbusq;
            }

            

            

            return $carrito;
      

    }

    public static function getEmailFactura($emailscliente,$id_txr)
    {
        
        try{
        

            $fecha = Carbon::now('America/Caracas')->format('d/m/Y');
            $nro_factura = $id_txr;
            $trx = Pedido::find($id_txr);
            $nombre_comprador =  !empty($trx->cliente->nombre_alumno) ? $trx->cliente->nombre_alumno.' '.$trx->cliente->apellido_alumno : '';
            $productos_vendidos = Productovendido::where('id_pedido',$trx->id)->with("productos")->with("promocion")->get();
            
            $datos_fact = [];
            $subtotal = 0;

            $moneda = '';

            $total = 0;
            $i = 0;
            $iva = 0;
            //id_forma_pago
            //tasa_monitordolar


            foreach ($productos_vendidos as  $prod_vend) {
                $i++;
                $new_array['numero'] = $i;
                $new_array['cantidad'] = $prod_vend->cantidad;

                if($prod_vend->tipo == 'Producto'){
                    $new_array['descripcion'] = $prod_vend->productos->nombre;
                    $new_array['precio_unitario'] = number_format($prod_vend->productos->costo,'2',',','.');
                    $subtotal += ($prod_vend->cantidad * $prod_vend->productos->costo);
                    $new_array['total'] = number_format($prod_vend->cantidad * $prod_vend->productos->costo,'2',',','.');


                }else{
                    $new_array['descripcion'] = $prod_vend->promocion->nombre;
                    $new_array['precio_unitario'] = number_format($prod_vend->promocion->costo,'2',',','.');
                    $subtotal += ($prod_vend->cantidad * $prod_vend->promocion->costo);
                    $new_array['total'] = number_format($prod_vend->cantidad * $prod_vend->promocion->costo,'2',',','.');

                }


                $datos_fact[] = $new_array;
            }
            
         
            $total = $subtotal;
            $data = [
                'nombre_comprador' => $nombre_comprador,
                'fecha' => $fecha,
                'datos_fact' => $datos_fact,
                'total' => $total,
                'subtotal' => $subtotal,
                'nro_factura' => $nro_factura,
                'moneda' => $moneda,
                
            ];
            
        
                
            $message = 'prueba';
            foreach($emailscliente as $email){
                if(!is_null($email)){
                    $emails[] = $email;
                }
            }
            $emails[] = 'eduardozapata290121@gmail.com';
            
            return  Mail::send('emails.factura', $data, function($message) use ($emails)
            {
                
                $message->from('no-reply@cumbres.cafe', 'Cumbres Café');
                
                $message->to($emails)->subject('SE HA HECHO UN CONSUMO');
                
            });
        }catch(\Exception $e) { 
            return $e->getMessage();
        }
    
    }
    public function getEmailNotifica($emailscliente,$cliente = null){
        
        $titulo  = 'SE AGOTA TU SALDO';
        $mensaje = 'Hola '.$cliente->nombre_alumno.' '.$cliente->apellido_alumno.', Te notificamos que tu saldo está por agotarse.';
        $mensaje2 = 'Te invitamos a que ingreses a nuestra plataforma y que recargues a la brevedad posible de tu saldo para que puedas seguir disfrutando de la experiencia Cumbres Café.';


        $data = [
            'titulo' => $titulo,
            'mensaje' => $mensaje,
            'mensaje2' => $mensaje2,
            ];
         
            $message = 'prueba';
            foreach($emailscliente as $email){
                if(!is_null($email)){
                    $emails[] = $email;
                }
            }
            $emails[] = 'eduardozapata290121@gmail.com';
        
            return  Mail::send('emails.recarga-notificacion', $data, function($message) use ($emails)
            {
                
                $message->from('no-reply@cumbres.cafe', 'Cumbres Café');
                
                $message->to($emails)->subject('SE AGOTA TU SALDO');
                
            });



    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
