<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categoria;
use App\Productos;
use App\Cliente;
use App\Auxiliar;
use App\Carrito;
use App\Pedido;
use App\Reserva;
use App\Productovendido;
use App\Productodeposito;
use App\Promociones;
use App\Productopromociones;
use App\User;
use App\Role;
use Auth;
use Hash;
use Carbon\Carbon;

class ComprasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        
        $categorias = Categoria::get();

        $hoy = Carbon::now('America/Caracas')->format('Y-m-d');
        $codidoinvitado = strtoupper(str_random(7));

        return view('Ventas.compras')
        ->with('categorias',$categorias)
        ->with('hoy',$hoy)
        ->with('codidoinvitado',$codidoinvitado);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBuscar()
    {
        $productos = Productos::
                        where('status',1)
                        ->get();

        return $productos;
    }


      public function getPreguntarlogin(Request $request)
    { 


            if (Auth::user()== "") {
               return 0;
            }else{

                $cliente = Cliente::
                    where('user_id',Auth::user()->id)
                    ->first();
              
                return $cliente;
            }
   

    }


       public function getReservarcompra(Request $request)
    {

       
        $hoy = Carbon::now('America/Caracas');
        $fechareserva = Carbon::parse($request->dia);
        $hoy2 = Carbon::now('America/Caracas')->format('Y-m-d 00:00:00');

        $cliente = Cliente::
                    where('id',$request->reservapara)
                    ->first();

        if ($hoy2 > $fechareserva) {
            return 3;
        }

        $carrito_actual = Carrito::
                    where('codigo_cliente',$request->codigo_personal)
                    ->where('reserva_id',0)
                    ->with("producto")
                    ->with("promo")
                    ->get();

        $totaltotal = 0;

        foreach ($carrito_actual as $key => $val) {


               if ($val->tipo =="Promo") {
                        
                        $costopro = (float)$val->promo->costo;
                        $producpromo = Productopromociones::
                                            where('id_promocion',$val->promo->id)
                                            ->with("producto")
                                            ->get();

                        foreach ($producpromo as $key => $value) {
                                   
                            $productodepo = Productodeposito::
                                        where('id_producto',$value->id_producto)
                                        ->first();
                                   
                            if ($productodepo->cantidad < ($value->cant_producto * $val->cantidad)) {
                                 return 2;
                            }

                        }


                }else{

                        $costopro = (float)$val->producto->costo;
                        $productodepo = Productodeposito::
                                    where('id_producto',$val->producto->id)
                                    ->first();
                                    
                        if ($productodepo->cantidad < $val->cantidad) {
                            return 2;
                        }
                } 

                $totalindi =  (float)$costopro * (float)$val->cantidad;
                $totaltotal = (float)$totaltotal + (float)$costopro * (float)$val->cantidad;
                $totaltotal = number_format($totaltotal, 2, '.', '');


        }


        if ( ((float)$cliente->abono_actual + (float)$cliente->monto_deuda_max) < $totaltotal) {
            return 0;
        }else{

               $creado =  Reserva::create([
                    'id_codigo' => $cliente->codigo,
                    'fecha_registro' => $hoy,
                    'status' => 1,
                    'horario' => $request->horario,
                    'fecha_reserva' => $request->dia
                    ]);


                foreach ($carrito_actual as $key => $val) {
                    $val->update(['reserva_id' => $creado->id]);
                }

            return 1;

        }


      
    }


    public function getBuscarauxiliar()
    {
        
        $auxiliar = Auxiliar::first();
        return $auxiliar;

    }

    public function getGuardardolir(Request $request)
    {
        
        $auxiliar = Auxiliar::first();
        $auxiliar->update(['dolar' => $request->valor]);

        return $auxiliar;

    }


    public function getGuardarcambios(Request $request)
    {
        
        $auxiliar = Auxiliar::first();
        $auxiliar->update(['zelle'  => $request->zelle]);
        $auxiliar->update(['zelle2' => $request->zelle2]);

        return $auxiliar;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getBuscarcategorias(Request $request)
    {

        if ($request->categoria == "Todos") {
            $productos = Productos::where('status',1)->get();
        }else{

             $productos = Productos::
                     where('id_categoria',$request->categoria)
                     ->where('status',1)
                     ->get();

        }

        return $productos;

    }


     public function getBuscarcliente(Request $request)
    {
       
      

            $cliente = Cliente::
                    where('correo_alumno',$request->email)
                    ->where('passwordalumno',$request->password)
                    ->first();
           
            $cliente->abono_actual_format = number_format($cliente->abono_actual,2,',','.');
            $cliente->abono_bs_format = number_format($cliente->abono_bs,2,',','.');
       
            if (empty($cliente)) {
                
                $cliente = Cliente::
                        where('cedula',$request->email)
                        ->where('passwordalumno',$request->password)
                        ->first();
            }
            
            if (empty($cliente)) {
               return 0;
            }

            $user = User::where('id',$cliente->user_id)->first();
            Auth::loginUsingId($user->id,true);
            $userlog = Auth::user();

            $pedidos = Pedido::
                       where('id_codigo',$request->codigocliente)
                       ->get();

            $pedido = "";
            $total = 0;

            foreach ($pedidos as $key => $value) {
              $total = (float)$total + (float)$value->monto;
            }

            if (count($pedidos) > 0) {

                $pedido = Pedido::
                     where('id_codigo',$request->codigocliente)
                     ->orderBy("id",'desc')
                     ->first();

                $pedido->fecha = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i:s A');
                $productovendido = Productovendido::
                     where('id_pedido',$pedido->id)
                     ->with("productos")
                     ->with("promocion")
                     ->get();
                
                $cliente->productovendido = $productovendido;
            }

            $hoy = Carbon::now('America/Caracas')->format('Y-m-d 00:00:00');

            $familia = Cliente::
                         where('codigo_familiar',$cliente->codigo_familiar)
                         ->get();


            $familialist = $familia->lists("codigo");
              
            $reservas = Reserva::
                             whereIn('id_codigo',$familialist)
                             ->with("carrito.producto")
                             ->with("carrito.promocion")
                             ->with("cliente")
                             ->orderBy("id",'desc')
                             ->where('fecha_reserva',">=",$hoy)
                             ->whereIn('status',[1,3])
                             ->get();

           
            foreach ($reservas as $key => $res) {
                
                $total = 0;

                $res->fecharees = Carbon::parse($res->fecha_reserva)->format('d/m/Y');
                $res->fecharegi = Carbon::parse($res->fecha_registro)->format('d/m/Y H:i A');

                foreach ($res->carrito as $key => $pi) {

                    if ($pi->tipo == "Promo") {
                        $total = $total +  $pi->promocion->costo;
                    }else{
                         $total = $total + $pi->producto->costo;
                    }
                }

                $res->totalres = $total;

            }

           

            foreach ($familia as $key => $value) {

                if ($value->tipo_cliente_id == 2) {
                  $value->tipo ="REPRESENTANTE";
                }else{
                  $value->tipo ="HIJO";
                }
            }

            $cliente->numpedidos = count($pedidos);
            $cliente->total = $total;
            $cliente->pedido = $pedido;
            $cliente->numpendientesreservas = count($reservas);
            $cliente->reservas = $reservas;
            $cliente->familias = $familia;

            

            return $cliente;
      

    }
    

    



     public function getActualizarcarrito(Request $request)
    {
         
            $hoy = Carbon::now('America/Caracas');
                
            $carrito = Carrito::
                    where('codigo_cliente',$request->codigo_personal)
                    ->where('id_producto',$request->id)
                    ->where('reserva_id',0)
                    ->first();

            if (empty($carrito)) {
                
                $creado =  Carrito::create([
                    'id_producto' => $request->id,
                    'codigo_cliente' => $request->codigo_personal,
                    'fecha_registro' => $hoy,
                    'cantidad' => $request->cantidad,
                    'tipo' => $request->tipo
                    ]);
            }else{

                if ($request->inicial == 0) {
                    $carrito->update(['cantidad' => $request->cantidad]);
                } 

                if ($carrito->cantidad == 0) {
                    $carrito->delete();
                }

            }


            $carrito = Carrito::
                    where('codigo_cliente',$request->codigo_personal)
                    ->with("producto")
                    ->where('reserva_id',0)
                    ->with("promo")
                    ->get();

            $totaltotal = 0;



            foreach ($carrito as $key => $car) {

                 if ($car->tipo == "Promo") {
                        $costopro = (float)$car->promo->costo;
                        $car->costopro = (float)$car->promo->costo;
                        $car->imagenpro = $car->promo->imagen;
                        $car->nombrepro = $car->promo->nombre;
                        $car->tipo = "Promo";
                        $car->idpro = $car->promo->id;

                        $producpromo = Productopromociones::
                                            where('id_promocion',$car->promo->id)
                                            ->with("producto")
                                            ->get();

                        $car->agotado = 0;

                        foreach ($producpromo as $key => $value) {
                                   
                            $productodepo = Productodeposito::
                                        where('id_producto',$value->id_producto)
                                        ->first();
                                   
                            if ($productodepo->cantidad < ($value->cant_producto * $car->cantidad)) {
                                $car->agotado = 1;
                                $value->canti = $productodepo->cantidad;
                                $value->mostrarsi = 1;
                            }else{
                                 $value->mostrarsi = 0;
                            }

                        }

                        $car->productpromo = $producpromo;

                }else{
                        $costopro =      (float)$car->producto->costo;
                        $car->costopro = (float)$car->producto->costo;
                        $car->imagenpro = $car->producto->imagen;
                        $car->nombrepro = $car->producto->nombre;
                        $car->tipo = "Producto";
                        $car->idpro = $car->producto->id;

                         $productodepo = Productodeposito::
                                    where('id_producto',$car->producto->id)
                                    ->first();
                                    
                        if ($productodepo->cantidad < $car->cantidad) {
                            $car->agotado =  1;
                            $car->cantiagot =  $productodepo->cantidad;
                        }else{
                            $car->agotado =  0;
                            $car->cantiagot =  $productodepo->cantidad;
                        }
                } 


               $car->totalindi =  (float)$costopro * (float)$car->cantidad;
               $totaltotal = (float)$totaltotal + (float)$costopro * (float)$car->cantidad;
               $car->totalindi = number_format($car->totalindi, 2, '.', '');

               $totaltotal = number_format($totaltotal, 2, '.', '');
            }

            if (count($carrito) > 0) {
               $carrito[0]->totaltotal = $totaltotal;
            }

            

            return $carrito;
      

    }


    public function getBuscarclientecarrito(Request $request)
    {

        
        $cliente = Cliente::
                     where('codigo',$request->codigo_actual)
                     ->first();

        $carrito_actual = Carrito::
                     where('codigo_cliente',$request->codigo_actual)
                     ->where('reserva_id',0)
                     ->with("producto")
                     ->get();


        $carrito = $carrito_actual;
        
                   
        $totaltotal = 0;

        if (count($carrito) > 0) {
            foreach ($carrito as $key => $val) {
                $val->update(['codigo_cliente' => $request->codigo_personal]); 

                 if ($val->tipo =="Promo") {
                    $costopro = (float)$val->promo->costo;
                    $val->costopro = (float)$val->promo->costo;
                    $val->imagenpro = $val->promo->imagen;
                    $val->nombrepro = $val->promo->nombre;
                    $val->tipo = "Promo";
                    $val->idpro = $val->promo->id;
                }else{
                    $costopro = (float)$val->producto->costo;
                    $val->costopro = (float)$val->producto->costo;
                    $val->imagenpro = $val->producto->imagen;
                    $val->nombrepro = $val->producto->nombre;
                    $val->tipo = "Producto";
                    $val->idpro = $val->producto->id;
                } 

                $val->totalindi =  (float)$costopro* (float)$val->cantidad;
                $totaltotal = (float)$totaltotal + (float)$costopro * (float)$val->cantidad;
                $totaltotal = number_format($totaltotal, 2, '.', '');

                $val->totalindi = number_format($val->totalindi, 2, '.', '');
            }

              $carrito[0]->totaltotal = $totaltotal;

              $cliente->tienecar = 1;
            
        }else{

          $cliente->tienecar = 0;   
        }

        $cliente->carrito = $carrito; 

        return $cliente;

    }


    public function getFinalizarpago(Request $request)
    {
               
                $hoy = Carbon::now('America/Caracas');

                $carrito = Carrito::
                     where('codigo_cliente',$request->codigo_personal)
                     ->with('producto')
                     ->get();

                if (count($carrito) == 0) {
                    return "Debes agregar productos al carrito";
                }

                $cliente = Cliente::
                     where('codigo',$request->codigo_personal)
                     ->first();

                $total = 0;

                foreach ($carrito as $key => $value){
                    $total = (float)$total + (float)$value->producto->costo; 
                }
                

                if ($request->tipo == "abonado") {
                    
                    if (empty($cliente)) {
                       return "No hay ningun cliente registrado, por lo tanto no puedes pagar con abonado";
                    }else{

                        if ((float)$cliente->abono_actual <  (float)$total ) {
                           return "No posees suficiente cantidad de abono para pagar";
                        }
                    }

                    $cliente->update(['abono_actual' => (float)$cliente->abono_actual - (float)$total]);

                }



                if ($request->tipo == "efectivo") {

                    if ($request->cash_cliente == "") {
                        $request->cash_cliente = 0;
                    }
                    if ($request->vuelto_cliente == "") {
                        $request->vuelto_cliente = 0;
                    }
                   
                    if ((float)$request->cash_cliente < (float)$total){
                        return "El monto cash, no puede ser menor al monto total del pedido";
                    }

                }


                $pedido =  Pedido::create([
                    'id_codigo' => $request->codigo_personal,
                    'monto' => $total,
                    'fecha_registro' => $hoy,
                    'tipo_pago' => $request->tipo,
                    'cash_cliente' => $request->cash_cliente,
                    'vuelto_cliente' => $request->vuelto_cliente
                ]);

                foreach ($carrito as $key => $value) {
                     
                     $creado =  Productovendido::create([
                        'id_pedido' => $pedido->id,
                        'id_producto' => $value->producto->id,
                        'monto' => $value->producto->costo,
                        'fecha_registro' => $hoy,
                        'cantidad' => $value->cantidad
                        ]);
                }

                    
                foreach ($carrito as $key => $value){
                    $value->delete();
                }

                return 0;
      
    }


    public function getVaciarcarrito(Request $request)
    {

            $carrito = Carrito::
                     where('codigo_cliente',$request->codigo_personal)
                     ->get();

            foreach ($carrito as $key => $car) {
               $car->delete();
            }
            
            return $carrito;
      
    }



     public function getActualizarcarritoinicio(Request $request)
    {
         
            $hoy = Carbon::now('America/Caracas');

            $sinstock = 0;
                
            $carrito = Carrito::
                    where('codigo_cliente',$request->codigo_personal)
                    ->where('id_producto',$request->id)
                    ->where('reserva_id',0)
                    ->first();

            if (empty($carrito)) {
                $canticarates = 0;
            }else{
               $canticarates = $carrito->cantidad;
            }

            if ($request->tipo == "Promo") {
                
                $productosbusq = Promociones::
                                where('id',$request->id)
                                ->with("categoria")
                                ->first();
            }else{
                
                $productosbusq = Productos::
                                where('id',$request->id)
                                ->with("categoria")
                                ->first();
            }
            
            if ($request->tipo == "Producto") {

                $productodepo = Productodeposito::
                                where('id_producto',$request->id)
                                ->first();
                                        
                if ($productodepo->cantidad < ($canticarates + $request->cantidad)) {
                   $sinstock = 1;
                }
                
            }else{

                $producpromo = Productopromociones::
                                    where('id_promocion',$request->id)
                                    ->with("producto")
                                    ->get();

                foreach ($producpromo as $key => $value) {
                                   
                    $productodepo = Productodeposito::
                                where('id_producto',$value->id_producto)
                                ->first();
                                   
                    if ($productodepo->cantidad < (($value->cant_producto * $request->cantidad) + $canticarates)) {
                       $sinstock = 1;
                    }

                }
            }

            if ($sinstock == 0) {

                    if (empty($carrito)) {
                        
                        $creado =  Carrito::create([
                            'id_producto' => $request->id,
                            'codigo_cliente' => $request->codigo_personal,
                            'fecha_registro' => $hoy,
                            'cantidad' => $request->cantidad,
                            'tipo' => $request->tipo
                            ]);
                    }else{
                            $carrito->update(['cantidad' => (float)$carrito->cantidad + (float)$request->cantidad]);
                        if ($carrito->cantidad == 0) {
                            $carrito->delete();
                        }

                    }

            }

            $carrito = Carrito::
                    where('codigo_cliente',$request->codigo_personal)
                    ->where('reserva_id',0)
                    ->with("promo")
                    ->with("producto")
                    ->get();
           

            $totaltotal = 0;

            foreach ($carrito as $key => $car) {

                 if ($car->tipo =="Promo") {
                    $costopro = (float)$car->promo->costo;
                    $car->costopro = (float)$car->promo->costo;
                    $car->imagenpro = $car->promo->imagen;
                    $car->nombrepro = $car->promo->nombre;
                    $car->tipo = "Promo";
                    $car->idpro = $car->promo->id;
                }else{
                    $costopro = (float)$car->producto->costo;
                    $car->costopro = (float)$car->producto->costo;
                    $car->imagenpro = $car->producto->imagen;
                    $car->nombrepro = $car->producto->nombre;
                    $car->tipo = "Producto";
                    $car->idpro = $car->producto->id;

               } 


                $car->totalindi =  (float)$costopro * (float)$car->cantidad;
                $totaltotal = (float)$totaltotal + (float)$costopro * (float)$car->cantidad;
                $totaltotal = number_format($totaltotal, 2, '.', '');
                $car->totalindi = number_format($car->totalindi, 2, '.', '');
            }

            if (count($carrito) > 0) {
               $carrito[0]->totaltotal = $totaltotal;
               $carrito[0]->sinstock = $sinstock;
               $carrito[0]->productosbusq = $productosbusq;
            }else{
                return 0;
            }

            

            return $carrito;
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
