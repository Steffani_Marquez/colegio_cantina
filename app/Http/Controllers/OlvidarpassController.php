<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Carritorecarga;
use App\Cliente;
use App\Recarga;
use App\Pedido;
use App\Carritoporvalidar;
use Carbon\Carbon;
use Mail;

class OlvidarpassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        return view('Ventas.olvidopass');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getEnviardatos(Request $request)
    {


         $cliente = Cliente::
                    where('correo_alumno',$request->email)
                    ->first();
       
        if (empty($cliente)) {
            
            $cliente = Cliente::
                    where('cedula',$request->email)
                    ->first();
        }

        if (empty($cliente)) {
            return 0;
        }else{
           $cliente->update(['num_entradas' => 0]); 
        }

        $clienteemails = $cliente->correo_alumno;
        $passcliente = $cliente->passwordalumno;
        
        self::getEmailNotifica($clienteemails,$passcliente); 


        return $clienteemails;

    }


    public function getEmailNotifica($emailscliente,$passcliente){

         


        $titulo  = 'RECUPERACIÓN DE CONTRASEÑA';
        $mensaje = 'Has intendado recuperar tu contraseña';
        $mensaje2 = 'Contaseña actual: '." ".$passcliente;
        $mensaje3 = 'Usuario actual: '." ".$emailscliente;

        $data = [
            'titulo' =>   $titulo,
            'mensaje' =>  $mensaje,
            'mensaje2' => $mensaje2,
            'mensaje3' => $mensaje3
        ];
        

        $emails[] = $emailscliente;
        
        return  Mail::send('emails.recuperacion', $data, function($message) use ($emails){
            $message->from('no-reply@cumbres.cafe', 'Cumbres Café');
            $message->to($emails)->subject('RECUPERACIÓN DE CONTRASEÑA');
        });


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
