<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cliente;
use Hash;
use Auth;
use App\User;
use Carbon\Carbon;
use Mail;

class PruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        // $Clientes = Cliente::all();
        // $repetidos = [];
        // $cdedulas = [];
        // foreach($Clientes as $cliente){
        //     $clie = Cliente::where('cedula',$cliente->cedula)->where('correo_alumno',$cliente->correo_alumno)->where('nombre_alumno',$cliente->nombre_alumno)->where('tipo_cliente_id',3)->count();
        //     if($clie > 1){
        //         if(!in_array($cliente->cedula,$cdedulas)){
        //             $repetidos[] = $cliente;
        //             $cdedulas[] = $cliente->cedula;
        //         }
        //     }
        // }
        // return $repetidos;

        $clientes = Cliente::get();


        foreach ($clientes as $key => $clien) {

           // $pass = Hash::make($clien->passwordalumno);
            
            $user = User::create([
                 'name' => $clien->nombre
                ,'username' => $clien->correo_alumno
                ,'email' => $clien->correo_alumno
                ,'password' => 0000
                ,'remember_token' => 0000
                ,'phone' => $clien->telefono
                ,'active' => 1
            ]);

            $clien->update(['user_id' => $user->id]);
            $user->roles()->attach(4);

        }


        return 1;



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
