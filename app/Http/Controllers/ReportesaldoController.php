<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Producto;
use App\Cliente;
use App\Mensajero;
use App\Statustrx;
use Carbon\Carbon;
use App\Trx;
use App\Recarga;
use App\Pedido;
use App\PromocionesVendidas;
use App\PagoCombinado;
use App\ProductoVendido;
use App\Seguimientopedido;
use App\User;
use App\Role;
use App\Promociones;
use Auth;
use Hash;
use App\Http\Controllers\Controller;

class ReportesaldoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
         
          $saldodolar =  Cliente::
                          where('abono_actual','>=',0)
                          ->sum("abono_actual");

          $saldodolar =  number_format($saldodolar,2,',','.');

          $saldobs =  Cliente::where('abono_bs','>=',0)->sum("abono_bs");
          $saldobs =  number_format($saldobs,2,',','.');

          $deudadolar =  Cliente::where('abono_actual','<',0)->sum("abono_actual");
          $deudadolar =  number_format($deudadolar,2,',','.');

          $recargadolar = Recarga::
                          where('validado',1)
                          ->where('fecha_registro','>',"2022-02-01 00:00:00")
                          ->get();

          $recargamovil = Recarga::
                            where('validado',1)
                            ->where('fecha_registro','>',"2022-02-01 00:00:00")
                            ->where('id','>',118)
                            ->where('tipo_pago','pago movil')
                            ->sum("monto");

          $recargazelle = 0;
          $recargapunto = 0;
          $recargaefectivo = 0;
          $recargapaypal = 0;

          foreach ($recargadolar as $key => $reg) {
              if ($reg->tipo_pago == "zelle") {
                  $recargazelle = (float)$recargazelle + (float)$reg->monto;
              }
              if ($reg->tipo_pago == "punto de venta") {
                  $recargapunto = (float)$recargapunto + (float)$reg->monto;
              }
              if ($reg->tipo_pago == "paypal") {
                  $recargapaypal = (float)$recargapaypal + (float)$reg->monto;
              }
              if ($reg->tipo_pago == "efectivo") {
                  $recargaefectivo = (float)$recargaefectivo + (float)$reg->monto;
              }
            
          }

          $recargazelle =  number_format($recargazelle,2,',','.');
          $recargapunto =  number_format($recargapunto,2,',','.');
          $recargapaypal =  number_format($recargapaypal,2,',','.');
          $recargaefectivo =  number_format($recargaefectivo,2,',','.');
          $recargamovil =  number_format($recargamovil,2,',','.');


          $pedido = Pedido::where('tipo_pago',"abonado")->where('fecha_registro','>',"2022-02-01 00:00:00")->sum("monto");

          $pedido2 = Pedido::where('tipo_pago',"<>","abonado")->where('fecha_registro','>',"2022-02-01 00:00:00")->sum("monto");

          $pedido = number_format($pedido,2,',','.');

          $pedido2 = number_format($pedido2,2,',','.');


          return view('admin.reportesaldo',compact('pedido2','pedido','recargazelle','recargapunto','recargamovil','recargaefectivo','recargapaypal','saldodolar','saldobs','deudadolar'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
