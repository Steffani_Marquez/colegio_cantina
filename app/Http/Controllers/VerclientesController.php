<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use App\Http\Requests;
use App\Producto;
use App\Cliente;
use App\Pedido;
use App\Recarga;
use App\Problemascliente;
use App\Auxiliar;
use App\Productovendido;
use App\Trx;
use App\Observacioncliente;
use App\Carritoporvalidar;
use App\TipoCliente;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Hash;

class VerclientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        $TipoCliente = TipoCliente::all();   

        $hoy2 = Carbon::now('America/Caracas')->format('Y-m-d');
        $ayer2 = Carbon::now('America/Caracas')->subDays(10)->format('Y-m-d');

        $auxiliar = Auxiliar::first();

        return view('admin.clientes',compact('TipoCliente','hoy2','ayer2','auxiliar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLeer()
    {   
       
        Excel::load('cliente.xls', function($reader) {
            
            foreach ($reader->get() as $book) {

                $clientes = Cliente::
                    where('telefono',$book->telefono)
                    ->first();

                if (empty($clientes)) {
                     $observacion = Cliente::create([
                    'nombre'=> $book->nombre,
                    'telefono' => $book->telefono,
                    'direccion' => $book->direccion
                    ]);
                }
            }
        });

        return "leer";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getInsertar(Request $request)
    {
            

            $observacion = Observacioncliente::create([
                'id_cliente' => $request->id,
                'observacion'=> $request->observacion,
                'created_at' => Carbon::now('America/Caracas')
            ]);

            $cliente = Cliente::with('tipo_cliente')->where('id_cliente',$request->id)->first();

            $cliente->update(['nombre'   =>   $request->nombre]);
            $cliente->update(['telefono' =>   $request->telefono]);
            $cliente->update(['cedula'   =>   $request->cedula]);
            $cliente->update(['correo'   =>   $request->email]);


            return 1;


    }


     public function getDetalle2(Request $request)
    {
             
            $cliente = Cliente::
                        where('id_cliente',$request->id)
                        ->first();

            return $cliente;

    }

    public function getInactivar(Request $request)
    {
             
            $cliente = Cliente::
                        where('id',$request->id)
                        ->first();

            $cliente->update(['inactivo' => 1]);

    }



     public function getBuscarpedidos(Request $request)
    {
            $cliente = Cliente::
                        where('id',$request->id)
                        ->first();

            $desde =  Carbon::parse($request->desdetra)->format('Y-m-d 00:00:00');
            $hasta =  Carbon::parse($request->hastatra)->format('Y-m-d 23:59:59');

            $totalmontis = 0;

            $pedidos = Pedido::
                            where('id_codigo',$cliente->codigo)
                            ->where('fecha_registro',">=",$desde)
                            ->where('fecha_registro',"<=",$hasta)
                            ->get();

            foreach ($pedidos as $key => $ped) {
                $ped->fechareg =  Carbon::parse($ped->fecha_registro)->format('d/m/Y h:i a');
                $totalmontis =    (float)$totalmontis + (float)$ped->monto;
            }


            if (!empty($pedidos)) {
                $pedidos[0]->totalmontis = number_format($totalmontis,2,',','.');

            }

            return $pedidos;

    }


    public function getExportarPedidos(Request $request){


        $cliente = Cliente::
                        where('id',$request->id_elegido)
                        ->first();


        $desde =  Carbon::parse($request->desdetra)->format('Y-m-d 00:00:00');
        $hasta =  Carbon::parse($request->hastatra)->format('Y-m-d 23:59:59');
        
        $pedidos = Pedido::
                    where('id_codigo',$cliente->codigo)
                    ->where('fecha_registro',">=",$desde)
                    ->where('fecha_registro',"<=",$hasta)
                    ->get();
    

        $transacciones_array = [];    

        foreach($pedidos as $pedido){
           
            $totaltotal = 0;
 
            $pedido->monto_format = number_format($pedido->monto,2,',','.');
            
            $pedido->fecha_reistro_format = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i A');
            
            $new_array['id'] = $pedido->id;
            $new_array['monto_total'] = $pedido->monto_format;
            $new_array['medio_pago'] = $pedido->tipo_pago;
            $new_array['caja_fiscal'] = $pedido->caja_fiscal == 0 ? 'NO' : 'SI';
            $new_array['fecha_registro'] = $pedido->fecha_registro;
            $transacciones_array[] = $new_array;                     
        }

        

        $nombre_archivo = 'REPORTE DE VENTA'.date('Ymdhis');
        Excel::create($nombre_archivo, function($excel) use ($transacciones_array) {

                $excel->sheet('REPORTE DE VENTA', function($sheet) use($transacciones_array) {
                    $sheet->setAutoSize(true);
                    $sheet->cell('A1','ID VENTA');
                    $sheet->cell('B1','MONTO');
                    $sheet->cell('C1','MEDIO DE PAGO');
                    $sheet->cell('D1','PASADO A CAJA FISCAL');
                    $sheet->cell('E1','FECHA REGISTRO');
                    
                    $sheet->row(1, function($row) {
                        $row->setFontSize(13);
                        $row->setFontWeight('bold');
                    });
                    $sheet->cells('A1:G1', function ($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->fromArray($transacciones_array, null, 'A2', true,false);
                });                  
            })->download('xlsx'); 


    }  

    public function getBuscarrecargas(Request $request)
    {
             
            $desde =  Carbon::parse($request->desdetra)->format('Y-m-d 00:00:00');
            $hasta =  Carbon::parse($request->hastatra)->format('Y-m-d 23:59:59');

            $recargas = Carritoporvalidar::
                            where('id_cliente',$request->id)
                            ->where('fecha_registro',">=",$desde)
                            ->where('fecha_registro',"<=",$hasta)
                            ->with('recarga')
                            ->get();

            $totalmontis = 0;

            foreach ($recargas as $key => $ped) {

                $ped->fechareg =  Carbon::parse($ped->fecha_registro)->format('d/m/Y h:i a');
                $totalmontis =    (float)$totalmontis + (float)$ped->monto;
               
            }


            if (count($recargas) > 0) {
                $recargas[0]->totalmontis = $totalmontis;
            }


            return $recargas;

    }



    public function getBuscar(Request $request)
    {
        
        $clientes = Cliente::
            with('tipo_cliente')
            ->orderBy('id','desc')
            ->where('inactivo',0)
            ->get();


        foreach ($clientes as $key => $value) {

            if ($value->abono_bs == "") {
                 $value->nuformatbs =  0;
            }else{
                $value->nuformatbs =  number_format($value->abono_bs,2,',','.');
            }

             if ($value->abono_actual == "") {
                 $value->nuformatdolar =  0;
            }else{
                $value->nuformatdolar =  number_format($value->abono_actual,2,',','.');
            }

        }

        return $clientes;
    }


    public function getCorazon(Request $request)
    {
        
        $clientes = Cliente::with('tipo_cliente')->where("id_cliente",$request->id)->first();
        $clientes->update(['corazon' => $request->corazon]);

        return $clientes;


    }

     public function getDetalle(Request $request)
    {

        $clientes = Cliente::with('tipo_cliente')->where("id",$request->id)->first();

      $clientes->abonodolar = number_format($clientes->abono_actual,2,',','.');
      $clientes->abonobolivar = number_format($clientes->abono_bs,2,',','.');

        return $clientes;

    }


     public function getCambiarmax(Request $request)
    {
        

        $clientes = Cliente::with('tipo_cliente')->where("id",$request->id)->first();
        $clientes->update(['monto_deuda_max' => $request->monto]);

        return $clientes;


    }
    public function getEditarCliente(Request $request)
    {
        
        $clientes = Cliente::with('tipo_cliente')->where("id",$request->cliente_id)->first();

        if($clientes->nombre_alumno != $request->nombre_alumno){
            $clientes->update(['nombre_alumno' => $request->nombre_alumno]);
        }

        if($clientes->apellido_alumno != $request->apellido_alumno){
            $clientes->update(['apellido_alumno' => $request->apellido_alumno]);
        }
        
        if($clientes->correo_alumno != $request->correo_alumno){
            $clientes->update(['correo_alumno' => $request->correo_alumno]);
        }

       
        $clientes->update(['cedula' => $request->cedula]);
        $clientes->update(['abono_bs' => $request->grado]);
        $clientes->update(['limitediario' => $request->limitediario]);
        $clientes->update(['monto_deuda_max' => $request->limitecredito]);

        $clientes->update(['codigo' => $request->codigoqredit]);
        

        $pos = strpos($request->abono_actual, ',');

        if($pos === true){
            $abonoActual = str_replace(",", ".", $request->abono_actual);
        }else{
            $abonoActual = $request->abono_actual;
        }

        if($clientes->abono_actual != $abonoActual ){
            $clientes->update(['abono_actual' => $abonoActual]);
        }

        $clientes_todos = Cliente::
                            with('tipo_cliente')
                            ->orderBy('id','desc')
                            ->get();

        return $clientes_todos;

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function postImportar(Request $request){

        $file = $request->file('fichero'); 
        $file2 = \Input::file('fichero'); 
        

        $name2 = date('dmYhis').'estudiantes'.'.'.$file->getClientOriginalExtension();
        $carga = $file2->move(public_path('uploads/'),$name2 );
        $codigo = [];
        //$archivo_servidor = 'https://linosgo.com/nidstore/uploads/'.$name2;
        
        if($file->getClientOriginalExtension() == 'csv' || $file->getClientOriginalExtension() == 'xlsx'){

            
            
            Excel::load(public_path().'/uploads/'.$name2, function($reader) use($codigo) {
                //$reader->skipRows(5);
                //$reader->limitRows(false, 5);

               
                $prueba = [];
                foreach ($reader->toArray() as $book) {
                    //$book = collect($booka)->toArray();
                    //print_r($book['nombrea1']);
                    //return collect($booka)->toArray();
                    //dd();    
                        //print_r($book['nombre_del_alumno']);
                    if(empty($book['codigo'])){

                    
                        if(!empty($book['cedulaa1'])){
                            $passworda1 =  $book['cedulaa1'];
                        }else{
                            $passworda1 =  $book['cedulap1'];

                        }

                        if(!empty($book['cedulaa2'])){
                            $passworda2 =  $book['cedulaa2'];
                        }else{
                            $passworda2 =  $book['cedulap1'];

                        }

                        $codigo_alumno = strtoupper(str_random(10));
                        $codigo_alumno2 = strtoupper(str_random(10));
                        $codigo_papa1 = strtoupper(str_random(10));
                        $codigo_papa2 = strtoupper(str_random(10));
                        $codigo_familia = strtoupper(str_random(10));
                    
                        
                        
                        $cliente = Cliente::create([
                            'nombre' => $book['nombrea1']
                            ,'fecha_creado' => Carbon::now('America/Caracas')
                            ,'curso_actual' => $book['gradoa1']
                            ,'abono_actual' => 0
                            ,'codigo' => $codigo_alumno
                            ,'grado' => $book['gradoa1']
                            ,'cedula' => $book['cedulaa1'] 
                            ,'nombre_alumno' => $book['nombrea1']
                            ,'apellido_alumno' => $book['apellidoa1'] 
                            ,'correo_alumno' => $book['emaila1'] 
                            ,'passwordalumno' => $passworda1
                            ,'limitediario' => 1000 
                            ,'codigo_familiar' => $codigo_familia
                            ,'tipo_cliente_id' => 1
                            ,'monto_deuda_max' => 5
                            ,'num_entradas' => 0
                        ]);
                        if(!empty($book['nombrea2'])){
                            $cliente = Cliente::create([
                                'nombre' => $book['nombrea2']
                                ,'fecha_creado' => Carbon::now('America/Caracas')
                                ,'curso_actual' => $book['gradoa2']
                                ,'abono_actual' => 0
                                ,'codigo' => $codigo_alumno2
                                ,'grado' => $book['gradoa2']
                                ,'cedula' => $book['cedulaa2'] 
                                ,'nombre_alumno' => $book['nombrea2']
                                ,'apellido_alumno' => $book['apellidoa2'] 
                                ,'correo_alumno' => $book['correoa2'] 
                                ,'passwordalumno' => $passworda2
                                ,'limitediario' => 1000 
                                ,'codigo_familiar' => $codigo_familia
                                ,'tipo_cliente_id' => 1
                                ,'monto_deuda_max' => 5
                                ,'num_entradas' => 0
                            ]);                            
                        }
                        
                        $cliente = Cliente::create([
                            'nombre' => $book['nombrep1']
                            ,'edad' => 0
                            ,'fecha_creado' => Carbon::now('America/Caracas')
                            ,'curso_actual' => 0
                            ,'abono_actual' => 0
                            ,'codigo' => $codigo_papa1
                            ,'grado' => 0
                            ,'seccion' => 0 
                            ,'cedula' =>  $book['cedulap1']
                            ,'nombre_alumno' => $book['nombrep1'] 
                            ,'apellido_alumno' => $book['apellidop1']
                            ,'correo_alumno' => $book['emailp1']
                            ,'passwordalumno' => $book['cedulap1']
                            ,'passwordrepresentante' => $book['cedulap1']
                            ,'limitediario' => 1000 
                            ,'codigo_familiar' => $codigo_familia
                            ,'tipo_cliente_id' => 2
                            ,'monto_deuda_max' => 5
                            ,'num_entradas' => 0
                        ]);

                        if(!empty($book['nombrep2'])){
                            $cliente = Cliente::create([
                                'nombre' => $book['nombrep2']
                                ,'edad' => 0
                                ,'fecha_creado' => Carbon::now('America/Caracas')
                                ,'curso_actual' => 0
                                ,'abono_actual' => 0
                                ,'codigo' => $codigo_papa2
                                ,'grado' => 0
                                ,'seccion' => 0 
                                ,'cedula' =>  $book['cedulap2']
                                ,'nombre_alumno' => $book['nombrep2'] 
                                ,'apellido_alumno' => $book['apellidop2']
                                ,'correo_alumno' => $book['emailp2']
                                ,'passwordalumno' => $book['cedulap2']
                                ,'passwordrepresentante' => $book['cedulap2']
                                ,'limitediario' => 1000 
                                ,'codigo_familiar' => $codigo_familia
                                ,'tipo_cliente_id' => 2
                                ,'monto_deuda_max' => 5
                                ,'num_entradas' => 0
                            ]);
                        }

                    }else{
                        print_r($book);                        
                        if(empty($book['correo'])){
                            $correo  = $book['cedula'];
                        }else{
                            $correo  = $book['correo'];

                        }

                        $cliente = Cliente::create([
                            'nombre' => $book['nombre']
                            ,'edad' => 0
                            ,'fecha_creado' => Carbon::now('America/Caracas')
                            ,'curso_actual' => 0
                            ,'abono_actual' => 0
                            ,'codigo' => $book['codigo']
                            ,'grado' => 0
                            ,'seccion' => 0 
                            ,'cedula' =>  $book['cedula']
                            ,'nombre_alumno' => $book['nombre'] 
                            ,'apellido_alumno' => $book['apellido']
                            ,'correo_alumno' => $book['correo']
                            ,'passwordalumno' => $book['cedula']
                            ,'passwordrepresentante' => $book['cedula']
                            ,'limitediario' => 1000 
                            ,'codigo_familiar' => $book['codigo']
                            ,'tipo_cliente_id' => 3
                            ,'monto_deuda_max' => 5
                            ,'num_entradas' => 0
                        ]);                        
                    }
                        
                    
                }
                //print_r($codigo);
                //$productos_activos = Producto::whereIn('code',$codigo)->lists('code');
                //$productos_inactivos = Producto::whereNotIn('code',$productos_activos)->update(['status' => 'INACTIVO']);
          
          });           
       
            return redirect('/verclientes')->with('success','Clientes cargados');
        }else{
            return redirect('/verclientes')->with('error','El archivo cargado debe ser .csv o .xlsx');
        
        }
    }
}
