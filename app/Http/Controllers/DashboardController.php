<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reserva;
use App\Pedido;
use App\Auxiliar;
use App\Productovendido;
use App\Cliente;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $start_mes = Carbon::now('America/Caracas')->startOfMonth();
        $end_mes = Carbon::now('America/Caracas')->endOfMonth();
        
        $start_day = Carbon::now('America/Caracas')->startOfDay();
        $end_day = Carbon::now('America/Caracas')->endOfDay();

        $pedidosmes = Pedido::where('fecha_registro','>=',$start_mes)->where('fecha_registro','<=',$end_mes)->get();
        $totalmespedido = 0;
        $cantmespedido = count($pedidosmes);
        
        foreach($pedidosmes as $pedido){
            $totalmespedido = $totalmespedido + $pedido->monto;
        }
        
        $pedidosday = Pedido::where('fecha_registro','>=',$start_day)->where('fecha_registro','<=',$end_day)->get();
        $totaldaypedido = 0;
        $cantdaypedido = count($pedidosday);
     
       foreach($pedidosday as $pedido){
            $totaldaypedido = $totaldaypedido + $pedido->monto;
        }

        
        $reservasday = Reserva::where('status',1)->where('fecha_registro','>=',$start_day)->where('fecha_registro','<=',$end_day)->get();
        $cantdayreserva = count($reservasday);
        $estudiantes = Cliente::where('tipo_cliente_id',1)->get();
        $cantestudiantes = count($estudiantes);

        $auxiliar = Auxiliar::first();

        return view('admin.dashboard',compact('cantmespedido','totalmespedido','totaldaypedido','cantdaypedido','cantdayreserva','cantestudiantes','auxiliar'));
   
    }



    public function getClientes(Request $request){

      
        $cliente = Cliente::with('tipo_cliente')->get();
        $transacciones_array = [];       
       
        foreach($cliente as $cli){
            $new_array['id'] = $cli->id;
            $new_array['cliente'] = $cli->nombre;
            $new_array['tipo'] = $cli->tipo_cliente->nombre;
            $new_array['cedula'] = $cli->cedula;
            $new_array['codigo'] = $cli->codigo;
            $new_array['saldo_actual_dolar'] = $cli->abono_actual;
            $new_array['saldo_actual_bolos'] = $cli->abono_bs;
            $transacciones_array[] = $new_array; 
        }

        $nombre_archivo = 'REPORTE DE CLIENTES'.date('Ymdhis');
        Excel::create($nombre_archivo, function($excel) use ($transacciones_array) {

                $excel->sheet('REPORTE DE VENTA', function($sheet) use($transacciones_array) {
                    $sheet->setAutoSize(true);
                    $sheet->cell('A1','ID');
                    $sheet->cell('B1','CLIENTE');
                    $sheet->cell('C1','TIPO');
                    $sheet->cell('D1','CEDULA');
                    $sheet->cell('E1','CODIGO');
                    $sheet->cell('F1','SALDO EN DOLARES');
                    $sheet->cell('G1','SALDO EN BOLIVARES');
                    
                    $sheet->row(1, function($row) {
                        $row->setFontSize(13);
                        $row->setFontWeight('bold');
                    });
                    $sheet->cells('A1:G1', function ($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->fromArray($transacciones_array, null, 'A2', true,false);
                });                  
            })->download('xlsx'); 


    }



    public function getExportarventa2(Request $request){

        

        $desde = Carbon::now('America/Caracas')->startOfMonth();
        $hasta = Carbon::now('America/Caracas')->endOfMonth();
      
        $pedidos = Pedido::
                    with('cliente')
                    ->orderBy("id",'desc')
                    ->where('fecha_registro',">=",$desde)
                    ->where('fecha_registro',"<=",$hasta)
                    ->get();
        
        $transacciones_array = [];       
       
        foreach($pedidos as $pedido){
            $totaltotal = 0;
         
            
            $pedido->monto_format = number_format($pedido->monto,2,',','.');
            
            $pedido->fecha_reistro_format = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i A');
            
            $Productovendido = Productovendido::
                                    with('productos','promocion')
                                    ->where('id_pedido',$pedido->id)
                                    ->get();
          
            $Productovendido[0]->monto_total = $pedido->monto_format;

            $cliente = Cliente::where('codigo',$pedido->id_codigo)->first();
            if(!empty($cliente)){
                $nombre = $cliente->nombre_alumno.' '.$cliente->apellido_alumno; 
                
            }else{
                $nombre = 'No existe';
            }
            $new_array['id'] = $pedido->id;
            $new_array['cliente'] = $nombre;
            $new_array['monto_total'] = $pedido->monto_format;
            $new_array['medio_pago'] = $pedido->tipo_pago;
            $new_array['caja_fiscal'] = $pedido->caja_fiscal == 0 ? 'NO' : 'SI';
            $new_array['fecha_registro'] = $pedido->fecha_registro;
            $transacciones_array[] = $new_array;                     
        }

        

        $nombre_archivo = 'REPORTE DE VENTA'.date('Ymdhis');
        Excel::create($nombre_archivo, function($excel) use ($transacciones_array) {

                $excel->sheet('REPORTE DE VENTA', function($sheet) use($transacciones_array) {
                    $sheet->setAutoSize(true);
                    $sheet->cell('A1','ID');
                    $sheet->cell('B1','CLIENTE');
                    $sheet->cell('C1','MONTO');
                    $sheet->cell('D1','MEDIO DE PAGO');
                    $sheet->cell('E1','PASADO A CAJA FISCAL');
                    $sheet->cell('F1','FECHA');
                    
                    $sheet->row(1, function($row) {
                        $row->setFontSize(13);
                        $row->setFontWeight('bold');
                    });
                    $sheet->cells('A1:G1', function ($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->fromArray($transacciones_array, null, 'A2', true,false);
                });                  
            })->download('xlsx'); 


    }



    public function getExportarventa1(Request $request){

        
        $desde = Carbon::now('America/Caracas')->startOfDay();
        $hasta = Carbon::now('America/Caracas')->endOfDay();
      
        $pedidos = Pedido::
            with('cliente')
            ->orderBy("id",'desc')
            ->where('fecha_registro',">=",$desde)
            ->where('fecha_registro',"<=",$hasta)
            ->get();
        
        $transacciones_array = [];       
       
        foreach($pedidos as $pedido){
            $totaltotal = 0;
         
            
            $pedido->monto_format = number_format($pedido->monto,2,',','.');
            
            $pedido->fecha_reistro_format = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i A');
            
            $Productovendido = Productovendido::
            with('productos','promocion')
            ->where('id_pedido',$pedido->id)
            ->get();
          
            $Productovendido[0]->monto_total = $pedido->monto_format;

            $cliente = Cliente::where('codigo',$pedido->id_codigo)->first();
            if(!empty($cliente)){
                $nombre = $cliente->nombre_alumno.' '.$cliente->apellido_alumno; 
                
            }else{
                $nombre = 'No existe';
            }
            $new_array['id'] = $pedido->id;
            $new_array['cliente'] = $nombre;
            $new_array['monto_total'] = $pedido->monto_format;
            $new_array['medio_pago'] = $pedido->tipo_pago;
            $new_array['caja_fiscal'] = $pedido->caja_fiscal == 0 ? 'NO' : 'SI';
            $new_array['fecha_registro'] = $pedido->fecha_registro;
            $transacciones_array[] = $new_array;                     
        }

        

        $nombre_archivo = 'REPORTE DE VENTA'.date('Ymdhis');
        Excel::create($nombre_archivo, function($excel) use ($transacciones_array) {

                $excel->sheet('REPORTE DE VENTA', function($sheet) use($transacciones_array) {
                    $sheet->setAutoSize(true);
                    $sheet->cell('A1','ID');
                    $sheet->cell('B1','CLIENTE');
                    $sheet->cell('C1','MONTO');
                    $sheet->cell('D1','MEDIO DE PAGO');
                    $sheet->cell('E1','PASADO A CAJA FISCAL');
                    $sheet->cell('F1','FECHA');
                    
                    $sheet->row(1, function($row) {
                        $row->setFontSize(13);
                        $row->setFontWeight('bold');
                    });
                    $sheet->cells('A1:G1', function ($cells) {
                        $cells->setAlignment('center');
                    });
                    $sheet->fromArray($transacciones_array, null, 'A2', true,false);
                });                  
            })->download('xlsx'); 


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
