<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Clients;
use App\Role;
use App\Recarga;
use Validator;
use App\Auxiliar;
use Hash;
use Auth;
use Carbon\Carbon;
use Mail;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        
        // $recarga = Recarga::all();
        
        // foreach ($recarga as $key => $value) {
           
        //    if ($value->tipo_pago == "pago movil") {
        //        $value->update(['monto' => $value->tasabs * $value->monto ]);
        //    }

        // }

        $auxiliar = Auxiliar::first();

        $roles = Role::all();
        return view('admin.registro',compact('roles','auxiliar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        //return $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username'=> 'required|unique:users',
            'email' => 'required',
            'telefono' => 'required',
            'password' => 'required',
            ]);
            //return collect($validator)->toArray();
        if ($validator->fails()) {
            return redirect('register')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $request->password = Hash::make($request->password);
        
        if(!empty($request->roles)){
            
            $user = User::create([
                 'name' => $request->name
                ,'username' => $request->username
                ,'email' => $request->email
                ,'password' => $request->password
                ,'remember_token' => $request->password
                ,'phone' => $request->telefono
                ,'active' => 1
            ]);
            
            $request->active = 1;
            
            foreach($request->roles as $roleId){
                $user->roles()->attach($roleId);
            }
        }else{

            return redirect('register')->with('error','Selecciona los roles');
        }
        return redirect('register')->with('success','Registro Exitoso');

    }

   
    public function getListUsers(){
        $auxiliar = Auxiliar::first();
        $roles = Role::all();
        return view('admin.lista-user',compact('roles','auxiliar'));
    }

    public function getUser(Request $request){
        if(Auth::check()){
            $user = User::find($request->id);
            $user->roles_id = $user->roles()->lists('role_id');
            return $user;
        }else{
            return -1;
        }
        
    }

    public function getBuscar(Request $request){
        if(Auth::check()){

            
            $users = User::with('roles')->get();

            foreach($users as $user){
                $user->roles_id = $user->roles()->lists('role_id');
            }
            
            return $users;
        }else{
            return -1;
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function postUpdate(Request $request)
    {
         
        $user = User::find($request->id);

        if(($request->username != $user->username)){

        
            $validator = Validator::make($request->all(), [
                'username'=> 'unique:users',     
            ]);
            if ($validator->fails()) {
                return redirect('/register/list-users')
                            ->withErrors($validator)
                            ->withInput();
            } 
        }    

        if(!empty($request->name) && ($request->name != $user->name)){
            $user->name = $request->name;
        }
        if(!empty($request->username) && ($request->username != $user->username)){
            $user->username = $request->username;
        }

        if(!empty($request->email) && ($request->email != $user->email)){
            $user->email = $request->email;
        }
                      
        if(!empty($request->password)){
            if($request->password != $request->password_confirmation){
                return redirect('/register/list-users')->with('error','Las contraseñas no coinciden');
            }else{
                $user->password = Hash::make($request->password);
            }
        }

        if(!empty($request->roles)){

            foreach ($user->roles()->lists('role_id') as $rol) {
                if(!in_array($rol,$request->roles)){
                    $user->roles()->detach($rol);
                }
            }

            foreach ($request->roles as $rol) {
                if(!in_array($rol,$user->roles()->lists('role_id')->toArray())){
                    $user->roles()->attach($rol);
                }
            }
            
        }else {
            return redirect('/register/list-users')->with('error','Debes asociar un rol al usuario');
        }
        $user->save();
        return redirect('/register/list-users')->with('success','Editado exitosamente');
        
    }

    public function getChangestatus(Request $request){
        $user = User::find($request->id);
        if($user->active == 1){
            $user->active = 0;
        }else{
            $user->active = 1;
        }
        $user->save();
        return $user->active;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
