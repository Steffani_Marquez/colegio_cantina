<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Carritorecarga;
use App\Cliente;
use App\Recarga;
use App\Pedido;
use App\Carritoporvalidar;
use Carbon\Carbon;
use Mail;
use Hash;
use Auth;
use App\User;
use Maatwebsite\Excel\Facades\Excel;

class Registrocliente2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('Ventas.registroclienteonly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getEnviardatos(Request $request)
    {
           
                $parada = 0;
                
                while ($parada == 0) {
                    
                    $qr = strtoupper(str_random(10));
                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }

                    $codigofam = $qr;
                }


       
                 $creado =  Cliente::create([
                    'nombre' => $request->nombrer1,
                    'nombre_alumno' => $request->nombrer1,
                    'apellido_alumno' => $request->apellidor1,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedular1,
                    'correo_alumno' => $request->emailr1,
                    'passwordalumno' => $request->cedular1,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 1,
                    'num_entradas' => 0,
                    'limitediario' => 500,
                    'fecha_creado' => Carbon::now('America/Caracas')
                    ]);

                 $user = User::create([
                   'name' => $request->nombrer1
                    ,'username' => $request->emailr1
                    ,'email' => $request->emailr1
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => 11
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);






            return 1;



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
