<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Carritorecarga;
use App\Cliente;
use App\Recarga;
use App\Pedido;
use App\Carritoporvalidar;
use Carbon\Carbon;
use Mail;
use Hash;
use Auth;
use App\User;
use Maatwebsite\Excel\Facades\Excel;

class RegistroclienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
       return view('Ventas.registrocliente');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getEnviardatos(Request $request)
    {
           
                $parada = 0;
                $clienterep = Cliente::
                                 where('correo_alumno',$request->emailr1)
                                ->first(); 

                if (!empty($clienterep)) {
                  return 0;
                }

                $clienterep2 = Cliente::
                                 where('correo_alumno',$request->emailh1)
                                ->first(); 

                if (!empty($clienterep2)) {
                    return 0;
                }

      
                
                while ($parada == 0) {
                    $qr = strtoupper(str_random(10));
                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }

                    $codigofam = $qr;
                }


       
                $creado =  Cliente::create([
                    'nombre' => $request->nombrer1,
                    'nombre_alumno' => $request->nombrer1,
                    'apellido_alumno' => $request->apellidor1,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedular1,
                    'correo_alumno' => $request->emailr1,
                    'passwordalumno' => $request->cedular1,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 2,
                    'num_entradas' => 0,
                    'limitediario' => 1000,
                    'fecha_creado' => Carbon::now('America/Caracas')
                ]);
                

                $user = User::create([
                   'name' => $request->nombrer1
                    ,'username' => $request->emailr1
                    ,'email' => $request->emailr1
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => $request->cedular1
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);


            if ($request->nombreh1 != '') {

                $parada = 0;
                
                while ($parada == 0) {
                    $qr = strtoupper(str_random(10));

                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }
                }

                if ($request->cedulah1 == "") {
                    $request->cedulah1 = $request->cedular1;
                }

                $creado =  Cliente::create([
                    'nombre' => $request->nombreh1,
                    'nombre_alumno' => $request->nombreh1,
                    'apellido_alumno' => $request->apellidoh1,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedulah1,
                    'correo_alumno' => $request->emailh1,
                    'passwordalumno' => $request->cedulah1,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 1,
                    'num_entradas' => 0,
                    'limitediario' => 20,
                    'curso_actual' => $request->gradoh1,
                    'fecha_creado' => Carbon::now('America/Caracas')
                    ]);

                 $user = User::create([
                   'name' => $request->nombreh1
                    ,'username' => $request->emailh1
                    ,'email' => $request->emailh1
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => $request->cedulah1
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);
                
            }

            if ($request->nombreh2 != '') {


                 $parada = 0;
                
                while ($parada == 0) {
                    $qr = strtoupper(str_random(10));

                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }
                }

                if ($request->cedulah2 == "") {
                    $request->cedulah2 = $request->cedular1;
                }

                $creado =  Cliente::create([
                    'nombre' => $request->nombreh2,
                    'nombre_alumno' => $request->nombreh2,
                    'apellido_alumno' => $request->apellidoh2,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedulah2,
                    'correo_alumno' => $request->emailh2,
                    'passwordalumno' => $request->cedulah2,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 1,
                    'num_entradas' => 0,
                    'limitediario' => 20,
                    'curso_actual' => $request->gradoh2,
                    'fecha_creado' => Carbon::now('America/Caracas')
                    ]);

                 $user = User::create([
                   'name' => $request->nombreh2
                    ,'username' => $request->emailh2
                    ,'email' => $request->emailh2
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => 11
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);
                
            }


              if ($request->nombreh3 != '') {


                 $parada = 0;
                
                while ($parada == 0) {
                    $qr = strtoupper(str_random(10));

                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }
                }

                if ($request->cedulah3 == "") {
                    $request->cedulah3 = $request->cedular1;
                }

                $creado =  Cliente::create([
                    'nombre' => $request->nombreh3,
                    'nombre_alumno' => $request->nombreh3,
                    'apellido_alumno' => $request->apellidoh3,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedulah3,
                    'correo_alumno' => $request->emailh3,
                    'passwordalumno' => $request->cedulah3,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 1,
                    'num_entradas' => 0,
                    'limitediario' => 20,
                    'curso_actual' => $request->gradoh3,
                    'fecha_creado' => Carbon::now('America/Caracas')
                    ]);


                $user = User::create([
                   'name' => $request->nombreh3
                    ,'username' => $request->emailh3
                    ,'email' => $request->emailh3
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => 11
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);
                
            }



             if ($request->nombreh4 != '') {


                 $parada = 0;
                
                while ($parada == 0) {
                    $qr = strtoupper(str_random(10));

                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }
                }

                if ($request->cedulah4 == "") {
                    $request->cedulah4 = $request->cedular1;
                }

                $creado =  Cliente::create([
                    'nombre' => $request->nombreh4,
                    'nombre_alumno' => $request->nombreh4,
                    'apellido_alumno' => $request->apellidoh4,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedulah4,
                    'correo_alumno' => $request->emailh4,
                    'passwordalumno' =>$request->cedulah4,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 1,
                    'num_entradas' => 0,
                    'limitediario' => 20,
                    'curso_actual' => $request->gradoh4,
                    'fecha_creado' => Carbon::now('America/Caracas')
                    ]);


                $user = User::create([
                   'name' => $request->nombreh4
                    ,'username' => $request->emailh4
                    ,'email' => $request->emailh4
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => 11
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);
                
            }

             if ($request->nombreh5 != '') {

                 $parada = 0;
                
                while ($parada == 0) {
                    $qr = strtoupper(str_random(10));

                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }
                }

                if ($request->cedulah5 == "") {
                    $request->cedulah5 = $request->cedular1;
                }

                $creado =  Cliente::create([
                    'nombre' => $request->nombreh5,
                    'nombre_alumno' => $request->nombreh5,
                    'apellido_alumno' => $request->apellidoh5,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedulah5,
                    'correo_alumno' => $request->emailh5,
                    'passwordalumno' => $request->cedulah5,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 1,
                    'num_entradas' => 0,
                    'limitediario' => 20,
                    'curso_actual' => $request->gradoh5,
                    'fecha_creado' => Carbon::now('America/Caracas')
                    ]);


                 $user = User::create([
                   'name' => $request->nombreh5
                    ,'username' => $request->emailh5
                    ,'email' => $request->emailh5
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => 11
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);
                
            }


            if ($request->nombrer2 != "") {

                 $parada = 0;
                
                while ($parada == 0) {
                    $qr = strtoupper(str_random(10));

                    $clienteqr = Cliente::
                         where('codigo', $qr)
                        ->first(); 

                    if (empty($clienteqr)) {
                      $parada = 1;
                    }
                }
                
                $creado =  Cliente::create([
                    'nombre' => $request->nombrer2,
                    'nombre_alumno' => $request->nombrer2,
                    'apellido_alumno' => $request->apellidor2,
                    'abono_actual' => 0,
                    'codigo' => $qr,
                    'cedula' => $request->cedular2,
                    'correo_alumno' => $request->emailr2,
                    'passwordalumno' => $request->cedular2,
                    'codigo_familiar' => $codigofam,
                    'monto_deuda_max' => 5,
                    'tipo_cliente_id' => 2,
                    'num_entradas' => 0,
                    'limitediario' => 20,
                    'fecha_creado' => Carbon::now('America/Caracas')
                    ]);


                $user = User::create([
                   'name' => $request->nombrer2
                    ,'username' => $request->emailr2
                    ,'email' => $request->emailr2
                    ,'password' => 0000
                    ,'remember_token' => 0000
                    ,'phone' => 11
                    ,'active' => 1
                ]);

                $creado->update(['user_id' => $user->id]);
                $user->roles()->attach(4);

            }


            return 1;



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
