<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categoria;
use App\Productos;
use App\Cliente;
use App\Recarga;
use App\Carrito;
use App\Pedido;
use App\Productovendido;
use App\Productodeposito;
use App\Carritoporvalidar;
use Carbon\Carbon;
use Mail;

class ValidarrecargasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
       return view('admin.validarrecargas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getBuscar(Request $request)
    {
       
            $recargas = Recarga::
                            where('validado',$request->id)
                            ->orderBy("id",'desc')
                            ->get();

            foreach ($recargas as $key => $value) {
            
                $value->fechis = Carbon::parse($value->fecha_registro)->format('d/m/Y h:i A');
                $clien = Cliente::where('codigo_familiar',$value->codigo_familiar)->first();
                $value->cliente = $clien;

                if (($value->tipo_pago == "pago movil") or ($value->tipo_pago == "punto de venta")) {
                    $value->montobs = $value->monto;
                    $value->montodolar =0;
                }else{
                    $value->montobs = 0;
                    $value->montodolar = $value->monto;
                }
            }

            return $recargas;

    }


     public function getActualizarmonto(Request $request)
    {     
             

            $carrritocarga = Carritoporvalidar::
                                    where('id',$request->id)
                                    ->first();

            $carrritocarga->update(['monto' => $request->monto]);
            $carritopor = Carritoporvalidar::
                                where('id_recarga',$carrritocarga->id_recarga)
                                ->get();
            $monto = 0;

            foreach ($carritopor as $key => $car){
                $monto = (float)$monto + (float)$car->monto;
            }

            $recarga1 = Recarga::
                            where('id',$carrritocarga->id_recarga)
                            ->first();

            $recarga1->update(['monto' =>$monto]);
  
            return $carrritocarga->id_recarga;

    }




      public function getDetalle(Request $request)
    {
       
            $recargas = Recarga::where('id',$request->id)
                            ->with("carrito.cliente")
                            ->get();

            foreach ($recargas as $key => $value) {
            
                $value->fechis = Carbon::parse($value->fecha_registro)->format('d/m/Y h:i A');
               
                $clien = Cliente::
                        where('codigo_familiar',$value->codigo_familiar)
                        ->first();

                $value->cliente = $clien;

                if (($value->tipo_pago == "pago movil") or ($value->tipo_pago == "punto de venta")) {
                    $value->montodolar = 0;
                    $value->montobs =  $value->monto;
                }else{
                    $value->montodolar = $value->monto;
                    $value->montobs =  0;
                }

                foreach ($value->carrito as $key => $vali) {

                    if (($value->tipo_pago == "pago movil") or ($value->tipo_pago == "punto de venta")) {
                        $vali->montodolar = 0;
                        $vali->montobs =  $vali->monto;
                    }else{
                        $vali->montodolar = $vali->monto;
                        $vali->montobs =  0;
                    }
                    
                }


            }





            return $recargas;

    }


     public function getCambiarstatus(Request $request)
    {
       
                $recargas = Recarga::
                                where('id',$request->id)
                                ->with("carrito")
                                ->first();

                $clienteemails = Cliente::
                                   where('codigo_familiar',$recargas->codigo_familiar)
                                   ->lists('correo_alumno');
               
                if ($recargas->validado == 0) {
                                    
                    if ($request->status == 1) {

                        if ($recargas->status != 1) {
                       
                        foreach ($recargas->carrito as $key => $caror) {

                            $clien = Cliente::
                                        where('id',$caror->cliente->id)
                                        ->first();

                            if (($recargas->tipo_pago == "pago movil") or ($recargas->tipo_pago == "punto de venta")) {

                                if ((float)$clien->abono_actual < 0 ) {
                                    
                                    $montodolis = (float)$caror->monto / (float)$recargas->tasabs;
                                    $nuevosaldodolar = (float)$clien->abono_actual + (float)$montodolis;

                                    if ($nuevosaldodolar < 0 ) {
                                        $clien->update(['abono_actual' => $nuevosaldodolar]);
                                    }else{
                                        $clien->update(['abono_actual' => 0]);
                                        $nuevomontobs = (float)$nuevosaldodolar * (float)$recargas->tasabs;
                                        $clien->update(['abono_bs' => $nuevomontobs + (float)$clien->abono_bs]);
                                    }
                                }else{
                                    $clien->update(['abono_bs' => (float)$caror->monto + (float)$clien->abono_bs]);
                                }

                            }else{
                                $clien->update(['abono_actual' => (float)$caror->monto + (float)$clien->abono_actual]);
                            }

                            
                        }

                        $recargas->update(['validado' => $request->status]);

                        $carrritocarga = Carritoporvalidar::
                            where('id_recarga',$recargas->id)
                            ->get();
                            foreach ($carrritocarga as $key => $value) {
                               $value->update(['valido' => $request->status]);
                            }

                        }

                        $titulo = 'SU RECARGA HA SIDO APROBADA';
                        $mensaje = 'Tu recarga de saldo ha sido verificada correctamente. A partir de estos momentos tendrás disponibilidad en tu saldo para que puedas disfrutar de nuestros productos.';
                       // self::getEmailNotifica($clienteemails,$titulo,$mensaje);
                    }


                }

                if ($recargas->validado == 0) {
                    
                    if ($request->status == 2) {
                        
                        if ($recargas->status != 2) {
                            
                            $recargas->update(['validado' => $request->status]);
                            $carrritocarga = Carritoporvalidar::
                                                where('id_recarga',$recargas->id)
                                                ->get();

                            foreach ($carrritocarga as $key => $value) {
                               $value->update(['valido' => $request->status]);
                            }

                            $titulo = 'SU RECARGA HA SIDO ANULADA';
                            $mensaje = 'Hemos revisado la recarga realizada, pero existe incongruencias en el pago. Contactanos';
                            
                        }

                    }

                }


                if ($recargas->validado == 1) {
                
                    if ($request->status == 2) {

                    if ($recargas->status != 2) {

                        foreach ($recargas->carrito as $key => $caror) {

                            $clien = Cliente::
                                where('id',$caror->cliente->id)
                                ->first();

                            if (($recargas->tipo_pago == "pago movil") or ($recargas->tipo_pago == "punto de venta")) {
                                 $clien->update(['abono_bs' => (float)$clien->abono_bs - (float)$caror->monto]);
                            }else{
                                $clien->update(['abono_actual' => (float)$clien->abono_actual - (float)$caror->monto]);
                            }
                        
                        }

                        $recargas->update(['validado' => $request->status]);

                        $carrritocarga = Carritoporvalidar::
                            where('id_recarga',$recargas->id)
                            ->get();
                            foreach ($carrritocarga as $key => $value) {
                               $value->update(['valido' => $request->status]);
                            }
                            $titulo = 'SU RECARGA HA SIDO ANULADA';
                            $mensaje = 'Hemos revisado la recarga realizada, pero existe incongruencias en el pago. Contactanos';
                           // self::getEmailNotifica($clienteemails,$titulo,$mensaje);    
                    }


                }
            }


                if ($recargas->validado == 2) {                    
                    if ($request->status == 1) {
                        if ($recargas->status != 1) {

                            foreach ($recargas->carrito as $key => $caror) {

                                $clien = Cliente::
                                            where('id',$caror->cliente->id)
                                            ->first();

                                if (($recargas->tipo_pago == "pago movil") or ($recargas->tipo_pago == "punto de venta")) {
                                    $clien->update(['abono_bs'     => (float)$caror->monto + (float)$clien->abono_bs]);
                                }else{
                                    $clien->update(['abono_actual' => (float)$caror->monto + (float)$clien->abono_actual]);
                                }

                            }

                            $recargas->update(['validado' => $request->status]);

                            $carrritocarga = Carritoporvalidar::
                                                where('id_recarga',$recargas->id)
                                                ->get();
                           
                            foreach ($carrritocarga as $key => $value) {
                               $value->update(['valido' => $request->status]);
                            }
                        }
                    }

                }


                
               $clienteemailss = Cliente::
                                   where('codigo_familiar',$recargas->codigo_familiar)
                                   ->get();
                


            return $clienteemailss;

    }

    public function getEmailNotifica($emailscliente,$titulo,$mensaje){
        

        $data = [
            'titulo' => $titulo,
            'mensaje' => $mensaje,
            ];
        
            $message = 'prueba';
            foreach($emailscliente as $email){
                $emails[] = $email;
            }
            $emails[] = 'eduardozapata290121@gmail.com';
        
            return  Mail::send('emails.recarga-notificacion', $data, function($message) use ($titulo,$emails)
            {
                
                $message->from('no-reply@cumbres.cafe', 'Cumbres Café');
                
                $message->to($emails)->subject($titulo);
                
            });



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
