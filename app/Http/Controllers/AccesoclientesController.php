<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Carritorecarga;
use App\Cliente;
use App\Recarga;
use App\Pedido; 
use App\Auxiliar;
use App\Carritoporvalidar;
use Carbon\Carbon;
use Mail;
use App\User;
use App\Role;
use App\Promociones;
use Auth;
use Hash;



class AccesoclientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {   
        //return self::getEmailNotifica();
        $primerodelmes = new Carbon('first day of this month');
        $primerodelmes = Carbon::parse($primerodelmes)->format('Y-m-d');

        $ultimodelmes = new Carbon('last day of this month');
        $ultimodelmes = Carbon::parse($ultimodelmes)->format('Y-m-d');

        $auxiliar = Auxiliar::first();

        return view('Ventas.recargarsaldo')
                        ->with("primerodelmes",$primerodelmes)
                        ->with("ultimodelmes",$ultimodelmes)
                        ->with("auxiliar",$auxiliar);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getInsertarcarrito(Request $request)
    {         

              
                $car = Carritorecarga::
                        where('id_cliente',$request->id_cliente)
                        ->first();

                        

                if (!empty($car)) {
                  
                    $car->update(['monto' => $request->valor]);
                    $car->update(['fecha_registro' => Carbon::now('America/Caracas')]);
               
                }else{
                   
                    $pedido =  Carritorecarga::create([
                        'id_cliente' => $request->id_cliente,
                        'fecha_registro' => Carbon::now('America/Caracas'),
                        'monto' => $request->valor,
                        'codigo_familiar' => $request->codigo_familiar,
                        'tasa' => $request->tasa
                    ]);

                }

                $totalcarris = Carritorecarga::
                        where('codigo_familiar', $request->codigo_familiar)
                        ->get();


                $montototal = 0;

                foreach ($totalcarris as $key => $car) {
                   $montototal = (float)$montototal + (float)$car->monto;
                }


                return $montototal;

 

    }


     public function getActualizardatos(Request $request)
    {     
            
            $cliente = Cliente::
                    where('id',$request->id)
                    ->first(); 
           
            if ($request->nombreedit != "") {
               $cliente->update(['nombre' => $request->nombreedit]);
               $cliente->update(['nombre_alumno' => $request->nombreedit]);
            }

            if ($request->apellidoedit != "") {
               $cliente->update(['apellido_alumno' => $request->apellidoedit]);
            }

            if ($request->emailedit != "") {
               $cliente->update(['correo_alumno' => $request->emailedit]);
            }

            if ($request->cedulaedit != "") {
               $cliente->update(['cedula' => $request->cedulaedit]);
            }

            if ($request->telefonoedit != "") {
               $cliente->update(['telefono' => $request->telefonoedit]);
            }

            if ($request->gradoh1 != "") {
               $cliente->update(['curso_actual' => $request->gradoh1]);
            }


            return 1;
 
    }



    public function getBuscarperfil(Request $request)
    {     
            
            $cliente = Cliente::
                    where('id',$request->id)
                    ->first(); 
           
            return $cliente;
 
    }


    public function getActualizarlimite(Request $request)
    {     
            
            $cliente = Cliente::
                    where('id',$request->id)
                    ->first(); 

            $cliente->update(['limitediario' => $request->limite]);
            
            return $cliente;
          
    }


    public function getActualizarrestriccion(Request $request)
    {     
            
            $cliente = Cliente::
                        where('id',$request->id)
                        ->first(); 
           
            $cliente->update(['restriccion_alimenticia' => $request->restriccion]);
           
            return $cliente;
          
    }



     public function getTransferirsaldo(Request $request)
    {     
            
            $cliente = Cliente::
                        where('codigo_familiar',$request->codigo_familiar)
                        ->get();

            $montodola = 0;
            $montobolo = 0;

            foreach ($cliente as $key => $value) {
                $montodola = (float)$montodola + (float)$value->abono_actual;
                $montobolo = (float)$montobolo + (float)$value->abono_bs;
            }

            $cliente_quitar = Cliente::
                                where('id',$request->id_personaquitar)
                                ->first();

            $cliente_poner =  Cliente::
                                where('id',$request->persontransf)
                                ->first();


            if ($request->id_personaquitar == $request->persontransf ) {
                return "ERROR! no puedes transferirle saldo a la misma persona";
            }
            if ((float)$request->montodolar > (float)$cliente_quitar->abono_actual) {
               return "ERRROR! El monto de lo que deseas transferir en dolares supera al actual disponible de esta persona";
            }
            if ((float)$request->montobs > (float)$cliente_quitar->abono_bs) {
               return "ERRROR! El monto de lo que deseas transferir en bolivares supera al actual disponible de esta persona";
            }

            if ((((float)$request->montobs > 0) and (float)$request->montodolar > 0) ){
                return "ERROR! No puedes transferir dos tipos de monedas a la vez";
            }


            if ( ((float)$cliente_poner->abono_actual < 0 ) and ((float)$request->montobs > 0) )  {
                                    
                    $montodolis =      (float)$request->montobs / (float)$request->tasa;
                    $nuevosaldodolar = (float)$cliente_poner->abono_actual + (float)$montodolis;

                    if ($nuevosaldodolar < 0 ) {
                        $cliente_poner->update(['abono_actual' => $nuevosaldodolar]);
                    }else{
                        $cliente_poner->update(['abono_actual' => 0]);
                        $nuevomontobs = (float)$nuevosaldodolar * (float)$request->tasa;
                        $cliente_poner->update(['abono_bs' => $nuevomontobs + (float)$cliente_poner->abono_bs]);
                    }

                    $cliente_quitar->update(['abono_actual' => (float)$cliente_quitar->abono_actual - (float)$request->montodolar]);
                    $cliente_quitar->update(['abono_bs' =>     (float)$cliente_quitar->abono_bs - (float)$request->montobs]);
           
            }else{


                $cliente_quitar->update(['abono_actual' => (float)$cliente_quitar->abono_actual - (float)$request->montodolar]);
                $cliente_quitar->update(['abono_bs' =>     (float)$cliente_quitar->abono_bs - (float)$request->montobs]);

                $cliente_poner->update(['abono_actual' => (float)$cliente_poner->abono_actual + (float)$request->montodolar]);
                $cliente_poner->update(['abono_bs' =>     (float)$cliente_poner->abono_bs + (float)$request->montobs]);

            }

            return 1;

    }


    public function getDetalle(Request $request)
    {     
            
            $cliente = Cliente::
                    where('codigo_familiar',$request->codigo_familiar)
                    ->get(); 
            
            return $cliente;
          
    }

     public function getDetalle2(Request $request)
    {     
            
  
            $recargas = Recarga::
                            where('codigo_familiar',$request->codigo_familiar)
                            ->where('validado',0)
                            ->get();

            foreach ($recargas as $key => $value) {

                if ($value->tipo_pago == "pago movil") {
                    $value->total  =  ((float)$value->monto);
                    $value->signo = "Bs.";
                }else{
                    $value->total =  (float)$value->monto;
                    $value->signo = "$";
                }

                $value->fechis = Carbon::parse($value->fecha_registro)->format('d/m/Y h:i a');
            }
            
            return $recargas;
          


    }




    public function getDetalle4(Request $request)
    {     
            
  
            $cliente = Cliente::
                    where('codigo_familiar',$request->codigo_familiar)
                    ->get(); 


            $listacodigos = $cliente->lists("codigo");

            $primerodelmes = new Carbon('first day of this month');
            $primerodelmes = Carbon::parse($primerodelmes)->format('Y-m-d');


            $ultimodelmes = new Carbon('last day of this month');
            $ultimodelmes = Carbon::parse($ultimodelmes)->format('Y-m-d');


            $pedidos = Pedido::
                      whereIn('id_codigo',$listacodigos)
                      ->where('fecha_registro',">=",$primerodelmes)
                      ->where('fecha_registro',"<=",$ultimodelmes)
                      ->with("productos")
                      ->with("cliente")
                      ->get(); 

            foreach ($pedidos as $key => $value) {
                $value->fechis = Carbon::parse($value->fecha_registro)->format('d/m/Y h:i a');
            }
            
            return $pedidos;
          


    }


    public function getBuscarpedidos(Request $request)
    {     
            
            $cliente = Cliente::
                    where('codigo_familiar',$request->codigo_familiar)
                    ->get();

            $listacodigos = $cliente->lists("codigo");

            $desde =  Carbon::parse($request->desde)->format('Y-m-d 00:00:00');
            $hasta =  Carbon::parse($request->hasta)->format('Y-m-d 23:59:59');

  
            $pedidos = Pedido::
                        whereIn('id_codigo',$listacodigos)
                        ->where('fecha_registro',">=",$desde)
                        ->where('fecha_registro',"<=",$hasta)
                        ->with("productos")
                        ->with("cliente")
                        ->get();

             foreach ($pedidos as $key => $value) {
                $value->fechis = Carbon::parse($value->fecha_registro)->format('d/m/Y h:i a');
            }
                       
            return $pedidos;

    }


   


    public function getDetallepedido(Request $request)
    {     
            
  
            $pedido = Pedido::
                        where('id',$request->id)
                        ->with("productos.productos")
                        ->with("productos.promocion")
                        ->with("cliente")
                        ->first();


           
                $pedido->fechis = Carbon::parse($pedido->fecha_registro)->format('d/m/Y h:i a');
        

                       
            return $pedido;

    }



    public function getDetalle3(Request $request)
    {     
            
  
            $recargas = Recarga::
                        where('codigo_familiar',$request->codigo_familiar)
                        ->where('validado',2)
                        ->get();

            foreach ($recargas as $key => $value) {

                 if ($value->tipo_pago == "pago movil") {
                    $value->total  =  ((float)$value->monto );
                    $value->signo = "Bs.";
                }else{
                    $value->total =  (float)$value->monto;
                    $value->signo = "$";
                }

                
                $value->fechis = Carbon::parse($value->fecha_registro)->format('d/m/Y h:i a');
            }
            
            return $recargas;
          


    }


    public function getRecargar(Request $request)
    {         

                $carrritocarga = Carritorecarga::
                            where('codigo_familiar',$request->codigo_familiar)
                            ->get();

                $montototal = 0;

                foreach ($carrritocarga as $key => $car) {
                   $montototal = (float)$montototal + (float)$car->monto;
                }

                if ($montototal <= 0) {
                    return 0;
                }

                if ($request->tipo_pago == "zelle") {
                   $ref = $request->refezelle;
                }

                if ($request->tipo_pago == "pago movil") {
                   $ref = $request->refepagomovil;
                }

                if ($request->tipo_pago == "paypal") {
                    $ref = $request->refepaypal;
                }

                if ($request->tipo_pago == "efectivo") {
                    $ref = $request->refeefectivo;
                }

                if ($request->tipo_pago == "punto de venta") {
                    $ref = 0;
                }


                $recarga =  Recarga::create([
                        'id_cliente' => $request->id_cliente,
                        'fecha_registro' => Carbon::now('America/Caracas'),
                        'monto' => $montototal,
                        'codigo_familiar' => $request->codigo_familiar,
                        'tipo_pago' => $request->tipo_pago,
                        'validado' => 0,
                        'referencia_pago' => $ref,
                        'tasabs' => $request->tasa,
                        'telefono' => $request->teleinfo,
                        'origen' => "Modulo cliente"
                ]);


                foreach ($carrritocarga as $key => $car) {
                   
                        $pedido =  Carritoporvalidar::create([
                            'id_cliente' => $car->id_cliente,
                            'fecha_registro' => Carbon::now('America/Caracas'),
                            'monto' => $car->monto,
                            'id_recarga' => $recarga->id,
                            'codigo_familiar' => $request->codigo_familiar,
                            'valido' => 0,
                        ]);

                }

                
                $cliente = Cliente::
                            where('codigo_familiar',$request->codigo_familiar)
                            ->lists('correo_alumno');
                
                
                self::getEmailNotifica($cliente);

                return 1;
            

    }



    public function getEmailNotifica($emailscliente){
        
        $titulo  = 'RECARGA RECIBIDA';
        $mensaje = 'Te notificamos que tu recarga se ha procesado y está siendo revisada.';
        $mensaje2 = 'En cuento validemos tu orden te llegará con correo confirmando la disponibilidad del saldo recargado y podrás hacer uso de este.';

        $data = [
            'titulo' => $titulo,
            'mensaje' => $mensaje,
            'mensaje2' => $mensaje2
            ];
        
            $message = 'prueba';
            foreach($emailscliente as $email){
                $emails[] = $email;
            }
            $emails[] = 'eduardozapata290121@gmail.com';
        
            return  Mail::send('emails.recarga-notificacion', $data, function($message) use ($emails)
            {
                
                $message->from('no-reply@cumbres.cafe', 'Cumbres Café');
                
                $message->to($emails)->subject('RECARGA RECIBIDA');
                
            });



    }


    public function getBuscarsaldoscliente(Request $request)
    {  


        $cliente = Cliente::
                    where('codigo_familiar',$request->codigo_familiar)
                    ->get(); 

        $listacodigos = $cliente->lists("codigo");

        $temprano = Carbon::now('America/Caracas')->format('Y-m-d 00:00:00');
        $tarde = Carbon::now('America/Caracas')->format('Y-m-d 23:23:23');

        $pedidohoy = Pedido::
                        where('fecha_registro',">=",$temprano)
                        ->where('fecha_registro',"<=",$tarde)
                        ->whereIn('id_codigo',$listacodigos)
                        ->get(); 

        $comprasdehoy = 0;

        foreach ($pedidohoy as $key => $value) {
           $comprasdehoy =  (float)$value->monto  + (float)$comprasdehoy;
        }
     
        $comprasdehoy =  number_format($comprasdehoy,2,',','.');
        $primerodelmes = new Carbon('first day of this month');
        $primerodelmes = Carbon::parse($primerodelmes)->format('Y-m-d 00:00:00');
        $ultimodelmes =  new Carbon('last day of this month');
        $ultimodelmes =  Carbon::parse($ultimodelmes)->format('Y-m-d 00:00:00');
        
        $pedidomes = Pedido::where('fecha_registro',">=",$primerodelmes)
                                ->where('fecha_registro',"<=",$ultimodelmes)
                                ->whereIn('id_codigo',$listacodigos)
                                ->get(); 

        $comprasdemes = 0;

        foreach ($pedidomes as $key => $value) {
           $comprasdemes =  (float)$value->monto  + (float)$comprasdemes;
        }

        $comprasdemes =  number_format($comprasdemes,2,',','.');
        $primerodelano = Carbon::parse('first day of January')->format('Y-m-d 00:00:00');
        $ultimodelano =  Carbon::parse('first day of December')->format('Y-m-d 00:00:00');

        $pedidoanoarr = Pedido::
                        where('fecha_registro',">=",$primerodelano)
                        ->where('fecha_registro',"<=",$ultimodelano)
                        ->whereIn('id_codigo',$listacodigos)
                        ->get(); 


        $pedidoano = 0;

        foreach ($pedidoanoarr as $key => $value) {
           $pedidoano =  (float)$value->monto  + (float)$pedidoano;
        }

        $pedidoano = number_format($pedidoano,2,',','.');

        $montototalbs = 0;
        $montototaldolar = 0;
        $montoporvalidardolar = 0;
        $montoporvalidarbs = 0;
        $montorechazadobs = 0;
        $montoporvalidarbs = 0;
        $montorechazadodolar = 0;

        
        foreach ($cliente as $key => $value) {
            $montototaldolar = (float)$montototaldolar + (float)$value->abono_actual;

            $montototalbs = (float)$montototalbs + (float)$value->abono_bs;

        }

        $montototaldolar =  number_format($montototaldolar,2,',','.');
        $montototalbs =  number_format($montototalbs,2,',','.');


        $carrritocarga = Recarga::
                        where('codigo_familiar',$request->codigo_familiar)
                        ->get();

        foreach ($carrritocarga as $key => $value) {

            if ($value->validado == 0) {

                if (($value->tipo_pago == "pago movil") or ($value->tipo_pago == "punto de venta")) {
                   $montoporvalidarbs =    (float)$montoporvalidarbs + ((float)$value->monto);
                }else{
                   $montoporvalidardolar = (float)$montoporvalidardolar + (float)$value->monto; 
                }
                
            }

            if ($value->validado == 2) {
                if (($value->tipo_pago == "pago movil") or ($value->tipo_pago == "punto de venta")) {
                    $montorechazadobs = (float)$montorechazadobs + ((float)$value->monto );
                }else{
                   $montorechazadodolar = (float)$montorechazadodolar + (float)$value->monto;
                }
            }
            
        }

        $montorechazadobs =  number_format($montorechazadobs,2,',','.');
        $montoporvalidardolar =  number_format($montoporvalidardolar,2,',','.');
        $montoporvalidarbs =  number_format($montoporvalidarbs,2,',','.');

        $clien = Cliente::where('codigo_familiar',$request->codigo_familiar)->first();

        $clien->montototalbs =         $montototalbs;
        $clien->montototaldolar =      $montototaldolar;
        $clien->montoporvalidardolar = $montoporvalidardolar;
        $clien->montoporvalidarbs =    $montoporvalidarbs;
        $clien->montorechazadobs =     $montorechazadobs;
        $clien->comprasdehoy =         $comprasdehoy;
        $clien->comprasdemes =         $comprasdemes;
        $clien->pedidoano =            $pedidoano;
        $clien->montorechazadodolar =  $montorechazadodolar;


        return $clien;

    }


    public function getNuevaclave(Request $request)
    {  


       $cliente = Cliente::
                    where('correo_alumno',$request->email)
                    ->where('passwordalumno',$request->password)
                    ->first();
       
        if (empty($cliente)) {
            
            $cliente = Cliente::
                    where('cedula',$request->email)
                    ->where('passwordalumno',$request->password)
                    ->first();
        }


        $cliente->update(['passwordalumno' => $request->clave1]);

        $cliente->update(['num_entradas' => 1]);



            $carrritocarga = Carritorecarga::
                            where('codigo_familiar',$cliente->codigo_familiar)
                            ->get();

            foreach ($carrritocarga as $key => $val) {
               $val->delete();
            }


            $familias = Cliente::
                        where('codigo_familiar',$cliente->codigo_familiar)
                         ->get();

            return $familias;



    }


     public function getLogout(Request $request)
    { 
           Auth::logout();
    }


    public function getPreguntarlogin(Request $request)
    { 


            if (Auth::user()== "") {
               return 0;
            }else{

                $cliente = Cliente::
                    where('user_id',Auth::user()->id)
                    ->first();
              
                return $cliente;
            }
   

    }


    public function getBuscarcliente(Request $request)
    {   

        $cliente = Cliente::
                    where('correo_alumno',$request->email)
                    ->where('passwordalumno',$request->password)
                    ->first();
       
        if (empty($cliente)) {
            
            $cliente = Cliente::
                    where('cedula',$request->email)
                    ->where('passwordalumno',$request->password)
                    ->first();
        }

        if (!empty($cliente)) {

            if ($cliente->inactivo == 1) {
               return 0;
            }
            
            if ($cliente->num_entradas == 0) {
               return 3;
            }
        }


        if (!empty( $cliente)) {

            

           $user = User::where('id',$cliente->user_id)->first();
           Auth::loginUsingId($user->id,true);
           $userlog = Auth::user();

                 
            $carrritocarga = Carritorecarga::
                                    where('codigo_familiar',$cliente->codigo_familiar)
                                    ->get();

            foreach ($carrritocarga as $key => $val) {
               $val->delete();
            }

            $familias = Cliente::
                        where('codigo_familiar',$cliente->codigo_familiar)
                         ->get();

            return $familias;
            
        }else{
            return 0;
        }


    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
