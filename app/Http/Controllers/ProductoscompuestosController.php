<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categoria;
use App\Promociones;
use App\Productopromociones;
use App\Productodeposito;
use App\Productos;


class ProductoscompuestosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $categorias = Categoria::all();
       
        $productostodos = Productos::where('status',1)->where('tipo','COMPONENTE')->get();
       
        $productos = $productostodos;
       
        // foreach($productostodos as $produto){
        //     $producpromo = Productodeposito::where('id_producto',$produto->id)->where('id_deposito',1)->first();
        //     if(!empty($producpromo)){
        //         if($producpromo->cantidad > 0){
        //             $productos[] = $produto;
        //         }
        //     }
            
        // }


        return view('admin.productoscompuestos',compact('categorias','productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBuscar(){
        $promociones = Promociones::with('categoria')->get();
        foreach($promociones as $promo){
            $promo->productos = Productopromociones::with('producto')->where('id_promocion',$promo->id)->get();
        }
        return $promociones;
    }

    
    

    public function postNuevo(Request $request){
        
        $file = $request->file('image'); 
        $file2 = \Input::file('image'); 
        
        $name2 = date('dmYhis').'image'.'.'.$file->getClientOriginalExtension();
        $carga = $file2->move(public_path('img/products'),$name2 );
        
        $promociones = Promociones::create([
            'id_categoria' => $request->categoria_id
            ,'nombre' => $request->nombre
            ,'status' => $request->status
            ,'descripcion' => $request->descripcion
            ,'costo' => $request->precio
            ,'imagen' => 'img/products/'.$name2
        ]);
        
        foreach($request->productos as $key => $producto){
            if(!empty($producto)){

            
                $cantidad = $request->cantidad[$key];
                Productopromociones::create([
                    'id_producto' => $producto
                    ,'id_promocion' => $promociones->id
                    ,'cant_producto' => $cantidad
                ]);
            }
            
        }

        return $promociones;
    }

    public function postEditar(Request $request){
        
        
        $promocion = Promociones::find($request->id_edicion);
        $file = $request->file('image_edicion'); 
        $file2 = \Input::file('image_edicion'); 
         
        if(!empty($file)){
        
            $name2 = date('dmYhis').'image'.'.'.$file->getClientOriginalExtension();
            $carga = $file2->move(public_path('img/products'),$name2 );
            $promocion->imagen = 'img/products/'.$name2;
    
        }
            
        if($promocion->id_categoria != $request->categoria_id_edicion){
            $promocion->id_categoria = $request->categoria_id_edicion;
        }

        if($promocion->nombre != $request->nombre_edicion){
            $promocion->nombre = $request->nombre_edicion;
        }
            
        if( $promocion->status != $request->status_edicion){
            $promocion->status = $request->status_edicion;
        }

        if($promocion->descripcion != $request->descripcion_edicion){
            $promocion->descripcion = $request->descripcion_edicion;
        }

        if($promocion->costo != $request->precio_edicion){
            $promocion->costo = $request->precio_edicion;
        }
        $promocion->save();
        
        $Productosdepromos = Productopromociones::whereNotIn('id_producto',$request->productos)->where('id_promocion',$promocion->id)->delete();
        

        //return $request->productos;
        foreach($request->productos as $key => $producto){
            if(!empty($producto)){
            
                $cantidad = $request->cantidad[$key];
                $Productopromociones = Productopromociones::where('id_producto',$producto)->where('id_promocion',$promocion->id)->first();
                if(empty($Productopromociones)){
                    Productopromociones::create([
                        'id_producto' => $producto
                        ,'id_promocion' => $promocion->id
                        ,'cant_producto' => $cantidad
                    ]);
                }else{
                    if($cantidad != $Productopromociones->cant_producto){
                        $Productopromociones->cant_producto = $cantidad;
                        $Productopromociones->save();
                    }
                }    
            
            }

        }


        //produstos

        return $promocion;
    }

}
