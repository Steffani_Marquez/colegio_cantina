<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reserva;
use App\Carrito;
use App\Status;
use App\Productopromociones;
use Carbon\Carbon;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $status = Status::all();
        $hoy2 = Carbon::now('America/Caracas')->format('Y-m-d');
        $manana2 = Carbon::now('America/Caracas')->addDays(1)->format('Y-m-d');
        
        return view('admin.reservas',compact('status','hoy2','manana2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBuscar(Request $request){

       

        $hoy = Carbon::now('America/Caracas')->format('Y-m-d');
        $findia = Carbon::now('America/Caracas')->addHours(23)->addMinutes(60)->addSeconds(60)->format('Y-m-d');

        if($request->nombre == 'Pendiente'){
            $ststus_id = 1;
        }else if($request->nombre == 'Entregado'){
            $ststus_id = 2;
        }else if($request->nombre == 'Anulado'){
            $ststus_id = 3;
        }else{
            $ststus_id = 1;
        }


        $reservas = Reserva::where('status',$ststus_id)->where('fecha_registro','>=',$hoy)->where('fecha_registro','<',$findia)->with('cliente','status')->orderBy("id",'desc')->get();
        
        $monto_total = 0;
        foreach($reservas as $reserva){
                
                $carrito = Carrito::with('cliente','producto','promocion')->where('reserva_id',$reserva->id)->get();
         
                $monto_total = 0;
                foreach($carrito as $car){
 
                    if($car->tipo == 'Promo'){
                        $monto_total += ($car->promo->costo * $car->cantidad);
                        $car->subtotal= number_format($car->promo->costo * $car->cantidad,2,',','.');
                        $car->precio_unitario = number_format($car->promo->costo,2,',','.');
                    }else{
                        $monto_total += ($car->producto->costo * $car->cantidad);
                        $car->subtotal= number_format($car->producto->costo * $car->cantidad,2,',','.');
                        $car->precio_unitario = number_format($car->producto->costo,2,',','.');
                        
                    }
                    $car->monto_total = $monto_total;
                }

                if (count($carrito) > 0) {
                   $carrito[0]->monto_total = $monto_total;
                }
            
            
            $reserva->fecha_registro_format = $reserva->fecha_registro_format;
            $reserva->carrito = $carrito;
            $reserva->monto_total = number_format($monto_total,2,',','.');
        }


        return $reservas;


    }




    public function getAnular(Request $request){
        
        $reserva = Reserva::find($request->id);
        $reserva->status = 3;
        $reserva->save();

        $reservas = Reserva::where('status',1)->with('cliente','status')->orderBy("id",'desc')->get();
        return $reservas;
    }
    
    public function getAnularProductos(Request $request){

        $hoy = Carbon::now('America/Caracas')->format('Y-m-d');
        $findia = Carbon::now('America/Caracas')->addHours(23)->addMinutes(60)->addSeconds(60)->format('Y-m-d');

        $reservas = Reserva::where('status',1)->where('fecha_registro','>=',$hoy)->where('fecha_registro','<',$findia)->orderBy("id",'desc')->get();
        $prueba = [];
        foreach($reservas as $reserva){
            $carrito = Carrito::where('fecha_registro','>=',$hoy)->where('fecha_registro','<',$findia)->where('id_producto',$request->id)->where('tipo','Producto')->where('reserva_id',$reserva->id)->get();
            foreach($carrito as $carr){
                $prueba[] = $carr;
                $carr->delete();
            }
            $carrito_promo = Carrito::where('fecha_registro','>=',$hoy)->where('fecha_registro','<',$findia)->where('tipo','Promo')->where('reserva_id',$reserva->id)->get();
            foreach($carrito_promo as $carrpromo){
            
                $productos_promo_compuesto = Productopromociones::where('id_promocion',$request->id)->get();
                if(count($productos_promo_compuesto) > 0){
                    //return $productos_promo_compuesto;
                    if($productos_promo_compuesto[0]->producto->tipo == 'COMPONENTE'){
                        $carrpromo->delete();
                    }
                }
                
                $productos_promo = Productopromociones::where('id_promocion',$carrpromo->id_producto)->where('id_producto',$request->id)->lists('id_promocion');
                if(in_array($carrpromo->id_producto,$productos_promo->toArray())){
                    $prueba[] = $carrpromo;
                    $carrpromo->delete();
                }
            }
            
            $carrito_vacio = Carrito::where('fecha_registro','>=',$hoy)->where('fecha_registro','<',$findia)->where('reserva_id',$reserva->id)->get();
            

            if(count($carrito_vacio) == 0){
                $reserva->update(['status' => 3]);
            }
        }

        return 1;
    }
        
    public function getBuscarProductosReservados(Request $request){

        $hoy = Carbon::now('America/Caracas')->format('Y-m-d');
        $findia = Carbon::now('America/Caracas')->addHours(23)->addMinutes(60)->addSeconds(60)->format('Y-m-d');

        if($request->nombre == 'Pendiente'){
            $ststus_id = 1;
        }else if($request->nombre == 'Entregado'){
            $ststus_id = 2;
        }else if($request->nombre == 'Anulado'){
            $ststus_id = 3;
        }else{
            $ststus_id = 1;
        }

        $reservas = Reserva::where('status',$ststus_id)->where('fecha_registro','>=',$hoy)->where('fecha_registro','<',$findia)->orderBy("id",'desc')->get();
        

        $monto_total = 0;
        $productos = [];
        $datos = [];
        $cantidad = 0;        
        foreach($reservas as $reserva){
            $carrito = Carrito::with('producto')->where('fecha_registro','>=',$hoy)->where('fecha_registro','<',$findia)->where('reserva_id',$reserva->id)->get();
            
                //$monto_total = 0;
            
                foreach($carrito as $car){

                    if($car->tipo == 'Promo'){
                        $productos_promo = Productopromociones::where('id_promocion',$car->id_producto)->get();
                        if($productos_promo[0]->producto->tipo == 'COMPONENTE'){
                            if(!in_array($productos_promo[0]->id_promocion.'COMPUESTO',$productos)){   
                                
                                $new['id'] = $productos_promo[0]->promociones->id;
                                $new['nombre_producto'] = $productos_promo[0]->promociones->nombre;
                                $new['cantidad'] = $car->cantidad;
                                $new['horario'] = $reserva->horario;
                    
                                $datos[] = $new;
                                $productos[] = $productos_promo[0]->id_promocion.'COMPUESTO';
                            }else{
                                $clave = array_search($productos_promo[0]->id_promocion.'COMPUESTO', $productos); // $clave = 2;
                                $datos[$clave]['cantidad'] += $car->cantidad ;
                            }
                        }
                        foreach($productos_promo as $propromo){
                            if($propromo->producto->tipo == 'NORMAL'){

                            
                                if(!in_array($propromo->id_producto,$productos)){   
                                    //$cantidad += $car->cantidad * $propromo->cant_producto; 
                                    $new['id'] = $propromo->producto->id;
                                    $new['nombre_producto'] = $propromo->producto->nombre;
                                    $new['cantidad'] = $car->cantidad * $propromo->cant_producto;
                                    $new['horario'] = $reserva->horario;
                        
                                    $datos[] = $new;
                                    $productos[] = $propromo->producto->id;
                                }else{
                                    $clave = array_search($propromo->producto->id, $productos); // $clave = 2;
                                    $datos[$clave]['cantidad'] += $car->cantidad * $propromo->cant_producto;
                                }
                            }

                            
                        }
                    }else{
                        if(!in_array($car->id_producto,$productos)){   
                            //$cantidad += $car->cantidad; 
                            $new['id'] = $car->producto->id;
                            $new['nombre_producto'] = $car->producto->nombre;
                            $new['cantidad'] = $car->cantidad;
                            $new['horario'] = $reserva->horario;
                    
                            $datos[] = $new;
                            $productos[] = $car->id_producto;
                        }else{
                            $clave = array_search($car->id_producto, $productos); // $clave = 2;
                            $datos[$clave]['cantidad'] += $car->cantidad;

                        }              
                            
                    }
                 

                }
            
            
        }
        
        return $datos;
    }

}
