<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model {

    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'pedido';
    protected $fillable = ['caja_fiscal','id_codigo','monto','fecha_registro','tipo_pago','cash_cliente','vuelto_cliente','status_id','menosbs','menosdolar'];
    
  public function getFechaRegistroFormatAttribute(){
		return  Carbon::parse($this->fecha_registro)->format('h:i:s A'); 
	}
    
  public function getMontoFormatAttribute(){
		return  number_format($this->monto,'2',',','.'); 
	}
  
  public function cliente() {
    return $this->hasOne('App\Cliente','codigo','id_codigo');
  }


   public function productos() {
    return $this->hasMany('App\Productovendido','id_pedido','id');
  }


}
