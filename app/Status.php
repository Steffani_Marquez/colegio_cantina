<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'status';
    protected $fillable = ['nombre'];
}
