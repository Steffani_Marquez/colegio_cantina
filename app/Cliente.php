<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'clientes';

    protected $fillable = ['nombre','edad','fecha_registro','curso_actual','abono_actual','codigo','monto_deuda_max','codigo_familiar','limitediario','passwordrepresentante','passwordalumno','correo_representante2','apellido_representante2','nombre_representante2','cedula_representante2','correo_representante','apellido_representante','nombre_representante','cedula_representante','correo_alumno','apellido_alumno','nombre_alumno','cedula','seccion','grado','grado','tipo_cliente_id','num_entradas','fecha_creado','telefono','abono_bs','user_id','restriccion_alimenticia','inactivo'];
    
    public function tipo_cliente() {
        return $this->hasOne('App\TipoCliente','id','tipo_cliente_id');
    }

}
