<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promociones extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'promociones';
    protected $fillable = ['id_categoria','nombre','status','descripcion','costo','imagen'];

    public function categoria() {
		return $this->hasOne('App\Categoria','id','id_categoria');
	}
}
