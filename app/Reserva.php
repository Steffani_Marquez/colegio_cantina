<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Reserva extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'reservas';
    protected $fillable = ['id_codigo','fecha_registro','status','horario','fecha_reserva'];
    
    public function cliente() {
      return $this->hasOne('App\Cliente','codigo','id_codigo');
    }

    public function carrito() {
      return $this->hasMany('App\Carrito','reserva_id','id');
    }
      
    public function status() {
      return $this->hasOne('App\Status','id','status');
    }

    public function getFechaRegistroFormatAttribute(){
      return  Carbon::parse($this->fecha_registro)->format('d/m/Y h:i:s A'); 
    }
    
 
}
