<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productovendido extends Model
{

    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'productosvendidos';
    protected $fillable = ['id','id_pedido','id_producto','monto','cantidad','fecha_registro','tipo'];

    public function pedido() {
		return $this->hasOne('App\Pedido','id','id_pedido');
	}

	public function productos() {
		return $this->hasOne('App\Productos','id','id_producto');
	}

	public function promocion() {
		return $this->hasOne('App\Promociones','id','id_producto');
	}

}
