<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productodeposito extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'producto_deposito';
    protected $fillable = ['id','id_producto','id_deposito','cantidad'];

    public function producto() {
		return $this->hasOne('App\Productos','id','id_producto');
	}
    
}
