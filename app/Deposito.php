<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposito extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'producto_deposito';
    protected $fillable = ['nombre'];
}
