<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carritoporvalidar extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'carritoporvalidar';
    protected $fillable = ['id_cliente','fecha_registro','monto','id_recarga','valido','codigo_familiar'];

	
	public function cliente() {
		return $this->hasOne('App\Cliente','id','id_cliente');
	}

	public function recarga() {
		return $this->hasOne('App\Recarga','id','id_recarga');
	}

}
