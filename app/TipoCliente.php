<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCliente extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'tipo_cliente';
    protected $fillable = ['nombre'];
}
