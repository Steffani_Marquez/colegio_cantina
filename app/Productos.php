<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'productos';
    protected $fillable = ['id','id_categoria','nombre','status','descripcion','costo','imagen','tipo','precio_costo'];

    public function categoria() {
		return $this->hasOne('App\Categoria','id','id_categoria');
	}
	
}
