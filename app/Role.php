<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Role extends Model {

    protected $table = 'role';

    public function users()
    {
        return $this->belongsToMany('App\User', 'role_user', 'role_id', 'user_id');
    }
 
    /**
     * many-to-many relationship method.
     *
     * @return QueryBuilder
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

}