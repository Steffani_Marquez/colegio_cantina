<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrito extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = 'id';
    protected $table = 'carrito';
    protected $fillable = ['id_producto','codigo_cliente','fecha_registro','cantidad','tipo','reserva_id'];

	
	public function cliente() {
		return $this->hasOne('App\Cliente','codigo','codigo_cliente');
	}

	public function producto() {
		return $this->hasOne('App\Productos','id','id_producto');
	}

	public function promocion() {
		return $this->hasOne('App\Promociones','id','id_producto');
	}

	public function promo() {
		return $this->hasOne('App\Promociones','id','id_producto');
	}

}
