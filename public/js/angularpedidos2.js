		
		var AllPost_app = angular.module('PedidosApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('PedidosController', function($scope, $http) {

 
			$scope.pedidos_arr = [];
			$http.get('pedidos/buscar', {params: { caja:0, desde: $('#desde').val(), hasta:$('#hasta').val()  }}).
			success(function(data, status, headers, config){
			$scope.pedidos_arr = data;
			$('.load').hide();
					if (data.length == 0) {
					  $('.nohay').show();
					  $('#precio').text(0);	
					}else{
						$('.nohay').hide();
						 $('#precio').text(data[0].total_total);	
					}
			
			});

			$scope.buscar = function(id){

				if (id == 0) {
					 $('#pasarcajis').show();
				}else{
					$('#pasarcajis').hide();
				}

				$('#seleccionado').val(id);
                $('.load').show();
				$scope.pedidos_arr = [];
				$http.get('pedidos/buscar', {params: { caja:id,desde: $('#desde').val(), hasta:$('#hasta').val() }}).
				success(function(data, status, headers, config){
				$scope.pedidos_arr = data;
					$('.load').hide();
					
					if (data.length == 0) {
					  $('.nohay').show();	
					   $('#precio').text(0);	
					}else{
                      $('.nohay').hide();
                      $('#precio').text(data[0].total_total);
					}

				});
			}


			$scope.modalelim = function(id){
				$('#openelim').modal();
				$('#elim_elegido').val(id);

				alert(id);
			}


			$scope.eliminarpedido = function(){

				
				$('#btnselimn').hide();
				$('.load').show();

				$scope.pedidos_arr = [];
				$http.get('pedidos/eliminarpedido', {params: { id:$('#elim_elegido').val(),desde: $('#desde').val(), hasta:$('#hasta').val() }}).
				success(function(data, status, headers, config){
				$scope.pedidos_arr = data;
				$('#openelim').modal("hide");
				$('#openmodaldetalle').modal("hide");

				$('#btnselimn').show();
				$('.load').hide();

				    $('.nohay').hide();
	                $('.load').show();
					$scope.pedidos_arr = [];
					$http.get('pedidos/buscar', {params: { caja: $('#seleccionado').val(),desde: $('#desde').val(), hasta:$('#hasta').val() }}).
					success(function(data, status, headers, config){
					$scope.pedidos_arr = data;
						$('.load').hide();
						
						if (data.length == 0) {
						  $('.nohay').show();	
						  $('#precio').text(0);
						}else{
	                      $('.nohay').hide();
	                      $('#precio').text(data[0].total_total);
						}

					});
				});

			}	



			$scope.buscar2 = function(id){

				var desde1 = $('#desde').val();
				var hasta1 = $('#hasta').val();

				var concatenar = "Desde: "+desde1+" "+"Hasta: "+hasta1;


				window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                'event': 'GenericGAEvent',
                'eventCategory': 'buscar',
                'eventAction': 'buscar-pedidos-click',
                'tipo': 'admin',
                'colegio': 'cumbres',
                'pais': 'VE',
				'fechabusqueda': concatenar
                });

                window.dataLayer.push(function() {
				  this.reset();
				})

                $('.load').show();
				$scope.pedidos_arr = [];
				$http.get('pedidos/buscar', {params: { caja: $('#seleccionado').val(),desde: $('#desde').val(), hasta:$('#hasta').val() }}).
				success(function(data, status, headers, config){
				$scope.pedidos_arr = data;
					$('.load').hide();
					
					if (data.length == 0) {
					  $('.nohay').show();	
					  $('#precio').text(0);
					}else{
                      $('.nohay').hide();
                      $('#precio').text(data[0].total_total);
					}

				});
			}



			$scope.pasaracaja = function(id){
                
                $('.load').show();

				$scope.pedidos_arr = [];
				$http.get('pedidos/pasaracaja', {params: { caja:id,desde: $('#desde').val(), hasta:$('#hasta').val() }}).
				success(function(data, status, headers, config){
				$scope.pedidos_arr = data;

					$('.load').hide();

					if (data.length == 0) {
					  $('.nohay').show();	
					  $('#precio').text(0);
					}else{
                      $('.nohay').hide();
                      $('#precio').text(data[0].total_total);
					}
				});
			}
			
			$scope.openmodaldetalle = function(productosvendidos){
				$('#openmodaldetalle').modal('toggle');
				$scope.productosvendidos = productosvendidos;
			}					
    

	});