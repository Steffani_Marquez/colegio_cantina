		
		var AllPost_app = angular.module('CantinaProductosApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('CantinaProductosController', function($scope, $http) {


                        $scope.productos_arr = [];
                        $http.get('productos/buscar', {params: { status:1}}).
                        success(function(data, status, headers, config){
                        $scope.productos_arr = data;
                        });

                    
                    $scope.buscar = function(id){

                        $('.load').show();
                        $('#id_elegida').val(id);

                        if (id == 1) {
                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'activos',
                            'eventAction': 'activos-click',
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE'
                            });
                        }

                        if (id == 0) {
                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'inactivos',
                            'eventAction': 'inactivos-click',
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE'
                            });
                        }
                      
    					$scope.productos_arr = [];
    					$http.get('productos/buscar', {params: { status: id}}).
    					success(function(data, status, headers, config){
    					$scope.productos_arr = data;
                          $('.load').hide();
    					});

                     }


                    $scope.nuevoproducto = function(){

                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                        'event': 'GenericGAEvent',
                        'eventCategory': 'nuevo-producto',
                        'eventAction': 'nuevo-producto-click',
                        'tipo': 'admin',
                        'colegio': 'cumbres',
                        'pais': 'VE'
                        });

                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                        'event': 'app.pageview',
                        'pageName' :'nuevo-producto',
                        'colegio': 'cumbres',
                        'pais' :'VE',
                        'tipo' : "",
                        'seccion' :'cliente'
                        });

                        $('#nuevoproducto').modal('toggle');
                    }
                    
                    $scope.editarproducto = function(producto){
                        
                        $('#nombre_edicion').val(producto.nombre);
                        $('#precio_edicion').val(producto.costo);
                        $('#precio_costo_edicion').val(producto.precio_costo);
                        $('#descripcion_edicion').text(producto.descripcion);
                        
                            $scope.producto_edicon = producto;

                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'editar-producto',
                            'eventAction': 'editar-producto-click',
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE',
                            'producto': producto.nombre
                            });

                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'app.pageview',
                            'pageName' :'editar-producto',
                            'colegio': 'cumbres',
                            'pais' :'VE',
                            'tipo' : "",
                            'seccion' :'cliente'
                            });


                        $('#editarproducto').modal('toggle');
                    }

                    var ultimopro = "";

                    $scope.modalinventario = function(producto){

                        $('#modalinventario').modal('toggle');
                        $scope.invprod = producto;

                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'ingresar-producto',
                            'eventAction': 'ingresar-producto-click',
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE',
                            'producto': producto.nombre
                            });

                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'app.pageview',
                            'pageName' :'carga-de-producto',
                            'colegio': 'cumbres',
                            'pais' :'VE',
                            'tipo' : "",
                            'seccion' :'cliente'
                            });


                            ultimopro = producto.nombre; 

                        $http.get('productos/seguimientoinventario', {params: { id: producto.id}}).
                        success(function(data, status, headers, config){
                        $scope.seguimientos_arr = data;
                        });
                        
                    }

                    $scope.guardarinventario = function(id){

                        if ( ($('#cantidad').val() == 0) || ($('#accion').val() == 0) ) {
                           
                            $('.error1').show();
                       
                        }else{
                            
                            $('.btningreso').hide();
                            $('.load').show();

                            if ($('#accion').val() == 1) {
                                var accion1 = "ingreso-productos";
                            }else{
                                var accion1 = "salida-productos";
                            }
                            
                            var cantinvin = $('#cantidad').val();

                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'ingresar-producto',
                            'eventAction': 'ingresar-producto-click',
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE',
                            'producto': ultimopro,
                            'eventAction': accion1,
                            'cantidadinventario':cantinvin
                            });


                            $http.get('productos/guardarinventario', {params: { id: id, accion: $('#accion').val(), cantidad: $('#cantidad').val(), nota: $('#detalle').val() }}).
                            success(function(data, status, headers, config){
                            $scope.seguimientos_arr = data['seguimientos'];
                            $scope.productos_arr = data['productos'];
                            $scope.invprod = data['producto'];
                            $('.btningreso').show();
                            $('.load').hide();
                            $('.error1').hide();

                                   

                            });
                        }
                    }
                    
                    


                    $scope.nuevo = function() {

                        $('.btnguardar').hide();
                        $('.load').show();

                        if (($('#nombre').val() == "") || ($('#preci').val() == "")) {
                            $('.error1').show();
                            $('.btnguardar').show();
                            $('.load').hide();
                        }else{
                            
                            $('.error1').hide();
                            var formData = new FormData(document.getElementById("nuevoproductoform"));
                            datos= [];

                            var nombrepro = $('#nombre').val();
                            var preciopro = $('#precio').val();
                            var costopro = $('#precio_costo').val();
                            
                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'crear-nuevo-producto',
                            'eventAction': 'crear-nuevo-producto-click',
                            'costoProducto': costopro,
                            'precioProducto': preciopro,
                            'producto': nombrepro,
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE',
                            'tipo': 'admin'
                            });


                            $.ajax({
                                url: "productos/nuevo",
                                type: "post",
                                dataType: "html",
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false
                            }).done(function(res){
                                $http.get('productos/buscar', {params: { status: 1}}).
                                success(function(data, status, headers, config){
                                $scope.productos_arr = data;
                                });
                                $('.btnguardar').show();
                                $('.load').hide();
                                $('#nuevoproducto').modal('toggle');
                            });

                        }
   
					}



                    $scope.editar = function() {

                         $('.load').show();
                         $('.edit1').hide();

                        var formData = new FormData(document.getElementById("editarproductoform"));

                        $.ajax({
                            url: "productos/editar",
                            type: "post",
                            dataType: "html",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false
                        }).done(function(res){
                            //$scope.productos_arr.push(res);
                            $http.get('productos/buscar', {params: { nombre:1}}).
                            success(function(data, status, headers, config){
                            $scope.productos_arr = data;
                            });
    
                            $('#editarproducto').modal('toggle');

                            $('.load').show();

                            $scope.productos_arr = [];
                            $http.get('productos/buscar', {params: { status:$('#id_elegida').val()}}).
                            success(function(data, status, headers, config){
                            $scope.productos_arr = data;
                            $('.load').hide();
                          
                             $('.edit1').show();

                            });
                        });

       
					}

                    

	});