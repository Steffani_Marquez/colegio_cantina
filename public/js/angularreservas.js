		
		var AllPost_app = angular.module('ReservaApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});
		
        AllPost_app.controller('ReservaController', function($scope, $http) {
			
 
					$scope.reservas_arr = [];
					$http.get('reservas/buscar', {params: { nombre:1, desde: $('#desdereser').val(), hasta:$('#hastareser').val() }}).
					success(function(data, status, headers, config){
					$scope.reservas_arr = data;
					console.log($scope.reservas_arr);
					});

					$scope.buscar = function(nombre){

						if (nombre == "Pendiente") {
							    window.dataLayer = window.dataLayer || [];
						        window.dataLayer.push({
						        'event': 'GenericGAEvent',
						        'eventCategory': 'pendientes',
						        'eventAction': 'pendientes-click',
						        'tipo': 'admin',
						        'colegio': 'cumbres',
						        'pais': 'VE',
						        });
						}

						if (nombre == "Anulado") {
							    window.dataLayer = window.dataLayer || [];
						        window.dataLayer.push({
						        'event': 'GenericGAEvent',
						        'eventCategory': 'anulados',
						        'eventAction': 'anulados-click',
						        'tipo': 'admin',
						        'colegio': 'cumbres',
						        'pais': 'VE',
						        });
						}

						$scope.reservas_arr = [];
						$http.get('reservas/buscar', {params: { nombre: nombre, desde: $('#desdereser').val(), hasta:$('#hastareser').val() }}).
						success(function(data, status, headers, config){
						$scope.reservas_arr = data;
						console.log($scope.reservas_arr);
						});
					}
					
					$scope.productosreservas_arr = [];
					$http.get('reservas/buscar-productos-reservados', {params: { nombre:1, desde: $('#desde').val(), hasta: $('#hasta').val()}}).
					success(function(data, status, headers, config){
					$scope.productosreservas_arr = data;
					console.log($scope.productosreservas_arr);
					});

					$scope.buscarproducto = function(nombre){

						$scope.productosreservas_arr = [];
						$http.get('reservas/buscar-productos-reservados', {params: { nombre: 1, desde: $('#desde').val(), hasta: $('#hasta').val()}}).
						success(function(data, status, headers, config){
						$scope.productosreservas_arr = data;
						
						});
					}

					$scope.openmodalanular = function(id){
						$('#reserva_id').val(id);
						$('#openmodalanular').modal('toggle');
					}

					$scope.openmodalanularprod = function(id){
						$('#productores_id').val(id);
						$('#openmodalanularprod').modal('toggle');
					}
					
					$scope.anular = function(){
						
						$scope.reservas_arr = [];
						$http.get('reservas/anular', {params: { id: $('#reserva_id').val()}}).
						success(function(data, status, headers, config){
						$scope.buscarproducto();
						$scope.buscar();

						$('#openmodalanular').modal('hide');
						});
					
					}
					$scope.anularrespro = function(){
						
						$http.get('reservas/anular-productos', {params: { id: $('#productores_id').val()}}).
						success(function(data, status, headers, config){
						
						$scope.buscarproducto();
						$scope.buscar();
						$('#openmodalanularprod').modal('hide');
						});

						
					
					}					
					
					$scope.openmodaldetalle = function(carrito){
						$('#openmodaldetalle').modal('toggle');
						$scope.carrito = carrito;
				
					}


	});