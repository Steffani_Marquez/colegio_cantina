		
		var AllPost_app = angular.module('CantinaApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('CantinaController', function($scope, $http) {


        		$scope.auxiliar_arr = [];
				$http.get('compras/buscarauxiliar', {params: { nombre:1}}).
				success(function(data, status, headers, config){
				$scope.auxiliar_arr = data;
				
				$("#tasamanual").val(data.dolar);
				});


 
			$scope.productos_arr = [];
			$http.get('ventas/buscar', {params: { nombre:1}}).
			success(function(data, status, headers, config){
			$scope.productos_arr = data;
					 
			});

			$scope.promos_arr = [];
			$http.get('ventas/buscarpromos', {params: { nombre:1}}).
			success(function(data, status, headers, config){
			$scope.promos_arr = data;
			});


				$scope.unmtele = function(id) {
                	$("#armasonpagos").show();
	       		}


	       	$scope.recargarsaldo = function(id) {
                	$("#recargarsaldo").show();
                	$("#conttodo").hide();
            }

            $scope.volverrecarga = function(id) {
                	$("#recargarsaldo").hide();
                	$("#conttodo").show();
            }

            


	       	$scope.recargar = function(id) {

     			var error = 0;

     			$("#movilerror").hide();
     			$("#zelleerror").hide();

     			if (id == "pago movil") {
                    if ($("#refepagomovil").val() == "") {
                        $("#movilerror").show();
                        error = 1;
                   }else{
                   		$("#movilerror").hide();
                   }
     			}

     			if (id == "zelle") {
                    if ($("#refezelle").val() == "") {
                        $("#zelleerror").show();
                        error = 1;
                   }else{
                   		$("#zelleerror").hide();
                   }
     			}

   

     			if (error == 0) {
                    
                       $(".load").show();
                       $(".btnfinalpago").hide();  
     			
			     		$scope.inser_arr = []; 
						$http.get('ventas/recargar', {params: { teleinfo: $("#teleinfo").val(),refeefectivo: $("#refeefect").val(),tasa: $("#tasisactual").val(),refezelle: $("#refezelle").val(), refepagomovil: $("#refepagomovil").val(), refepaypal: $("#refepaypal").val() ,tipo_pago:id, codigo_familiar: $("#codigo_familiar").val()}}).
						success(function(data, status, headers, config){
						$scope.inser_arr = data;
						 $(".recarre").show();
						 $scope.buscarcliente();
						   //location.reload();

						});

				}
     		}


				$scope.cambiomodo = function(id) {
                   
                    $("#recarga1").show();

			        if (id  == "dolares") {

				            $("#pagodolares").show();
				            $("#pagobolivares").hide();
				            $(".bntcer").removeClass("marcado");
				            $(".doo1").addClass("marcado"); 

				            vartipo = "Dolares";

				            $(".totalbs").hide();
				            $(".totaldol").show();


				            $(".palbs").text("DOLARES A RECARGAR"); 

			        }else{

			            $("#pagobolivares").show();
			            $("#pagodolares").hide();
			            $(".bntcer").removeClass("marcado");
			            $(".boo1").addClass("marcado");

			            vartipo = "Bolivares";

			            $(".totalbs").show();
			            $(".totaldol").hide();

			            $(".palbs").text("BOLIVARES A RECARGAR"); 
			          }

                    
                  $scope.inser();
     		}

                   
                   


                   $scope.refres = function() {
                      
                        
						$("#cliente_vista").hide();
						$("#codigobar").show();
					    $scope.carrito_arr = [];
					    $scope.cliente_arr = [];
					    
					    $("#clientecodigo").val("");

						$("#topcliente").hide();
						$("#emtycarr").show();
						$("#cerosub").show();
					    $("#cerototal").show();
					     $("#modal2").modal("hide");

					     $(".contenshe").hide();
					     $(".btnbotn2").removeClass("activecar");
     					 $(".btnheadercliente").addClass("activecar");

     					 $("#headercliente").show();

     					 
						$(".sincodigo").show();
						$("#abonopago").hide();
     					 

					    

                   	}


					$scope.back = function() {
						$("#clientecodigo").val(0);
						$("#cliente_vista").hide();
						$("#totalmontobolivares").text(0);

						$(".sincodigo").show();
						$("#abonopago").hide();
						
						$("#totalmontodolares").text(0);

						$("#codigobar").show();

						const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        				let result1= Math.random().toString(36).substring(0,15);  
						var  codigocliente_antiguo =  $("#codigo_personal_antiguo").val();
					    $scope.carrito_arr = [];
					    $scope.cliente_arr = [];
					    $("#codigo_personal").val(result1);
					    $("#clientecodigo").val("");
						$("#topcliente").hide();
						$("#emtycarr").show();
						$("#cerosub").show();
					    $("#cerototal").show();
					}


					$scope.elegirreserva = function(id) {

                         $(".cajisreser").removeClass("reserelegido");
						 $("#reser"+id).addClass("reserelegido");

						var  codigo_personal =  $("#clientecodigo").val();

						
						$("#reservaelegida").val(id);

                        $scope.carrito_arr = [];
						$http.get('ventas/elegirreserva', {params: {  id:id,codigo_personal: codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = [];
										$("#emtycarr").hide();
										$("#cerosub").hide();
										$("#cerototal").hide();
										$("#cerototal2").hide();
										$("#codigo_personal").val(data.codigo);
									
										$scope.carrito_arr = data.carrito;

										cambio('headercarrito');


						});

					}


					$scope.inser = function(id) {

     			    if (id == undefined) {return false;}
     		 	
		     		var valor = $("#input"+id).val();
		     		var tasa =  $("#tasadolartoday").val();

	     		 	if (vartipo == "Dolares") {

			     		 	if (parseFloat(valor) > 999 ) {
			     		 		alert("Recuerda que el monto que debe colocar es el equivalente en dolares. por lo tanto no se permiten mas de 3 cifras por seguridad");
			     		 	}

			     	}
				  
			     	if (valor == "") {
		                    valor = 0;
			     	}

		     		 	$scope.inser_arr = []; 
						$http.get('accesoclientes/insertarcarrito', {params: { tasa: $("#tasadolartoday").val(),vartipo: vartipo,codigo_familiar: $("#codigo_familiar").val(),id_cliente: id,valor:valor}}).
						success(function(data, status, headers, config){
						$scope.inser_arr = data;
							if (vartipo == "Dolares") {

								$(".totalbs").hide();
                                $(".totaldol").show();
    							$(".totalmon").text(data);
								$("#totalpaypal").val(data);
								var valortruncado = parseFloat(data);
								valortruncado = valortruncado.toFixed(2);
								$(".totalmonbs").text(addCommas(valortruncado));
								
							}else{

								
                                $(".totalbs").show();
                                $(".totaldol").hide();

								
								$(".totalmon").text(data);
								$("#totalpaypal").val(data);
								var valortruncado =  parseFloat(data);
								valortruncado = valortruncado.toFixed(2);
								$(".totalmonbs").text(addCommas(valortruncado));
							}
						});
     		
     		}




					$scope.buscarcliente = function() {

								var  codigocliente =  $("#clientecodigo").val();
						
								$("#totalmontobolivares").text(0);
								$("#totalmontodolares").text(0);
								$(".codigin").removeClass("animate__pulse");
								$(".codigin").removeClass("animate__animated");
								$(".codigin").addClass("animate__animated");
								$(".codigin").addClass("animate__headShake");
								$(".load").show(); 
								$(".botonis").hide();
								$(".erri").hide();
								$(".btnfinalpago").show();
								$("#teleinfo").val("");
								$("#refezelle").val("");
								$("#refeefect").val(""); 
								$("#refepagomovil").val(""); 
								$("#recargarsaldo").hide();
                				$("#conttodo").show();
                				
                				setTimeout(function(){ 
		                        	$(".recarre").hide();
		                    	}, 3000);
                				

								if (codigocliente != "") {
			                   
					                    $scope.cliente_arr = [];
										$http.get('ventas/buscarcliente2',{params: { tasa_actual: $("#tasisactual").val(),codigocliente: codigocliente}}).
										success(function(data, status, headers, config){
										$scope.cliente_arr = data;

											$(".load").hide(); 
											$(".botonis").show();

											$(".codigin").addClass("animate__pulse");
											$(".codigin").removeClass("animate__headShake");

										if (data != 0) {

											$(".sincodigo").hide();
                                            
                                            $("#abonopago").show();
											
											
											$scope.cliente2_arr = [];
											$http.get('ventas/buscarcliente3',{params: { codigocliente: codigocliente}}).
											success(function(data, status, headers, config){
											$scope.cliente2_arr = data;
												$("#codigo_familiar").val(data[0].codigo_familiar);
											});

											window.dataLayer = window.dataLayer || [];
											window.dataLayer.push({
											'event': 'GenericGAEvent',
											'eventCategory': 'navegaciontienda',
											'eventAction': 'buscar-cliente',
											'eventLabel': 'buscar-cliente-click',
											'codigo': codigocliente,
											'cliente': data.nombre,
											'tipo': 'admin',
											'colegio': 'cumbres',
											'pais': 'VE'
											});

											if (data.numpedidos > 0) {
												 $("#ultimacompra").show();
											}else{
												$("#ultimacompra").hide();
											}

		   								    $("#topcliente").show();
											$("#cliente_vista").show();
											$("#codigobar").hide();
											var codigo_personal = $("#codigo_personal").val();
											$scope.carrito_arr = [];
											$http.get('ventas/buscarclientecarrito', {params: { codigo_actual: codigo_personal,codigo_personal: data.codigo}}).
											success(function(data, status, headers, config){
											if (data.tienecar == 1) {
												$("#emtycarr").hide();
												$("#cerosub").hide();
												$("#cerototal").hide();
											}else{
												$("#emtycarr").show();
												$("#cerosub").show();
												$("#cerototal").show();
											}

											$scope.carrito_arr = data.carrito;
											$("#codigo_personal").val(data.codigo);
											});
										}else{
											$("#cliente_vista").hide();
											$("#codigobar").show();
											$(".erri").show();
										}

										});

								}else{
									$(".erri").show();
									$(".botonis").show();
									$(".load").hide();
								}


					}

					$scope.vaciar = function() {

						var codigo_personal = $("#codigo_personal").val();
       
						$http.get('ventas/vaciarcarrito', {params: { codigo_personal: codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = [];
							$("#cerosub").show();
							$("#cerototal").show();
						});

					}


					$scope.buscarproducto = function(id) {

						if (id == "Todos") {
							$(".btnbotn").removeClass("activecar2");
	    					$(".btnt"+"Todos").addClass("activecar2");
						}
	                     
						$http.get('ventas/buscarcategorias', {params: { categoria: id}}).
						success(function(data, status, headers, config){
						$scope.productos_arr = data;
						});

						$http.get('ventas/buscarcategoriaspromos', {params: { categoria: id}}).
						success(function(data, status, headers, config){
						$scope.promos_arr = data;
						});

					}


					$scope.restarppv = function(id) { 

					   	valor = $("#inputvalor"+id).text();
						
					    valor = $("#inputvalor"+id).text();
						nuevovalor = parseFloat(valor) - parseFloat(1);
						$("#inputvalor"+id).text(nuevovalor);
						codigo_personal = $("#codigo_personal").val();

						$http.get('ventas/actualizarcarrito', {params: { reserva_elegida: $("#reservaelegida").val(),inicial:0,id: id,cantidad:nuevovalor,codigo_personal:codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;
							if (data == "") {
								$("#emtycarr").show();
								$("#cerosub").show();
								$("#cerototal").show();
							}
						
						});
	                     
					}

					$scope.sumarppv = function(id) { 
	                  
	                    valor = $("#inputvalor"+id).text();
						nuevovalor = parseFloat(valor) + parseFloat(1);
						$("#inputvalor"+id).text(nuevovalor);
						codigo_personal = $("#codigo_personal").val();

						$http.get('ventas/actualizarcarrito', {params: { reserva_elegida: $("#reservaelegida").val(),inicial:0,id: id,cantidad:nuevovalor,codigo_personal:codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;
								$("#cerosub").hide();
								$("#cerototal").hide();
						});

					}


					$scope.agregaralcarr_carproducto = function(id) { 
						valor = 1;
					   	codigo_personal = $("#codigo_personal").val();
						$http.get('ventas/actualizarcarrito', {params: { reserva_elegida: $("#reservaelegida").val(),tipo:'Producto',inicial:1,id:id,cantidad:valor,codigo_personal:codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;

							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event': 'GenericGAEvent',
							'eventCategory': 'navegaciontienda',
							'eventAction': 'agregar-carrito',
							'eventLabel':data[0].productosbusq.nombre,
							'categoria': data[0].productosbusq.categoria.nombre,
							'precio': data[0].productosbusq.costo,
							'tipo': 'admin',
							'colegio': 'cumbres',
							'pais': 'VE'
							});

						   	$("#emtycarr").hide();
						   	$("#cerosub").hide();
							$("#cerototal").hide();

						});

					}


					$scope.agregaralcarr_carpromo = function(id) { 

						valor = 1;
					   	codigo_personal = $("#codigo_personal").val();
						$http.get('ventas/actualizarcarrito', {params: { reserva_elegida: $("#reservaelegida").val(),tipo:'Promo',inicial:1,id:id,cantidad:valor,codigo_personal:codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;
							
							   	$("#emtycarr").hide();
							   	$("#cerosub").hide();
								$("#cerototal").hide();

								window.dataLayer = window.dataLayer || [];
								window.dataLayer.push({
								'event': 'GenericGAEvent',
								'eventCategory': 'navegaciontienda',
								'eventAction': 'agregar-carrito',
								'eventLabel':data[0].productosbusq.nombre,
								'categoria': data[0].productosbusq.categoria.nombre,
								'precio': data[0].productosbusq.costo,
								'tipo': 'admin',
								'colegio': 'cumbres',
								'pais': 'VE'
								});

						});

					}

					$scope.finalizarpago = function(id) { 
                        
                        $(".load").show();
                        $(".paguis").hide();

					   	codigo_personal = $("#codigo_personal").val();

						$http.get('ventas/finalizarpago', {params: { tasa: $("#tasisactual").val(),reserva_elegida: $("#reservaelegida").val(),tipo:id,codigo_personal:codigo_personal,vuelto_cliente:$("#vuelto_cliente").val(),cash_cliente:$("#cash_cliente").val() }}).
						success(function(data, status, headers, config){
							
							$(".load").hide();
		                    $(".paguis").show();
	                        
	                        if (data.id == undefined) {
		                        $("#modal1").modal();
		                        $("#textomodal").text(data);
	                     	}else{

	                     		
	                     		$("#codigo_personal").val(data.codigoinv);

	                     		  window.dataLayer = window.dataLayer || [];
	                              window.dataLayer.push({
	                               'event': 'compra',
	                               'transactionId': data.id, 
	                               'transactionAffiliation':'cumbres cafe',
	                               'transactionTotal': data.monto,
	                               'transactionTax': '0',
	                               'transactionShipping': '0',
	                               'transactionProducts': data.datos_fact
	                              });
                                
                                $scope.clientecomprado_arr = [];
							    $http.get('ventas/buscarcliente2',{params: { tasa_actual: $("#tasisactual").val(),codigocliente: codigo_personal}}).
								success(function(data, status, headers, config){
								$scope.clientecomprado_arr = data;
                                
								$("#modal2").modal({backdrop: 'static', keyboard: false});



								});

                                
	                     	}


						});

					}



			function addCommas(nStr){
				nStr += '';
				x = nStr.split(',');
				x1 = x[0];
				x2 = x.length > 1 ? ',' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}

	
	});