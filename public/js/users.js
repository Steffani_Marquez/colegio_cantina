var AllPost_app = angular.module('UsersApp', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});


AllPost_app.controller('UsersController', function ($scope, $http) {

    $scope.users_arr = [];
    
    $http.get('buscar', { params: { id: 1 } }).
        success(function (data, status, headers, config) {
            $scope.users_arr = data;
        });	

    $scope.openeditmodal = function (id) {

        $('#loadadd2').show();
        $http.get('user', { params: {  id: id} }).
        success(function (data, status, headers, config) {
            if(isNaN(parseInt(data.id))){

                $('#lostsessionModal').modal();

            }else{
                $scope.datosuser = data;     
                $('#ediatruser').modal();
                $('#loadadd2').hide();

            }
            
        });
    }
    //$scope.status = $scope.status;
    $scope.estatususer = function (id,index) {
        
        $http.get('changestatus', { params: {  id: id} }).
        success(function (data, status, headers, config) {
            
            if(isNaN(parseInt(data))){
                $('#lostsessionModal').modal();
            }else{
                $scope.users_arr[index].active = data;            

            }
        });
    }
    
    $scope.guardar = function (id) {
        var bandera = 'false';
    
        if($scope.datosuser.name != $('#name').val()){
            bandera = 'true';   
        }

        if($scope.datosuser.username != $('#username').val()){
            bandera = 'true';
        }

        if($scope.datosuser.email != $('#email').val()){
            bandera = 'true';
        }

        if($scope.datosuser.phone != $('#telefono').val()){
            bandera = 'true';
        }
        
        if($('#password').val() != '' && $('#password_confirmation').val() != ''){
            bandera = 'true';
        }
        
        roles = $('#roles').val();
        roles_id = $scope.datosuser.roles_id;
        if(roles.length > 0){
            bandera = 'true';
        }
        /*
        roles.forEach(function(element, index, array) {
            
            if(!roles_id.includes(parseInt(element))){
                //bandera = 'false';
                bandera = 'true';
            }
            //else{
                
            //}
        });
        */
        if(bandera == 'true'){
            
            $('#formeditar').removeAttr('onsubmit');
            $('#formeditar').submit();
        }
        
    }

});




