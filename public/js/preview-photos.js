$(document).ready(function() {
  
    $(function() {
      $('#file-image').change(function(e) {
        addImage(e); 
      });
   
      function addImage(e){
         var file = e.target.files[0],
         imageType = /image.*/;
       
         if (!file.type.match(imageType))
          return;
     
         var reader = new FileReader();
         reader.onload = fileOnload;
         reader.readAsDataURL(file);
      }
     
      function fileOnload(e) {
         var result=e.target.result;
         $('#imgSalida').attr("src",result);
        }
    });

    $(function() {
      $('#file-imageedit').change(function(e) {
        addImage(e); 
      });
      
      function addImage(e){
        var file = e.target.files[0],
        imageType = /image.*/;
          
        if (!file.type.match(imageType))
        return;
        
        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
      }
        
      function fileOnload(e) {
        var result=e.target.result;
        $('#imgSalidaedit').attr("src",result);
      }
    });

    $(function() {

      var count = 0;
    
        $('#nuevoclonproducto').click(function() {
    
          
            var source = $('.formclone:first')
                clone = source.clone(true,true);
    
            clone.find('.formclone').attr('id', function(i, val) {
                return val + count;
            });
            
            clone.insertAfter(this);
            
            count++;
            $('.eliminar').removeAttr('hidden');
        });
  
        $('.eliminarc').on('click',function(){
          //console.log($(this).parent());
          $(this).closest('.formclone').remove();
        })
        
    });


    $(function() {
      
      var count = 0;
  
      $('#nuevoclonproductoedit').click(function() {

        
        var source = $('.formcloneedit:first')
            clone = source.clone(true,true);
  
        clone.find('.formcloneedit').attr('id', function(i, val) {
            return val + count;
        });
        
        clone.insertAfter(this);
        
        count++;
        $('.eliminaredit').removeAttr('hidden');
    });
    //console.log($('.eliminaredit'));
    $(document).on("click", ".eliminaredit", function(){
      //$('.eliminaredit').on('click',function(){
      $(this).closest('.formcloneedit').remove();
    });
  });
      

});


