		
		var AllPost_app = angular.module('RoncolapedidosApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});
        
        AllPost_app.controller('PedidosController', function($scope, $http) {

     			$scope.clientes_arr = [];
				$http.get('verclientes/buscar', {params: { desde: 1 }}).
				success(function(data, status, headers, config){
				$scope.clientes_arr = data;
				});


				$scope.detalle = function(id) { 

                    $("#modaldetalle").modal();
                    $("#id_elegido").val(id);

					$scope.detalle_arr = [];
					$http.get('verclientes/detalle', {params: { id: id}}).
					success(function(data, status, headers, config){
					$scope.detalle_arr = data;
					});

					$scope.pedidos_arr = [];
					$http.get('verclientes/buscarpedidos', {params: { desdetra : $('#desdetra').val(),hastatra : $('#hastatra').val(), id: $("#id_elegido").val() }}).
					success(function(data, status, headers, config){
					$scope.pedidos_arr = data;
					});

					$scope.recargas_arr = [];
					$http.get('verclientes/buscarrecargas', {params: { desdetra : $('#desdetra').val(),hastatra : $('#hastatra').val(), id: $("#id_elegido").val()}}).
					success(function(data, status, headers, config){
					$scope.recargas_arr = data;
					});
	              
					
				}


				$scope.clicktran = function(id) { 
         
                    $("#transaccionesselect").val(id);
                    $(".btnvv").removeClass("btnactive");

                    if (id == 1) {
                    	$("#contpedidos").show();
                    	$("#contrecargas").hide();
                    }
                   
                    if (id == 2) {
                    	$("#contrecargas").show();
                    	$("#contpedidos").hide();
                    }

                    $scope.pedidos_arr = [];
					$http.get('verclientes/buscarpedidos', {params: { desdetra : $('#desdetra').val(),hastatra : $('#hastatra').val(), id: $("#id_elegido").val() }}).
					success(function(data, status, headers, config){
					$scope.pedidos_arr = data;
					});

					$scope.recargas_arr = [];
					$http.get('verclientes/buscarrecargas', {params: { desdetra : $('#desdetra').val(),hastatra : $('#hastatra').val(), id: $("#id_elegido").val()}}).
					success(function(data, status, headers, config){
					$scope.recargas_arr = data;
					});

				}


				
				$scope.editar = function(cliente) {
               
                    $("#modaleditar").modal();
					$('#nombre_alumno').val(cliente.nombre_alumno);
					$('#apellido_alumno').val(cliente.apellido_alumno);
					$('#correo_alumno').val(cliente.correo_alumno);
					$('#cedula_editar').val(cliente.cedula);
					$('#grado').val(cliente.abono_bs);
					$('#abono_actual').val(cliente.abono_actual);

					$('#limitediario').val(cliente.limitediario);
					$('#limitecredito').val(cliente.monto_deuda_max);

					$('#codigoqredit').val(cliente.codigo);

					$scope.cliente_editar = cliente;	
					
				}

				$scope.editarCliente = function(id){

					if (  ($('#codigoqredit').val() == "") ||  ($('#limitecredito').val() == "") || ($('#limitediario').val() == "") || ($('#abono_actual').val() == "") || ($('#grado').val() == "") ) { alert("No pueden haber campos vacios"); return false;}
					
					$http.get('verclientes/editar-cliente', {params: { codigoqredit : $('#codigoqredit').val(), limitecredito : $('#limitecredito').val(), limitediario : $('#limitediario').val(), cliente_id: id, nombre_alumno : $('#nombre_alumno').val(),apellido_alumno : $('#apellido_alumno').val() ,correo_alumno : $('#correo_alumno').val() ,cedula : $('#cedula_editar').val() ,grado : $('#grado').val() ,abono_actual : $('#abono_actual').val() }}).
					success(function(data, status, headers, config){
						$scope.clientes_arr = [];
						$http.get('verclientes/buscar', {params: { desde: 1 }}).
						success(function(data, status, headers, config){
						$scope.clientes_arr = data;
						});
					});
				}

				$scope.inactivar = function(id){

    				$http.get('verclientes/inactivar', {params: { id : id  }}).
					success(function(data, status, headers, config){
						$scope.clientes_arr = [];
						$http.get('verclientes/buscar', {params: { desde: 1 }}).
						success(function(data, status, headers, config){
						$scope.clientes_arr = data;
						});
					});
				}

				$scope.cambiarmax = function(id) { 
                    $("#modalmax").modal();
				}

				$scope.cambiolin = function(id) { 

					if ($("#limite").val() == "") {
						alert("Debes ingresar un monto valido");
					}else{

						$("#botolin").hide();
						$(".load").show();

						$scope.detalle_arr = [];
						$http.get('verclientes/cambiarmax', {params: { id: $("#id_elegido").val(),monto:$("#limite").val()}}).
						success(function(data, status, headers, config){
						$scope.detalle_arr = data;
						   $("#modalmax").modal("hide");
						   $("#botolin").show();
						   $(".load").hide();
						   $("#limite").val('');
						});

					}

				}




				$scope.edit = function(id) { 

                   $("#id_elegido").val(id);
                   $("#modalinsertar").modal();

                    $scope.detalle2_arr = [];
					$http.get('verclientes/detalle2', {params: { id: $("#id_elegido").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle2_arr = data;
					});
					
				}




				$scope.insertar = function() { 

					$(".load").show();
					$("#btnguardar").hide();

					$scope.insertar_arr = [];
					$http.get('verclientes/insertar', {params: { cedula: $("#cedula").val(),telefono: $("#telefono").val(),nombre: $("#nombre").val(),email: $("#email").val(),id: $("#id_elegido").val(),observacion:  $("#observacion").val()}}).
					success(function(data, status, headers, config){
					$scope.insertar_arr = data;
						 $("#observacion").val("");
						 $("#modalinsertar").modal("hide"); 
						 $(".load").hide();
						 $("#btnguardar").show();

						 	$scope.clientes_arr = [];
							$http.get('verclientes/buscar', {params: { desde: 1 }}).
							success(function(data, status, headers, config){
							$scope.clientes_arr = data;
							});
					});
	              
					
				}



				//////////////////
				$scope.nuevocliente = function(){
					$("#modalnuevocliente").modal();

				}
				$scope.importarclientes = function(){
					$("#modalimportarclientes").modal();

				}
				

                
	});

