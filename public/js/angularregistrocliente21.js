		
		var AllPost_app = angular.module('CantinaApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('CantinaController', function($scope, $http) {

            

            $scope.insertar = function(id){

		                $('#btnenvio').hide();
						$('.load').show();

						$('#eroremail').hide();
						$('.error').hide();

						var error = 0;

						if (($('#nombrer1').val() == "") || ($('#apellidor1').val() == "") || ($('#emailr1').val() == "") || ($('#cedular1').val() == "")) {
		                   error = 1;
						}



						if ($("#emailr1").val() != "") {
					
							if($("#emailr1").val().indexOf('@', 0) == -1 || $("#emailr1").val().indexOf('.', 0) == -1) {
						            
								$('#eroremail').show();
					            error = 2;
						    }
						}

						if (error == 1) {
							$('#btnenvio').show();
							$('.load').hide();
							$('.error').show();
							$('.listo2').hide();
						}

						if (error == 2) {
							$('#btnenvio').show();
							$('.load').hide();
							$('#eroremail').show();
						}

						if (error == 0) {
							
							$scope.pedidos_arr = [];
							$http.get('registroclienteonly/enviardatos', {params: {cedular2: $('#cedular2').val(),emailr2: $('#emailr2').val(),apellidor2: $('#apellidor2').val(),nombrer2: $('#nombrer2').val(),gradoh5: $('#gradoh5').val(),apellidoh5: $('#apellidoh5').val(),emailh5: $('#emailh5').val(),apellidoh5: $('#apellidoh5').val(),nombreh5: $('#nombreh5').val(),gradoh4: $('#gradoh4').val(),apellidoh4: $('#apellidoh4').val(),emailh4: $('#emailh4').val(),apellidoh4: $('#apellidoh4').val(),nombreh4: $('#nombreh4').val(),gradoh3: $('#gradoh3').val(),apellidoh3: $('#apellidoh3').val(),emailh3: $('#emailh3').val(),apellidoh3: $('#apellidoh3').val(),nombreh3: $('#nombreh3').val(),gradoh2: $('#gradoh2').val(),apellidoh2: $('#apellidoh2').val(),emailh2: $('#emailh2').val(),apellidoh2: $('#apellidoh2').val(),nombreh2: $('#nombreh2').val(),gradoh1: $('#gradoh1').val(),apellidoh1: $('#apellidoh1').val(),emailh1: $('#emailh1').val(),apellidoh1: $('#apellidoh1').val(),nombreh1: $('#nombreh1').val(),numhijos: $('#numhijos').val(),cedular1: $('#cedular1').val(),emailr1: $('#emailr1').val(), nombrer1: $('#nombrer1').val(), apellidor1:$('#apellidor1').val()}}).
							success(function(data, status, headers, config){
							$scope.pedidos_arr = data;

							    if (data == 0) {

							    	$('.load').hide();
								    $('.listo2').show();
								    $('.error').hide();
								    $('#btnenvio').show();

							    }else{ 
								    $('.load').hide();
								    $('.listo').show();
								    $('.error').hide();
								}

							});
						}

					}

		});