		
		var AllPost_app = angular.module('ReservaApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('ReservaController', function($scope, $http) {

 
			$scope.recargas_arr = [];
			$http.get('recargas/buscar', {params: { id:0}}).
			success(function(data, status, headers, config){
			$scope.recargas_arr = data;
				if (data.length == 0) {
					$(".nohay").show();
				}else{
					$(".nohay").hide();
				}
			});



			$scope.buscarstatus = function(id) { 

					if (id == 0) {
						window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'pendientes',
						'eventAction': 'pendientes-click',
						'tipo': 'admin',
						'colegio': 'cumbres',
                		'pais': 'VE'
						});
					}

					if (id == 1) {

						window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'aprobados',
						'eventAction': 'aprobados-click',
						'tipo': 'admin',
						'colegio': 'cumbres',
                		'pais': 'VE'
						});


					}

					if (id == 2) {

						window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'anulados',
						'eventAction': 'anulados-click',
						'tipo': 'admin',
						'colegio': 'cumbres',
                		'pais': 'VE'
						});
					}

                    
                    $(".load").show();
                    $(".nohay").hide();
                    $scope.recargas_arr = [];
					$http.get('recargas/buscar', {params: { id:id }}).
					success(function(data, status, headers, config){
					$scope.recargas_arr = data;
					$(".load").hide();
						if (data.length == 0) {
							$(".nohay").show();
						}else{
							$(".nohay").hide();
						}
					});
	
			}

			

			$scope.abrirmodalmonto = function(id) {
                  
                $("#modaleditmonto").modal();
                $("#montoelegido").val(id);
                   
			}



			$scope.actualizarmonto = function(id) {

				
                    $(".load2").show();
                    $(".btnactual").hide();

                    var monto1 = $("#montoact").val();

                    $scope.actual_arr = [];
					$http.get('recargas/actualizarmonto', {params: { id:$("#montoelegido").val(),monto:$("#montoact").val()}}).
					success(function(data, status, headers, config){
					$scope.actual_arr = data;


					    window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'actualizar-monto-click',
						'eventAction': monto1,
						'tipo': 'admin',
						'colegio': 'cumbres',
			            'pais': 'VE'
						});

					    
					    $scope.detalle_arr = [];
						$http.get('recargas/detalle', {params: { id:data}}).
						success(function(data, status, headers, config){
						$scope.detalle_arr = data;

									$(".load").show();
				                    $(".nohay").hide();
				                    $scope.recargas_arr = [];
									$http.get('recargas/buscar', {params: { id:0 }}).
									success(function(data, status, headers, config){
									$scope.recargas_arr = data;
									$(".load").hide();
										if (data.length == 0) {
											$(".nohay").show();
										}else{
											$(".nohay").hide();
										}

										$(".load2").hide();
                   						$(".btnactual").show();
                   						$("#modaleditmonto").modal("hide");
                   						$("#montoact").val("");
									});

						});
					});
	
			}


			$scope.detalles = function(id) {

				    $("#modalvalidar").modal();
                    $scope.detalle_arr = [];
					$http.get('recargas/detalle', {params: { id:id }}).
					success(function(data, status, headers, config){
					$scope.detalle_arr = data;

								window.dataLayer = window.dataLayer || [];
								window.dataLayer.push({
								'event': 'GenericGAEvent',
								'eventCategory': 'detalle-regarca',
								'eventAction': 'detalle-regarca-click',
								'tipo': 'admin',
								'colegio': 'cumbres',
			                	'pais': 'VE'
								});
					});
	
			}

		
			$scope.cambiarstatus = function(id) {

						$(".load").show();
						$(".bntcam").hide();

						var staa = $("#statuscam").val();

						if (staa == 1) {
							staasta = "APROBADO";
						}

						if (staa == 2) {
							staasta = "ANULADO";
						}
                    
	                    $scope.cambio_arr = [];
						$http.get('recargas/cambiarstatus', {params: { id: id, status: $("#statuscam").val() }}).
						success(function(data, status, headers, config){
						$scope.cambio_arr = data;

								window.dataLayer = window.dataLayer || [];
								window.dataLayer.push({
								'event': 'GenericGAEvent',
								'eventCategory': 'cambiar-status',
								'eventAction': staasta,
								'tipo': 'admin',
								'colegio': 'cumbres',
			                	'pais': 'VE'
								});

					    $scope.detalle_arr = [];
						$http.get('recargas/detalle', {params: { id:data }}).
						success(function(data, status, headers, config){
						$scope.detalle_arr = data;
						});

						$http.get('recargas/buscar', {params: { id:id }}).
						success(function(data, status, headers, config){
							$scope.recargas_arr = data;
						    $(".load").hide();
							$(".bntcam").show();
							$("#modalvalidar").modal("hide");
						});

					
					});
	
			}


			function addCommas(nStr){
				nStr += '';
				x = nStr.split(',');
				x1 = x[0];
				x2 = x.length > 1 ? ',' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}


	});