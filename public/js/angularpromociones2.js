		
		var AllPost_app = angular.module('CantinaPromosionesApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});
        
        AllPost_app.controller('CantinaPromosionesController', function($scope, $http) {
            
 
					$scope.promociones_arr = [];
					$http.get('promociones/buscar', {params: { nombre:1}}).
					success(function(data, status, headers, config){
					$scope.promociones_arr = data;                      
					});

                    $scope.nuevopromociones = function(){

                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                        'event': 'GenericGAEvent',
                        'eventCategory': 'nuevo-promociones',
                        'eventAction': 'nuevo-promociones-click',
                        'tipo': 'admin',
                        'colegio': 'cumbres',
                        'pais': 'VE'
                        });

                        $('#nuevopromociones').modal('toggle');

                    }

                    var count = 0;
                   
                    $scope.nuevoclonproducto = function(){
                        var source = $('.formclone:first'),
                        clone = source.clone();
                        clone.find(':input').attr('id', function(i, val) {
                            return val + count;
                        });
                        clone.insertBefore(this);
                        count++;
                    }

                    $scope.productospro = [];

                    $scope.editarpromociones = function(promo){


                        $('#nombre_edicion').val(promo.nombre);
                        $('#precio_edicion').val(promo.costo);
                        $('#descripcion_edicion').text(promo.descripcion);
                        $scope.productospro = promo.productos;

                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'editar-promocion',
                            'eventAction': 'editar-promocion-click',
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE',
                            'promocion': promo.nombre
                            });

                        $scope.promo_edicon = promo;

                        $('.productosselect:first').val('');
                        $('.cantidadinput:first').val('');
                        $('#editarpromociones').modal('toggle');

                    }
       

           
                    
                    $scope.nuevo = function() {
                          
                            var formData = new FormData(document.getElementById("nuevopromocionform"));
                            datos= [];

                            var nombrepro = $('#nombre').val();
                            var preciopro = $('#precio').val();
                            var category = $('select[name="categoria_id"] option:selected').text();
                            
                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'GenericGAEvent',
                            'eventCategory': 'crear-nueva-promocion',
                            'eventAction': 'crear-nueva-promocion-click',
                            'precio': preciopro,
                            'promocion': nombrepro,
                            'tipo': 'admin',
                            'colegio': 'cumbres',
                            'pais': 'VE',
                            'tipo': 'admin',
                            'categoria': category
                            });

                        $.ajax({
                            url: "promociones/nuevo",
                            type: "post",
                            dataType: "html",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false
                        }).done(function(res){

                            // $http.get('promociones/buscar', {params: { nombre:1}}).
                            // success(function(data, status, headers, config){
                            // $scope.promociones_arr = data;
                            // });
                            //$('#nuevopromociones').modal('toggle');
                            location.reload();
                        });
           
					}

                    $scope.editar = function() {
                        var formData = new FormData(document.getElementById("editarpromocionform"));
                        console.log(formData);
                        $.ajax({
                            url: "promociones/editar",
                            type: "post",
                            dataType: "html",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false
                        }).done(function(res){

                            $http.get('promociones/buscar', {params: { nombre:1}}).
                            success(function(data, status, headers, config){
                            $scope.promociones_arr = data;
                            });

                            $('#editarpromociones').modal('toggle');
                        });

					}

                    

	});