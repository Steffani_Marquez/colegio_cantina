        

        var vartipo = "";

        var tipodeusuario = "";

		var AllPost_app = angular.module('CantinaApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('CantinaController', function($scope, $http) {

        
		        window.dataLayer = window.dataLayer || [];
		        window.dataLayer.push({
		        'event': 'app.pageview',
		        'pageName' :'acceso-clientes',
		        'colegio': 'cumbres',
		        'pais' :'VE',
		        'tipo' : "",
		        'seccion' :'cliente'
		        });
      
            var inisioconsesion  = 0;

        	$scope.login_arr = []; 
			$http.get('accesoclientes/preguntarlogin', {params: {  clave1:0 }}).
			success(function(data, status, headers, config){
			$scope.login_arr = data;

				if (data != 0) {

					$("#email").val(data.correo_alumno);
					$("#password").val(data.passwordalumno);
					$scope.buscarcliente();
					inisioconsesion = 1;

				}else{
					inisioconsesion  = 0;
				}

			});

			$scope.cerrar = function() {

				window.dataLayer = window.dataLayer || [];
				window.dataLayer.push({
				'event': 'GenericGAEvent',
				'eventCategory': 'cerrar-sesion',
				'eventAction': 'cerrar-sesion-click',
				'colegio': 'cumbres',
		        'pais' :'VE',
		        'seccion' :'cliente',
				'tipo': tipodeusuario
				});

                   
                $scope.log_arr = []; 
				$http.get('accesoclientes/logout', {params: {  clave1:0 }}).
				success(function(data, status, headers, config){
				$scope.log_arr = data;
				window.location.href = '/accesoclientes';
				});

			}


        	$scope.nuevaclave = function() {

                    $("#error2").hide();
                    $(".load").show();
                    $(".bntingre2").hide();
                    
        		    if ($("#clave1").val() == $("#clave2").val()) {
	                   
	                    $scope.clientes_arr = []; 
						$http.get('accesoclientes/nuevaclave', {params: { clave1: $("#clave1").val(),email: $("#email").val(),password:$("#password").val()}}).
						success(function(data, status, headers, config){
						$scope.clientes_arr = data;

								$("#codigo_familiar").val(data[0].codigo_familiar);
		                      	$("#entradacliente").show();
		                      	$("#headercliente").hide();

		                      	$("body").removeClass("fondis1");

		                      		$scope.montos_arr = []; 
									$http.get('accesoclientes/buscarsaldoscliente', {params: { codigo_familiar:data[0].codigo_familiar }}).
									success(function(data, status, headers, config){
									$scope.montos_arr = data;
									  $(".load").hide();
                    					$(".bntingre2").show();
									});
						});
					}else{
						$("#error2").show();
						$(".load").hide();
                    	$(".bntingre2").show();
					}

            }
 
			$scope.buscarcliente = function() {
                 
	                $(".load").show();
					$(".bntingre").hide();
					$(".error").hide();
					$scope.clientes_arr = []; 
					$http.get('accesoclientes/buscarcliente', {params: { email: $("#email").val(),password:$("#password").val()}}).
					success(function(data, status, headers, config){
					$scope.clientes_arr = data;

					if (data.tipo_cliente_id == 2) {
						tipodeusuario = "Representante";
					}else{
						tipodeusuario = "Estudiante";
					}

					if (inisioconsesion == 1) {

						window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'inicio-de-sesion',
						'eventAction': 'inicio-de-sesion-con-sesion-activa',
						'colegio': 'cumbres',
			        	'pais' :'VE',
			        	'seccion' :'cliente',
						'tipo': tipodeusuario
						});

					}else{

						window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'inicio-de-sesion',
						'eventAction': 'inicio-de-sesion-click',
						'colegio': 'cumbres',
			        	'pais' :'VE',
			        	'seccion' :'cliente',
						'tipo': tipodeusuario
						});

					}
    
      

					if (data == 3) { //Si es primera vez que entra
                        $("#codigobar").hide();
                        $("#primeravez").show();
                        $(".load").hide();

					}else{
							
							$(".load").hide();
							$(".bntingre").show();
	                        if (data == 0) {
								$(".error").show();
							}else{
								$("#codigo_familiar").val(data[0].codigo_familiar);
		                      	$("#entradacliente").show();
		                      	$("#headercliente").hide();

		                      	$("body").removeClass("fondis1");

		                      		$scope.montos_arr = []; 
									$http.get('accesoclientes/buscarsaldoscliente', {params: { codigo_familiar:data[0].codigo_familiar }}).
									success(function(data, status, headers, config){
									$scope.montos_arr = data;
									});
							}
					}

					});

     		}

     		

     		$scope.abrirrecargas = function() {


     			    window.dataLayer = window.dataLayer || [];
			        window.dataLayer.push({
			        'event': 'app.pageview',
			        'pageName' :'recargar-saldo',
			        'colegio': 'cumbres',
			        'pais' :'VE',
			        'tipo' : tipodeusuario,
			        'seccion' :'cliente'
			        });

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'recargar-saldo',
					'eventAction': 'recargar-saldo-click',
					'colegio': 'cumbres',
		        	'pais' :'VE',
		        	'seccion' :'cliente',
					'tipo': tipodeusuario
					});


                $("#paneles1").hide();
                $("#recargarsaldo").show();
                $("#paneles1").hide();
                $("#limitediario").hide(); 
                $("#restriccionesmodal").hide();
                 $("#transferir").hide();
     		}

     		$scope.abriredit = function(id) {

     			window.dataLayer = window.dataLayer || [];
				window.dataLayer.push({
				'event': 'GenericGAEvent',
				'eventCategory': 'editar-datos-perfil',
				'colegio': 'cumbres',
		        'pais' :'VE',
		        'seccion' :'cliente',
				'eventAction': 'editar-datos-perfil-click',
				'tipo': tipodeusuario
				});


				window.dataLayer = window.dataLayer || [];
		        window.dataLayer.push({
		        'event': 'app.pageview',
		        'pageName' :'datos-personales',
		        'colegio': 'cumbres',
		        'pais' :'VE',
		        'tipo' : "",
		        'seccion' :'cliente'
		        });
                
                $("#detalles").hide();
                $("#edituser").show();
                
                $scope.detalleedit_arr = [];
                $http.get('accesoclientes/buscarperfil', {params: { id: id}}).
				success(function(data, status, headers, config){
				$scope.detalleedit_arr = data;
				   $("#nombreedit").val(data.nombre);
				   $("#apellidoedit").val(data.apellido_alumno);
				   $("#emailedit").val(data.correo_alumno);
				   $("#cedulaedit").val(data.cedula);
				   $("#telefonoedit").val(data.telefono);
				   $("#gradoh1").val(data.curso_actual);
				});

     		}



     		$scope.actualizardatos = function(id) {

                   
                    window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'actualizar-datos',
					'eventAction': 'actualizar-datos-perfil-click',
					'colegio': 'cumbres',
		        	'pais' :'VE',
		        	'seccion' :'cliente',
					'tipo': tipodeusuario
					});


			        $(".load").show();
			        $(".bntactual").hide();
	                $scope.detalleedit2_arr = [];
	                $http.get('accesoclientes/actualizardatos', {params: { gradoh1: $("#gradoh1").val(),telefonoedit: $("#telefonoedit").val(),cedulaedit: $("#cedulaedit").val(),emailedit: $("#emailedit").val(),apellidoedit: $("#apellidoedit").val(),id: id, nombreedit: $("#nombreedit").val()  }}).
					success(function(data, status, headers, config){
					$scope.detalleedit2_arr = data;
					$(".load").hide();
	                $(".bntactual").show();
	                $(".actualisi").show();
	                setTimeout(function(){ $(".actualisi").hide(); }, 2000);

	                window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'actualizar-datos-exito',
					'eventAction': 'actualizar-datos-perfil-exito',
					'colegio': 'cumbres',
		        	'pais' :'VE',
		        	'seccion' :'cliente',
					'tipo': tipodeusuario
					});

				});

     		}

     		


             $scope.mostrarqr = function(id) {  


             	    window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'mostrar-qr',
					'eventAction': 'mostrar-qr-click',
					'colegio': 'cumbres',
		        	'pais' :'VE',
		        	'seccion' :'cliente',
					'tipo': tipodeusuario
					});


	             	$("#modalqr").modal();
	    			$("#codigoqr").val(id);
	    			$("#qrcode").empty();

					var qrcode = new QRCode(document.getElementById("qrcode"), {
					width : 200,
					height : 200
					});

				
					function makeCode () {
						var elText = document.getElementById("codigoqr");
						if (!elText.value) {
							alert("Ingresa un texto");
							elText.focus();
							return;
						}
						qrcode.makeCode(elText.value);
					}

					
					makeCode();
					$("#text").
					on("blur", function () {
						makeCode();
					}).
					on("keydown", function (e) {
						if (e.keyCode == 13) {
							makeCode();
						}
					});
		         
		        }



     		$scope.actualizarlimite = function(id) {

	
     			    		var limite = $("#limite"+id).val();

		     			    $(".load").show();
		     			    $(".btnlimite").hide();

		     			    window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event': 'GenericGAEvent',
							'eventCategory': 'actualizar-datos',
							'eventAction': 'actualizar-limite-diario-click',
							'colegio': 'cumbres',
				        	'pais' :'VE',
				        	'seccion' :'cliente',
							'tipo': tipodeusuario,
							'nuevolimitegasto': limite
							});

							window.dataLayer.push(function() {
							  this.reset();
							})

	                
		                 	$scope.limite_arr = []; 
							$http.get('accesoclientes/actualizarlimite', {params: { limite:limite,id:id, codigo_familiar: $("#codigo_familiar").val()}}).
							success(function(data, status, headers, config){
							$scope.limite_arr = data;

						        $(".load").hide();
	     			    		$(".btnlimite").show();

								$http.get('accesoclientes/detalle', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
								success(function(data, status, headers, config){
								$scope.detalle_arr = data;
								});
							});

     		}

     		var ultimomontotransbs = 0;
     		var ultimomontotransdolar = 0;


     		$scope.transferirsaldo = function(id) {


     			var montodolar   =        $("#montodolartransferir"+id).val();
     			var montobs      =    	  $("#montobolostransferir"+id).val();
     			var persontransf =    	  $("#persontrans"+id).val();
     			var id_personaquitar =    $("#person_id"+id).val();

     			ultimomontotransbs = montobs;
     			ultimomontotransdolar = montodolar;

     			$(".load").show();
		     	$(".btnlimite").hide();
		
                $scope.transferencia_arr = []; 
				$http.get('accesoclientes/transferirsaldo', {params: { tasa: $("#tasadolartoday").val(), id_personaquitar: id_personaquitar, persontransf: persontransf,montobs: montobs,montodolar: montodolar, codigo_familiar:$("#codigo_familiar").val()}}).
				success(function(data, status, headers, config){
				$scope.transferencia_arr = data;

				      if (data != 1) {
                       alert(data);
                        $(".load").hide();
					    $(".btnlimite").show();


	     				window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'error-al-transferir-saldo',
						'eventAction': data,
						'colegio': 'cumbres',
			        	'pais' :'VE',
			        	'seccion' :'cliente',
						'tipo': tipodeusuario
						});

				      }else{

					      	window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event': 'GenericGAEvent',
							'eventCategory': 'transferencia-saldo',
							'eventAction': 'transferencia-saldo-exitosa',
							'colegio': 'cumbres',
				        	'pais' :'VE',
				        	'seccion' :'cliente',
							'tipo': tipodeusuario,
							'transferenciabs': ultimomontotransbs,
							'transferenciadolar': ultimomontotransdolar
							});
						   	
						   	$http.get('accesoclientes/detalle', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
							success(function(data, status, headers, config){
							$scope.detalle_arr = data;
									$(".load").hide();
					     			$(".btnlimite").show();
							});

						}

				});

     		}



     		$scope.transferir = function(id) {

	     			window.dataLayer = window.dataLayer || [];
			        window.dataLayer.push({
			        'event': 'app.pageview',
			        'pageName' :'transferir-saldo',
			        'colegio': 'cumbres',
			        'pais' :'VE',
			        'tipo' : "",
			        'seccion' :'cliente'
			        });

     			    window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'transferir-saldo',
					'eventAction': 'transferir-saldo-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     				if (id == 1) {
     					$("#paneles1").hide();
	                	$("#limitediario").hide();
	                	$("#transferir").show();
	                	$("#restriccionesmodal").hide();
     				}

     				if (id == 2) {
     				   $("#recargarsaldo").hide();
                   	   $("#limitediario").hide();
                   	   $("#transferir").show();
                   	    $("#restriccionesmodal").hide();
     				}
     				
                	$scope.detalle_arr = []; 
					$http.get('accesoclientes/detalle', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle_arr = data;
					});

     		}


     		$scope.reservag = function(id) {

     						window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event': 'GenericGAEvent',
							'eventCategory': 'ir-realizar-reservar',
							'eventAction': 'ir-realizar-reservar-click',
							'colegio': 'cumbres',
				       		'pais' :'VE',
				       		'seccion' :'cliente',
							'tipo': tipodeusuario
							});

     		}

     		$scope.vermenug = function(id) {

     						window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event': 'GenericGAEvent',
							'eventCategory': 'ir-ver-menu',
							'eventAction': 'ir-ver-menu-click',
							'colegio': 'cumbres',
				       		'pais' :'VE',
				       		'seccion' :'cliente',
							'tipo': tipodeusuario
							});

     		}



     		$scope.storerestriccion = function(id) {

                    		var restriccion = $("#resticinput"+id).val();

                    		window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event': 'GenericGAEvent',
							'eventCategory': 'actualizar-datos-restriccion-alimenticia',
							'eventAction': restriccion,
							'colegio': 'cumbres',
				       		'pais' :'VE',
				       		'seccion' :'cliente',
							'tipo': tipodeusuario
							});


		     			    $(".load").show();
		     			    $(".btnlimite").hide();
	                
		                 	$scope.limite_arr = []; 
							$http.get('accesoclientes/actualizarrestriccion', {params: { restriccion:restriccion,id:id, codigo_familiar: $("#codigo_familiar").val()}}).
							success(function(data, status, headers, config){
							$scope.limite_arr = data;

						        $(".load").hide();
	     			    		$(".btnlimite").show();

								$http.get('accesoclientes/detalle', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
								success(function(data, status, headers, config){
								$scope.detalle_arr = data;
								});
							});

     		}





     		$scope.restricciones = function(id) {


     			        window.dataLayer = window.dataLayer || [];
				        window.dataLayer.push({
				        'event': 'app.pageview',
				        'pageName' :'restricciones-alimenticias',
				        'colegio': 'cumbres',
				        'pais' :'VE',
				        'tipo' : "",
				        'seccion' :'cliente'
				        });


     			        window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'restricciones',
						'eventAction': 'restricciones-click',
						'colegio': 'cumbres',
			       		'pais' :'VE',
			       		'seccion' :'cliente',
						'tipo': tipodeusuario
						});

                       $("#paneles1").hide();
     				   $("#recargarsaldo").hide();
                   	   $("#limitediario").hide();
                   	   $("#transferir").hide();
                   	   $("#restriccionesmodal").show();
     				

                	$scope.detalle_arr = []; 
					$http.get('accesoclientes/detalle', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle_arr = data;
					});

     		}


     		


     		$scope.limitediario = function(id) {


     			    window.dataLayer = window.dataLayer || [];
			        window.dataLayer.push({
			        'event': 'app.pageview',
			        'pageName' :'limite-diario',
			        'colegio': 'cumbres',
			        'pais' :'VE',
			        'tipo' : "",
			        'seccion' :'cliente'
			        });


     			    window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'limite-diario',
					'eventAction': 'limite-diario-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     				if (id == 1) {
     					$("#paneles1").hide();
	                	$("#limitediario").show();
	                	$("#transferir").hide();
	                	$("#restriccionesmodal").hide(); 
     				}

     				if (id == 2) {
     				   $("#recargarsaldo").hide();
                   	   $("#limitediario").show();
                   	   $("#transferir").hide();
                   	   $("#restriccionesmodal").hide();
     				}

                	$scope.detalle_arr = []; 
					$http.get('accesoclientes/detalle', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle_arr = data;
					});

     		}




     		$scope.volver1 = function() {
               
                $("#paneles1").show();
                $("#recargarsaldo").hide();
                $("#limitediario").hide();
                $("#restriccionesmodal").hide();

                window.dataLayer = window.dataLayer || [];
		        window.dataLayer.push({
		        'event': 'app.pageview',
		        'pageName' :'acceso-clientes',
		        'colegio': 'cumbres',
		        'pais' :'VE',
		        'tipo' : "",
		        'seccion' :'cliente'
		        });

     		}


     		$scope.volver2 = function(id) {

     			if (id == "transferencia") {

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'volver-transferencia',
					'eventAction': 'volver-transferencia-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     			}

     			if (id == "limitediario") {

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'volver-limite-diario',
					'eventAction': 'volver-limite-diario-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     			}

     			if (id == "restriccion") {

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'volver-restriccion',
					'eventAction': 'volver-restriccion-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     			}

     			if (id == "4") {

                    window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'volver',
					'eventAction': 'volver-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});
     			}

     			if (id == "5") {

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'volver',
					'eventAction': 'volver-click-compras',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     			}

                $("#paneles1").show();
                $("#detalles").hide();
                $("#detalles2").hide();
                $("#detalles3").hide();
                $("#detalles4").hide();
                $("#limitediario").hide();
                $("#restriccionesmodal").hide();
                $("#recargarsaldo").hide(); 
                $("#transferir").hide(); 


                window.dataLayer = window.dataLayer || [];
		        window.dataLayer.push({
		        'event': 'app.pageview',
		        'pageName' :'acceso-clientes',
		        'colegio': 'cumbres',
		        'pais' :'VE',
		        'tipo' : "",
		        'seccion' :'cliente'
		        });


     		}

     		$scope.volver3 = function() {
              
                $("#paneles1").hide();
                $("#detalles").hide();
                $("#detalles2").hide();
                $("#detalles3").hide();
                $("#detalles4").hide();
                $("#edituser").hide();
                $("#detalles").show();
                $("#limitediario").hide();
                $("#restriccionesmodal").hide();
                $("#recargarsaldo").hide();

                window.dataLayer = window.dataLayer || [];
			    window.dataLayer.push({
			    'event': 'app.pageview',
			    'pageName' :'detalle-de-saldo',
			    'colegio': 'cumbres',
			    'pais' :'VE',
			    'tipo' : "",
			    'seccion' :'cliente'
			    });

     		}


     		$scope.cambio = function(id) {
                  
                    $("#recarga1").show();

			        if (id  == "dolares") {

				          	window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event': 'GenericGAEvent',
							'eventCategory': 'selecciono-dolares-recarga',
							'eventAction': 'selecciono-dolares-recarga-click',
							'colegio': 'cumbres',
				       		'pais' :'VE',
				       		'seccion' :'cliente',
							'tipo': tipodeusuario
							});


				            $("#pagodolares").show();
				            $("#pagobolivares").hide();
				            $(".bntcer").removeClass("marcado");
				            $(".doo1").addClass("marcado"); 

				            vartipo = "Dolares";

				            $(".totalbs").hide();
				            $(".totaldol").show();

				            $(".palbs").text("DOLARES A RECARGAR"); 

			        }else{

			          	window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'selecciono-bolivares-recarga',
						'eventAction': 'selecciono-bolivares-recarga-click',
						'colegio': 'cumbres',
			       		'pais' :'VE',
			       		'seccion' :'cliente',
						'tipo': tipodeusuario
						});


			            $("#pagobolivares").show();
			            $("#pagodolares").hide();
			            $(".bntcer").removeClass("marcado");
			            $(".boo1").addClass("marcado");

			            vartipo = "Bolivares";

			            $(".totalbs").show();
			            $(".totaldol").hide();

			            $(".palbs").text("BOLIVARES A RECARGAR"); 
			          }

                    
                  $scope.inser();
     		}



          

	        $scope.unmtele = function(id) {
                $("#armasonpagos").show();
	        }


     		$scope.inser = function(id) {

     			    if (id == undefined) {return false;}
     		 	
		     		var valor = $("#input"+id).val();
		     		var tasa =  $("#tasadolartoday").val();

	     		 	if (vartipo == "Dolares") {

			     		 	if (parseFloat(valor) > 999 ) {
			     		 		alert("Recuerda que el monto que debe colocar es el equivalente en dolares. por lo tanto no se permiten mas de 3 cifras por seguridad");
			     		 	}

			     	}
				  
			     	if (valor == "") {
		                    valor = 0;
			     	}

		     		 	$scope.inser_arr = []; 
						$http.get('accesoclientes/insertarcarrito', {params: { tasa: $("#tasadolartoday").val(),vartipo: vartipo,codigo_familiar: $("#codigo_familiar").val(),id_cliente: id,valor:valor}}).
						success(function(data, status, headers, config){
						$scope.inser_arr = data;
							if (vartipo == "Dolares") {

								$(".totalbs").hide();
                                $(".totaldol").show();
    							$(".totalmon").text(data);
								$("#totalpaypal").val(data);
								var valortruncado = parseFloat(data); //* parseFloat(data);
								valortruncado = valortruncado.toFixed(2);
								$(".totalmonbs").text(addCommas(valortruncado));
								
							}else{

								
                                $(".totalbs").show();
                                $(".totaldol").hide();

								
								$(".totalmon").text(data);
								$("#totalpaypal").val(data);
								var valortruncado =  parseFloat(data);
								valortruncado = valortruncado.toFixed(2);
								$(".totalmonbs").text(addCommas(valortruncado));
							}
						});
     		
     		}



     		$scope.volver1 = function(id) {

     			if (id == "recarga") {

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'volver-recarga-saldo',
					'eventAction': 'volver-recarga-saldo-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     			}
                   
                $("#modallisto").modal("hide");
                $("#paneles1").show();
                $("#recargarsaldo").hide();
                $("#limitediario").hide();

     		}


     		$scope.recargar = function(id) {

     			var error = 0;

     			$("#movilerror").hide();
     			$("#zelleerror").hide();

     			 if (id == "pago movil"){
                    if ($("#refepagomovil").val() == "") {
                        $("#movilerror").show();
                        error = 1;
                   }else{
                   		$("#movilerror").hide();
                   }
     			}

     			if (id == "zelle") {
                    if ($("#refezelle").val() == "") {
                        $("#zelleerror").show();
                        error = 1;
                   }else{
                   		$("#zelleerror").hide();
                   }
     			}

   

     			if (error == 0) {

     				    if ((id == "pago movil") || (id == "punto de venta")){
                          var montor = $("#totalmontobolivares").text()+"Bs";
     				    }else{
     				   	  var montor = $("#totalmontodolares").text()+"$";
     				    }


     				    window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'recargar',
						'eventAction': id,
						'recarga': montor,
						'colegio': 'cumbres',
			       		'pais' :'VE',
			       		'seccion' :'cliente',
						'tipo': tipodeusuario
						});

                       
                       $(".load").show();
                       $(".btnfinalpago").hide();  
     			
			     		$scope.inser_arr = []; 
						$http.get('accesoclientes/recargar', {params: { teleinfo: $("#teleinfo").val(),refeefectivo: $("#refeefect").val(),tasa: $("#tasadolartoday").val(),refezelle: $("#refezelle").val(), refepagomovil: $("#refepagomovil").val(), refepaypal: $("#refepaypal").val() ,tipo_pago:id, codigo_familiar: $("#codigo_familiar").val()}}).
						success(function(data, status, headers, config){
						$scope.inser_arr = data;

						       if (data == 0) {
                                  
                                   $("#montocero").modal();
                                   $(".load").hide();
						           $(".btnfinalpago").show();
						        
						        }else{

									$(".load").hide();
						            $(".btnfinalpago").show();

			                    	$scope.montos_arr = []; 
									$http.get('accesoclientes/buscarsaldoscliente', {params: { codigo_familiar:$("#codigo_familiar").val()}}).
									success(function(data, status, headers, config){
									$scope.montos_arr = data;
	                                     $("#modallisto").modal({backdrop: 'static', keyboard: false});
									});

								}

								window.dataLayer.push(function() {
								  this.reset();
								})
						});

				}
     		}


     		$scope.detalle = function(id) {


     			    window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'ver-detalle-saldo',
					'eventAction': 'ver-detalle-saldo-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});


					window.dataLayer = window.dataLayer || [];
			        window.dataLayer.push({
			        'event': 'app.pageview',
			        'pageName' :'detalle-de-saldo',
			        'colegio': 'cumbres',
			        'pais' :'VE',
			        'tipo' : "",
			        'seccion' :'cliente'
			        });

     		 	
         		 	$scope.detalle_arr = []; 
					$http.get('accesoclientes/detalle', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle_arr = data;
					    $("#paneles1").hide();
                		$("#detalles").show();

                		if (data.length == 0) {
							$(".nohayregistros").show();
						}else{
							$(".nohayregistros").hide();
						}


					});
				

     		}


     		$scope.detalle2 = function(id) {


     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'por-validar',
					'eventAction': 'por-validar-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});



					window.dataLayer = window.dataLayer || [];
			        window.dataLayer.push({
			        'event': 'app.pageview',
			        'pageName' :'recargas-por-validar',
			        'colegio': 'cumbres',
			        'pais' :'VE',
			        'tipo' : "",
			        'seccion' :'cliente'
			        });


     		 	
         		 	$scope.detalle2_arr = []; 
					$http.get('accesoclientes/detalle2', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle2_arr = data;
					    $("#paneles1").hide();
                		$("#detalles2").show();

                		if (data.length == 0) {
							$(".nohayregistros").show();
						}else{
							$(".nohayregistros").hide();
						}

					});
				
     		}


     		$scope.detalle3 = function(id) {

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'rechazado',
					'eventAction': 'rechazado-click',
					'colegio': 'cumbres',
		       		'pais' :'VE',
		       		'seccion' :'cliente',
					'tipo': tipodeusuario
					});

					window.dataLayer = window.dataLayer || [];
			        window.dataLayer.push({
			        'event': 'app.pageview',
			        'pageName' :'recargas-rechazadas',
			        'colegio': 'cumbres',
			        'pais' :'VE',
			        'tipo' : "",
			        'seccion' :'cliente'
			        });
     		 	
         		 	$scope.detalle2_arr = []; 
					$http.get('accesoclientes/detalle3', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle2_arr = data;
					 
					    $("#paneles1").hide();
                		$("#detalles3").show();

                		if (data.length == 0) {
							$(".nohayregistros").show();
						}else{
							$(".nohayregistros").hide();
						}

					});
				
     		}


     		$scope.detalle4 = function(id) {
                    

                    if (id == "hoy") {

		     			window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'compras-de-hoy',
						'eventAction': 'compras-de-hoy-click',
						'colegio': 'cumbres',
			       		'pais' :'VE',
			       		'seccion' :'cliente',
						'tipo': tipodeusuario
						});

						window.dataLayer = window.dataLayer || [];
				        window.dataLayer.push({
				        'event': 'app.pageview',
				        'pageName' :'compras-de-hoy',
				        'colegio': 'cumbres',
				        'pais' :'VE',
				        'tipo' : "",
				        'seccion' :'cliente'
				        });


					}

					if (id == "mes") {

		     			window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'compras-del-mes',
						'eventAction': 'compras-del-mes-click',
						'colegio': 'cumbres',
			       		'pais' :'VE',
			       		'seccion' :'cliente',
						'tipo': tipodeusuario
						});

						window.dataLayer = window.dataLayer || [];
				        window.dataLayer.push({
				        'event': 'app.pageview',
				        'pageName' :'compras-del-mes',
				        'colegio': 'cumbres',
				        'pais' :'VE',
				        'tipo' : "",
				        'seccion' :'cliente'
				        });

					}

					if (id == "anual") {

		     			window.dataLayer = window.dataLayer || [];
						window.dataLayer.push({
						'event': 'GenericGAEvent',
						'eventCategory': 'compras-del-ano',
						'eventAction': 'compras-del-ano-click',
						'colegio': 'cumbres',
			       		'pais' :'VE',
			       		'seccion' :'cliente',
						'tipo': tipodeusuario
						});

						window.dataLayer = window.dataLayer || [];
				        window.dataLayer.push({
				        'event': 'app.pageview',
				        'pageName' :'compras-anuales',
				        'colegio': 'cumbres',
				        'pais' :'VE',
				        'tipo' : "",
				        'seccion' :'cliente'
				        });


					}
     		 	
         		 	$scope.detalle4_arr = []; 
					$http.get('accesoclientes/detalle4', {params: { codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle4_arr = data;
					    $("#paneles1").hide();
                		$("#detalles4").show();

                		if (data.length == 0) {
							$(".nohayregistros").show();
						}else{
							$(".nohayregistros").hide();
						}

					});
				
     		}



     		$scope.detis = function(id) {

     				window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'detalle',
					'eventAction': 'detalle-click',
					'colegio': 'cumbres',
			       	'pais' :'VE',
			       	'seccion' :'cliente',
					'tipo': tipodeusuario
					});

     			    $("#modaldetalle").modal();
     		 	
         		 	$scope.detallepedido_arr = []; 
					$http.get('accesoclientes/detallepedido', {params: { id: id}}).
					success(function(data, status, headers, config){
					$scope.detallepedido = data;
					});
				
     		}


     		$scope.buscarpedidos = function(id) {


     			    window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
					'event': 'GenericGAEvent',
					'eventCategory': 'compras',
					'eventAction': 'buscar-click',
					'colegio': 'cumbres',
			       	'pais' :'VE',
			       	'seccion' :'cliente',
					'tipo': tipodeusuario,
					'fechaDesde': $("#desde").val(),
					'fechaHasta': $("#hasta").val()
					});
     		 	
         		 	$scope.detalle4_arr = []; 
					$http.get('accesoclientes/buscarpedidos', {params: { desde: $("#desde").val(), hasta: $("#hasta").val(),codigo_familiar: $("#codigo_familiar").val()}}).
					success(function(data, status, headers, config){
					$scope.detalle4_arr = data;

						if (data.length == 0) {
							$(".nohayregistros").show();
						}else{
							$(".nohayregistros").hide();
						}


					});
				
     		}




     		function addCommas(nStr){
				nStr += '';
				x = nStr.split(',');
				x1 = x[0];
				x2 = x.length > 1 ? ',' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}



	
	});