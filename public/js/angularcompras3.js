		
		var AllPost_app = angular.module('CantinaApp', [], function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		});

        AllPost_app.controller('CantinaController', function($scope, $http) {


		        	$scope.login_arr = []; 
					$http.get('compras/preguntarlogin', {params: {  clave1:0 }}).
					success(function(data, status, headers, config){
					$scope.login_arr = data;

						if (data != 0) {

							$("#email").val(data.correo_alumno);
							$("#password").val(data.passwordalumno);
							$scope.buscarcliente();

						}

					});

 
					$scope.productos_arr = [];
					$http.get('ventas/buscar', {params: { nombre:1}}).
					success(function(data, status, headers, config){
					$scope.productos_arr = data;
					});


					$scope.promos_arr = [];
					$http.get('ventas/buscarpromos', {params: { nombre:1}}).
					success(function(data, status, headers, config){
					$scope.promos_arr = data;
					});



					$scope.back = function() {
						$("#cliente_vista").hide();
						$("#codigobar").show();
						var  codigocliente_antiguo =  $("#codigo_personal_antiguo").val();
					    $scope.carrito_arr = [];
					    $scope.cliente_arr = [];
					    $("#codigo_personal").val(codigocliente_antiguo);
					    $("#clientecodigo").val("");
						$("#topcliente").hide();
						$("#emtycarr").show();
						$("#cerosub").show();
					    $("#cerototal").show();
					}



					$scope.buscarcliente = function() {

								$("#error").hide();
                                $(".load").show(); 
								$(".bntingre").hide(); 

								$(".codigin").removeClass("animate__pulse");
								$(".codigin").removeClass("animate__animated");

								$(".codigin").addClass("animate__animated");
								$(".codigin").addClass("animate__headShake");

								var  codigocliente =  $("#clientecodigo").val();
			                    $scope.cliente_arr = [];
								$http.get('compras/buscarcliente',{params: { email: $("#email").val(), password: $("#password").val()}}).
								success(function(data, status, headers, config){
								$scope.cliente_arr = data;

								$(".load").hide(); 
								$(".bntingre").show(); 

								$(".codigin").addClass("animate__pulse");
								$(".codigin").removeClass("animate__headShake");



								if (data != 0) {

									      $("#codigoqr").val(data.codigo);

									      var qrcode = new QRCode(document.getElementById("qrcode"), {
										   width : 200,
										   height : 200
										  });
										  function makeCode () {
										   var elText = document.getElementById("codigoqr");
										   if (!elText.value) {
										    alert("Ingresa un texto");
										    elText.focus();
										    return;
										   }
										   qrcode.makeCode(elText.value);
										  }
										  makeCode();
										  $("#text").
										   on("blur", function () {
										    makeCode();
										   }).
										   on("keydown", function (e) {
										    if (e.keyCode == 13) {
										     makeCode();
										    }
										   });


									$("#error").hide();

									if (data.numpedidos > 0) {
										 $("#ultimacompra").show();
									}else{
										$("#ultimacompra").hide();
									}

   								    $("#topcliente").show();
									$("#cliente_vista").show();
									$("#codigobar").hide();
                                    $("#productoscarr").show(); 
                                    $("#headeruser").show();  
                                    $(".botonerabajo").show();


                                    $("body").removeClass("colorbody");
									
									var codigo_personal = data.codigo;
									$scope.carrito_arr = [];
									$http.get('compras/buscarclientecarrito', {params: { codigo_actual: codigo_personal,codigo_personal: data.codigo}}).
									success(function(data, status, headers, config){
									if (data.tienecar == 1) {
										$("#emtycarr").hide();
										$("#cerosub").hide();
										$("#cerototal").hide();
										$("#cerototal2").hide();
										$("#codigo_personal").val(data.codigo);
									}else{
										$("#emtycarr").show();
										$("#cerosub").show();
										$("#cerototal").show();
										$("#cerototal2").show();
										$("#codigo_personal").val(data.codigo);
									}

									$scope.carrito_arr = data.carrito;
									
									});
								}else{
									$("#cliente_vista").hide();
									$("#codigobar").show();
									$("#error").show();
								}


						});


					}

					$scope.vaciar = function() {

						var codigo_personal = $("#codigo_personal").val();
       
						$http.get('ventas/vaciarcarrito', {params: { codigo_personal: codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = [];
							$("#cerosub").show();
							$("#cerototal").show();
						});

					}


					



					$scope.reservarcompramodal = function() {
                        $("#modalreservar").modal({backdrop: 'static', keyboard: false});
                        $("#opcionesreservas").show();
                       	$("#reservadonoexito").hide();
						$("#reservadoexito").hide();
						$("#mensajereserva").hide();
    				}

					$scope.volver = function() {
                        $("#modalreservar").modal("hide");
					}

					

					$scope.listo = function() {

						
                  	    $("#btnlis").hide();
                  	    $(".load").show();
                         
                        var codigo_personal = $("#codigo_personal").val();
       
						$http.get('compras/reservarcompra', {params: { reservapara: $("#reservapara").val(),dia: $("#dia").val(),horario: $("#horario").val(), codigo_personal: codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.reservar_arr = [];

						    $("#btnlis").show();
                  	    	$(".load").hide();

                  	    	if (data == 2) {
                                $("#opcionesreservas").hide();
						   		$("#reservadonoexito").show();
						   		$("#resersinstock").text("Tienes productos agregados al carrito que exceden la cantidad de stock disponible");
						   		$("#resersinstock").show();
						   		$("#reservadonoexito").hide();
						   		$("#reservadoexito").hide();
						   		$("#mensajereserva").show();
  
                  	    	}

                  	    	if (data == 3) {
                                
                                $("#opcionesreservas").hide();
						   		$("#reservadonoexito").show();
						   		$("#resersinstock").text("La fecha de reserva no puede ser menor a la de hoy");
						   		$("#resersinstock").show();
						   		$("#reservadonoexito").hide();
						   		$("#reservadoexito").hide();
						   		$("#mensajereserva").show();
                                    
                  	    	}

						    if (data == 1) {
						    	
						    	$("#opcionesreservas").hide();
						   		$("#reservadoexito").show();
						   		$("#reservadoexito").text("El pedido ha sido reservado exitosamente");
						   		$("#reservadoexito").show();
						   		$("#reservadonoexito").hide();
						   		$("#resersinstock").hide();
						   		$("#mensajereserva").show();


									$scope.carrito_arr = [];
									$http.get('compras/buscarclientecarrito', {params: { codigo_actual: $("#codigo_personal").val()}}).
									success(function(data, status, headers, config){
									$scope.carrito_arr = data.carrito;
									});

                                    $scope.cliente_arr =  [];
									$http.get('compras/buscarcliente',{params: { email: $("#email").val(), password: $("#password").val()}}).
									success(function(data, status, headers, config){
									$scope.cliente_arr = data;
									});

									$("#emtycarr").show();
									$("#cerosub").show();
									$("#cerototal").show();
									$("#cerototal2").show();
									$("#cerototal2").text("0,00");

									$(".seccionh").hide();
                       				$("#reservascar").show();	
									
						   		
						    }
						    if (data == 0) {
						    	$("#opcionesreservas").hide();
						   		$("#reservadonoexito").show();
						   		$("#reservadonoexito").text("El saldo no es suficiente para reservar este pedido, puedes ir al enlace de recarga de saldo");
						   		$("#reservadonoexito").show();
						   		$("#reservadoexito").hide();
						   		$("#resersinstock").hide();
						   		$("#mensajereserva").show();
						    }
						});
						
					}


					$scope.buscarproducto = function(id) {

						if (id == "Todos") {
							$(".btnbotn").removeClass("activecar2");
	    					$(".btnt"+"Todos").addClass("activecar2");
						}
	                     
						$http.get('ventas/buscarcategorias', {params: { categoria: id}}).
						success(function(data, status, headers, config){
						$scope.productos_arr = data;
						});

						$http.get('ventas/buscarcategoriaspromos', {params: { categoria: id}}).
						success(function(data, status, headers, config){
						$scope.promos_arr = data;
						});

					}


					$scope.elegir = function(id) { 
						
						$(".seccionh").hide();
                        $("#"+id).show();

                        if (id == "codigobar") {
                        	$("#headeruser").hide();
                        }else{
                        	$("#headeruser").show();
                        }



					}


					$scope.restarppv = function(id,tipo) { 


					   	valor = $("#inputvalor"+tipo+id).text();

						nuevovalor = parseFloat(valor) - parseFloat(1);
						$("#inputvalor"+tipo+id).text(nuevovalor);
						codigo_personal = $("#codigo_personal").val();

						$http.get('compras/actualizarcarrito', {params: { tipo:tipo,inicial:0,id: id,cantidad:nuevovalor,codigo_personal:codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;
							if (data == "") {
								$("#emtycarr").show();
								$("#cerosub").show();
								$("#cerototal").show();
							}
						
						});
	                     
					}

					$scope.sumarppv = function(id,tipo) { 
	                  
	                    valor = $("#inputvalor"+tipo+id).text();
						nuevovalor = parseFloat(valor) + parseFloat(1);
						$("#inputvalor"+tipo+id).text(nuevovalor);
						codigo_personal = $("#codigo_personal").val();

						$http.get('compras/actualizarcarrito', {params: { tipo:tipo,inicial:0,id: id,cantidad:nuevovalor,codigo_personal:codigo_personal }}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;
								$("#cerosub").hide();
								$("#cerototal").hide();
						});

					}


					$scope.agregaralcarr_carpromo = function(id) { 

						valor = $("#inputinipromo"+id).text();
					   	codigo_personal = $("#codigo_personal").val();
                  
					    $(".carpro").show();
		                setTimeout(function() {
		                  $(".carpro").fadeOut(2000);
		                },500);

						$http.get('compras/actualizarcarritoinicio', {params: { tipo:'Promo',inicial:valor,id:id,cantidad:valor,codigo_personal:codigo_personal}}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;

						    if (data !=0) {
							   	$("#emtycarr").hide();
							   	$("#cerosub").hide();
								$("#cerototal").hide();
								$("#cerototal2").hide();
								valor = $("#inputinipromo"+id).text("1");

								window.dataLayer = window.dataLayer || [];
								window.dataLayer.push({
								'event': 'GenericGAEvent',
								'eventCategory': 'navegacioncompra',
								'eventAction': 'agregar-carrito',
								'eventLabel':data[0].productosbusq.nombre,
								'categoria': data[0].productosbusq.categoria.nombre,
								'precio': data[0].productosbusq.costo,
								'tipo': 'admin',
								'colegio': 'cumbres',
								'pais': 'VE'
								});


							}else{
								alert("no tienes");
							}
						});

					}


					$scope.agregaralcarr_carproducto = function(id) { 

						valor = $("#inputiniproducto"+id).text();
					   	codigo_personal = $("#codigo_personal").val();
                  
					    $(".carpro").show();
		                setTimeout(function() {
		                  $(".carpro").fadeOut(2000);
		                },500);

						$http.get('compras/actualizarcarritoinicio', {params: { tipo:'Producto',inicial:valor,id:id,cantidad:valor,codigo_personal:codigo_personal}}).
						success(function(data, status, headers, config){
						$scope.carrito_arr = data;

							if ((data[0].sinstock != 1) && (data !=0)){
							   	$("#emtycarr").hide();
							   	$("#cerosub").hide();
								$("#cerototal").hide(); 
								$("#cerototal2").hide();
								valor = $("#inputiniproducto"+id).text("1");

								window.dataLayer = window.dataLayer || [];
								window.dataLayer.push({
								'event': 'GenericGAEvent',
								'eventCategory': 'navegacioncompra',
								'eventAction': 'agregar-carrito',
								'eventLabel':data[0].productosbusq.nombre,
								'categoria': data[0].productosbusq.categoria.nombre,
								'precio': data[0].productosbusq.costo,
								'tipo': 'admin',
								'colegio': 'cumbres',
								'pais': 'VE'
								});

							}else{
								$("#modaldispon").modal();
							
							}

						});

					}


					$scope.finalizarpago = function(id) { 
                        $(".load").show();
                        $(".paguis").hide();
					   	codigo_personal = $("#codigo_personal").val();
						$http.get('ventas/finalizarpago', {params: {tipo:id,codigo_personal:codigo_personal,vuelto_cliente:$("#vuelto_cliente").val() ,cash_cliente:$("#cash_cliente").val() }}).
						success(function(data, status, headers, config){
							 $(".load").hide();
	                         $(".paguis").show();
	                         if (data != 0) {
		                         $("#modal1").modal();
		                         $("#textomodal").text(data);
	                     	 }else{
                                 $("#modal2").modal({backdrop: 'static', keyboard: false});
	                     	 }
						});

					}


					$scope.restariniproducto = function(id) { 
	                    valor = $("#inputiniproducto"+id).text();
                        if (valor == 0) { return false; }
						nuevovalor = parseFloat(valor) - parseFloat(1);
						$("#inputiniproducto"+id).text(nuevovalor);
					}

					$scope.sumariniproducto = function(id) { 
                        valor = $("#inputiniproducto"+id).text();
						nuevovalor = parseFloat(valor) + parseFloat(1);
						$("#inputiniproducto"+id).text(nuevovalor);
					}

					$scope.restarinipromo = function(id) { 
	                    valor = $("#inputinipromo"+id).text();
                        if (valor == 0) { return false; }
						nuevovalor = parseFloat(valor) - parseFloat(1);
						$("#inputinipromo"+id).text(nuevovalor);
					}

					$scope.sumarinipromo = function(id) { 
                        valor = $("#inputinipromo"+id).text();
						nuevovalor = parseFloat(valor) + parseFloat(1);
						$("#inputinipromo"+id).text(nuevovalor);
					}

	
	});